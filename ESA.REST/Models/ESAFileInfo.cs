﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESA.REST.Models
{
    public class ESAFileInfo
    {
        public string FileName = "File not found";
        public string FilePath = "File not found";
        public string FileNameWithPath = "File not found";
        public string FileDate;
        public double FileSize = 0;
        public string errorMessage = "";
    }
}
