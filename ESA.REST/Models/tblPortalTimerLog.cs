//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ESA.REST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPortalTimerLog
    {
        public int logID { get; set; }
        public Nullable<System.DateTime> logDate { get; set; }
        public Nullable<int> timerID { get; set; }
        public Nullable<System.DateTime> startTime { get; set; }
        public Nullable<System.DateTime> originalStopTime { get; set; }
        public string logText { get; set; }
    }
}
