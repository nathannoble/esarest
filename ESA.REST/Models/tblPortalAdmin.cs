//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ESA.REST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPortalAdmin
    {
        public int ID { get; set; }
        public string companyName { get; set; }
        public string tollFree { get; set; }
        public string fax { get; set; }
        public string webAddress { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipCode { get; set; }
        public string color1 { get; set; }
        public string color2 { get; set; }
        public string color3 { get; set; }
        public Nullable<System.DateTime> payrollLockDate { get; set; }
        public string payrollFileDelim { get; set; }
        public string payrollProduct { get; set; }
        public string payrollVersion { get; set; }
        public string payrollRelease { get; set; }
        public string payrollFileExt { get; set; }
    }
}
