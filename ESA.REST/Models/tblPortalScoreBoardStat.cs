//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ESA.REST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPortalScoreBoardStat
    {
        public int year { get; set; }
        public Nullable<int> clients { get; set; }
        public Nullable<decimal> target { get; set; }
        public Nullable<decimal> ytd { get; set; }
        public Nullable<decimal> remainGoal { get; set; }
    }
}
