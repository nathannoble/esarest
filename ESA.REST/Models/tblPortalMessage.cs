//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ESA.REST.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPortalMessage
    {
        public int messageID { get; set; }
        public string messageDate { get; set; }
        public string messageText { get; set; }
    }
}
