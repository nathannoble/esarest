﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.Odbc;
using System.Linq;
using System.Text;
using ThriveDAL.ThriveDataService;
using static ThriveDAL.OpenDentalEntities;

namespace ThriveDAL
{
    public class OpenDental
    {
        string connectionString;
        string dexisConnectionString = @"metadata=res://*/DexisDataModel.csdl|res://*/DexisDataModel.ssdl|res://*/DexisDataModel.msl;provider=System.Data.SqlClient;provider connection string='data source=THRIVELAPTOP\DEXIS_DATA;initial catalog=DEXIS;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework'";

        AppData _appData;
        LogErrors errorLog = new LogErrors();
        AnalyzeData analyzeData;
        string _completedApptStatus;
        List<long> _procsToIgnoreList;

        DateTime extractionRunMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        string todayFormattedDate = "";

        ConvertData convertData = new ConvertData();
        CommonData commonData = new CommonData();

        //datatables
        DataTable _patientARDataTable = new DataTable();

        //define type/definition objects
        Dictionary<string, string> paymentTypes = new Dictionary<string, string>();
        Dictionary<string, string> aDACodes = new Dictionary<string, string>();
        Dictionary<int, string> referrerTypes = new Dictionary<int, string>();
        Dictionary<string, string> adjustmentTypes = new Dictionary<string, string>();
        Dictionary<int, int> insuredList = new Dictionary<int, int>();
        List<ProvidersBrokenAppointment> updatedBrokenAppointments = new List<ProvidersBrokenAppointment>();
        List<int> existingBrokenApptIDs = new List<int>();
        List<int> walkoutPatientIDs = new List<int>();

        //providers lists
        List<string> _doctorProviders { get; set; }
        List<string> _hygieneProviders { get; set; }
        List<string> _otherProviders { get; set; }

        bool _processBrokenAppointments = true;

        ThriveDataServiceClient thriveService = new ThriveDataServiceClient();

        public OpenDental(string ConnString, AppData appData)
        {
            connectionString = ConnString;
            _appData = appData;

            analyzeData = new AnalyzeData(_appData.OfficeKeyGuid);
            todayFormattedDate = FormatExtractionDate(DateTime.Today);

            //fill types lists
            FillTypesLists();

            //initialize lists
            _doctorProviders = new List<string>();
            _hygieneProviders = new List<string>();
            _otherProviders = new List<string>();

            //office specefic data
            if (_appData.OfficeData != null)
            {
                //add custom hygiene codes
                if (_appData.OfficeData.CustomHygieneCodes != null)
                {
                    commonData.HygieneCodes.AddRange(_appData.OfficeData.CustomHygieneCodes.Split(','));
                }
            }

            //get existing broken appointments ids
            try
            {
                GetProvidersBrokenAppointmentIDsRequest request = new GetProvidersBrokenAppointmentIDsRequest(_appData.OfficeKey);
                GetProvidersBrokenAppointmentIDsResponse response = new GetProvidersBrokenAppointmentIDsResponse();

                using (ThriveDataServiceClient thriveService = new ThriveDataServiceClient())
                {
                    response = thriveService.GetProvidersBrokenAppointmentIDs(request);
                    string result = response.GetProvidersBrokenAppointmentIDsResult;

                    existingBrokenApptIDs = JsonConvert.DeserializeObject<List<int>>(result);
                }

            }
            catch (Exception ex)
            {

                _processBrokenAppointments = false;

                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                }
                catch { }
            }

            if (_processBrokenAppointments == true)
            {
                //get existing walkout patient ids
                try
                {
                    GetProvidersWalkoutPatientIDsRequest request = new GetProvidersWalkoutPatientIDsRequest(_appData.OfficeKey);
                    GetProvidersWalkoutPatientIDsResponse response = new GetProvidersWalkoutPatientIDsResponse();

                    using (ThriveDataServiceClient thriveService = new ThriveDataServiceClient())
                    {
                        response = thriveService.GetProvidersWalkoutPatientIDs(request);
                        string result = response.GetProvidersWalkoutPatientIDsResult;

                        walkoutPatientIDs = JsonConvert.DeserializeObject<List<int>>(result);
                    }

                }
                catch (Exception ex)
                {
                    _processBrokenAppointments = false;

                    try
                    {
                        errorLog.LogError(_appData.OfficeKeyGuid, ex);
                    }
                    catch { }
                }
            }

        }

        private void FillTypesLists()
        {
            //clear/null first
            paymentTypes.Clear();
            aDACodes.Clear();
            referrerTypes.Clear();
            adjustmentTypes.Clear();
            insuredList.Clear();

            //get types/definitions
            paymentTypes = FillTypeData(paymentTypes, "SELECT * FROM definition Where category = 10", "defnum", "itemname");
            aDACodes = FillTypeData(aDACodes, "Select proccode, codenum From procedurecode Where proccode Is Not Null", "codenum", "proccode");
            referrerTypes = FillReferrersData();
            adjustmentTypes = FillTypeData(adjustmentTypes, "SELECT * FROM definition Where category = 1", "defnum", "itemname");
            insuredList = FillTypeDataInt(insuredList, "Select subscriber,plannum From inssub", "subscriber", "plannum");

            try
            {
                if (_appData.OfficeData.AnalyzeData == true)
                {
                    string json = JsonConvert.SerializeObject(aDACodes, Formatting.Indented);

                    errorLog.LogDiagnostic(_appData.OfficeKeyGuid, json, "ADA Codes");
                }
            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                }
                catch { }
            }
        }

        private DataTable DentalData(string DataQuery, bool logData = false)
        {
            try
            {
                if (_appData.OfficeData.AnalyzeData == true && logData == true)
                {
                    errorLog.LogDiagnostic(_appData.OfficeKeyGuid, DataQuery, "DentalData()");
                }
            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                }
                catch { }
            }

            DataTable dentalData = new DataTable();

            using (MySqlConnection dbConnection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand dbCommand = new MySqlCommand(DataQuery, dbConnection))
                {
                    using (MySqlDataAdapter dbAdapter = new MySqlDataAdapter(dbCommand))
                    {
                        dbConnection.Open();
                        dbAdapter.Fill(dentalData);
                        dbConnection.Close();
                    }
                }
            }

            return dentalData;
        }

        private void PatientLastContact(int PatID, ProvidersBrokenAppointment PBA)
        {
            try
            {
                DataTable contactData = new DataTable();

                //contactData = DentalData("Select Top 2 ContactDate, ContactType From admin.contact Where Patid = " + PatID + 
                //" And (ContactType = 3 Or ContactType = 1) Order By ContactDate DESC");

                contactData = DentalData("Select CommDateTime, CommType, Mode_ From commlog Where PatNum = " + PatID +
                   " And (Mode_ = 3 Or Mode_ = 2) Order By CommDateTime DESC LIMIT 2");

                if (contactData.Rows.Count == 1)
                {
                    //set last(latest) contact
                    PBA.LastContactDate = contactData.Rows[0].Field<DateTime?>("CommDateTime");

                    if (contactData.Rows[0].Field<System.Byte>("Mode_") == 2)
                    {
                        PBA.LastContactType = "Letter";
                    }
                    else
                    {
                        if (contactData.Rows[0].Field<System.Byte>("Mode_") == 3)
                        {
                            PBA.LastContactType = "Phone Call";
                        }
                    }
                }
                if (contactData.Rows.Count == 2)
                {
                    //set last(latest) contact
                    PBA.LastContactDate = contactData.Rows[0].Field<DateTime?>("CommDateTime");

                    if (contactData.Rows[0].Field<System.Byte>("Mode_") == 2)
                    {
                        PBA.LastContactType = "Letter";
                    }
                    else
                    {
                        if (contactData.Rows[0].Field<System.Byte>("Mode_") == 3)
                        {
                            PBA.LastContactType = "Phone Call";
                        }
                    }

                    //set previous to last contact
                    PBA.LastContactDatePrevious = contactData.Rows[1].Field<DateTime?>("CommDateTime");

                    if (contactData.Rows[1].Field<System.Byte>("Mode_") == 2)
                    {
                        PBA.LastContactTypePrevious = "Letter";
                    }
                    else
                    {
                        if (contactData.Rows[1].Field<System.Byte>("Mode_") == 3)
                        {
                            PBA.LastContactTypePrevious = "Phone Call";
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                }
                catch { }
            }
        }

        private bool PatientLastContactUpdate(int PatID, ProvidersBrokenAppointment PBA)
        {
            bool updateRecord = false;

            try
            {
                DataTable contactData = new DataTable();

                //contactData = DentalData("Select Top 2 ContactDate, ContactType From admin.contact Where Patid = " + PatID + 
                //    " And (ContactType = 3 Or ContactType = 1) Order By ContactDate DESC");
                contactData = DentalData("Select CommDateTime, CommType, Mode_ From commlog Where PatNum = " + PatID +
                    "  And (Mode_= 3 Or Mode_ = 2) Order By CommDateTime DESC LIMIT 2");

                if (contactData.Rows.Count == 1)
                {
                    //set last(latest) contact
                    var lastContactDate = contactData.Rows[0].Field<DateTime?>("CommDateTime");
                    if (lastContactDate > PBA.LastContactDate || (lastContactDate != null && PBA.LastContactDate == null))
                    {
                        updateRecord = true;
                        PBA.LastContactDate = contactData.Rows[0].Field<DateTime?>("CommDateTime");

                        if (contactData.Rows[0].Field<System.Byte>("Mode_") == 2)
                        {
                            PBA.LastContactType = "Letter";
                        }
                        else
                        {
                            if (contactData.Rows[0].Field<System.Byte>("Mode_") == 3)
                            {
                                PBA.LastContactType = "Phone Call";
                            }
                        }
                    }
                }
                if (contactData.Rows.Count == 2)
                {

                    //set last(latest) contact
                    var lastContactDate = contactData.Rows[0].Field<DateTime?>("CommDateTime");
                    if (lastContactDate > PBA.LastContactDate || (lastContactDate != null && PBA.LastContactDate == null))
                    {
                        updateRecord = true;
                        PBA.LastContactDate = contactData.Rows[0].Field<DateTime?>("CommDateTime");

                        if (contactData.Rows[0].Field<System.Byte>("Mode_") == 2)
                        {
                            PBA.LastContactType = "Letter";
                        }
                        else
                        {
                            if (contactData.Rows[0].Field<System.Byte>("Mode_") == 3)
                            {
                                PBA.LastContactType = "Phone Call";
                            }
                        }
                    }

                    //set previous to last contact
                    var lastContactDate2 = contactData.Rows[1].Field<DateTime?>("CommDateTime");
                    if (lastContactDate2 > PBA.LastContactDatePrevious || (lastContactDate2 != null && PBA.LastContactDatePrevious == null))
                    {
                        updateRecord = true;
                        PBA.LastContactDatePrevious = contactData.Rows[1].Field<DateTime?>("CommDateTime");

                        if (contactData.Rows[1].Field<System.Byte>("Mode_") == 2)
                        {
                            PBA.LastContactTypePrevious = "Letter";
                        }
                        else
                        {
                            if (contactData.Rows[1].Field<System.Byte>("Mode_") == 3)
                            {
                                PBA.LastContactTypePrevious = "Phone Call";
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                }
                catch { }
            }

            return updateRecord;
        }

        internal List<Provider> GetProviders(DateTime ProcessDate, Guid OfficeKey)
        {
            string formattedDate = FormatExtractionDate(ProcessDate);

            List<Provider> providers = new List<Provider>();
            //DataTable providersProduction = DentalData("Select Distinct(provid) From admin.fullproclog Where chartstatus = 102 And procdate >= {d'" + formattedDate + "'}");
            DataTable providersProduction = DentalData("Select Distinct(ProvNum) From procedurelog Where ProcStatus = 2 And ProcDate >= {d'" + formattedDate + "'}");


            //DataTable providersAdjustments = DentalData("Select Distinct(provid) From admin.fullproclog Where chartstatus = 90 And proclogclass = 2 And procdate >= {d'" + formattedDate + "'}");
            //DataTable providersPayments = DentalData("Select Distinct(provid) From admin.fullproclog Where chartstatus = 90 And proclogclass = 1 And procdate >= {d'" + formattedDate + "'}");
            //DataTable providersInsPayments = DentalData("Select Distinct(provid) From admin.fullproclog Where chartstatus = 90 And proclogclass = 3 And procdate >= {d'" + formattedDate + "'}");

            List<int> providersToTrack = new List<int>();
            StringBuilder providerIds = new StringBuilder();
            Dictionary<int, List<string>> providersProcedures = new Dictionary<int, List<string>>();

            foreach (DataRow prov in providersProduction.Rows)
            {
                if (!providersToTrack.Contains(convertData.ConvertToInt(prov[0].ToString())))
                {
                    providersToTrack.Add(convertData.ConvertToInt(prov[0].ToString()));
                }
            }

            foreach (int prov in providersToTrack)
            {
                List<string> provProcCodes = new List<string>();
                //List<string> provProcAbbr = new List<string>();

                //DataTable provCompletedProcedures = DentalData("Select Distinct(proccodeid) From admin.fullproclog Where provid = '" + prov + "' And chartstatus = 102 And procdate >= {d'" + formattedDate + "'} Order By proccodeid");
                DataTable provCompletedProcedures = DentalData("Select Distinct(CodeNum) From procedurelog Where ProvNum = '" + prov + "' And ProcStatus = 2 And ProcDate >= {d'" + formattedDate + "'} Order By CodeNum");

                //DataTable providersProduction = DentalData("Select Distinct(ProvNum) From procedurelog Where ProcStatus = 2 And ProcDate >= {d'" + formattedDate + "'}");

                foreach (DataRow proc in provCompletedProcedures.Rows)
                {
                    string procCodeKey = proc[0].ToString();
                    if (aDACodes.ContainsKey(procCodeKey))
                    {
                        provProcCodes.Add(aDACodes[procCodeKey]);
                    }
                    //provProcAbbr.Add(proc.Field<string>("Abbr"));
                }
                providersProcedures.Add(prov, provProcCodes);

                providerIds.Append("'" + prov + "',");
            }

            if (providerIds.Length > 1)
            {
                providerIds.Remove(providerIds.Length - 1, 1);

                //DataTable providersData = DentalData("Select provider_id, first_name, last_name From admin.v_provider Where provider_id In (" + providerIds + ")");
                DataTable providersData = DentalData("Select ProvNum, Abbr, FName, LName From provider Where ProvNum In (" + providerIds + ")");


                foreach (DataRow prov in providersData.Rows)
                {
                    int providerType = 2;

                    int hygieneCount = 0;
                    int doctorCount = 0;

                    foreach (var p in providersProcedures)
                    {
                        foreach (var proc in p.Value)
                        {
                            if (p.Key.ToString() == prov["ProvNum"].ToString())
                            {
                                if (commonData.HygieneCodes.Contains(proc))
                                {
                                    hygieneCount++;
                                }
                                if (commonData.DoctorCodes.Contains(proc))
                                {
                                    doctorCount++;
                                }
                            }
                        }
                    }

                    if (doctorCount > 0 && hygieneCount > 0)
                    {
                        if (doctorCount > hygieneCount)
                        {
                            providerType = 0;
                        }
                        else
                        {
                            providerType = 1;
                        }
                    }

                    if (providerType == 2 && doctorCount > 0)
                    {
                        providerType = 0;
                    }

                    if (providerType == 2)
                    {
                        if (prov["Abbr"].ToString().ToLower().StartsWith("d"))
                        {
                            providerType = 0;
                        }
                        else
                        {
                            if (prov["Abbr"].ToString().ToLower().StartsWith("h"))
                            {
                                providerType = 1;
                            }
                        }
                    }

                    string provDisplayName = "";

                    Provider newProv = new Provider();

                    if (prov["LName"].ToString().Trim().Length >= 1)
                    {
                        provDisplayName = prov["FName"].ToString().Trim() + " " + prov["LName"].ToString().Substring(0, 1) + ".";
                    }
                    else
                    {
                        provDisplayName = prov["FName"].ToString().Trim();
                    }

                    newProv.OfficeID = OfficeKey;
                    newProv.PMName = prov["Abbr"].ToString().Trim() + "-" + prov["FName"].ToString().Trim() + " " + prov["LName"].ToString().Trim();
                    newProv.DisplayName = provDisplayName;
                    newProv.PMID = prov["ProvNum"].ToString();
                    newProv.ProvType = providerType;

                    if (providerType != 2)
                    {
                        newProv.Track = true;
                        newProv.VisibleOnIndex = true;
                    }
                    else
                    {
                        newProv.Track = false;
                        newProv.VisibleOnIndex = false;
                    }
                    newProv.VisibleOnIndex = true;

                    providers.Add(newProv);
                }

                providersData.Clear();
                providersData = null;
            }

            return providers;
        }

        private decimal ConvertToPositive(decimal amt)
        {
            if (amt < 0)
            {
                amt = (amt * -1);
            }

            return amt;
        }

        private void GetBillingTypes()
        {
            //DataTable billingTypesData = DentalData("Select Distinct(defid) From admin.fulldef Where deftype = 1");
            DataTable billingTypesData = DentalData("SELECT * FROM definition Where category = 4");
            List<UInt16> billingTypesList = (from a in billingTypesData.AsEnumerable()
                                             select a.Field<UInt16>("ItemOrder")).ToList(); //ItemOrder, ItemName comb.?

            string billingTypeIds = string.Join<UInt16>(",", billingTypesList);

            if (billingTypeIds.Length > 100)
            {
                billingTypeIds = billingTypeIds.Substring(0, 99);
            }

            if (billingTypeIds.EndsWith(","))
            {
                billingTypeIds = billingTypeIds.Remove(billingTypeIds.Length - 1, 1);
            }

            _appData.OfficeData.BillingTypes = billingTypeIds;
            _appData.SetOfficeData();
        }

        internal void ProcessARData(Office OfficeData)
        {
            //put this here for now (first process so populate data)
            //get/create completed appointment string
            if (_appData.OfficeData.CompletedAppointmentStatus != null && _appData.OfficeData.CompletedAppointmentStatus != "")
            {
                List<string> customStatuses = _appData.OfficeData.CompletedAppointmentStatus.Split(',').ToList();
                //_completedApptStatus = "status = -106 Or status = 150";
                _completedApptStatus = "AptStatus = 2";

                foreach (string cs in customStatuses)
                {
                    _completedApptStatus += " Or AptStatus = " + cs;
                }
            }
            else
            {
                _completedApptStatus = "AptStatus = 2";
            }

            //set procedures to ignore: this is for broken appointments
            _procsToIgnoreList = new List<long>();

            foreach (string proc in commonData.ProcsToIgnore)
            {
                Dictionary<string, string> procsDict = new Dictionary<string, string>();

                procsDict = (from a in aDACodes
                             where a.Value == proc
                             select a).ToDictionary(a => a.Key, a => a.Value);

                foreach (var p in procsDict)
                {
                    _procsToIgnoreList.Add(Convert.ToInt64(p.Key));
                }
            }

            //get billing types
            if (_appData.OfficeData.BillingTypes == null)
            {
                GetBillingTypes();
            }

            //get default billing types if null
            List<OfficesAR> officesAr = new List<OfficesAR>();

            //DataTable patientAR = new DataTable();

            if (_appData.OfficeData.BillingTypes == null)
            {
                _patientARDataTable = DentalData("Select Guarantor,PatNum,Bal_0_30,Bal_31_60,Bal_61_90,BalOver90 from patient"); // where billingtype <> 9
            }
            else
            {
                _patientARDataTable = DentalData("Select Guarantor,PatNum,Bal_0_30,Bal_31_60,Bal_61_90,BalOver90 from patient"); // where billingtype In (" + _appData.OfficeData.BillingTypes + ")");
            }

            //fill provider lists
            foreach (Provider prov in _appData.Providers)
            {
                switch (prov.ProvType)
                {
                    case 0:
                        _doctorProviders.Add(prov.PMID);
                        break;

                    case 1:
                        _hygieneProviders.Add(prov.PMID);
                        break;

                    case 2:
                        _otherProviders.Add(prov.PMID);
                        break;

                    default:
                        break;
                }
            }

            //patient ar
            double age0To30 = 0;
            double age31To60 = 0;
            double age61To90 = 0;
            double ageOver90 = 0;
            double credit0To30 = 0;
            double credit31To60 = 0;
            double credit61To90 = 0;
            double creditOver90 = 0;

            foreach (DataRow oRow in _patientARDataTable.Rows)
            {
                double tmpAmt = 0;
                tmpAmt = oRow.Field<double>("Bal_0_30");

                if (tmpAmt >= 0)
                {
                    age0To30 += tmpAmt;
                }
                else
                {
                    credit0To30 += tmpAmt;
                }

                tmpAmt = 0;
                tmpAmt = oRow.Field<double>("Bal_31_60");
                if (tmpAmt >= 0)
                {
                    age31To60 += tmpAmt;
                }
                else
                {
                    credit31To60 += tmpAmt;
                }

                tmpAmt = 0;
                tmpAmt = oRow.Field<double>("Bal_61_90");
                if (tmpAmt >= 0)
                {
                    age61To90 += tmpAmt;
                }
                else
                {
                    credit61To90 += tmpAmt;
                }

                tmpAmt = 0;
                tmpAmt = oRow.Field<double>("BalOver90");
                if (tmpAmt >= 0)
                {
                    ageOver90 += tmpAmt;
                }
                else
                {
                    creditOver90 += tmpAmt;
                }

            }

            OfficesAR officeAr = new OfficesAR();
            officeAr.OfficeID = _appData.OfficeKeyGuid;
            officeAr.CreatedDate = DateTime.Now;
            officeAr.Credit0To30 = (decimal)credit0To30;
            officeAr.Credit31To60 = (decimal)credit31To60;
            officeAr.Credit61To90 = (decimal)credit61To90;
            officeAr.CreditOver90 = (decimal)creditOver90;
            officeAr.Insurance0To30 = 0;
            officeAr.Insurance31To60 = 0;
            officeAr.Insurance61To90 = 0;
            officeAr.InsuranceOver90 = 0;
            officeAr.Patient0To30 = (decimal)age0To30;
            officeAr.Patient31To60 = (decimal)age31To60;
            officeAr.Patient61To90 = (decimal)age61To90;
            officeAr.PatientOver90 = (decimal)ageOver90;

            officesAr.Add(officeAr);

            var jsonData = JsonConvert.SerializeObject(officesAr, Formatting.Indented);

            try
            {
                BulkUploadOfficesARDataRequest request = new BulkUploadOfficesARDataRequest(officeAr.OfficeID.ToString(), jsonData);
                BulkUploadOfficesARDataResponse response = new BulkUploadOfficesARDataResponse();

                using (ThriveDataServiceClient thriveService = new ThriveDataServiceClient())
                {
                    response = thriveService.BulkUploadOfficesARData(request);
                    bool result = response.BulkUploadOfficesARDataResult;
                }

            }
            catch (Exception ex)
            {

            }
        }

        internal void ProcessOfficeProcedureData(Office OfficeData)
        {
            // All Open Dental raw data processed in ProcessRawData function (below)
            return;
        }

        private void ProcessODAppointments(string apptProcessDate, Office OfficeData)
        {
            DataTable data = new DataTable();

            //data lists
            List<od_appointment> dataList = new List<od_appointment>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            //appointments
            if (OfficeData.AppointmentFullUpload == true)
            {
                data = DentalData("SELECT AptNum,PatNum,AptStatus,Pattern,Confirmed,TimeLocked,Op,Note,ProvNum,ProvHyg,AptDateTime,NextAptNum,UnschedStatus,IsNewPatient,ProcDescript, " +
                                   "Assistant,ClinicNum,IsHygiene,DateTStamp,DateTimeArrived, " +
                                   "DateTimeSeated,DateTimeDismissed,InsPlan1,InsPlan2,DateTimeAskedToArrive,ProcsColored,ColorOverride,AppointmentTypeNum " +
                                   "From appointment a Where DATE(AptDateTime) >= DATE('" + apptProcessDate + "')");

            }
            else
            {
                data = DentalData("SELECT AptNum,PatNum,AptStatus,Pattern,Confirmed,TimeLocked,Op,Note,ProvNum,ProvHyg,AptDateTime,NextAptNum,UnschedStatus,IsNewPatient,ProcDescript,Assistant,ClinicNum,IsHygiene,DateTStamp,DateTimeArrived, " +
                                 "DateTimeSeated,DateTimeDismissed,InsPlan1,InsPlan2,DateTimeAskedToArrive,ProcsColored,ColorOverride,AppointmentTypeNum " +
                                 "From appointment a Where DATE(DateTStamp) >= DATE('" + apptProcessDate + "')");
            }

            foreach (DataRow oRow in data.Rows)
            {
                try
                {
                    od_appointment record = new od_appointment();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.AptNum = oRow.Field<long>("AptNum");
                    record.PatNum = oRow.Field<long>("PatNum");
                    record.AptStatus = oRow.Field<byte>("AptStatus");
                    record.Pattern = oRow.Field<string>("Pattern");
                    record.Confirmed = oRow.Field<long>("Confirmed");
                    record.TimeLocked = Convert.ToInt16(oRow.Field<bool>("TimeLocked"));
                    record.Op = oRow.Field<long>("Op");
                    record.Note = oRow.Field<string>("Note");
                    record.ProvNum = oRow.Field<long>("ProvNum");
                    record.ProvHyg = oRow.Field<long>("ProvHyg");
                    //convertData.ConvertToNullableDate(oRow.Field<DateTime?>("AptDateTime").ToString()); 
                    record.AptDateTime = oRow.Field<DateTime>("AptDateTime");
                    record.NextAptNum = oRow.Field<long>("NextAptNum");
                    record.UnschedStatus = oRow.Field<long>("UnschedStatus");
                    record.IsNewPatient = oRow.Field<byte>("IsNewPatient");
                    record.ProcDescript = oRow.Field<string>("ProcDescript");
                    record.Assistant = oRow.Field<long>("Assistant");
                    record.ClinicNum = oRow.Field<long>("ClinicNum");
                    record.IsHygiene = oRow.Field<byte>("IsHygiene");
                    record.DateTStamp = oRow.Field<DateTime>("DateTStamp");
                    record.DateTimeArrived = oRow.Field<DateTime>("DateTimeArrived");
                    record.DateTimeSeated = oRow.Field<DateTime>("DateTimeSeated");
                    record.DateTimeDismissed = oRow.Field<DateTime>("DateTimeDismissed");
                    record.InsPlan1 = oRow.Field<long>("InsPlan1");
                    record.InsPlan2 = oRow.Field<long>("InsPlan2");
                    record.DateTimeAskedToArrive = oRow.Field<DateTime>("DateTimeAskedToArrive");
                    record.ProcsColored = oRow.Field<string>("ProcsColored");
                    record.ColorOverride = oRow.Field<int>("ColorOverride");
                    record.AppointmentTypeNum = oRow.Field<long>("AppointmentTypeNum");

                    dataList.Add(record);


                    if (dataList.Count >= 100)
                    {
                        //upload appointment data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_appointment");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }

            }

            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_appointment");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }
        }


        private void ProcessODPatients(string apptProcessDate, Office OfficeData)
        {
            DataTable data = new DataTable();

            //data lists
            List<od_patient> dataList = new List<od_patient>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // patients

            data = DentalData("SELECT * From patient");

            foreach (DataRow oRow in data.Rows)
            {
                try
                {
                    od_patient record = new od_patient();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.PatNum = oRow.Field<long>("PatNum");
                    record.LName = oRow.Field<string>("LName");
                    record.FName = oRow.Field<string>("FName");
                    record.MiddleI = oRow.Field<string>("MiddleI");
                    record.Preferred = oRow.Field<string>("Preferred");
                    record.PatStatus = oRow.Field<byte>("PatStatus");
                    record.Gender = oRow.Field<byte>("Gender");
                    record.Position = oRow.Field<byte>("Position");
                    record.Birthdate = oRow.Field<DateTime>("Birthdate");
                    record.SSN = oRow.Field<string>("SSN");
                    record.Address = oRow.Field<string>("Address");
                    record.Address2 = oRow.Field<string>("Address2");
                    record.City = oRow.Field<string>("City");
                    record.State = oRow.Field<string>("State");
                    record.Zip = oRow.Field<string>("Zip");
                    record.HmPhone = oRow.Field<string>("HmPhone");
                    record.WkPhone = oRow.Field<string>("WkPhone");
                    record.WirelessPhone = oRow.Field<string>("WirelessPhone");
                    record.Guarantor = oRow.Field<long>("Guarantor");
                    record.CreditType = oRow.Field<string>("CreditType");
                    record.Email = oRow.Field<string>("Email");
                    record.Salutation = oRow.Field<string>("Salutation");
                    record.EstBalance = oRow.Field<double>("EstBalance");
                    record.PriProv = oRow.Field<long>("PriProv");
                    record.SecProv = oRow.Field<long>("SecProv");
                    record.FeeSched = oRow.Field<long>("FeeSched");
                    record.BillingType = oRow.Field<long>("BillingType");
                    record.ImageFolder = oRow.Field<string>("ImageFolder");
                    record.AddrNote = oRow.Field<string>("AddrNote");
                    record.FamFinUrgNote = oRow.Field<string>("FamFinUrgNote");
                    record.MedUrgNote = oRow.Field<string>("MedUrgNote");
                    record.ApptModNote = oRow.Field<string>("ApptModNote");
                    record.StudentStatus = oRow.Field<string>("StudentStatus");
                    record.SchoolName = oRow.Field<string>("SchoolName");
                    record.ChartNumber = oRow.Field<string>("ChartNumber");
                    record.MedicaidID = oRow.Field<string>("MedicaidID");
                    record.Bal_0_30 = oRow.Field<double>("Bal_0_30");
                    record.Bal_31_60 = oRow.Field<double>("Bal_31_60");
                    record.Bal_61_90 = oRow.Field<double>("Bal_61_90");
                    record.BalOver90 = oRow.Field<double>("BalOver90");
                    record.InsEst = oRow.Field<double>("InsEst");
                    record.BalTotal = oRow.Field<double>("BalTotal");
                    record.EmployerNum = oRow.Field<long>("EmployerNum");
                    record.EmploymentNote = oRow.Field<string>("EmploymentNote");
                    record.County = oRow.Field<string>("County");
                    record.GradeLevel = Convert.ToInt16(oRow.Field<sbyte>("GradeLevel"));
                    record.Urgency = Convert.ToInt16(oRow.Field<sbyte>("Urgency"));
                    record.DateFirstVisit = oRow.Field<DateTime>("DateFirstVisit");
                    record.ClinicNum = oRow.Field<long>("ClinicNum");
                    record.HasIns = oRow.Field<string>("HasIns");
                    record.TrophyFolder = oRow.Field<string>("TrophyFolder");
                    record.PlannedIsDone = oRow.Field<byte>("PlannedIsDone");
                    record.Premed = oRow.Field<byte>("Premed");
                    record.Ward = oRow.Field<string>("Ward");
                    record.PreferConfirmMethod = oRow.Field<byte>("PreferConfirmMethod");
                    record.PreferContactMethod = oRow.Field<byte>("PreferContactMethod");
                    record.PreferRecallMethod = oRow.Field<byte>("PreferRecallMethod");
                    record.SchedBeforeTime = oRow.Field<TimeSpan?>("SchedBeforeTime");
                    record.SchedAfterTime = oRow.Field<TimeSpan?>("SchedAfterTime");
                    record.SchedDayOfWeek = oRow.Field<byte>("SchedDayOfWeek");
                    record.Language = oRow.Field<string>("Language");
                    record.AdmitDate = oRow.Field<DateTime>("AdmitDate");
                    record.Title = oRow.Field<string>("Title");
                    record.PayPlanDue = oRow.Field<double>("PayPlanDue");
                    record.SiteNum = oRow.Field<long>("SiteNum");
                    record.DateTStamp = oRow.Field<DateTime>("DateTStamp");
                    record.ResponsParty = oRow.Field<long>("ResponsParty");
                    record.CanadianEligibilityCode = Convert.ToInt16(oRow.Field<sbyte>("CanadianEligibilityCode"));
                    record.AskToArriveEarly = oRow.Field<int>("AskToArriveEarly");
                    record.OnlinePassword = ""; // oRow.Field<string>("OnlinePassword");
                    record.PreferContactConfidential = Convert.ToInt16(oRow.Field<sbyte>("PreferContactConfidential"));
                    record.SuperFamily = oRow.Field<long>("SuperFamily");
                    record.TxtMsgOk = Convert.ToInt16(oRow.Field<sbyte>("TxtMsgOk"));
                    record.SmokingSnoMed = oRow.Field<string>("SmokingSnoMed");
                    record.Country = oRow.Field<string>("Country");
                    record.DateTimeDeceased = oRow.Field<DateTime>("DateTimeDeceased");
                    record.BillingCycleDay = oRow.Field<int>("BillingCycleDay");

                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload patient data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_patient");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }

                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }


            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_patient");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }
        }

        private void ProcessODProviders(string apptProcessDate, Office OfficeData)
        {
            DataTable data = new DataTable();

            //data lists
            List<od_provider> dataList = new List<od_provider>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // providers

            data = DentalData("SELECT * From provider");

            foreach (DataRow oRow in data.Rows)
            {
                try
                {
                    od_provider record = new od_provider();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.ProvNum = oRow.Field<long>("ProvNum");
                    record.ItemOrder = oRow.Field<ushort>("ItemOrder");
                    record.Abbr = oRow.Field<string>("Abbr");
                    record.LName = oRow.Field<string>("LName");
                    record.FName = oRow.Field<string>("FName");
                    record.MI = oRow.Field<string>("MI");
                    record.Suffix = oRow.Field<string>("Suffix");
                    record.FeeSched = oRow.Field<long>("FeeSched");
                    record.Specialty = oRow.Field<long>("Specialty");
                    record.SSN = oRow.Field<string>("SSN");
                    record.StateLicense = oRow.Field<string>("StateLicense");
                    record.DEANum = oRow.Field<string>("DEANum");
                    record.IsSecondary = oRow.Field<byte>("IsSecondary");
                    record.ProvColor = oRow.Field<int>("ProvColor");
                    record.IsHidden = oRow.Field<byte>("IsHidden");
                    record.UsingTIN = oRow.Field<byte>("UsingTIN");
                    record.BlueCrossID = oRow.Field<string>("BlueCrossID");
                    record.SigOnFile = oRow.Field<byte>("SigOnFile");
                    record.MedicaidID = oRow.Field<string>("MedicaidID");
                    record.OutlineColor = oRow.Field<int>("OutlineColor");
                    record.SchoolClassNum = oRow.Field<long>("SchoolClassNum");
                    record.NationalProvID = oRow.Field<string>("NationalProvID");
                    record.CanadianOfficeNum = oRow.Field<string>("CanadianOfficeNum");
                    record.DateTStamp = oRow.Field<DateTime>("DateTStamp");
                    record.AnesthProvType = oRow.Field<long>("AnesthProvType");
                    record.TaxonomyCodeOverride = oRow.Field<string>("TaxonomyCodeOverride");
                    record.IsCDAnet = Convert.ToInt16(oRow.Field<sbyte>("IsCDAnet"));
                    record.EcwID = oRow.Field<string>("EcwID");
                    record.StateRxID = oRow.Field<string>("StateRxID");
                    record.IsNotPerson = Convert.ToInt16(oRow.Field<sbyte>("IsNotPerson"));
                    record.StateWhereLicensed = oRow.Field<string>("StateWhereLicensed");
                    record.EmailAddressNum = oRow.Field<long>("EmailAddressNum");
                    record.IsInstructor = Convert.ToInt16(oRow.Field<sbyte>("IsInstructor"));
                    record.EhrMuStage = oRow.Field<int>("EhrMuStage");
                    record.ProvNumBillingOverride = oRow.Field<long>("ProvNumBillingOverride");
                    record.CustomID = oRow.Field<string>("CustomID");
                    record.ProvStatus = Convert.ToInt16(oRow.Field<sbyte>("ProvStatus"));

                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload provider data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_provider");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental 
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }

            }

            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_provider");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }

        }

        private void ProcessODPayments(string apptProcessDate, Office OfficeData)
        {

            DataTable data = new DataTable();

            //data lists
            List<od_payment> dataList = new List<od_payment>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // payments

            data = DentalData("SELECT * From payment " +
            "Where DATE(PayDate) >= DATE('" + apptProcessDate + "')");


            foreach (DataRow oRow in data.Rows)
            {
                try
                {
                    od_payment record = new od_payment();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.PayNum = oRow.Field<long>("PayNum");
                    record.PayType = oRow.Field<long>("PayType");
                    record.PayDate = oRow.Field<DateTime>("PayDate");
                    record.PayAmt = oRow.Field<double>("PayAmt");
                    record.CheckNum = oRow.Field<string>("CheckNum");
                    record.BankBranch = oRow.Field<string>("BankBranch");
                    record.PayNote = oRow.Field<string>("PayNote");
                    record.IsSplit = oRow.Field<byte>("IsSplit");
                    record.PayNum = oRow.Field<long>("PayNum");
                    record.ClinicNum = oRow.Field<long>("ClinicNum");
                    record.DateEntry = oRow.Field<DateTime>("DateEntry");
                    record.Receipt = oRow.Field<string>("Receipt");
                    record.DepositNum = oRow.Field<long>("DepositNum");
                    record.IsRecurringCC = Convert.ToInt16(oRow.Field<sbyte>("IsRecurringCC"));

                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload payment data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_payment");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental 
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }

            }

            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_payment");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }
        }

        private void ProcessODPaySplits(string apptProcessDate, Office OfficeData)
        {

            DataTable data = new DataTable();

            //data lists
            List<od_paysplit> dataList = new List<od_paysplit>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // paysplits

            data = DentalData("SELECT * From paysplit " +
            "Where DATE(DatePay) >= DATE('" + apptProcessDate + "')");


            foreach (DataRow oRow in data.Rows)
            {
                try
                {
                    od_paysplit record = new od_paysplit();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.SplitNum = oRow.Field<long>("SplitNum");
                    record.SplitAmt = oRow.Field<double>("SplitAmt");
                    record.PatNum = oRow.Field<long>("PatNum");
                    record.ProcDate = oRow.Field<DateTime>("ProcDate");
                    record.PayNum = oRow.Field<long>("PayNum");
                    record.IsDiscount = oRow.Field<byte>("IsDiscount");
                    record.DiscountType = oRow.Field<byte>("DiscountType");
                    record.ProvNum = oRow.Field<long>("ProvNum");
                    record.PayPlanNum = oRow.Field<long>("PayPlanNum");
                    record.DatePay = oRow.Field<DateTime>("DateEntry");
                    record.ProcNum = oRow.Field<long>("ProcNum");
                    record.DateEntry = oRow.Field<DateTime>("DateEntry");
                    record.UnearnedType = oRow.Field<long>("UnearnedType");
                    record.ClinicNum = oRow.Field<long>("ClinicNum");


                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload paysplit data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_paysplit");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental 
                }

            }

            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_paysplit");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }
        }
        private void ProcessODProcedureLogs(string apptProcessDate, Office OfficeData)
        {
            DataTable data = new DataTable();

            //data lists
            List<od_procedurelog> dataList = new List<od_procedurelog>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // procedurelogs

            data = DentalData("SELECT * From procedurelog " +
           "Where DATE(ProcDate) >= DATE('" + apptProcessDate + "')");


            foreach (DataRow oRow in data.Rows)
            {
                try
                {

                    od_procedurelog record = new od_procedurelog();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.ProcNum = oRow.Field<long>("ProcNum");
                    record.PatNum = oRow.Field<long>("PatNum");
                    record.AptNum = oRow.Field<long>("AptNum");
                    record.OldCode = oRow.Field<string>("OldCode");
                    record.ProcDate = oRow.Field<DateTime>("ProcDate");
                    record.ProcFee = oRow.Field<double>("ProcFee");
                    record.Surf = oRow.Field<string>("Surf");
                    record.ToothNum = oRow.Field<string>("ToothNum");
                    record.ToothRange = oRow.Field<string>("ToothRange");
                    record.Priority = oRow.Field<long>("Priority");
                    record.ProcStatus = oRow.Field<byte>("ProcStatus");
                    record.ProvNum = oRow.Field<long>("ProvNum");
                    record.Dx = oRow.Field<long>("Dx");
                    record.PlannedAptNum = oRow.Field<long>("PlannedAptNum");
                    record.PlaceService = oRow.Field<byte>("PlaceService");
                    record.Prosthesis = oRow.Field<string>("Prosthesis");
                    record.DateOriginalProsth = oRow.Field<DateTime>("DateOriginalProsth");
                    record.ClaimNote = oRow.Field<string>("ClaimNote");
                    record.DateEntryC = oRow.Field<DateTime>("DateEntryC");
                    record.ClinicNum = oRow.Field<long>("ClinicNum");
                    record.MedicalCode = oRow.Field<string>("MedicalCode");
                    record.DiagnosticCode = oRow.Field<string>("DiagnosticCode");
                    record.IsPrincDiag = oRow.Field<byte>("IsPrincDiag");
                    record.ProcNumLab = oRow.Field<long>("ProcNumLab");
                    record.BillingTypeOne = oRow.Field<long>("BillingTypeOne");
                    record.BillingTypeTwo = oRow.Field<long>("BillingTypeTwo");
                    record.CodeNum = oRow.Field<long>("CodeNum");
                    record.CodeMod1 = oRow.Field<string>("CodeMod1");
                    record.CodeMod2 = oRow.Field<string>("CodeMod2");
                    record.CodeMod3 = oRow.Field<string>("CodeMod3");
                    record.CodeMod4 = oRow.Field<string>("CodeMod4");
                    record.RevCode = oRow.Field<string>("RevCode");
                    record.UnitQty = oRow.Field<int>("UnitQty");
                    record.BaseUnits = oRow.Field<int>("BaseUnits");
                    record.StartTime = oRow.Field<int>("StartTime");
                    record.StopTime = oRow.Field<int>("StopTime");
                    record.DateTP = oRow.Field<DateTime>("DateTP");
                    record.SiteNum = oRow.Field<long>("SiteNum");
                    record.HideGraphics = Convert.ToInt16(oRow.Field<sbyte>("HideGraphics"));
                    record.CanadianTypeCodes = oRow.Field<string>("CanadianTypeCodes");
                    record.ProcTimeEnd = oRow.Field<TimeSpan>("ProcTimeEnd");
                    record.ProcTime = oRow.Field<TimeSpan>("ProcTime");
                    record.DateTStamp = oRow.Field<DateTime>("DateTStamp");
                    record.Prognosis = oRow.Field<long>("Prognosis");
                    record.DrugUnit = Convert.ToInt16(oRow.Field<sbyte>("DrugUnit"));
                    record.DrugQty = oRow.Field<float>("DrugQty");
                    record.Prognosis = oRow.Field<long>("Prognosis");
                    record.UnitQtyType = Convert.ToInt16(oRow.Field<sbyte>("UnitQtyType"));
                    record.StatementNum = oRow.Field<long>("StatementNum");
                    record.IsLocked = Convert.ToInt16(oRow.Field<sbyte>("IsLocked"));
                    record.BillingNote = oRow.Field<string>("BillingNote");
                    record.RepeatChargeNum = oRow.Field<long>("RepeatChargeNum");
                    record.SnomedBodySite = oRow.Field<string>("SnomedBodySite");
                    record.DiagnosticCode2 = oRow.Field<string>("DiagnosticCode2");
                    record.DiagnosticCode3 = oRow.Field<string>("DiagnosticCode3");
                    record.DiagnosticCode4 = oRow.Field<string>("DiagnosticCode4");
                    record.ProvOrderOverride = oRow.Field<long>("ProvOrderOverride");
                    record.Discount = oRow.Field<double>("Discount");
                    record.IsDateProsthEst = Convert.ToInt16(oRow.Field<sbyte>("IsDateProsthEst"));
                    record.IcdVersion = oRow.Field<byte>("IcdVersion");
                    record.IsCpoe = Convert.ToInt16(oRow.Field<sbyte>("IsCpoe"));

                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload procedurelog data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_procedurelog");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -2);
                }

            }

            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_procedurelog");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -3);
                }
            }

        }

        private void ProcessODAdjustments(string apptProcessDate, Office OfficeData)
        {
            DataTable data = new DataTable();

            //data lists
            List<od_adjustment> dataList = new List<od_adjustment>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // adjustments
            data = DentalData("SELECT * From adjustment " +
           "Where DATE(AdjDate) >= DATE('" + apptProcessDate + "')");


            foreach (DataRow oRow in data.Rows)
            {
                try
                {
                    od_adjustment record = new od_adjustment();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.AdjNum = oRow.Field<long>("AdjNum");
                    record.AdjDate = oRow.Field<DateTime>("AdjDate");
                    record.AdjAmt = oRow.Field<double>("AdjAmt");
                    record.PatNum = oRow.Field<long>("PatNum");
                    record.AdjType = oRow.Field<long>("AdjType");
                    record.ProvNum = oRow.Field<long>("ProvNum");
                    record.AdjNote = oRow.Field<string>("AdjNote");
                    record.ProcDate = oRow.Field<DateTime>("ProcDate");
                    record.ProcNum = oRow.Field<long>("ProcNum");
                    record.DateEntry = oRow.Field<DateTime>("DateEntry");
                    record.ClinicNum = oRow.Field<long>("ClinicNum");
                    record.StatementNum = oRow.Field<long>("StatementNum");

                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload adjustment data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_adjustment");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental 
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }

            }

            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_adjustment");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }
        }

        private void ProcessODClaimProcs(string apptProcessDate, Office OfficeData)
        {
            DataTable data = new DataTable();

            //data lists
            List<od_claimproc> dataList = new List<od_claimproc>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // claimprocs
            data = DentalData("SELECT * From claimproc " +
           "Where DATE(DateCP) >= DATE('" + apptProcessDate + "')");


            foreach (DataRow oRow in data.Rows)
            {
                try
                {
                    od_claimproc record = new od_claimproc();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.ClaimProcNum = oRow.Field<long>("ClaimProcNum");
                    record.ProcNum = oRow.Field<long>("ProcNum");
                    record.ClaimNum = oRow.Field<long>("ClaimNum");
                    record.PatNum = oRow.Field<long>("PatNum");
                    record.ProvNum = oRow.Field<long>("ProvNum");
                    record.FeeBilled = oRow.Field<double>("FeeBilled");
                    record.InsPayEst = oRow.Field<double>("InsPayEst");
                    record.DedApplied = oRow.Field<double>("DedApplied");
                    record.Status = oRow.Field<byte>("Status");
                    record.InsPayAmt = oRow.Field<double>("InsPayAmt");
                    record.Remarks = oRow.Field<string>("Remarks");
                    record.ClaimPaymentNum = oRow.Field<long>("ClaimPaymentNum");
                    record.PlanNum = oRow.Field<long>("PlanNum");
                    record.DateCP = oRow.Field<DateTime>("DateCP");
                    record.WriteOff = oRow.Field<double>("WriteOff");
                    record.CodeSent = oRow.Field<string>("CodeSent");
                    record.AllowedOverride = oRow.Field<double>("AllowedOverride");
                    record.Percentage = Convert.ToInt16(oRow.Field<sbyte>("Percentage"));
                    record.PercentOverride = Convert.ToInt16(oRow.Field<sbyte>("PercentOverride"));
                    record.CopayAmt = oRow.Field<double>("CopayAmt");
                    record.NoBillIns = oRow.Field<byte>("NoBillIns");
                    record.PaidOtherIns = oRow.Field<double>("PaidOtherIns");
                    record.BaseEst = oRow.Field<double>("BaseEst");
                    record.CopayOverride = oRow.Field<double>("CopayOverride");
                    record.ProcDate = oRow.Field<DateTime>("ProcDate");
                    record.DateEntry = oRow.Field<DateTime>("DateEntry");
                    record.LineNumber = oRow.Field<byte>("LineNumber");
                    record.DedEst = oRow.Field<double>("DedEst");
                    record.DedEstOverride = oRow.Field<double>("DedEstOverride");
                    record.InsEstTotal = oRow.Field<double>("InsEstTotal");
                    record.InsEstTotalOverride = oRow.Field<double>("InsEstTotalOverride");
                    record.PaidOtherInsOverride = oRow.Field<double>("PaidOtherInsOverride");
                    record.EstimateNote = oRow.Field<string>("EstimateNote");
                    record.WriteOffEst = oRow.Field<double>("WriteOffEst");
                    record.WriteOffEstOverride = oRow.Field<double>("WriteOffEstOverride");
                    record.ClinicNum = oRow.Field<long>("ClinicNum");
                    record.InsSubNum = oRow.Field<long>("InsSubNum");
                    record.PaymentRow = oRow.Field<int>("PaymentRow");
                    record.PayPlanNum = oRow.Field<long>("PayPlanNum");
                    record.ClaimPaymentTracking = oRow.Field<long>("ClaimPaymentTracking");

                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload claimproc data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_claimproc");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental 
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }


            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_claimproc");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }
        }

        private void ProcessODCommLogs(string apptProcessDate, Office OfficeData)
        {
            DataTable data = new DataTable();

            //data lists
            List<od_commlog> dataList = new List<od_commlog>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // commlogs
            data = DentalData("SELECT * From commlog " +
           "Where DATE(CommDateTime) >= DATE('" + apptProcessDate + "')");


            foreach (DataRow oRow in data.Rows)
            {
                try
                {

                    od_commlog record = new od_commlog();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.CommlogNum = oRow.Field<long>("CommlogNum");
                    record.PatNum = oRow.Field<long>("PatNum");
                    record.CommType = oRow.Field<long>("CommType");
                    record.CommDateTime = oRow.Field<DateTime>("CommDateTime");
                    record.Note = oRow.Field<string>("Note");
                    record.Mode_ = oRow.Field<byte>("Mode_");
                    record.SentOrReceived = oRow.Field<byte>("SentOrReceived");
                    record.UserNum = oRow.Field<long>("UserNum");
                    record.Signature = oRow.Field<string>("Signature");
                    record.SigIsTopaz = Convert.ToInt16(oRow.Field<sbyte>("SigIsTopaz"));
                    record.DateTStamp = oRow.Field<DateTime>("DateTStamp");
                    record.DateTimeEnd = oRow.Field<DateTime>("DateTimeEnd");
                    record.CommSource = oRow.Field<sbyte?>("CommSource");
                    record.ProgramNum = oRow.Field<long>("ProgramNum");

                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload commlog data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_commlog");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental 
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }

            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_commlog");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }
        }

        private void ProcessODInsPlans(string apptProcessDate, Office OfficeData)
        {
            DataTable data = new DataTable();

            //data lists
            List<od_insplan> dataList = new List<od_insplan>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // insplans
            data = DentalData("SELECT * From insplan");

            foreach (DataRow oRow in data.Rows)
            {
                try
                {
                    od_insplan record = new od_insplan();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.PlanNum = oRow.Field<long>("PlanNum");
                    record.GroupName = oRow.Field<string>("GroupName");
                    record.GroupNum = oRow.Field<string>("GroupNum");
                    record.PlanNote = oRow.Field<string>("PlanNote");
                    record.FeeSched = oRow.Field<long>("FeeSched");
                    record.PlanType = oRow.Field<string>("PlanType");
                    record.ClaimFormNum = oRow.Field<long>("ClaimFormNum");
                    record.UseAltCode = oRow.Field<byte>("UseAltCode");
                    record.ClaimsUseUCR = oRow.Field<byte>("ClaimsUseUCR");
                    record.CopayFeeSched = oRow.Field<long>("CopayFeeSched");
                    record.EmployerNum = oRow.Field<long>("EmployerNum");
                    record.CarrierNum = oRow.Field<long>("CarrierNum");
                    record.AllowedFeeSched = oRow.Field<long>("AllowedFeeSched");
                    record.TrojanID = oRow.Field<string>("TrojanID");
                    record.DivisionNo = oRow.Field<string>("DivisionNo");
                    record.IsMedical = oRow.Field<byte>("IsMedical");
                    record.FilingCode = oRow.Field<long>("FilingCode");
                    record.DentaideCardSequence = oRow.Field<byte>("DentaideCardSequence");
                    record.ShowBaseUnits = Convert.ToInt16(oRow.Field<bool>("ShowBaseUnits"));
                    record.CodeSubstNone = Convert.ToInt16(oRow.Field<bool>("CodeSubstNone"));
                    record.IsHidden = Convert.ToInt16(oRow.Field<sbyte>("IsHidden"));
                    record.MonthRenew = Convert.ToInt16(oRow.Field<sbyte>("MonthRenew"));
                    record.FilingCodeSubtype = oRow.Field<long>("FilingCodeSubtype");
                    record.CanadianPlanFlag = oRow.Field<string>("CanadianPlanFlag");
                    record.CanadianDiagnosticCode = oRow.Field<string>("CanadianDiagnosticCode");
                    record.CanadianInstitutionCode = oRow.Field<string>("CanadianInstitutionCode");
                    record.RxBIN = oRow.Field<string>("RxBIN");
                    record.CobRule = Convert.ToInt16(oRow.Field<sbyte>("CobRule"));
                    record.SopCode = oRow.Field<string>("SopCode");

                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload insplan data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_insplan");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental 
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }

            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_insplan");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }
        }

        private void ProcessODPatPlans(string apptProcessDate, Office OfficeData)
        {
            DataTable data = new DataTable();

            //data lists
            List<od_patplan> dataList = new List<od_patplan>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // patplans
            data = DentalData("SELECT * From patplan");


            foreach (DataRow oRow in data.Rows)
            {
                try
                {

                    od_patplan record = new od_patplan();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.PatPlanNum = oRow.Field<long>("PatPlanNum");
                    record.PatNum = oRow.Field<long>("PatNum");
                    record.Ordinal = oRow.Field<byte>("Ordinal");
                    record.IsPending = oRow.Field<byte>("IsPending");
                    record.Relationship = oRow.Field<byte>("Relationship");
                    record.PatID = oRow.Field<string>("PatID");
                    record.InsSubNum = oRow.Field<long>("InsSubNum");

                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload patplan data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_patplan");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental 
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }

            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_patplan");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }
        }

        private void ProcessODReferrals(string apptProcessDate, Office OfficeData)
        {
            DataTable data = new DataTable();

            //data lists
            List<od_referral> dataList = new List<od_referral>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // referrals
            data = DentalData("SELECT * From referral");

            foreach (DataRow oRow in data.Rows)
            {
                try
                {

                    od_referral record = new od_referral();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.ReferralNum = oRow.Field<long>("ReferralNum");
                    record.LName = oRow.Field<string>("LName");
                    record.FName = oRow.Field<string>("FName");
                    record.MName = oRow.Field<string>("MName");
                    record.PatNum = oRow.Field<long>("PatNum");
                    record.IsHidden = oRow.Field<byte>("IsHidden");
                    record.NotPerson = oRow.Field<byte>("NotPerson");
                    record.NationalProvID = oRow.Field<string>("NationalProvID");
                    record.Slip = oRow.Field<long>("Slip");
                    record.IsDoctor = Convert.ToInt16(oRow.Field<sbyte>("IsDoctor"));
                    record.IsTrustedDirect = Convert.ToInt16(oRow.Field<sbyte>("IsTrustedDirect"));
                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload referral data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_referral");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental 
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }

            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_referral");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }
        }

        private void ProcessODRefAttaches(string apptProcessDate, Office OfficeData)
        {
            DataTable data = new DataTable();

            //data lists
            List<od_refattach> dataList = new List<od_refattach>();

            //clear/null objects before filling each day
            data.Clear();
            data = null;

            // refattachs
            data = DentalData("SELECT * From refattach " +
          "Where DATE(RefDate) >= DATE('" + apptProcessDate + "')");

            foreach (DataRow oRow in data.Rows)
            {
                try
                {

                    od_refattach record = new od_refattach();
                    record.OfficeID = _appData.OfficeKeyGuid;
                    record.CreatedDate = DateTime.Now;
                    record.RefAttachNum = oRow.Field<long>("RefAttachNum");
                    record.ReferralNum = oRow.Field<long>("ReferralNum");
                    record.PatNum = oRow.Field<long>("PatNum");
                    record.ItemOrder = unchecked((short)oRow.Field<ushort>("ItemOrder"));
                    record.RefDate = oRow.Field<DateTime>("RefDate");
                    record.IsFrom = 0; //oRow.Field<byte>("IsFrom");
                    record.RefToStatus = oRow.Field<byte>("RefToStatus");
                    record.Note = oRow.Field<string>("Note");
                    record.IsTransitionOfCare = Convert.ToInt16(oRow.Field<sbyte>("IsTransitionOfCare"));
                    record.ProcNum = oRow.Field<long>("ProcNum");

                    DateTime dateProcCompleteOld = new DateTime(1753, 3, 1, 7, 0, 0);
                    DateTime dateProcComplete = oRow.Field<DateTime>("DateProcComplete");

                    if (dateProcCompleteOld > dateProcComplete)
                    {
                        record.DateProcComplete = dateProcCompleteOld;
                    }
                    else
                    {
                        record.DateProcComplete = oRow.Field<DateTime>("DateProcComplete");
                    }

                    record.ProvNum = oRow.Field<long>("ProvNum");

                    dataList.Add(record);

                    if (dataList.Count >= 100)
                    {
                        //upload refattach data
                        var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_refattach");
                            BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOpenDentalData(request);
                                bool result = response.BulkUploadOpenDentalDataResult;
                            }

                            json = null;
                            dataList.Clear();

                        }
                        catch (Exception ex)
                        {
                            //Retry?
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Catch for failure of setting values from Open Dental 
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }

            if (dataList.Count > 0)
            {
                var json = JsonConvert.SerializeObject(dataList, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    BulkUploadOpenDentalDataRequest request = new BulkUploadOpenDentalDataRequest(_appData.OfficeKey, json, "od_refattach");
                    BulkUploadOpenDentalDataResponse response = new BulkUploadOpenDentalDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.BulkUploadOpenDentalData(request);
                        bool result = response.BulkUploadOpenDentalDataResult;
                    }

                    json = null;
                    dataList.Clear();

                }
                catch (Exception ex)
                {
                    //Retry?
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                }
            }
        }


        internal void ProcessRawData(Office OfficeData)
        {
            string apptProcessDate = FormatExtractionDate(OfficeData.AppointmentProcessDate.Value);

            //adjustment
            //appointment
            //claimproc
            //commlog
            //insplan
            //patient
            //patplan
            //payment
            //procedurelog
            //provider

            ProcessODAppointments(apptProcessDate, OfficeData);
            ProcessODPatients(apptProcessDate, OfficeData);
            ProcessODProviders(apptProcessDate, OfficeData);
            ProcessODPayments(apptProcessDate, OfficeData);
            ProcessODPaySplits(apptProcessDate, OfficeData);
            ProcessODProcedureLogs(apptProcessDate, OfficeData);
            ProcessODAdjustments(apptProcessDate, OfficeData);
            ProcessODClaimProcs(apptProcessDate, OfficeData);
            ProcessODCommLogs(apptProcessDate, OfficeData);
            ProcessODInsPlans(apptProcessDate, OfficeData);
            ProcessODPatPlans(apptProcessDate, OfficeData);
            ProcessODReferrals(apptProcessDate, OfficeData);
            ProcessODRefAttaches(apptProcessDate, OfficeData);

            _appData.OfficeData.AppointmentFullUpload = false;
            _appData.OfficeData.AppointmentProcessDate = DateTime.Today;
            _appData.SetOfficeData();

        }

        private string GetNewPatientProcedureCode(List<long> PatientProcedures)
        {
            try
            {
                foreach (var proc in PatientProcedures)
                {
                    string adaCode = "";
                    string procCodeKey = proc.ToString();

                    if (aDACodes.ContainsKey(procCodeKey))
                    {
                        adaCode = aDACodes[procCodeKey];
                        if (commonData.NewProcCodes.Contains(adaCode))
                        {
                            return adaCode;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
            }

            return "Non-Exam";
        }

        internal void ProcessProvidersData(DateTime ProcessDate, List<Provider> Providers)
        {
            int errorLineCount = 0;

            DataTable completedProceduresData = new DataTable();
            DataTable completedAppointmentsData = new DataTable();
            DataTable adjustmentsData = new DataTable();
            DataTable patientPaymentsData = new DataTable();
            DataTable insurancePaymentsData = new DataTable();
            DataTable insuranceCompanies = new DataTable();
            DataTable patientsData = new DataTable();

            //processing lists
            List<long> patientsTreated = new List<long>();

            //data lists
            List<ProvidersProduction> providersProductions = new List<ProvidersProduction>();
            List<ProvidersAdjustment> providersAdjustments = new List<ProvidersAdjustment>();
            List<ProvidersPayment> providersPayments = new List<ProvidersPayment>();
            List<ProvidersInsPayment> providersInsPayments = new List<ProvidersInsPayment>();
            List<ProvidersProcedure> providersProcedures = new List<ProvidersProcedure>();
            List<OfficesPatientsTreated> officePatientsTreated = new List<OfficesPatientsTreated>();
            //List<ProvidersBrokenAppointment> providersBrokenAppointments = new List<ProvidersBrokenAppointment>();

            insuranceCompanies = DentalData("Select carrierNum, carrierName from carrier");

            ProcessDate = new DateTime(ProcessDate.Year, ProcessDate.Month, ProcessDate.Day);

            for (DateTime processingDate = ProcessDate; processingDate.Date <= DateTime.Today; processingDate = processingDate.AddDays(1))
            {
                try
                {
                    errorLineCount = 1;

                    string formattedProcessDate = FormatExtractionDate(processingDate);

                    errorLineCount = 111;

                    //clear/null objects before filling each day
                    if (completedProceduresData != null)
                    {
                        completedProceduresData.Clear();
                    }
                    completedProceduresData = null;

                    errorLineCount = 112;

                    if (completedAppointmentsData != null)
                    {
                        completedAppointmentsData.Clear();
                    }
                    completedAppointmentsData = null;

                    errorLineCount = 113;

                    if (adjustmentsData != null)
                    {
                        adjustmentsData.Clear();
                    }
                    adjustmentsData = null;

                    errorLineCount = 114;

                    if (patientPaymentsData != null)
                    {
                        patientPaymentsData.Clear();
                    }
                    patientPaymentsData = null;

                    errorLineCount = 115;

                    if (insurancePaymentsData != null)
                    {
                        insurancePaymentsData.Clear();
                    }
                    insurancePaymentsData = null;

                    errorLineCount = 116;

                    if (patientsData != null)
                    {
                        patientsData.Clear();
                    }
                    patientsData = null;

                    errorLineCount = 117;

                    // procedurelog table
                    completedProceduresData = DentalData("Select ProvNum, CodeNum, PatNum, ProcFee, ProcStatus, ProcDate, ProcTime, ProcTimeEnd From procedurelog Where ProcStatus = 2 And DATE(ProcDate) = DATE('" + formattedProcessDate + "')");

                    errorLineCount = 2;

                    // appointment table
                    completedAppointmentsData = DentalData("Select a.ProvHyg As ProvHyg, a.IsHygiene As IsHygiene, a.ProvNum as ProvNum, a.PatNum as PatNum, " +
                        "a.AptDateTime as AptDateTime, a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed, CHAR_LENGTH(a.Pattern)*5  AS 'AptTime' " +
                        "From appointment a Where DATE(AptDateTime) = DATE('" + formattedProcessDate + "') And (" + _completedApptStatus + ")");

                    errorLineCount = 3;

                    // adjustment table
                    adjustmentsData = DentalData("Select AdjDate,AdjNum, ProvNum,AdjAmt, AdjType From adjustment Where DATE(AdjDate) = DATE('" + formattedProcessDate + "')");

                    errorLineCount = 4;

                    // paysplit table
                    patientPaymentsData = DentalData("SELECT p.PayType AS 'PayType', ps.SplitAmt AS 'SplitAmt', ps.ProvNum as 'ProvNum' FROM payment p INNER JOIN paysplit ps ON ps.PayNum = p.PayNum Where p.PayDate = {d'" + formattedProcessDate + "'}");

                    errorLineCount = 5;

                    insurancePaymentsData = DentalData("Select ProvNum, InsPayAmt, PatNum, WriteOff, Status from claimproc Where DATE(DateCP) = DATE('" + formattedProcessDate + "')"); // AND Status in (0,1,4)");

                    errorLineCount = 6;

                    if (_appData.OfficeData.AnalyzeData == true)
                    {
                        analyzeData.AnalyzeDataTable("completedAppointmentsData", completedAppointmentsData);
                    }

                    //process office data
                    decimal officeProduction = (from p in completedProceduresData.AsEnumerable()
                                                where p.Field<byte>("ProcStatus") == 2
                                                select (decimal)p.Field<double>("ProcFee")).Sum();
                    errorLineCount = 7;

                    patientsTreated = (from p in completedProceduresData.AsEnumerable()
                                       where p.Field<byte>("ProcStatus") == 2 && !_procsToIgnoreList.Contains(p.Field<long>("CodeNum"))
                                       select p.Field<long>("PatNum")).Distinct().ToList();

                    errorLineCount = 8;

                    if (patientsTreated.Count > 0)
                    {
                        SPDiagnostics spd = new SPDiagnostics();

                        try
                        {
                            spd.PatGuid = "";
                            spd.pba = null;

                            string patIds = string.Join<long>(",", patientsTreated);

                            spd.DiagData = patIds;

                            patientsData = DentalData("Select PatNum, PatPlanNum From patplan Where PatNum In (" + patIds + ")");
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, spd);
                            }
                            catch { }
                        }
                        finally
                        {
                            if (spd != null)
                            {
                                spd = null;
                            }
                        }
                    }

                    errorLineCount = 81;

                    foreach (var pat in patientsTreated)
                    {
                        errorLineCount = 9;

                        OfficesPatientsTreated officesPatTreated = new OfficesPatientsTreated();
                        officesPatTreated.OfficeID = _appData.OfficeKeyGuid;
                        officesPatTreated.PatientID = (int)pat;
                        officesPatTreated.CreatedDate = DateTime.Now;
                        officesPatTreated.ModifiedDate = DateTime.Now;
                        officesPatTreated.TreatedDate = processingDate;
                        officesPatTreated.ProductionAmount = (from p in completedProceduresData.AsEnumerable()
                                                              where p.Field<byte>("ProcStatus") == 2 && p.Field<long>("PatNum") == pat
                                                              select (decimal)p.Field<double>("ProcFee")).Sum();
                        errorLineCount = 10;

                        if (officesPatTreated.ProductionAmount > 0)
                        {
                            officesPatTreated.PercentOfGrossProduction = Math.Round((officesPatTreated.ProductionAmount / officeProduction), 2);
                        }
                        else
                        {
                            officesPatTreated.PercentOfGrossProduction = 0;
                        }

                        errorLineCount = 101;

                        long primInsuredId = (from p in patientsData.AsEnumerable()
                                              where p.Field<long>("PatNum") == pat
                                              select p.Field<long>("PatPlanNum")).FirstOrDefault();

                        officesPatTreated.BillingTypeDescription = GetPatientPaymentType((int)pat, insuranceCompanies);

                        errorLineCount = 11;

                        if (IsNewPatient((int)pat, formattedProcessDate))
                        {
                            errorLineCount = 12;

                            officesPatTreated.NewPatient = true;
                            officesPatTreated.ReferralSource = GetReferralType((int)pat);
                            officesPatTreated.ExternalReferral = IsReferralExternal((int)pat);
                            List<long> newPatProcs = (from p in completedProceduresData.AsEnumerable()
                                                      where p.Field<long>("PatNum") == pat
                                                      select p.Field<long>("CodeNum")).ToList();
                            errorLineCount = 13;

                            officesPatTreated.NewPatProcCode = GetNewPatientProcedureCode(newPatProcs);
                        }

                        //check for reappoint
                        //DataTable reappoint = DentalData("Select Top 1 apptdate From admin.appt Where patid = " + pat + " And apptdate > {d'" + formattedProcessDate + "'}");
                        DataTable reappoint = DentalData("Select AptDateTime From appointment Where PatNum = " + pat + " And DATE(AptDateTime) > DATE('" + formattedProcessDate + "') ORDER BY AptDateTime DESC LIMIT 1");
                        errorLineCount = 14;

                        if (reappoint.Rows.Count > 0)
                        {
                            officesPatTreated.ReAppointed = true;
                        }
                        else
                        {
                            officesPatTreated.ReAppointed = false;
                        }

                        officePatientsTreated.Add(officesPatTreated);

                        //OfficesPatientsTreated
                        if (officePatientsTreated.Count >= 100)
                        {
                            var json = JsonConvert.SerializeObject(officePatientsTreated, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                BulkUploadOfficesPatientsTreatedsDataRequest request = new BulkUploadOfficesPatientsTreatedsDataRequest(_appData.OfficeKey, json);
                                BulkUploadOfficesPatientsTreatedsDataResponse response = new BulkUploadOfficesPatientsTreatedsDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.BulkUploadOfficesPatientsTreatedsData(request);
                                    bool result = response.BulkUploadOfficesPatientsTreatedsDataResult;
                                }

                                json = null;
                                officePatientsTreated.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                                }
                                catch { }
                            }
                        }

                    }

                    //OfficesPatientsTreated
                    if (officePatientsTreated.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(officePatientsTreated, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOfficesPatientsTreatedsDataRequest request = new BulkUploadOfficesPatientsTreatedsDataRequest(_appData.OfficeKey, json);
                            BulkUploadOfficesPatientsTreatedsDataResponse response = new BulkUploadOfficesPatientsTreatedsDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOfficesPatientsTreatedsData(request);
                                bool result = response.BulkUploadOfficesPatientsTreatedsDataResult;
                            }

                            json = null;
                            officePatientsTreated.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -2);
                            }
                            catch { }
                        }
                    }

                    //process for each provider
                    foreach (Provider prov in Providers)
                    {
                        errorLineCount = 15;
                        decimal providerWriteoffs = 0;

                        //check to see if provider had any production
                        if (completedProceduresData.Select().Where(p => p.Field<long>("ProvNum").ToString() == prov.PMID).Any())
                        {
                            //set lwday to most recent date, unless that date is today
                            if (processingDate != DateTime.Today)
                            {
                                //_appData.OfficeData.LWDay = processingDate;
                                if (processingDate >= _appData.OfficeData.LWDay)
                                {
                                    _appData.OfficeData.LWDay = processingDate;
                                }

                            }

                            ProvidersProduction provProd = new ProvidersProduction();
                            provProd.ProviderID = prov.ID;
                            provProd.CreatedDate = DateTime.Now;
                            provProd.ModifiedDate = DateTime.Now;
                            provProd.ProductionDate = processingDate;

                            errorLineCount = 16;

                            //PROCEDURES
                            decimal providerProduction = (from p in completedProceduresData.AsEnumerable()
                                                          where p.Field<byte>("ProcStatus") == 2 && p.Field<long>("ProvNum").ToString() == prov.PMID
                                                          select (decimal)p.Field<double>("ProcFee")).Sum();

                            var groupedProcs = (from p in completedProceduresData.AsEnumerable()
                                                where p.Field<byte>("ProcStatus") == 2 && p.Field<long>("ProvNum").ToString() == prov.PMID
                                                group p by p.Field<long>("CodeNum") into g
                                                select g);

                            providerWriteoffs = 0;
                            providerWriteoffs = (from p in insurancePaymentsData.AsEnumerable()
                                                 where (p.Field<byte>("Status") == 0 || p.Field<byte>("Status") == 1 || p.Field<byte>("Status") == 4)
                                                  && p.Field<long>("ProvNum").ToString() == prov.PMID
                                                  && p.Field<double>("WriteOff") > 0
                                                 select (decimal)p.Field<double>("WriteOff")).Sum();

                            errorLineCount = 17;

                            foreach (var grp in groupedProcs)
                            {
                                ProvidersProcedure provProc = new ProvidersProcedure();
                                provProc.ProviderID = prov.ID;
                                provProc.CreatedDate = DateTime.Now;
                                provProc.ModifiedDate = DateTime.Now;
                                provProc.ProcedureDate = processingDate;
                                provProc.Amount = (from p in grp
                                                   select (decimal)p.Field<double>("ProcFee")).Sum();

                                provProc.TotalCount = grp.Count();

                                errorLineCount = 18;

                                string adaCode = "";
                                string procCodeKey = (from p in grp
                                                      select p.Field<long>("CodeNum")).FirstOrDefault().ToString();

                                if (aDACodes.ContainsKey(procCodeKey))
                                {
                                    adaCode = aDACodes[procCodeKey];
                                }
                                provProc.ProcedureCode = adaCode;

                                providersProcedures.Add(provProc);
                            }

                            errorLineCount = 19;

                            decimal providerProductionPercent = 0;
                            if (providerProduction > 0)
                            {
                                providerProductionPercent = Math.Round((providerProduction / officeProduction), 2);
                            }

                            // Hours worked - from OD procedures
                            decimal hoursWorked = 0;
                            List<int> treatedPatients = new List<int>();
                            treatedPatients.Clear();

                            errorLineCount = 20;

                            foreach (DataRow apptRow in completedAppointmentsData.Rows)
                            {
                                int patNum = (int)apptRow.Field<long>("PatNum");

                                //get appointed provider
                                string appointedProv = "";
                                if (apptRow.Field<byte>("IsHygiene") == 1)
                                {
                                    appointedProv = apptRow.Field<long>("ProvHyg").ToString();
                                }
                                else
                                {
                                    appointedProv = apptRow.Field<long>("ProvNum").ToString();
                                }

                                if (appointedProv == prov.PMID)
                                {
                                    if (!treatedPatients.Contains(patNum))
                                    {
                                        treatedPatients.Add(patNum);
                                    }
                                }
                            }

                            errorLineCount = 208;
                            provProd.TreatedPatients = treatedPatients.Count();

                            errorLineCount = 21;

                            if (_appData.OfficeData.AnalyzeData == true)
                            {
                                AnalyzeTreatedPatientsData atpd = new AnalyzeTreatedPatientsData();
                                atpd.Provider = prov.PMID;
                                atpd.TreatedDate = processingDate;
                                atpd.TreatedPatients = treatedPatients;

                                analyzeData.AnalyzeTreatedPatients("TreatedPatients", atpd);
                            }

                            errorLineCount = 22;

                            //check to see if treated patients were reappointed
                            int nonAppointed = 0;
                            int reAppointed = 0;

                            foreach (int pat in treatedPatients)
                            {
                                DataTable patientsReAppointed = new DataTable();

                                // TODO add check for aptStatus - must be 1
                                patientsReAppointed = DentalData("Select PatNum, AptDateTime From appointment Where PatNum = " + pat + " And DATE(AptDateTime) > DATE('" + formattedProcessDate + "') ORDER BY AptDateTime DESC LIMIT 1");

                                if (patientsReAppointed.Rows.Count > 0)
                                {
                                    reAppointed++;
                                }
                                else
                                {
                                    nonAppointed++;
                                }

                                patientsReAppointed.Clear();
                                patientsReAppointed = null;
                            }

                            provProd.ReAppointedPatients = reAppointed;
                            provProd.NonAppointedPatients = nonAppointed;

                            errorLineCount = 23;

                            //cancelled appointments
                            provProd.CancelledPatients = 0;

                            //individual provider done, do any cleanup & set any remaining properties
                            provProd.Amount = providerProduction;
                            provProd.WriteOffAmount = providerWriteoffs;
                            provProd.PercentOfProduction = providerProductionPercent;

                            provProd.HoursWorked = hoursWorked;

                            if (providerProduction > 0)
                            {
                                if (hoursWorked > 0)
                                {
                                    provProd.HourlyAmount = providerProduction / hoursWorked;
                                }
                                else
                                {
                                    provProd.HourlyAmount = 0;
                                }
                            }
                            else
                            {
                                provProd.HourlyAmount = 0;
                            }

                            providersProductions.Add(provProd);
                        }

                        errorLineCount = 24;

                        //process all adjustments here
                        var groupedAdjs = (from a in adjustmentsData.AsEnumerable()
                                           where a.Field<long>("ProvNum").ToString() == prov.PMID
                                           group a by a.Field<long>("AdjType") into g
                                           select g).ToList();

                        foreach (var grp in groupedAdjs)
                        {
                            var amt = (from a in grp
                                       select (decimal)a.Field<double>("AdjAmt")).Sum();

                            var cnt = (from a in grp
                                       select a).Count();

                            var descId = (from a in grp
                                          select a.Field<long>("AdjType")).FirstOrDefault();

                            ProvidersAdjustment provAdj = new ProvidersAdjustment();
                            provAdj.CreatedDate = DateTime.Now;
                            provAdj.ModifiedDate = DateTime.Now;
                            provAdj.ProviderID = prov.ID;
                            provAdj.AdjustmentDate = processingDate;
                            provAdj.Amount = amt;
                            provAdj.TotalCount = cnt;
                            provAdj.Description = GetAdjustmentType(descId.ToString());

                            providersAdjustments.Add(provAdj);
                        }

                        if (providerWriteoffs > 0)
                        {
                            ProvidersAdjustment provAdj = new ProvidersAdjustment();
                            provAdj.CreatedDate = DateTime.Now;
                            provAdj.ModifiedDate = DateTime.Now;
                            provAdj.ProviderID = prov.ID;
                            provAdj.AdjustmentDate = processingDate;
                            provAdj.Amount = (providerWriteoffs * -1);
                            provAdj.TotalCount = 1;
                            provAdj.Description = "Prod Writeoff - " + prov.DisplayName;

                            providersAdjustments.Add(provAdj);
                        }

                        errorLineCount = 25;

                        //process all payments here
                        var groupedPayments = (from a in patientPaymentsData.AsEnumerable()
                                               where a.Field<long>("ProvNum").ToString() == prov.PMID
                                               group a by a.Field<long>("PayType") into g
                                               select g).ToList();

                        foreach (var grp in groupedPayments)
                        {
                            var amt = (from a in grp
                                       select (decimal)a.Field<double>("SplitAmt")).Sum();

                            var cnt = (from a in grp
                                       select a).Count();

                            var descId = (from a in grp
                                          select a.Field<long>("PayType")).FirstOrDefault();

                            ProvidersPayment provPay = new ProvidersPayment();
                            provPay.CreatedDate = DateTime.Now;
                            provPay.ModifiedDate = DateTime.Now;
                            provPay.ProviderID = prov.ID;
                            provPay.PaymentDate = processingDate;
                            provPay.Amount = amt; // ConvertToPositive(amt);
                            provPay.TotalCount = cnt;
                            provPay.Description = GetPaymentType(descId.ToString());

                            providersPayments.Add(provPay);
                        }

                        errorLineCount = 26;

                        List<ProvidersInsPayment> ungroupedInsPayments = new List<ProvidersInsPayment>();

                        foreach (DataRow row in insurancePaymentsData.Rows)
                        {
                            if (row.Field<long>("ProvNum").ToString() == prov.PMID && (row.Field<byte>("Status") == 1 || row.Field<byte>("Status") == 4))
                            {
                                ProvidersInsPayment provInsPay = new ProvidersInsPayment();
                                provInsPay.CreatedDate = DateTime.Now;
                                provInsPay.ModifiedDate = DateTime.Now;
                                provInsPay.PaymentDate = processingDate;
                                provInsPay.ProviderID = prov.ID;
                                provInsPay.Amount = (decimal)row.Field<double>("InsPayAmt");
                                provInsPay.TotalCount = 1;
                                provInsPay.Description = GetInsurancePaymentCarrier((int)row.Field<long>("PatNum"), insuranceCompanies);

                                ungroupedInsPayments.Add(provInsPay);
                            }
                        }

                        var groupedInsPayments = (from p in ungroupedInsPayments
                                                  group p by p.Description into g
                                                  select g).ToList();

                        foreach (var grp in groupedInsPayments)
                        {
                            var amt = (from a in grp
                                       select a.Amount).Sum();

                            var cnt = (from a in grp
                                       select a).Count();

                            var descId = (from a in grp
                                          select a.Description).FirstOrDefault();

                            ProvidersInsPayment provInsPay = new ProvidersInsPayment();
                            provInsPay.CreatedDate = DateTime.Now;
                            provInsPay.ModifiedDate = DateTime.Now;
                            provInsPay.PaymentDate = processingDate;
                            provInsPay.ProviderID = prov.ID;
                            provInsPay.Amount = ConvertToPositive(amt);
                            provInsPay.TotalCount = cnt;
                            provInsPay.Description = descId;

                            providersInsPayments.Add(provInsPay);
                        }

                        //upload data if counts over 100
                        //providers procedures
                        if (providersProductions.Count >= 100)
                        {
                            var json = JsonConvert.SerializeObject(providersProductions, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                BulkUploadProvidersProductionsDataRequest request = new BulkUploadProvidersProductionsDataRequest(_appData.OfficeKey, json);
                                BulkUploadProvidersProductionsDataResponse response = new BulkUploadProvidersProductionsDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.BulkUploadProvidersProductionsData(request);
                                    bool result = response.BulkUploadProvidersProductionsDataResult;
                                }

                                json = null;
                                providersProductions.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -3);
                                }
                                catch { }
                            }
                        }
                        //providers adjustments
                        if (providersAdjustments.Count >= 100)
                        {
                            var json = JsonConvert.SerializeObject(providersAdjustments, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                BulkUploadProvidersAdjustmentsDataRequest request = new BulkUploadProvidersAdjustmentsDataRequest(_appData.OfficeKey, json);
                                BulkUploadProvidersAdjustmentsDataResponse response = new BulkUploadProvidersAdjustmentsDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.BulkUploadProvidersAdjustmentsData(request);
                                    bool result = response.BulkUploadProvidersAdjustmentsDataResult;
                                }

                                json = null;
                                providersAdjustments.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -4);
                                }
                                catch { }
                            }
                        }
                        //providers payments
                        if (providersPayments.Count >= 100)
                        {
                            var json = JsonConvert.SerializeObject(providersPayments, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                BulkUploadProvidersPaymentsDataRequest request = new BulkUploadProvidersPaymentsDataRequest(_appData.OfficeKey, json);
                                BulkUploadProvidersPaymentsDataResponse response = new BulkUploadProvidersPaymentsDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.BulkUploadProvidersPaymentsData(request);
                                    bool result = response.BulkUploadProvidersPaymentsDataResult;
                                }

                                json = null;
                                providersPayments.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -5);
                                }
                                catch { }
                            }
                        }
                        //providers insurance payments
                        if (providersInsPayments.Count >= 100)
                        {
                            var json = JsonConvert.SerializeObject(providersInsPayments, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                BulkUploadProvidersInsPaymentsDataRequest request = new BulkUploadProvidersInsPaymentsDataRequest(_appData.OfficeKey, json);
                                BulkUploadProvidersInsPaymentsDataResponse response = new BulkUploadProvidersInsPaymentsDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.BulkUploadProvidersInsPaymentsData(request);
                                    bool result = response.BulkUploadProvidersInsPaymentsDataResult;
                                }

                                json = null;
                                providersInsPayments.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -6);
                                }
                                catch { }
                            }
                        }
                        //providers procedures
                        if (providersProcedures.Count >= 100)
                        {
                            var json = JsonConvert.SerializeObject(providersProcedures, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                BulkUploadProvidersProceduresDataRequest request = new BulkUploadProvidersProceduresDataRequest(_appData.OfficeKey, json);
                                BulkUploadProvidersProceduresDataResponse response = new BulkUploadProvidersProceduresDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.BulkUploadProvidersProceduresData(request);
                                    bool result = response.BulkUploadProvidersProceduresDataResult;
                                }

                                json = null;
                                providersProcedures.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -7);
                                }
                                catch { }
                            }
                        }

                    }

                    //upload any remaining data
                    //providers procedures
                    if (providersProductions.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersProductions, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersProductionsDataRequest request = new BulkUploadProvidersProductionsDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersProductionsDataResponse response = new BulkUploadProvidersProductionsDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersProductionsData(request);
                                bool result = response.BulkUploadProvidersProductionsDataResult;
                            }

                            json = null;
                            providersProductions.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -8);
                            }
                            catch { }
                        }
                    }
                    //providers adjustments
                    if (providersAdjustments.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersAdjustments, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersAdjustmentsDataRequest request = new BulkUploadProvidersAdjustmentsDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersAdjustmentsDataResponse response = new BulkUploadProvidersAdjustmentsDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersAdjustmentsData(request);
                                bool result = response.BulkUploadProvidersAdjustmentsDataResult;
                            }


                            json = null;
                            providersAdjustments.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -9);
                            }
                            catch { }
                        }
                    }
                    //providers payments
                    if (providersPayments.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersPayments, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersPaymentsDataRequest request = new BulkUploadProvidersPaymentsDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersPaymentsDataResponse response = new BulkUploadProvidersPaymentsDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersPaymentsData(request);
                                bool result = response.BulkUploadProvidersPaymentsDataResult;
                            }


                            json = null;
                            providersPayments.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -10);
                            }
                            catch { }
                        }
                    }
                    //providers insurance payments
                    if (providersInsPayments.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersInsPayments, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersInsPaymentsDataRequest request = new BulkUploadProvidersInsPaymentsDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersInsPaymentsDataResponse response = new BulkUploadProvidersInsPaymentsDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersInsPaymentsData(request);
                                bool result = response.BulkUploadProvidersInsPaymentsDataResult;
                            }


                            json = null;
                            providersInsPayments.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -11);
                            }
                            catch { }
                        }
                    }
                    //providers procedures
                    if (providersProcedures.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersProcedures, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersProceduresDataRequest request = new BulkUploadProvidersProceduresDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersProceduresDataResponse response = new BulkUploadProvidersProceduresDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersProceduresData(request);
                                bool result = response.BulkUploadProvidersProceduresDataResult;
                            }


                            json = null;
                            providersProcedures.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -12);
                            }
                            catch { }
                        }
                    }

                    _appData.OfficeData.ProcessDate = processingDate;
                    _appData.SetOfficeData();

                }
                catch (Exception ex)
                {
                    try
                    {
                        errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                    }
                    catch { }
                }
            }
        }

        internal void ProcessUnscheduledAppointmentsData(DateTime UnscheduledProcessDate, List<Provider> Providers)
        {
            if (_processBrokenAppointments == false)
            {
                return;
            }

            int errorLineCount = 0;

            DataTable completedAppointmentsData = new DataTable();

            DataTable brokenAppointments = new DataTable();
            DataTable brokenProcedures = new DataTable();

            //data lists
            List<ProvidersBrokenAppointment> providersBrokenAppointments = new List<ProvidersBrokenAppointment>();

            UnscheduledProcessDate = UnscheduledProcessDate.AddDays(-2);

            errorLineCount = 1;

            for (DateTime processingDate = UnscheduledProcessDate; processingDate.Date <= DateTime.Today; processingDate = processingDate.AddDays(1))
            {
                try
                {
                    string formattedProcessDate = FormatExtractionDate(processingDate);

                    //clear/null objects before filling each day
                    completedAppointmentsData.Clear();
                    completedAppointmentsData = null;
                    brokenAppointments.Clear();
                    brokenAppointments = null;

                    completedAppointmentsData = DentalData("Select " +
                         "a.PatNum as PatNum, a.ProvNum as ProvNum, a.AptDateTime as AptDateTime, a.DateTStamp as DateTStamp, a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed, CHAR_LENGTH(a.Pattern)*5  AS 'AptTime', " +
                         "CONCAT(p.LName, ', ', p.FName)  AS 'PatName' " +
                         "From appointment a " +
                         "INNER JOIN patient p ON p.PatNum = a.PatNum " +
                         "Where DATE(a.AptDateTime) = DATE('" + formattedProcessDate + "') And (" + _completedApptStatus + ")");

                    errorLineCount = 2;

                    if (processingDate == DateTime.Today)
                    {
                        brokenAppointments = DentalData("Select " +
                          "a.AptDateTime as AptDateTime, a.DateTStamp as DateTStamp, a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed, a.ProvNum as ProvNum, a.PatNum as PatNum, a.AptNum as AptNum, a.AptStatus as AptStatus," +
                          "CONCAT(p.LName, ', ', p.FName)  AS 'PatName' " +
                          "From appointment a " +
                          "INNER JOIN patient p ON p.PatNum = a.PatNum " +
                          "Where a.AptStatus in (3,5) AND  DATE(a.DateTStamp) >= DATE('" + formattedProcessDate + "') " +
                          "Group BY PatNum");
                    }
                    else
                    {
                        brokenAppointments = DentalData("Select " +
                          "a.AptDateTime as AptDateTime, a.DateTStamp as DateTStamp, a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed, a.ProvNum as ProvNum, a.PatNum as PatNum, a.AptNum as AptNum, a.AptStatus as AptStatus," +
                          "CONCAT(p.LName, ', ', p.FName)  AS 'PatName' " +
                          "From appointment a " +
                          "INNER JOIN patient p ON p.PatNum = a.PatNum " +
                          "Where a.AptStatus in (3,5) AND  DATE(a.DateTStamp) = DATE('" + formattedProcessDate + "') " +
                          "Group BY PatNum");
                    }

                    errorLineCount = 3;

                    //process for each provider
                    foreach (Provider prov in Providers)
                    {
                        if (completedAppointmentsData.Select().Where(p => p.Field<long>("ProvNum").ToString() == prov.PMID).Any())
                        {
                            foreach (DataRow row in completedAppointmentsData.Rows)
                            {
                                errorLineCount = 4;

                                if (row.Field<long>("ProvNum").ToString() == prov.PMID)
                                {
                                    bool processPatient = false;

                                    if (processingDate == DateTime.Today)
                                    {
                                        var timeNow = DateTime.Now.TimeOfDay;

                                        //check appointment time and allow for 1 upload grace period
                                        DateTime apptDateTime = row.Field<DateTime>("AptDateTime"); //DateTStamp

                                        int apptHour = apptDateTime.Hour;
                                        int apptMin = apptDateTime.Minute;

                                        int apptLen = (int)row.Field<long>("AptTime");

                                        TimeSpan apptFinishTime = new TimeSpan(apptHour, (apptMin + apptLen), 0);
                                        TimeSpan currentTime = DateTime.Now.TimeOfDay;
                                        var elapsedTime = currentTime - apptFinishTime;

                                        if (row.Field<DateTime>("DateTimeArrived") != null && row.Field<DateTime>("DateTimeArrived") != null)
                                        {
                                            DateTime timeArrived = row.Field<DateTime>("DateTimeArrived");
                                            DateTime timeDismissed = row.Field<DateTime>("DateTimeDismissed");
                                            if ((timeDismissed - timeArrived).TotalMinutes > 0)
                                            {
                                                elapsedTime = (TimeSpan)(timeDismissed - timeArrived);
                                            }
                                        }

                                        errorLineCount = 6;

                                        if (elapsedTime.Hours == 0)
                                        {
                                            if (elapsedTime.Minutes > 15)
                                            {
                                                processPatient = true;
                                            }
                                        }
                                        else
                                        {
                                            processPatient = true;
                                        }
                                    }
                                    else
                                    {
                                        processPatient = true;
                                    }

                                    if (processPatient == true)
                                    {
                                        int patID = (int)row.Field<long>("PatNum");
                                        string patName = row.Field<string>("PatName").Trim();

                                        DataTable patReAppointed = new DataTable();
                                        patReAppointed.Clear();
                                        patReAppointed = null;
                                        patReAppointed = new DataTable();

                                        // TODO Add AptStatus to query
                                        patReAppointed = DentalData("Select PatNum, AptDateTime From appointment Where PatNum = " + patID + " And DATE(AptDateTime) > DATE('" + formattedProcessDate + "') ORDER BY AptDateTime DESC LIMIT 1");

                                        //no future appointment if count = 0
                                        if (patReAppointed.Rows.Count == 0)
                                        {
                                            //now check to see if they have a future BROKEN appt
                                            DataTable patFutureBrokenAppt = new DataTable();
                                            patFutureBrokenAppt.Clear();
                                            patFutureBrokenAppt = null;
                                            patFutureBrokenAppt = new DataTable();

                                            patFutureBrokenAppt = DentalData("Select AptDateTime, PatNum From appointment Where AptStatus in (3,5) AND DATE(AptDateTime) > DATE('" + formattedProcessDate + "') And PatNum = " + patID + " ORDER BY AptDateTime DESC LIMIT 1");

                                            errorLineCount = 8;

                                            if (patFutureBrokenAppt.Rows.Count == 0)
                                            {

                                                errorLineCount = 9;


                                                //check if we already have patient
                                                //if not, create a new record for tracking
                                                if (!walkoutPatientIDs.Contains(patID))
                                                {
                                                    ProvidersBrokenAppointment pba = new ProvidersBrokenAppointment();
                                                    pba.CreatedDate = DateTime.Now;
                                                    pba.PMApptID = -1;
                                                    pba.PMPatientID = patID;
                                                    pba.ProviderID = prov.ID;
                                                    pba.BrokenDate = processingDate;
                                                    pba.BrokenAmount = 0;
                                                    pba.Completed = false;
                                                    pba.DeletedAppointment = false;
                                                    pba.Walkout = true;
                                                    pba.PinBoarded = false;
                                                    pba.AppointmentStatus = -1;
                                                    pba.PatientName = patName;

                                                    DataTable patient = DentalData("Select FName, LName, PatStatus, PatNum From patient Where PatNum = " + patID);

                                                    string patGuid = "";

                                                    if (patID != 0)
                                                    {
                                                        if (patient.Rows.Count == 1)
                                                        {
                                                            patGuid = patient.Rows[0].Field<long>("PatNum").ToString();

                                                            var patStatus = patient.Rows[0].Field<byte>("PatStatus");

                                                            // Map to Dentrix values
                                                            if (patStatus < 4)
                                                            {
                                                                patStatus++;
                                                            }
                                                            pba.PatientStatus = patStatus;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        patGuid = "";

                                                        pba.PatientStatus = 0;
                                                    }

                                                    errorLineCount = 10;

                                                    //check for contact info
                                                    PatientLastContact(patID, pba);

                                                    if (pba.BrokenAmount != null)
                                                    {
                                                        providersBrokenAppointments.Add(pba);
                                                        walkoutPatientIDs.Add(patID);
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //check for cancelled appointments
                        if (brokenAppointments.Select().Where(p => p.Field<long>("ProvNum").ToString() == prov.PMID).Any())
                        {
                            foreach (DataRow row in brokenAppointments.Rows)
                            {
                                if (row.Field<long>("ProvNum").ToString() == prov.PMID)
                                {
                                    errorLineCount = 11;

                                    int patID = (int)row.Field<long>("PatNum");
                                    int apptID = (int)row.Field<long>("AptNum");
                                    string brokenDate = FormatExtractionDate(row.Field<DateTime>("DateTStamp"));
                                    string patName = row.Field<string>("PatName").Trim();

                                    //check if we already have appointment
                                    //if not, create a new record for tracking
                                    if (!existingBrokenApptIDs.Contains(apptID))
                                    {

                                        brokenProcedures = DentalData("Select SUM(ProcFee) AS 'SumProcFee' From procedurelog Where AptNum = " + apptID);

                                        errorLineCount = 12;

                                        ProvidersBrokenAppointment pba = new ProvidersBrokenAppointment();
                                        pba.CreatedDate = DateTime.Now;
                                        pba.PMApptID = apptID;
                                        pba.PMPatientID = patID;
                                        pba.ProviderID = prov.ID;
                                        pba.BrokenDate = processingDate;

                                        errorLineCount = 1201;
                                        double? sumProcFee = (from p in brokenProcedures.AsEnumerable()
                                                              select p.Field<double?>("SumProcFee")).DefaultIfEmpty(0).FirstOrDefault();
                                        errorLineCount = 1202;

                                        pba.BrokenAmount = (decimal?)sumProcFee;

                                        errorLineCount = 1203;
                                        pba.Completed = false;
                                        pba.DeletedAppointment = false;
                                        pba.Walkout = false;
                                        if (row.Field<byte?>("AptStatus") != null)
                                        {
                                            pba.AppointmentStatus = convertData.ConvertToInt(row.Field<byte?>("AptStatus").ToString());
                                        }
                                        else
                                        {
                                            pba.AppointmentStatus = -2;
                                        }
                                        pba.PatientName = patName;

                                        pba.PinBoarded = false;

                                        DataTable patient = DentalData("Select FName, LName, PatStatus, PatNum From patient Where PatNum = " + patID);

                                        string patGuid = "";

                                        errorLineCount = 13;

                                        if (patID != 0)
                                        {
                                            if (patient.Rows.Count == 1)
                                            {
                                                patGuid = patient.Rows[0].Field<long>("PatNum").ToString();
                                                var patStatus = patient.Rows[0].Field<byte>("PatStatus");

                                                // Map to Dentrix values
                                                if (patStatus < 4)
                                                {
                                                    patStatus++;
                                                }
                                                pba.PatientStatus = patStatus;

                                            }

                                            DataTable futureAppointment = DentalData("Select " +
                                               "a.AptNum, a.AptDateTime, a.ProvNum " +
                                               "From appointment a " +
                                               "Where a.PatNum = " + patID + " And DATE(a.AptDateTime) > DATE('" + brokenDate + "') And a.AptNum <> " + pba.PMApptID +
                                               " ORDER BY AptDateTime DESC LIMIT 1");

                                            DataTable futureProcedure = DentalData("Select AptNum, SUM(ProcFee) AS 'SumProcFee' From procedurelog Where PatNum = " + patID + " And DATE(ProcDate) > DATE('" + brokenDate + "') Group By AptNum");


                                            if (futureAppointment.Rows.Count == 1)
                                            {
                                                //they have a future appointment
                                                double? sumProcFee2 = (from p in futureProcedure.AsEnumerable()
                                                                       where p.Field<long>("AptNum") == futureAppointment.Rows[0].Field<long>("AptNum")
                                                                       select p.Field<double?>("SumProcFee")).DefaultIfEmpty(0).FirstOrDefault();

                                                pba.OtherAppointmentAmount = (decimal?)sumProcFee2;
                                                pba.OtherAppointmentDate = futureAppointment.Rows[0].Field<DateTime?>("AptDateTime");
                                                pba.OtherAppointmentProvider = futureAppointment.Rows[0].Field<long?>("ProvNum").ToString();
                                            }
                                        }
                                        else
                                        {
                                            patGuid = "";

                                            pba.PatientStatus = 0;
                                        }

                                        //check for contact info
                                        PatientLastContact(patID, pba);

                                        if (pba.BrokenAmount != null)
                                        {
                                            providersBrokenAppointments.Add(pba);
                                            existingBrokenApptIDs.Add(apptID);
                                        }


                                    }
                                }
                            }
                        }

                        //providers broken appointments
                        if (providersBrokenAppointments.Count >= 100)
                        {
                            var json = JsonConvert.SerializeObject(providersBrokenAppointments, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                BulkUploadProvidersBrokenAppointmentsDataRequest request = new BulkUploadProvidersBrokenAppointmentsDataRequest(_appData.OfficeKey, json);
                                BulkUploadProvidersBrokenAppointmentsDataResponse response = new BulkUploadProvidersBrokenAppointmentsDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.BulkUploadProvidersBrokenAppointmentsData(request);
                                    bool result = response.BulkUploadProvidersBrokenAppointmentsDataResult;
                                }

                                json = null;
                                providersBrokenAppointments.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                                }
                                catch { }
                            }
                        }

                    }

                    //upload any remaining data
                    //providers broken appointments
                    if (providersBrokenAppointments.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersBrokenAppointments, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersBrokenAppointmentsDataRequest request = new BulkUploadProvidersBrokenAppointmentsDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersBrokenAppointmentsDataResponse response = new BulkUploadProvidersBrokenAppointmentsDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersBrokenAppointmentsData(request);
                                bool result = response.BulkUploadProvidersBrokenAppointmentsDataResult;
                            }

                            json = null;
                            providersBrokenAppointments.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -2);
                            }
                            catch { }
                        }
                    }

                    _appData.OfficeData.UnscheduledProcessDate = processingDate;
                    _appData.SetOfficeData();

                }
                catch (Exception ex)
                {
                    try
                    {
                        errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                    }
                    catch { }
                }
            }
        }

        internal void ProcessProvidersScheduleData(DateTime ProcessDate, List<Provider> Providers)
        {
            int errorLineCount = 0;

            try
            {
                DataTable appointmentsData = new DataTable();
                DataTable procedureLogPaymentData = new DataTable();

                DataTable completedProceduresData = new DataTable();
                DataTable uncompletedProceduresData = new DataTable();
                DataTable allProceduresData = new DataTable();

                DataTable lastFutureAppointmentData = new DataTable();
                DataTable plannedTreatmentData = new DataTable();
                DataTable patientFutureAppointmentsData = new DataTable();

                //List<Visual> imagesList = new List<Visual>();
                List<long> processedAppointments = new List<long>();

                //data lists
                List<ProvidersSchedule> providersScheduleAddList = new List<ProvidersSchedule>();
                List<ProvidersSchedule> providersScheduleUpdateList = new List<ProvidersSchedule>();
                List<string> allCompletedProceduresAdaCodes = new List<string>();
                List<ProvidersSchedulesProcedure> providersSchedulesProceduresAddList = new List<ProvidersSchedulesProcedure>();

                //ProcessDate = ProcessDate.AddDays(-1);

                //get last future appointment for date we need to process through
                //lastFutureAppointmentData = DentalData("Select Top 1 AptDateTime From admin.appt Order By AptDateTime DESC");
                lastFutureAppointmentData = DentalData("Select DATE(AptDateTime) as AptDateTime From appointment ORDER BY AptDateTime DESC LIMIT 1");

                DateTime lastFutureAppointmentDate = lastFutureAppointmentData.Rows[0].Field<DateTime>("AptDateTime");

                if (_appData.OfficeData.ScheduleProcessRefresh == true)
                {
                    #region refresh = true; past appointments
                    for (DateTime processingDate = ProcessDate; processingDate.Date <= lastFutureAppointmentDate; processingDate = processingDate.AddDays(1))
                    {
                        string formattedProcessDate = FormatExtractionDate(processingDate);

                        // appointment table
                        appointmentsData = DentalData("Select a.PatNum as PatNum, a.ProvNum as ProvNum, a.ProvHyg As ProvHyg, a.IsHygiene As IsHygiene, a.AptNum as AptNum, a.AptStatus as AptStatus," +
                        "a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed, DATE(a.AptDateTime) as AptDateTime, a.DateTStamp as DateTStamp " +
                        "From appointment a " +
                        "Where DATE(AptDateTime) = DATE('" + formattedProcessDate + "') And ( a.AptStatus <> 5 ) ");

                        procedureLogPaymentData = DentalData("Select AptNum, SUM(ProcFee) AS 'SumProcFee' From procedurelog Where DATE(ProcDate) = DATE('" + formattedProcessDate + "') Group By AptNum");

                        //get unique list of patients seen for the day (patients may have more than 1 appointment)
                        List<long> patientsSeen = (from a in appointmentsData.AsEnumerable()
                                                   select a.Field<long>("PatNum")).Distinct().ToList();

                        foreach (int patient in patientsSeen)
                        {
                            //get all appointments & procedures for each patient
                            List<DataRow> patientAppointments = (from a in appointmentsData.AsEnumerable()
                                                                 where a.Field<long>("PatNum") == patient
                                                                 select a).ToList();

                            allProceduresData.Clear();
                            allProceduresData = null;
                            allProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp,DateTP From procedurelog Where PatNum = " + patient); // + " AND DATE(ProcDate) >= DATE('" + formattedProcessDate + "')");

                            completedProceduresData.Clear();
                            completedProceduresData = null;
                            //completedProceduresData = DentalData("Select procid, proccodeid, amt, provid, procdate, createdate From admin.fullproclog " + 
                            //Where PatNum = '" + patient + "' And chartstatus = 102 And procdate = {d'" + formattedProcessDate + "'}");
                            completedProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp From procedurelog Where ProcStatus = 2 And PatNum = " + patient + " AND DATE(ProcDate) = DATE('" + formattedProcessDate + "')");

                            plannedTreatmentData.Clear();
                            plannedTreatmentData = null;
                            //plannedTreatmentData = DentalData("Select procid, proccodeid, amt, provid, procdate, createdate From admin.fullproclog Where PatNum = '" + patient + "' And chartstatus = 105");
                            plannedTreatmentData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp,DateTP From procedurelog Where ProcStatus = 1 And PatNum = " + patient);

                            //future appointments
                            patientFutureAppointmentsData.Clear();
                            patientFutureAppointmentsData = null;
                            patientFutureAppointmentsData = DentalData("Select a.AptNum as AptNum, a.PatNum as PatNum, a.ProvNum as ProvNum, " +
                            " a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed,  a.DateTStamp as DateTStamp,DATE(a.AptDateTime) as AptDateTime From appointment a " +
                            "Where DATE(AptDateTime) > DATE('" + formattedProcessDate + "') And PatNum = " + patient);

                            //create list for patient procedures
                            List<long> allAppointmentsProceduresList = new List<long>();

                            //create list for all completed procedures
                            List<long> allCompletedProceduresList = (from c in completedProceduresData.AsEnumerable()
                                                                     select c.Field<long>("ProcNum")).ToList();

                            //process each appointment
                            foreach (var appointment in patientAppointments.AsEnumerable())
                            {
                                if (!processedAppointments.Contains(appointment.Field<long>("AptNum")))
                                {
                                    var aptNum = appointment.Field<long>("AptNum");
                                    processedAppointments.Add(aptNum);

                                    List<long> appointmentProceduresList = new List<long>();
                                    DateTime aptDateTime = appointment.Field<DateTime>("AptDateTime");
                                    string formattedAptDateTime = FormatExtractionDate(appointment.Field<DateTime>("AptDateTime"));
                                    int PatNum = (int)appointment.Field<long>("PatNum");
                                    string appointedProvider = appointment.Field<long>("ProvNum").ToString();

                                    //check if appointment is a hygiene appointment, if so change to ProvHyg
                                    byte isHygieneAppt = appointment.Field<byte>("IsHygiene");
                                    if (isHygieneAppt == 1)
                                    {
                                        appointedProvider = appointment.Field<long>("ProvHyg").ToString();
                                    }

                                    string appointedProcedures = "";

                                    //create list for all procedures associated with this aptNum
                                    List<long> allProceduresForThisAptNumList = (from c in allProceduresData.AsEnumerable()
                                                                                 where c.Field<long>("AptNum") == aptNum
                                                                                 select c.Field<long>("ProcNum")).ToList();

                                    //if completed procedure count is 0, look for specific procNumss
                                    if (completedProceduresData.Rows.Count == 0)
                                    {
                                        string procCodeIds = "";

                                        foreach (var procNum in allProceduresForThisAptNumList.AsEnumerable())
                                        {
                                            if (procNum != 0)
                                            {
                                                procCodeIds += procNum + ",";
                                            }
                                        }


                                        if (procCodeIds != "" && procCodeIds.Length > 1)
                                        {
                                            procCodeIds = procCodeIds.Remove(procCodeIds.Length - 1, 1);

                                            completedProceduresData.Clear();
                                            completedProceduresData = null;
                                            //completedProceduresData = DentalData("Select procid, proccodeid, amt, provid, procdate, createdate From admin.fullproclog Where PatNum = '" + patient + "' And chartstatus = 102 And procid In (" + procCodeIds + ")");
                                            completedProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp From procedurelog Where ProcStatus = 2 And PatNum = " + patient + " AND CodeNum In (" + procCodeIds + ")");

                                            allCompletedProceduresList.Clear();
                                            allCompletedProceduresList = (from c in completedProceduresData.AsEnumerable()
                                                                          select c.Field<long>("ProcNum")).ToList();
                                        }
                                    }

                                    //get appointed procedures

                                    int i = 1;
                                    foreach (var procCode in allProceduresForThisAptNumList.AsEnumerable())
                                    {

                                        if (procCode != 0)
                                        {
                                            int procNum = (int)(from p in completedProceduresData.AsEnumerable()
                                                                where p.Field<long>("ProcNum") == procCode
                                                                select p.Field<long>("ProcNum")).FirstOrDefault();

                                            //see if procedure record exists for the appointment scheduled procedures
                                            if (procNum != 0)
                                            {
                                                appointmentProceduresList.Add(procNum);

                                                //is procedure still appointed to original provider?
                                                bool originalProvider = (from p in completedProceduresData.AsEnumerable()
                                                                         where p.Field<long>("ProcNum") == procNum && p.Field<long>("ProvNum").ToString() == appointedProvider
                                                                         select p).Any();

                                                if (originalProvider == true)
                                                {
                                                    allAppointmentsProceduresList.Add(procNum);
                                                }

                                                int procCodeID = (int)(from p in completedProceduresData.AsEnumerable()
                                                                       where p.Field<long>("ProcNum") == procCode
                                                                       select p.Field<long>("CodeNum")).FirstOrDefault();

                                                string pc = procCodeID.ToString();

                                                if (aDACodes.ContainsKey(pc))
                                                {
                                                    if (i > 1)
                                                    {
                                                        appointedProcedures += ",";
                                                    }
                                                    appointedProcedures += aDACodes[pc];
                                                }
                                                else
                                                {
                                                    //we don't have the adacode??
                                                    if (i > 1)
                                                    {
                                                        appointedProcedures += ",";
                                                    }
                                                    appointedProcedures += "unavailable";
                                                }
                                            }
                                            else
                                            {
                                                //check treatment planned procedures
                                                int txProc = (int)(from p in plannedTreatmentData.AsEnumerable()
                                                                   where p.Field<long>("ProcNum") == procCode
                                                                   select p.Field<long>("ProcNum")).FirstOrDefault();

                                                //see if procedure record exists for the appointment scheduled procedures
                                                if (txProc != 0)
                                                {
                                                    allAppointmentsProceduresList.Add(txProc);
                                                    appointmentProceduresList.Add(txProc);

                                                    int procCodeID = (int)(from p in plannedTreatmentData.AsEnumerable()
                                                                           where p.Field<long>("ProcNum") == procCode
                                                                           select p.Field<long>("CodeNum")).FirstOrDefault();

                                                    string txpc = procCodeID.ToString();

                                                    if (aDACodes.ContainsKey(txpc))
                                                    {
                                                        if (i > 1)
                                                        {
                                                            appointedProcedures += ",";
                                                        }
                                                        appointedProcedures += aDACodes[txpc];
                                                    }
                                                    else
                                                    {
                                                        //we don't have the adacode??
                                                        if (i > 1)
                                                        {
                                                            appointedProcedures += ",";
                                                        }
                                                        appointedProcedures += "unavailable";
                                                    }
                                                }
                                                else
                                                {
                                                    string pc = procCode.ToString();

                                                    if (aDACodes.ContainsKey(pc))
                                                    {
                                                        if (i > 1)
                                                        {
                                                            appointedProcedures += ",";
                                                        }
                                                        appointedProcedures += aDACodes[pc];
                                                    }
                                                }
                                            }
                                        }
                                        i++;
                                    }

                                    //create record for each scheduled appointment
                                    ProvidersSchedule newProviderSchedule = new ProvidersSchedule();
                                    newProviderSchedule.ApptBroken = false;
                                    newProviderSchedule.ApptDeleted = false;
                                    //newProviderSchedule.AppointedTime = appointment.Field<Int16>("apptlen");

                                    newProviderSchedule.ProviderID = (from p in Providers
                                                                      where p.PMID == appointedProvider
                                                                      select p.ID).FirstOrDefault();

                                    newProviderSchedule.PMPatientID = PatNum;
                                    newProviderSchedule.AppointmentDate = aptDateTime;
                                    if (newProviderSchedule.PMCreatedDate == null)
                                    {
                                        newProviderSchedule.PMCreatedDate = appointment.Field<DateTime?>("DateTStamp");
                                    }
                                    newProviderSchedule.PMModifiedDate = appointment.Field<DateTime?>("DateTStamp");
                                    newProviderSchedule.PMApptID = (int)appointment.Field<long>("AptNum");
                                    if (appointment.Field<byte?>("AptStatus") != null)
                                    {
                                        if (appointment.Field<byte>("AptStatus") == 2)
                                        {
                                            newProviderSchedule.AppointmentStatus = 1;
                                        }
                                        else
                                        {
                                            newProviderSchedule.AppointmentStatus = 0;
                                        }
                                        // newProviderSchedule.AppointmentStatus = commonData.AppointmentStatus(appointment.Field<byte>("AptStatus"));
                                    }
                                    else
                                    {
                                        newProviderSchedule.AppointmentStatus = -2;
                                    }


                                    decimal provProcAmt = 0;
                                    string provCompletedProcedures = "";
                                    int completeCount = 1;

                                    foreach (int proc in appointmentProceduresList)
                                    {
                                        //is procedure still appointed to original provider?
                                        bool originalProvider = (from p in completedProceduresData.AsEnumerable()
                                                                 where p.Field<long>("ProcNum") == proc && p.Field<long>("ProvNum").ToString() == appointedProvider
                                                                 select p).Any();

                                        if (originalProvider == true)
                                        {
                                            provProcAmt += (from c in completedProceduresData.AsEnumerable()
                                                            where c.Field<long>("ProcNum") == proc
                                                            select (decimal)c.Field<double>("ProcFee")).DefaultIfEmpty(0).FirstOrDefault();

                                            int procCodeID = (int)(from p in completedProceduresData.AsEnumerable()
                                                                   where p.Field<long>("ProcNum") == proc
                                                                   select p.Field<long>("CodeNum")).FirstOrDefault();

                                            string pc = procCodeID.ToString();

                                            if (aDACodes.ContainsKey(pc))
                                            {
                                                if (completeCount > 1)
                                                {
                                                    provCompletedProcedures += ",";
                                                }
                                                provCompletedProcedures += aDACodes[pc];

                                                completeCount++;
                                            }
                                            else
                                            {
                                                //we don't have the adacode??
                                                if (completeCount > 1)
                                                {
                                                    provCompletedProcedures += ",";
                                                }
                                                provCompletedProcedures += "unavailable";

                                                completeCount++;
                                            }
                                        }
                                    }

                                    newProviderSchedule.CompletedAmount = provProcAmt;
                                    newProviderSchedule.CompletedProcedures = provCompletedProcedures;

                                    //add procedures to ProvidersSchedulesProcedures objects
                                    //completed type = 2
                                    List<string> completedProceduresList = provCompletedProcedures.Split(',').ToList();
                                    foreach (string proc in completedProceduresList)
                                    {
                                        if (proc != null && proc != "")
                                        {
                                            ProvidersSchedulesProcedure psp = new ProvidersSchedulesProcedure();
                                            psp.ProcedureTypeID = 2;
                                            psp.ProviderID = newProviderSchedule.ProviderID;
                                            psp.PMPatientID = newProviderSchedule.PMPatientID.Value;

                                            if (newProviderSchedule.PMApptID.HasValue)
                                            {
                                                psp.PMApptID = newProviderSchedule.PMApptID.Value;
                                            }

                                            psp.AppointmentDate = newProviderSchedule.AppointmentDate.Value;
                                            psp.ProcedureCode = proc;
                                            psp.InitialSnapshot = false;

                                            providersSchedulesProceduresAddList.Add(psp);
                                        }
                                    }

                                    //accepted images/xrays taken
                                    newProviderSchedule.AcceptedPicturesTaken = 0;
                                    newProviderSchedule.AcceptedXRaysTaken = 0;

                                    //hidden images/xrays taken
                                    newProviderSchedule.HiddenPicturesTaken = 0;
                                    newProviderSchedule.HiddenXRaysTaken = 0;

                                    //deleted images/xrays taken
                                    newProviderSchedule.DeletedPicturesTaken = 0;
                                    newProviderSchedule.DeletedXRaysTaken = 0;

                                    //treatment planner/case acceptance
                                    decimal presented = 0;
                                    decimal accepted = 0;
                                    string txAccepted = "";
                                    int txCount = 1;
                                    //bool examPerformed = false;

                                    //get list of completed adacodes
                                    foreach (DataRow cp in completedProceduresData.Rows)
                                    {
                                        int procCodeID = (int)cp.Field<long>("CodeNum");
                                        string pc = procCodeID.ToString();

                                        if (aDACodes.ContainsKey(pc))
                                        {
                                            if (commonData.ExamCodes.Contains(aDACodes[pc]))
                                            {
                                                //examPerformed = true;
                                                newProviderSchedule.ExamPerformed = true;
                                                break;
                                            }
                                        }
                                    }

                                    //if (examPerformed == true)
                                    //{
                                    presented = (from p in plannedTreatmentData.AsEnumerable()
                                                 select (decimal)p.Field<double>("ProcFee")).DefaultIfEmpty(0).Sum();

                                    Dictionary<long, decimal> plannedTreatmentsDict = new Dictionary<long, decimal>();

                                    foreach (DataRow pt in plannedTreatmentData.AsEnumerable())
                                    {
                                        if (!plannedTreatmentsDict.ContainsKey(pt.Field<long>("ProcNum")))
                                        {
                                            plannedTreatmentsDict.Add(pt.Field<long>("ProcNum"), (decimal)pt.Field<double>("ProcFee"));
                                        }
                                    }


                                    //look at future appointments for accepted planned treatment
                                    foreach (DataRow pt in patientFutureAppointmentsData.AsEnumerable())
                                    {
                                        var aptNum3 = pt.Field<long>("AptNum");
                                        List<long> allProceduresForThisAptNumList3 = (from c in plannedTreatmentData.AsEnumerable()  //allProceduresData
                                                                                      where c.Field<long>("AptNum") == aptNum3
                                                                                      select c.Field<long>("ProcNum")).ToList();
                                        foreach (var procCode in allProceduresForThisAptNumList3.AsEnumerable())
                                        {
                                            if (procCode != 0)
                                            {
                                                if (plannedTreatmentsDict.ContainsKey(procCode))
                                                {
                                                    //treament was accepted!
                                                    accepted += plannedTreatmentsDict[procCode];

                                                    int procCodeID = (int)(from p in plannedTreatmentData.AsEnumerable()
                                                                           where p.Field<long>("ProcNum") == procCode
                                                                           select p.Field<long>("CodeNum")).FirstOrDefault();

                                                    string pc = procCodeID.ToString();

                                                    if (aDACodes.ContainsKey(pc))
                                                    {
                                                        if (txCount > 1)
                                                        {
                                                            txAccepted += ",";
                                                        }
                                                        txAccepted += aDACodes[pc];

                                                        txCount++;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                    //}

                                    newProviderSchedule.PresentedAmount = presented;
                                    newProviderSchedule.AcceptedAmount = accepted;

                                    //line below was commented out prior to 7-31-17
                                    newProviderSchedule.AcceptedProcedures = txAccepted;

                                    newProviderSchedule.CreatedDate = DateTime.Now;

                                    InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, appointment, appointedProcedures, providersSchedulesProceduresAddList);
                                    ClosingScheduleSnapshot(newProviderSchedule, appointedProvider, allProceduresData, plannedTreatmentData);

                                    if (newProviderSchedule.ProviderID != 0)
                                    {
                                        providersScheduleAddList.Add(newProviderSchedule);
                                    }

                                }
                            }

                            //check for any unappointed procedures and process accordingly
                            List<long> unAppointedProcedures = allCompletedProceduresList.Except(allAppointmentsProceduresList).ToList();

                            if (unAppointedProcedures.Count > 0)
                            {
                                List<DataRow> unAppointedProceduresList = (from c in completedProceduresData.AsEnumerable()
                                                                           where unAppointedProcedures.Contains(c.Field<long>("ProcNum"))
                                                                           select c).ToList();

                                List<string> unAppointedProviders = (from u in unAppointedProceduresList
                                                                     select u.Field<long>("ProvNum").ToString()).Distinct().ToList();

                                foreach (string prov in unAppointedProviders)
                                {
                                    //create record for each provider that completed procedures
                                    ProvidersSchedule providerSchedule = new ProvidersSchedule();
                                    providerSchedule.AppointmentStatus = 0;

                                    List<DataRow> providerUnappointedProcedures = (from u in unAppointedProceduresList
                                                                                   where u.Field<long>("ProvNum").ToString() == prov
                                                                                   select u).ToList();

                                    DateTime procDate = providerUnappointedProcedures[0].Field<DateTime>("ProcDate");
                                    DateTime createDate = providerUnappointedProcedures[0].Field<DateTime>("DateTStamp");
                                    decimal provProcAmt = 0;
                                    string provCompletedProcedures = "";
                                    int completeCount = 1;

                                    foreach (DataRow p in providerUnappointedProcedures)
                                    {
                                        provProcAmt += (decimal)p.Field<double>("ProcFee");

                                        int procCodeID = (int)p.Field<long>("CodeNum");

                                        string pc = procCodeID.ToString();

                                        if (aDACodes.ContainsKey(pc))
                                        {
                                            if (completeCount > 1)
                                            {
                                                provCompletedProcedures += ",";
                                            }
                                            provCompletedProcedures += aDACodes[pc];

                                            completeCount++;
                                        }
                                        else
                                        {
                                            //we don't have the adacode??
                                            if (completeCount > 1)
                                            {
                                                provCompletedProcedures += ",";
                                            }
                                            provCompletedProcedures += "unavailable";

                                            completeCount++;
                                        }

                                    }

                                    providerSchedule.ProviderID = (from p in Providers
                                                                   where p.PMID == prov
                                                                   select p.ID).FirstOrDefault();

                                    providerSchedule.PMPatientID = patient;
                                    providerSchedule.AppointmentDate = procDate;
                                    providerSchedule.PMCreatedDate = createDate;

                                    providerSchedule.CompletedAmount = provProcAmt;
                                    providerSchedule.CompletedProcedures = provCompletedProcedures;

                                    //accepted images/xrays taken
                                    providerSchedule.AcceptedPicturesTaken = 0;
                                    providerSchedule.AcceptedXRaysTaken = 0;

                                    //hidden images/xrays taken
                                    providerSchedule.HiddenPicturesTaken = 0;
                                    providerSchedule.HiddenXRaysTaken = 0;

                                    //deleted images/xrays taken
                                    providerSchedule.DeletedPicturesTaken = 0;
                                    providerSchedule.DeletedXRaysTaken = 0;

                                    //treatment planner/case acceptance
                                    providerSchedule.PresentedAmount = 0;
                                    providerSchedule.AcceptedAmount = 0;
                                    providerSchedule.AcceptedProcedures = "";

                                    providerSchedule.CreatedDate = DateTime.Now;

                                    if (providerSchedule.ProviderID != 0)
                                    {
                                        providersScheduleAddList.Add(providerSchedule);
                                    }
                                }
                            }
                        }

                        if (providersScheduleAddList.Count >= 100)
                        {
                            var json = JsonConvert.SerializeObject(providersScheduleAddList, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                BulkUploadProvidersSchedulesDataRequest request = new BulkUploadProvidersSchedulesDataRequest(_appData.OfficeKey, json);
                                BulkUploadProvidersSchedulesDataResponse response = new BulkUploadProvidersSchedulesDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.BulkUploadProvidersSchedulesData(request);
                                    bool result = response.BulkUploadProvidersSchedulesDataResult;
                                }

                                json = null;
                                providersScheduleAddList.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                                }
                                catch { }
                            }
                        }
                        if (providersSchedulesProceduresAddList.Count >= 100)
                        {
                            var json = JsonConvert.SerializeObject(providersSchedulesProceduresAddList, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                BulkUploadProvidersSchedulesProceduresDataRequest request = new BulkUploadProvidersSchedulesProceduresDataRequest(_appData.OfficeKey, json);
                                BulkUploadProvidersSchedulesProceduresDataResponse response = new BulkUploadProvidersSchedulesProceduresDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.BulkUploadProvidersSchedulesProceduresData(request);
                                    bool result = response.BulkUploadProvidersSchedulesProceduresDataResult;
                                }

                                json = null;
                                providersSchedulesProceduresAddList.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                                }
                                catch { }
                            }
                        }
                    }

                    if (providersScheduleAddList.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersScheduleAddList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersSchedulesDataRequest request = new BulkUploadProvidersSchedulesDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersSchedulesDataResponse response = new BulkUploadProvidersSchedulesDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersSchedulesData(request);
                                bool result = response.BulkUploadProvidersSchedulesDataResult;
                            }

                            json = null;
                            providersScheduleAddList.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex);
                            }
                            catch { }
                        }
                    }
                    if (providersSchedulesProceduresAddList.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersSchedulesProceduresAddList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersSchedulesProceduresDataRequest request = new BulkUploadProvidersSchedulesProceduresDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersSchedulesProceduresDataResponse response = new BulkUploadProvidersSchedulesProceduresDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersSchedulesProceduresData(request);
                                bool result = response.BulkUploadProvidersSchedulesProceduresDataResult;
                            }

                            json = null;
                            providersSchedulesProceduresAddList.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex);
                            }
                            catch { }
                        }
                    }
                    #endregion

                    #region refresh = true; future appointments
                    DateTime futureApptStartDate = DateTime.Today; //.AddDays(1);
                    DateTime futureApptEndDate = DateTime.Today.AddMonths(3);

                    //scheduled/future appointments
                    for (DateTime processingDate = futureApptStartDate; processingDate.Date <= futureApptEndDate; processingDate = processingDate.AddDays(1))
                    {
                        string formattedProcessDate = FormatExtractionDate(processingDate);

                        // appointment table
                        appointmentsData = DentalData("Select a.PatNum as PatNum, a.ProvNum as ProvNum, a.ProvHyg As ProvHyg, a.IsHygiene As IsHygiene, a.AptNum as AptNum, a.AptStatus as AptStatus," +
                        "a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed,DATE(a.AptDateTime) as AptDateTime, a.DateTStamp as DateTStamp " +
                        "From appointment a " +
                        "Where DATE(AptDateTime) = DATE('" + formattedProcessDate + "') And ( a.AptStatus <> 5 ) ");

                        procedureLogPaymentData = DentalData("Select AptNum, SUM(ProcFee) AS 'SumProcFee' From procedurelog Where DATE(ProcDate) = DATE('" + formattedProcessDate + "') Group By AptNum");

                        //get unique list of patients seen for the day (patients may have more than 1 appointment)
                        List<long> patientsSeen = (from a in appointmentsData.AsEnumerable()
                                                   select a.Field<long>("PatNum")).Distinct().ToList();

                        foreach (int patient in patientsSeen)
                        {
                            //get all appointments & procedures for each patient
                            List<DataRow> patientAppointments = (from a in appointmentsData.AsEnumerable()
                                                                 where a.Field<long>("PatNum") == patient
                                                                 select a).ToList();


                            // procedurelog table
                            //completedProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus From procedurelog Where ProcStatus = 2 And PatNum = " + patient + " AND DATE(ProcDate) = DATE('" + formattedProcessDate + "')");
                            //procdate, createdate needed?

                            uncompletedProceduresData.Clear();
                            uncompletedProceduresData = null;
                            uncompletedProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum From procedurelog Where ProcStatus = 1 AND PatNum = " + patient + " AND DATE(ProcDate) = DATE('" + formattedProcessDate + "')");

                            plannedTreatmentData.Clear();
                            plannedTreatmentData = null;
                            //plannedTreatmentData = DentalData("Select procid, proccodeid, amt, provid, procdate, createdate From admin.fullproclog Where PatNum = '" + patient + "' And chartstatus = 105");
                            plannedTreatmentData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp,DateTP From procedurelog Where ProcStatus = 1 And PatNum = " + patient);
                            // ProcStatus = 1 is scheduled...same as planned?

                            //create list for patient procedures
                            List<long> allAppointmentsProceduresList = new List<long>();

                            //process each appointment
                            foreach (var appointment in patientAppointments.AsEnumerable())
                            {
                                if (!processedAppointments.Contains(appointment.Field<long>("AptNum")))
                                {
                                    var aptNum = appointment.Field<long>("AptNum");
                                    processedAppointments.Add(appointment.Field<long>("AptNum"));

                                    List<long> appointmentProceduresList = new List<long>();
                                    DateTime aptDateTime = appointment.Field<DateTime>("AptDateTime");
                                    string formattedAptDateTime = FormatExtractionDate(appointment.Field<DateTime>("AptDateTime"));
                                    int PatNum = (int)appointment.Field<long>("PatNum");
                                    string appointedProvider = appointment.Field<long>("ProvNum").ToString();

                                    //check if appointment is a hygiene appointment, if so change to ProvHyg
                                    byte isHygieneAppt = appointment.Field<byte>("IsHygiene");
                                    if (isHygieneAppt == 1)
                                    {
                                        appointedProvider = appointment.Field<long>("ProvHyg").ToString();
                                    }

                                    string appointedProcedures = "";
                                    string addlProvider = ""; // appointment.Field<string>("addlprov");

                                    //create list for all procedures associated with this aptNum
                                    List<long> uncompletedProceduresForThisAptNumList = (from c in uncompletedProceduresData.AsEnumerable()
                                                                                         where c.Field<long>("AptNum") == aptNum
                                                                                         select c.Field<long>("ProcNum")).ToList();


                                    //if uncompleted procedure count is 0, look for specific procid's
                                    if (uncompletedProceduresData.Rows.Count == 0)
                                    {
                                        //string procCodeIds = "";
                                        int i = 1;
                                        foreach (var procCode in uncompletedProceduresForThisAptNumList.AsEnumerable())
                                        {
                                            if (procCode != 0)
                                            {
                                                int proc = (int)(from p in uncompletedProceduresData.AsEnumerable()
                                                                 where p.Field<long>("ProcNum") == procCode
                                                                 select p.Field<long>("ProcNum")).FirstOrDefault();

                                                //see if procedure record exists for the appointment scheduled procedures
                                                if (proc != 0)
                                                {
                                                    allAppointmentsProceduresList.Add(proc);
                                                    appointmentProceduresList.Add(proc);

                                                    int procCodeID = (int)(from p in uncompletedProceduresData.AsEnumerable()
                                                                           where p.Field<long>("ProcNum") == procCode
                                                                           select p.Field<long>("CodeNum")).FirstOrDefault();

                                                    string pc = procCodeID.ToString();

                                                    if (aDACodes.ContainsKey(pc))
                                                    {
                                                        if (i > 1)
                                                        {
                                                            appointedProcedures += ",";
                                                        }
                                                        appointedProcedures += aDACodes[pc];
                                                    }
                                                    else
                                                    {
                                                        //we don't have the adacode??
                                                        if (i > 1)
                                                        {
                                                            appointedProcedures += ",";
                                                        }
                                                        appointedProcedures += "unavailable";
                                                    }

                                                }
                                                else
                                                {
                                                    //check treatment planned procedures
                                                    int txProc = (int)(from p in plannedTreatmentData.AsEnumerable()
                                                                       where p.Field<long>("ProcNum") == procCode
                                                                       select p.Field<long>("ProcNum")).FirstOrDefault();

                                                    //see if procedure record exists for the appointment scheduled procedures
                                                    if (txProc != 0)
                                                    {
                                                        allAppointmentsProceduresList.Add(txProc);
                                                        appointmentProceduresList.Add(txProc);

                                                        int procCodeID = (int)(from p in plannedTreatmentData.AsEnumerable()
                                                                               where p.Field<long>("ProcNum") == procCode
                                                                               select p.Field<long>("CodeNum")).FirstOrDefault();

                                                        string txpc = procCodeID.ToString();

                                                        if (aDACodes.ContainsKey(txpc))
                                                        {
                                                            if (i > 1)
                                                            {
                                                                appointedProcedures += ",";
                                                            }
                                                            appointedProcedures += aDACodes[txpc];
                                                        }
                                                        else
                                                        {
                                                            //we don't have the adacode??
                                                            if (i > 1)
                                                            {
                                                                appointedProcedures += ",";
                                                            }
                                                            appointedProcedures += "unavailable";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        string pc = procCode.ToString();

                                                        if (aDACodes.ContainsKey(pc))
                                                        {
                                                            if (i > 1)
                                                            {
                                                                appointedProcedures += ",";
                                                            }
                                                            appointedProcedures += aDACodes[pc];
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }

                                        //create record for each scheduled appointment
                                        ProvidersSchedule newProviderSchedule = new ProvidersSchedule();

                                        newProviderSchedule.ApptBroken = false;
                                        newProviderSchedule.ApptDeleted = false;
                                        //newProviderSchedule.AppointedTime = appointment.Field<Int16>("apptlen");


                                        newProviderSchedule.ProviderID = (from p in Providers
                                                                          where p.PMID == appointedProvider
                                                                          select p.ID).FirstOrDefault();

                                        newProviderSchedule.PMPatientID = PatNum;
                                        newProviderSchedule.AppointmentDate = aptDateTime;
                                        // TODO - tweak this?
                                        newProviderSchedule.PMCreatedDate = appointment.Field<DateTime?>("DateTStamp");
                                        newProviderSchedule.PMModifiedDate = appointment.Field<DateTime?>("DateTStamp");
                                        newProviderSchedule.PMApptID = (int)appointment.Field<long>("AptNum");
                                        if (appointment.Field<byte?>("AptStatus") != null)
                                        {
                                            if (appointment.Field<byte>("AptStatus") == 2)
                                            {
                                                newProviderSchedule.AppointmentStatus = 1;
                                            }
                                            else
                                            {
                                                newProviderSchedule.AppointmentStatus = 0;
                                            }
                                            // newProviderSchedule.AppointmentStatus = commonData.AppointmentStatus(appointment.Field<byte>("AptStatus"));

                                        }
                                        else
                                        {
                                            newProviderSchedule.AppointmentStatus = -2;
                                        }

                                        newProviderSchedule.CreatedDate = DateTime.Now;

                                        InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, appointment, appointedProcedures, providersSchedulesProceduresAddList);
                                        ClosingScheduleSnapshot(newProviderSchedule, appointedProvider, allProceduresData, plannedTreatmentData);

                                        if (newProviderSchedule.ProviderID != 0)
                                        {
                                            providersScheduleAddList.Add(newProviderSchedule);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (providersScheduleAddList.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersScheduleAddList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersSchedulesDataRequest request = new BulkUploadProvidersSchedulesDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersSchedulesDataResponse response = new BulkUploadProvidersSchedulesDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersSchedulesData(request);
                                bool result = response.BulkUploadProvidersSchedulesDataResult;
                            }

                            json = null;
                            providersScheduleAddList.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex);
                            }
                            catch { }
                        }
                    }
                    if (providersSchedulesProceduresAddList.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersSchedulesProceduresAddList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersSchedulesProceduresDataRequest request = new BulkUploadProvidersSchedulesProceduresDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersSchedulesProceduresDataResponse response = new BulkUploadProvidersSchedulesProceduresDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersSchedulesProceduresData(request);
                                bool result = response.BulkUploadProvidersSchedulesProceduresDataResult;
                            }

                            json = null;
                            providersSchedulesProceduresAddList.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex);
                            }
                            catch { }
                        }
                    }
                    #endregion

                }
                else
                {
                    //refresh is false; this affects how we process scheduled procedures
                    errorLineCount = 1;

                    bool processAppointments = false;
                    List<ProvidersSchedule> existingProvScheduleList = new List<ProvidersSchedule>();

                    try
                    {
                        GetProvidersSchedulesByDateRequest request = new GetProvidersSchedulesByDateRequest(_appData.OfficeKey, ProcessDate, DateTime.Today);
                        GetProvidersSchedulesByDateResponse response = new GetProvidersSchedulesByDateResponse();

                        using (ThriveDataServiceClient thriveService = new ThriveDataServiceClient())
                        {
                            response = thriveService.GetProvidersSchedulesByDate(request);
                            string result = response.GetProvidersSchedulesByDateResult;

                            existingProvScheduleList = JsonConvert.DeserializeObject<List<ProvidersSchedule>>(result);

                            processAppointments = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            errorLog.LogError(_appData.OfficeKeyGuid, ex);
                        }
                        catch { }
                    }

                    errorLineCount = 2;

                    //process completed appointments (process from processDate to today (the past))
                    #region no refresh yesterday and older appointments
                    if (processAppointments == true)
                    {
                        for (DateTime processingDate = ProcessDate; processingDate.Date <= DateTime.Today.AddDays(-1); processingDate = processingDate.AddDays(1))
                        {

                            errorLineCount = 3;

                            string formattedProcessDate = FormatExtractionDate(processingDate);

                            //get all appointments/procedures for the day
                            appointmentsData = DentalData(
                                "Select a.PatNum as PatNum, a.ProvNum as ProvNum, a.ProvHyg As ProvHyg, a.IsHygiene As IsHygiene, a.AptNum as AptNum, a.AptStatus as AptStatus," +
                               "a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed,DATE(a.AptDateTime) as AptDateTime, a.DateTStamp as DateTStamp " +
                               "From appointment a " +
                               "Where DATE(AptDateTime) = DATE('" + formattedProcessDate + "') And ( a.AptStatus <> 5 ) ");

                            procedureLogPaymentData = DentalData("Select AptNum, SUM(ProcFee) AS 'SumProcFee' From procedurelog Where DATE(ProcDate) = DATE('" + formattedProcessDate + "') Group By AptNum");

                            //from the cloud db, get list of appointments we've already gathered
                            List<ProvidersSchedule> providersSchedulesByApptDate = (from ps in existingProvScheduleList
                                                                                    where ps.AppointmentDate == processingDate.Date
                                                                                    select ps).ToList();


                            errorLineCount = 4;

                            //was appointment broken or deleted?
                            foreach (ProvidersSchedule ps in providersSchedulesByApptDate)
                            {
                                bool apptStillValid = true;

                                if (ps.PMApptID.HasValue == true)
                                {
                                    apptStillValid = (from a in appointmentsData.AsEnumerable()
                                                      where a.Field<long>("AptNum") == ps.PMApptID.Value
                                                      select a).Any();
                                }

                                //false = appt is either broken or deleted
                                if (apptStillValid == false)
                                {
                                    //check to see if appt was broken or deleted
                                    DataTable apptData = DentalData("Select a.AptStatus as AptStatus " +
                                   "From appointment a Where AptNum = " + ps.PMApptID.Value.ToString());

                                    long? aptStatus = (long?)(from a in apptData.AsEnumerable()
                                                              select a.Field<byte>("AptStatus")).FirstOrDefault();

                                    //if we have no records then the appointment was deleted; confirm by looking up history?
                                    if (aptStatus == null || aptStatus == 6)
                                    {
                                        //appointment was deleted
                                        ps.ApptDeleted = true;
                                        providersScheduleUpdateList.Add(ps);
                                    }
                                    if (aptStatus == 5 || aptStatus == 3)
                                    {
                                        //appointment was broken mark appropriately
                                        ps.ApptBroken = true;
                                        providersScheduleUpdateList.Add(ps);
                                    }
                                }
                            }

                            errorLineCount = 5;

                            //get unique list of patients seen for the day (patients may have more than 1 appointment)
                            List<long> patientsSeenToday = (from a in appointmentsData.AsEnumerable()
                                                            select a.Field<long>("PatNum")).Distinct().ToList();

                            foreach (int patient in patientsSeenToday)
                            {
                                //get all appointments & procedures for each patient
                                List<DataRow> patientAppointments = (from a in appointmentsData.AsEnumerable()
                                                                     where a.Field<long>("PatNum") == patient
                                                                     select a).ToList();


                                allProceduresData.Clear();
                                allProceduresData = null;
                                allProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp,DateTP From procedurelog Where PatNum = " + patient); // + " AND DATE(ProcDate) >= DATE('" + formattedProcessDate + "')");

                                completedProceduresData.Clear();
                                completedProceduresData = null;
                                completedProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp From procedurelog Where ProcStatus = 2 And PatNum = " + patient + " AND DATE(ProcDate) = DATE('" + formattedProcessDate + "')");

                                plannedTreatmentData.Clear();
                                plannedTreatmentData = null;
                                //plannedTreatmentData = DentalData("Select procid, proccodeid, amt, provid, procdate, createdate From admin.fullproclog Where PatNum = '" + patient + "' And chartstatus = 105");
                                plannedTreatmentData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus, AptNum,ProcDate, DateTStamp,DateTP From procedurelog Where ProcStatus = 1 And PatNum = " + patient);

                                //future appointments
                                patientFutureAppointmentsData.Clear();
                                patientFutureAppointmentsData = null;
                                patientFutureAppointmentsData = DentalData("Select a.AptNum as AptNum, a.PatNum as PatNum, a.ProvNum as ProvNum, a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed,  a.DateTStamp as DateTStamp, DATE(a.AptDateTime) as AptDateTime From appointment a " +
                                "Where DATE(AptDateTime) > DATE('" + formattedProcessDate + "') And PatNum = " + patient);

                                //create list for patient procedures
                                List<long> allAppointmentsProceduresList = new List<long>();

                                //create list for all completed procedures
                                List<long> allCompletedProceduresList = (from c in completedProceduresData.AsEnumerable()
                                                                         select c.Field<long>("ProcNum")).ToList();

                                List<string> txDiagnosedProvidersNotOnAppointmentList = (from pt in plannedTreatmentData.AsEnumerable()
                                                                                         where pt.Field<DateTime>("ProcDate") == processingDate
                                                                                         select pt.Field<long>("ProvNum").ToString()).Distinct().ToList();


                                //process each appointment
                                foreach (var appointment in patientAppointments.AsEnumerable())
                                {
                                    if (!processedAppointments.Contains(appointment.Field<long>("AptNum")))
                                    {
                                        errorLineCount = 9;

                                        var aptNum = appointment.Field<long>("AptNum");
                                        processedAppointments.Add(appointment.Field<long>("AptNum"));

                                        List<long> appointmentProceduresList = new List<long>();
                                        DateTime aptDateTime = appointment.Field<DateTime>("AptDateTime");
                                        string formattedAptDateTime = FormatExtractionDate(appointment.Field<DateTime>("AptDateTime"));
                                        int PatNum = (int)appointment.Field<long>("PatNum");
                                        string appointedProvider = appointment.Field<long>("ProvNum").ToString();

                                        //check if appointment is a hygiene appointment, if so change to ProvHyg
                                        byte isHygieneAppt = appointment.Field<byte>("IsHygiene");
                                        if (isHygieneAppt == 1)
                                        {
                                            appointedProvider = appointment.Field<long>("ProvHyg").ToString();
                                        }

                                        string appointedProcedures = "";
                                        //string addlProvider = appointment.Field<string>("addlprov");

                                        //remove providers from txDiagnosedList
                                        if (txDiagnosedProvidersNotOnAppointmentList.Contains(appointedProvider))
                                        {
                                            txDiagnosedProvidersNotOnAppointmentList.Remove(appointedProvider);
                                        }

                                        //create list for all procedures associated with this aptNum
                                        List<long> completedProceduresForThisAptNumList = (from c in completedProceduresData.AsEnumerable()
                                                                                           where c.Field<long>("AptNum") == aptNum
                                                                                           select c.Field<long>("ProcNum")).ToList();

                                        //create list for all procedures associated with this aptNum
                                        List<long> allProceduresForThisAptNumList = (from c in allProceduresData.AsEnumerable()
                                                                                     where c.Field<long>("AptNum") == aptNum
                                                                                     select c.Field<long>("ProcNum")).ToList();

                                        errorLineCount = 10;

                                        //if completed procedure count is 0, look for specific procid's
                                        if (completedProceduresData.Rows.Count == 0)
                                        {
                                            string procNums = "";

                                            foreach (var procNum in allProceduresForThisAptNumList.AsEnumerable())
                                            {
                                                if (procNum != 0)
                                                {
                                                    procNums += procNum + ",";
                                                }
                                            }

                                            if (procNums != "" && procNums.Length > 1)
                                            {
                                                procNums = procNums.Remove(procNums.Length - 1, 1);

                                                completedProceduresData.Clear();
                                                completedProceduresData = null;
                                                //completedProceduresData = DentalData("Select procid, proccodeid, amt, provid, procdate, createdate From admin.fullproclog Where PatNum = '" + patient + "' And chartstatus = 102 And procid In (" + procCodeIds + ")");
                                                completedProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp From procedurelog Where ProcStatus = 2 And PatNum = " + patient + " AND CodeNum In (" + procNums + ")");

                                                allCompletedProceduresList.Clear();
                                                allCompletedProceduresList = (from c in completedProceduresData.AsEnumerable()
                                                                              select c.Field<long>("ProcNum")).ToList();
                                            }
                                        }

                                        errorLineCount = 11;

                                        //get appointed procedures

                                        int i = 1;
                                        foreach (var procCode in allProceduresForThisAptNumList.AsEnumerable())
                                        {
                                            //int procCode = appointment.Field<long>("codeid" + i);

                                            if (procCode != 0)
                                            {
                                                int procNum = (int)(from p in completedProceduresData.AsEnumerable()
                                                                    where p.Field<long>("ProcNum") == procCode
                                                                    select p.Field<long>("ProcNum")).FirstOrDefault();

                                                //see if procedure record exists for the appointment scheduled procedures
                                                if (procNum != 0)
                                                {
                                                    appointmentProceduresList.Add(procNum);

                                                    //is procedure still appointed to original provider?
                                                    bool originalProvider = (from p in completedProceduresData.AsEnumerable()
                                                                             where p.Field<long>("ProcNum") == procNum && p.Field<long>("ProvNum").ToString() == appointedProvider
                                                                             select p).Any();

                                                    if (originalProvider == true)
                                                    {
                                                        allAppointmentsProceduresList.Add(procNum);
                                                    }

                                                    int procCodeID = (int)(from p in completedProceduresData.AsEnumerable()
                                                                           where p.Field<long>("ProcNum") == procCode
                                                                           select p.Field<long>("CodeNum")).FirstOrDefault();

                                                    string pc = procCodeID.ToString();

                                                    if (aDACodes.ContainsKey(pc))
                                                    {
                                                        if (i > 1)
                                                        {
                                                            appointedProcedures += ",";
                                                        }
                                                        appointedProcedures += aDACodes[pc];
                                                    }
                                                    else
                                                    {
                                                        //we don't have the adacode??
                                                        if (i > 1)
                                                        {
                                                            appointedProcedures += ",";
                                                        }
                                                        appointedProcedures += "unavailable";
                                                    }
                                                }
                                                else
                                                {
                                                    //check treatment planned procedures
                                                    int txProc = (int)(from p in plannedTreatmentData.AsEnumerable()
                                                                       where p.Field<long>("ProcNum") == procCode
                                                                       select p.Field<long>("ProcNum")).FirstOrDefault();

                                                    //see if procedure record exists for the appointment scheduled procedures
                                                    if (txProc != 0)
                                                    {
                                                        allAppointmentsProceduresList.Add(txProc);
                                                        appointmentProceduresList.Add(txProc);

                                                        int procCodeID = (int)(from p in plannedTreatmentData.AsEnumerable()
                                                                               where p.Field<long>("ProcNum") == procCode
                                                                               select p.Field<long>("CodeNum")).FirstOrDefault();

                                                        string txpc = procCodeID.ToString();

                                                        if (aDACodes.ContainsKey(txpc))
                                                        {
                                                            if (i > 1)
                                                            {
                                                                appointedProcedures += ",";
                                                            }
                                                            appointedProcedures += aDACodes[txpc];
                                                        }
                                                        else
                                                        {
                                                            //we don't have the adacode??
                                                            if (i > 1)
                                                            {
                                                                appointedProcedures += ",";
                                                            }
                                                            appointedProcedures += "unavailable";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        string pc = procCode.ToString();

                                                        if (aDACodes.ContainsKey(pc))
                                                        {
                                                            if (i > 1)
                                                            {
                                                                appointedProcedures += ",";
                                                            }
                                                            appointedProcedures += aDACodes[pc];
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }

                                        errorLineCount = 12;

                                        //create record for each scheduled appointment
                                        ProvidersSchedule newProviderSchedule = new ProvidersSchedule();
                                        newProviderSchedule.CreatedDate = DateTime.Now;
                                        newProviderSchedule.PMApptID = (int)appointment.Field<long>("AptNum");

                                        //see if existing record exists (from cloud)
                                        ProvidersSchedule existingProviderSchedule = (from ps in providersSchedulesByApptDate
                                                                                      where ps.PMApptID == newProviderSchedule.PMApptID
                                                                                      select ps).FirstOrDefault();

                                        //if existing record exists, set new record to existing record
                                        if (existingProviderSchedule != null)
                                        {
                                            newProviderSchedule = commonData.DeepCopy(existingProviderSchedule);
                                        }

                                        newProviderSchedule.ApptBroken = false;
                                        newProviderSchedule.ApptDeleted = false;
                                        //newProviderSchedule.AppointedTime = appointment.Field<Int16>("apptlen");

                                        newProviderSchedule.ProviderID = (from p in Providers
                                                                          where p.PMID == appointedProvider
                                                                          select p.ID).FirstOrDefault();

                                        newProviderSchedule.PMPatientID = PatNum;
                                        newProviderSchedule.AppointmentDate = aptDateTime;
                                        if (newProviderSchedule.PMCreatedDate == null)
                                        {
                                            newProviderSchedule.PMCreatedDate = appointment.Field<DateTime?>("DateTStamp");
                                        }
                                        newProviderSchedule.PMModifiedDate = appointment.Field<DateTime?>("DateTStamp");
                                        if (appointment.Field<byte?>("AptStatus") != null)
                                        {
                                            if (appointment.Field<byte>("AptStatus") == 2)
                                            {
                                                newProviderSchedule.AppointmentStatus = 1;
                                            }
                                            else
                                            {
                                                newProviderSchedule.AppointmentStatus = 0;
                                            }
                                            // newProviderSchedule.AppointmentStatus = commonData.AppointmentStatus(appointment.Field<byte>("AptStatus"));

                                        }
                                        else
                                        {
                                            newProviderSchedule.AppointmentStatus = -2;
                                        }


                                        decimal provProcAmt = 0;
                                        string provCompletedProcedures = "";
                                        int completeCount = 1;

                                        errorLineCount = 13;

                                        foreach (int proc in appointmentProceduresList)
                                        {
                                            //is procedure still appointed to original provider?
                                            bool originalProvider = (from p in completedProceduresData.AsEnumerable()
                                                                     where p.Field<long>("ProcNum") == proc && p.Field<long>("ProvNum").ToString() == appointedProvider
                                                                     select p).Any();

                                            if (originalProvider == true)
                                            {
                                                provProcAmt += (from c in completedProceduresData.AsEnumerable()
                                                                where c.Field<long>("ProcNum") == proc
                                                                select (decimal)c.Field<double>("ProcFee")).DefaultIfEmpty(0).FirstOrDefault();

                                                int procCodeID = (int)(from p in completedProceduresData.AsEnumerable()
                                                                       where p.Field<long>("ProcNum") == proc
                                                                       select p.Field<long>("CodeNum")).FirstOrDefault();

                                                string pc = procCodeID.ToString();

                                                if (aDACodes.ContainsKey(pc))
                                                {
                                                    if (completeCount > 1)
                                                    {
                                                        provCompletedProcedures += ",";
                                                    }
                                                    provCompletedProcedures += aDACodes[pc];

                                                    completeCount++;
                                                }
                                                else
                                                {
                                                    //we don't have the adacode??
                                                    if (completeCount > 1)
                                                    {
                                                        provCompletedProcedures += ",";
                                                    }
                                                    provCompletedProcedures += "unavailable";

                                                    completeCount++;
                                                }
                                            }
                                        }

                                        errorLineCount = 14;

                                        newProviderSchedule.CompletedAmount = provProcAmt;
                                        newProviderSchedule.CompletedProcedures = provCompletedProcedures;

                                        //add procedures to ProvidersSchedulesProcedures objects
                                        //completed type = 2
                                        List<string> completedProceduresList = provCompletedProcedures.Split(',').ToList();
                                        foreach (string proc in completedProceduresList)
                                        {
                                            if (proc != null && proc != "")
                                            {
                                                ProvidersSchedulesProcedure psp = new ProvidersSchedulesProcedure();
                                                psp.ProcedureTypeID = 2;
                                                psp.ProviderID = newProviderSchedule.ProviderID;
                                                psp.PMPatientID = newProviderSchedule.PMPatientID.Value;

                                                if (newProviderSchedule.PMApptID.HasValue)
                                                {
                                                    psp.PMApptID = newProviderSchedule.PMApptID.Value;
                                                }

                                                psp.AppointmentDate = newProviderSchedule.AppointmentDate.Value;
                                                psp.ProcedureCode = proc;
                                                psp.InitialSnapshot = false;

                                                providersSchedulesProceduresAddList.Add(psp);
                                            }
                                        }

                                        errorLineCount = 15;

                                        //accepted images/xrays taken

                                        newProviderSchedule.AcceptedPicturesTaken = 0;
                                        newProviderSchedule.AcceptedXRaysTaken = 0;

                                        //hidden images/xrays taken

                                        newProviderSchedule.HiddenPicturesTaken = 0;
                                        newProviderSchedule.HiddenXRaysTaken = 0;

                                        //deleted images/xrays taken

                                        newProviderSchedule.DeletedPicturesTaken = 0;
                                        newProviderSchedule.DeletedXRaysTaken = 0;

                                        //treatment planner/case acceptance
                                        decimal presented = 0;
                                        decimal accepted = 0;
                                        string txAccepted = "";
                                        int txCount = 1;
                                        //bool examPerformed = false;

                                        //get list of completed adacodes
                                        foreach (DataRow cp in completedProceduresData.Rows)
                                        {
                                            int procCodeID = (int)cp.Field<long>("CodeNum");
                                            string pc = procCodeID.ToString();

                                            if (aDACodes.ContainsKey(pc))
                                            {
                                                if (commonData.ExamCodes.Contains(aDACodes[pc]))
                                                {
                                                    //examPerformed = true;
                                                    newProviderSchedule.ExamPerformed = true;
                                                    break;
                                                }
                                            }
                                        }

                                        //if (examPerformed == true)
                                        //{
                                        presented = (from p in plannedTreatmentData.AsEnumerable()
                                                     select (decimal)p.Field<double>("ProcFee")).DefaultIfEmpty(0).Sum();

                                        Dictionary<long, decimal> plannedTreatmentsDict = new Dictionary<long, decimal>();

                                        foreach (DataRow pt in plannedTreatmentData.AsEnumerable())
                                        {
                                            if (!plannedTreatmentsDict.ContainsKey(pt.Field<long>("ProcNum")))
                                            {
                                                plannedTreatmentsDict.Add(pt.Field<long>("ProcNum"), (decimal)pt.Field<double>("ProcFee"));
                                            }
                                        }

                                        //look at future appointments for accepted planned treatment
                                        foreach (DataRow pt in patientFutureAppointmentsData.AsEnumerable())
                                        {
                                            var aptNum2 = pt.Field<long>("AptNum");
                                            List<long> allProceduresForThisAptNumList2 = (from c in plannedTreatmentData.AsEnumerable()  //allProceduresData
                                                                                          where c.Field<long>("AptNum") == aptNum2
                                                                                          select c.Field<long>("ProcNum")).ToList();
                                            foreach (var procNum in allProceduresForThisAptNumList2.AsEnumerable())
                                            {
                                                if (procNum != 0)
                                                {
                                                    if (plannedTreatmentsDict.ContainsKey(procNum))
                                                    {
                                                        //treament was accepted!
                                                        accepted += plannedTreatmentsDict[procNum];

                                                        int procCodeID = (int)(from p in plannedTreatmentData.AsEnumerable()
                                                                               where p.Field<long>("ProcNum") == procNum
                                                                               select p.Field<long>("CodeNum")).FirstOrDefault();

                                                        string pc = procCodeID.ToString();

                                                        if (aDACodes.ContainsKey(pc))
                                                        {
                                                            if (txCount > 1)
                                                            {
                                                                txAccepted += ",";
                                                            }
                                                            txAccepted += aDACodes[pc];

                                                            txCount++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //}

                                        errorLineCount = 18;

                                        newProviderSchedule.PresentedAmount = presented;
                                        newProviderSchedule.AcceptedAmount = accepted;
                                        //line below was commented out prior to 7-31-17
                                        newProviderSchedule.AcceptedProcedures = txAccepted;


                                        if (newProviderSchedule.ProviderID != 0)
                                        {
                                            if (existingProviderSchedule != null)
                                            {
                                                errorLineCount = 19;

                                                bool skipCompare = false;

                                                //if initial snapshot date is null then get the snapshot
                                                if (existingProviderSchedule.InitialSnapshotDate == null)
                                                {
                                                    InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, appointment, appointedProcedures, providersSchedulesProceduresAddList);
                                                    skipCompare = true;
                                                }

                                                //closing snapshot
                                                byte closeSnapTime = 17;
                                                if (_appData.OfficeData.CloseSnapshotTime.HasValue == true)
                                                {
                                                    closeSnapTime = _appData.OfficeData.CloseSnapshotTime.Value;
                                                }

                                                if (DateTimeOffset.Now.LocalDateTime.Hour >= closeSnapTime)
                                                {
                                                    ClosingScheduleSnapshot(newProviderSchedule, appointedProvider, allProceduresData, plannedTreatmentData);
                                                    skipCompare = true;
                                                }

                                                if (skipCompare == false)
                                                {
                                                    if (commonData.CompareProvidersSchedules(newProviderSchedule, existingProviderSchedule) == false)
                                                    {
                                                        providersScheduleUpdateList.Add(newProviderSchedule);
                                                    }
                                                }
                                                else
                                                {
                                                    providersScheduleUpdateList.Add(newProviderSchedule);
                                                }
                                            }
                                            else
                                            {
                                                errorLineCount = 20;

                                                InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, appointment, appointedProcedures, providersSchedulesProceduresAddList);
                                                ClosingScheduleSnapshot(newProviderSchedule, appointedProvider, allProceduresData, plannedTreatmentData);

                                                providersScheduleAddList.Add(newProviderSchedule);
                                            }
                                        }
                                    }

                                    //check for any unappointed procedures and process accordingly
                                    List<long> unAppointedProcedures = allCompletedProceduresList.Except(allAppointmentsProceduresList).ToList();

                                    if (unAppointedProcedures.Count > 0)
                                    {
                                        errorLineCount = 21;

                                        List<DataRow> unAppointedProceduresList = (from c in completedProceduresData.AsEnumerable()
                                                                                   where unAppointedProcedures.Contains(c.Field<long>("ProcNum"))
                                                                                   select c).ToList();

                                        List<string> unAppointedProviders = (from u in unAppointedProceduresList
                                                                             select u.Field<long>("ProvNum").ToString()).Distinct().ToList();

                                        foreach (string prov in unAppointedProviders)
                                        {
                                            //remove providers from txDiagnosedList
                                            if (txDiagnosedProvidersNotOnAppointmentList.Contains(prov))
                                            {
                                                txDiagnosedProvidersNotOnAppointmentList.Remove(prov);
                                            }

                                            //create record for each provider that completed procedures
                                            ProvidersSchedule newProviderSchedule = new ProvidersSchedule();
                                            newProviderSchedule.AppointmentStatus = 0;

                                            List<DataRow> providerUnappointedProcedures = (from u in unAppointedProceduresList
                                                                                           where u.Field<long>("ProvNum").ToString() == prov
                                                                                           select u).ToList();

                                            DateTime procDate = providerUnappointedProcedures[0].Field<DateTime>("ProcDate");
                                            DateTime createDate = providerUnappointedProcedures[0].Field<DateTime>("DateTStamp");

                                            newProviderSchedule.ProviderID = (from p in Providers
                                                                              where p.PMID == prov
                                                                              select p.ID).FirstOrDefault();

                                            errorLineCount = 22;

                                            newProviderSchedule.PMPatientID = patient;
                                            newProviderSchedule.AppointmentDate = procDate;

                                            //see if existing record exists (from cloud)
                                            ProvidersSchedule existingProviderSchedule = (from ps in providersSchedulesByApptDate
                                                                                          where ps.ProviderID == newProviderSchedule.ProviderID && ps.PMPatientID == newProviderSchedule.PMPatientID && ps.AppointmentDate == newProviderSchedule.AppointmentDate && ps.PMApptID == null
                                                                                          select ps).FirstOrDefault();

                                            //if existing record exists, set new record to existing record
                                            if (existingProviderSchedule != null)
                                            {
                                                newProviderSchedule = commonData.DeepCopy(existingProviderSchedule);
                                            }

                                            newProviderSchedule.ApptBroken = false;
                                            newProviderSchedule.ApptDeleted = false;

                                            newProviderSchedule.AppointmentStatus = 0;

                                            decimal provProcAmt = 0;
                                            string provCompletedProcedures = "";
                                            int completeCount = 1;

                                            foreach (DataRow p in providerUnappointedProcedures)
                                            {
                                                provProcAmt += (decimal)p.Field<double>("ProcFee");

                                                int procCodeID = (int)p.Field<long>("CodeNum");

                                                string pc = procCodeID.ToString();

                                                if (aDACodes.ContainsKey(pc))
                                                {
                                                    if (completeCount > 1)
                                                    {
                                                        provCompletedProcedures += ",";
                                                    }
                                                    provCompletedProcedures += aDACodes[pc];

                                                    completeCount++;
                                                }
                                                else
                                                {
                                                    //we don't have the adacode??
                                                    if (completeCount > 1)
                                                    {
                                                        provCompletedProcedures += ",";
                                                    }
                                                    provCompletedProcedures += "unavailable";

                                                    completeCount++;
                                                }

                                            }

                                            errorLineCount = 23;

                                            newProviderSchedule.PMCreatedDate = createDate;
                                            newProviderSchedule.CompletedAmount = provProcAmt;
                                            newProviderSchedule.CompletedProcedures = provCompletedProcedures;

                                            //add procedures to ProvidersSchedulesProcedures objects
                                            //completed type = 2
                                            errorLineCount = 231;
                                            List<string> completedProceduresList = provCompletedProcedures.Split(',').ToList();
                                            foreach (string proc in completedProceduresList)
                                            {
                                                if (proc != null && proc != "")
                                                {
                                                    ProvidersSchedulesProcedure psp = new ProvidersSchedulesProcedure();
                                                    psp.ProcedureTypeID = 2;
                                                    psp.ProviderID = newProviderSchedule.ProviderID;
                                                    psp.PMPatientID = newProviderSchedule.PMPatientID.Value;

                                                    if (newProviderSchedule.PMApptID.HasValue)
                                                    {
                                                        psp.PMApptID = newProviderSchedule.PMApptID.Value;
                                                    }

                                                    psp.AppointmentDate = newProviderSchedule.AppointmentDate.Value;
                                                    psp.ProcedureCode = proc;
                                                    psp.InitialSnapshot = false;

                                                    providersSchedulesProceduresAddList.Add(psp);
                                                }
                                            }

                                            //accepted images/xrays taken

                                            newProviderSchedule.AcceptedPicturesTaken = 0;
                                            newProviderSchedule.AcceptedXRaysTaken = 0;

                                            //hidden images/xrays taken

                                            newProviderSchedule.HiddenPicturesTaken = 0;
                                            newProviderSchedule.HiddenXRaysTaken = 0;


                                            newProviderSchedule.DeletedPicturesTaken = 0;
                                            newProviderSchedule.DeletedXRaysTaken = 0;

                                            //treatment planner/case acceptance
                                            newProviderSchedule.PresentedAmount = 0;
                                            newProviderSchedule.AcceptedAmount = 0;
                                            newProviderSchedule.AcceptedProcedures = "";

                                            errorLineCount = 24;

                                            if (newProviderSchedule.ProviderID != 0)
                                            {
                                                if (existingProviderSchedule != null)
                                                {
                                                    errorLineCount = 25;

                                                    bool skipCompare = false;

                                                    //if initial snapshot date is null then get the snapshot
                                                    if (existingProviderSchedule.InitialSnapshotDate == null)
                                                    {
                                                        InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, null, null, providersSchedulesProceduresAddList);
                                                        skipCompare = true;
                                                    }

                                                    //closing snapshot
                                                    byte closeSnapTime = 17;
                                                    if (_appData.OfficeData.CloseSnapshotTime.HasValue == true)
                                                    {
                                                        closeSnapTime = _appData.OfficeData.CloseSnapshotTime.Value;
                                                    }

                                                    if (DateTimeOffset.Now.LocalDateTime.Hour >= closeSnapTime)
                                                    {
                                                        ClosingScheduleSnapshot(newProviderSchedule, prov, allProceduresData, plannedTreatmentData);
                                                        skipCompare = true;
                                                    }

                                                    if (skipCompare == false)
                                                    {
                                                        if (commonData.CompareProvidersSchedules(newProviderSchedule, existingProviderSchedule) == false)
                                                        {
                                                            providersScheduleUpdateList.Add(newProviderSchedule);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        providersScheduleUpdateList.Add(newProviderSchedule);
                                                    }
                                                }
                                                else
                                                {
                                                    errorLineCount = 26;

                                                    InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, null, null, providersSchedulesProceduresAddList);
                                                    ClosingScheduleSnapshot(newProviderSchedule, prov, allProceduresData, plannedTreatmentData);

                                                    providersScheduleAddList.Add(newProviderSchedule);
                                                }
                                            }
                                        }
                                    }

                                    //check for any diagnosed treatment not on appointment and process accordingly
                                    if (txDiagnosedProvidersNotOnAppointmentList.Count > 0)
                                    {
                                        foreach (string prov in txDiagnosedProvidersNotOnAppointmentList)
                                        {
                                            errorLineCount = 27;

                                            //create record for each provider that completed procedures
                                            ProvidersSchedule newProviderSchedule = new ProvidersSchedule();
                                            newProviderSchedule.CreatedDate = DateTime.Now;

                                            DateTime procDate = processingDate;
                                            DateTime createDate = processingDate;

                                            newProviderSchedule.ProviderID = (from p in Providers
                                                                              where p.PMID == prov
                                                                              select p.ID).FirstOrDefault();

                                            newProviderSchedule.PMPatientID = patient;
                                            newProviderSchedule.AppointmentDate = processingDate;

                                            //see if existing record exists (from cloud)
                                            ProvidersSchedule existingProviderSchedule = (from ps in providersSchedulesByApptDate
                                                                                          where ps.ProviderID == newProviderSchedule.ProviderID && ps.PMPatientID == newProviderSchedule.PMPatientID && ps.AppointmentDate == newProviderSchedule.AppointmentDate && ps.PMApptID == null
                                                                                          select ps).FirstOrDefault();

                                            //if existing record exists, set new record to existing record
                                            if (existingProviderSchedule != null)
                                            {
                                                newProviderSchedule = commonData.DeepCopy(existingProviderSchedule);
                                            }

                                            newProviderSchedule.ApptBroken = false;
                                            newProviderSchedule.ApptDeleted = false;

                                            newProviderSchedule.AppointmentStatus = 0;

                                            decimal provProcAmt = 0;
                                            string provCompletedProcedures = "";

                                            newProviderSchedule.PMCreatedDate = createDate;

                                            newProviderSchedule.CompletedAmount = provProcAmt;
                                            newProviderSchedule.CompletedProcedures = provCompletedProcedures;
                                            //add procedures to ProvidersSchedulesProcedures objects
                                            //completed type = 2
                                            List<string> completedProceduresList = provCompletedProcedures.Split(',').ToList();
                                            foreach (string proc in completedProceduresList)
                                            {
                                                if (proc != null && proc != "")
                                                {
                                                    ProvidersSchedulesProcedure psp = new ProvidersSchedulesProcedure();
                                                    psp.ProcedureTypeID = 2;
                                                    psp.ProviderID = newProviderSchedule.ProviderID;
                                                    psp.PMPatientID = newProviderSchedule.PMPatientID.Value;

                                                    if (newProviderSchedule.PMApptID.HasValue)
                                                    {
                                                        psp.PMApptID = newProviderSchedule.PMApptID.Value;
                                                    }

                                                    psp.AppointmentDate = newProviderSchedule.AppointmentDate.Value;
                                                    psp.ProcedureCode = proc;
                                                    psp.InitialSnapshot = false;

                                                    providersSchedulesProceduresAddList.Add(psp);
                                                }
                                            }

                                            //accepted images/xrays taken
                                            newProviderSchedule.AcceptedPicturesTaken = 0;
                                            newProviderSchedule.AcceptedXRaysTaken = 0;

                                            //hidden images/xrays taken
                                            newProviderSchedule.HiddenPicturesTaken = 0;
                                            newProviderSchedule.HiddenXRaysTaken = 0;

                                            //deleted images/xrays taken
                                            newProviderSchedule.DeletedPicturesTaken = 0;
                                            newProviderSchedule.DeletedXRaysTaken = 0;

                                            //treatment planner/case acceptance
                                            newProviderSchedule.PresentedAmount = 0;
                                            newProviderSchedule.AcceptedAmount = 0;
                                            newProviderSchedule.AcceptedProcedures = "";

                                            if (newProviderSchedule.ProviderID != 0)
                                            {
                                                if (existingProviderSchedule != null)
                                                {
                                                    bool skipCompare = false;

                                                    //if initial snapshot date is null then get the snapshot
                                                    if (existingProviderSchedule.InitialSnapshotDate == null)
                                                    {
                                                        InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, null, null, providersSchedulesProceduresAddList);
                                                        skipCompare = true;
                                                    }

                                                    //closing snapshot
                                                    byte closeSnapTime = 17;
                                                    if (_appData.OfficeData.CloseSnapshotTime.HasValue == true)
                                                    {
                                                        closeSnapTime = _appData.OfficeData.CloseSnapshotTime.Value;
                                                    }

                                                    if (DateTimeOffset.Now.TimeOfDay.Hours >= closeSnapTime)
                                                    {
                                                        ClosingScheduleSnapshot(newProviderSchedule, prov, allProceduresData, plannedTreatmentData);
                                                        skipCompare = true;
                                                    }

                                                    if (skipCompare == false)
                                                    {
                                                        if (commonData.CompareProvidersSchedules(newProviderSchedule, existingProviderSchedule) == false)
                                                        {
                                                            providersScheduleUpdateList.Add(newProviderSchedule);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        providersScheduleUpdateList.Add(newProviderSchedule);
                                                    }
                                                }
                                                else
                                                {
                                                    InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, null, null, providersSchedulesProceduresAddList);

                                                    byte closeSnapTime = 17;
                                                    if (_appData.OfficeData.CloseSnapshotTime.HasValue == true)
                                                    {
                                                        closeSnapTime = _appData.OfficeData.CloseSnapshotTime.Value;
                                                    }

                                                    if (DateTimeOffset.Now.TimeOfDay.Hours >= closeSnapTime)
                                                    {
                                                        ClosingScheduleSnapshot(newProviderSchedule, prov, allProceduresData, plannedTreatmentData);
                                                    }

                                                    providersScheduleAddList.Add(newProviderSchedule);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (providersScheduleAddList.Count >= 100)
                                {
                                    var json = JsonConvert.SerializeObject(providersScheduleAddList, Newtonsoft.Json.Formatting.Indented);

                                    try
                                    {
                                        UpdateProvidersSchedulesDataRequest request = new UpdateProvidersSchedulesDataRequest(_appData.OfficeKey, json);
                                        UpdateProvidersSchedulesDataResponse response = new UpdateProvidersSchedulesDataResponse();

                                        using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                        {
                                            response = sc.UpdateProvidersSchedulesData(request);
                                            bool result = response.UpdateProvidersSchedulesDataResult;
                                        }

                                        json = null;
                                        providersScheduleAddList.Clear();

                                    }
                                    catch (Exception ex)
                                    {
                                        try
                                        {
                                            errorLog.LogError(_appData.OfficeKeyGuid, ex);
                                        }
                                        catch { }
                                    }
                                }

                                if (providersScheduleUpdateList.Count >= 100)
                                {
                                    var json = JsonConvert.SerializeObject(providersScheduleUpdateList, Newtonsoft.Json.Formatting.Indented);

                                    try
                                    {
                                        UpdateModifiedProvidersSchedulesRequest request = new UpdateModifiedProvidersSchedulesRequest(_appData.OfficeKey, json);
                                        UpdateModifiedProvidersSchedulesResponse response = new UpdateModifiedProvidersSchedulesResponse();

                                        using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                        {
                                            response = sc.UpdateModifiedProvidersSchedules(request);
                                            bool result = response.UpdateModifiedProvidersSchedulesResult;
                                        }

                                        json = null;
                                        providersScheduleUpdateList.Clear();

                                    }
                                    catch (Exception ex)
                                    {
                                        try
                                        {
                                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -2);
                                        }
                                        catch { }
                                    }
                                }

                                if (providersSchedulesProceduresAddList.Count >= 100)
                                {
                                    var json = JsonConvert.SerializeObject(providersSchedulesProceduresAddList, Newtonsoft.Json.Formatting.Indented);

                                    try
                                    {
                                        BulkUploadProvidersSchedulesProceduresDataRequest request = new BulkUploadProvidersSchedulesProceduresDataRequest(_appData.OfficeKey, json);
                                        BulkUploadProvidersSchedulesProceduresDataResponse response = new BulkUploadProvidersSchedulesProceduresDataResponse();

                                        using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                        {
                                            response = sc.BulkUploadProvidersSchedulesProceduresData(request);
                                            bool result = response.BulkUploadProvidersSchedulesProceduresDataResult;
                                        }

                                        json = null;
                                        providersSchedulesProceduresAddList.Clear();

                                    }
                                    catch (Exception ex)
                                    {
                                        try
                                        {
                                            errorLog.LogError(_appData.OfficeKeyGuid, ex);
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }

                        if (providersScheduleAddList.Count > 0)
                        {
                            var json = JsonConvert.SerializeObject(providersScheduleAddList, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                UpdateProvidersSchedulesDataRequest request = new UpdateProvidersSchedulesDataRequest(_appData.OfficeKey, json);
                                UpdateProvidersSchedulesDataResponse response = new UpdateProvidersSchedulesDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.UpdateProvidersSchedulesData(request);
                                    bool result = response.UpdateProvidersSchedulesDataResult;
                                }

                                json = null;
                                providersScheduleAddList.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                                }
                                catch { }
                            }
                        }
                        if (providersScheduleUpdateList.Count > 0)
                        {
                            var json = JsonConvert.SerializeObject(providersScheduleUpdateList, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                UpdateModifiedProvidersSchedulesRequest request = new UpdateModifiedProvidersSchedulesRequest(_appData.OfficeKey, json);
                                UpdateModifiedProvidersSchedulesResponse response = new UpdateModifiedProvidersSchedulesResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.UpdateModifiedProvidersSchedules(request);
                                    bool result = response.UpdateModifiedProvidersSchedulesResult;
                                }

                                json = null;
                                providersScheduleUpdateList.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -4);
                                }
                                catch { }
                            }
                        }
                        if (providersSchedulesProceduresAddList.Count > 0)
                        {
                            var json = JsonConvert.SerializeObject(providersSchedulesProceduresAddList, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                BulkUploadProvidersSchedulesProceduresDataRequest request = new BulkUploadProvidersSchedulesProceduresDataRequest(_appData.OfficeKey, json);
                                BulkUploadProvidersSchedulesProceduresDataResponse response = new BulkUploadProvidersSchedulesProceduresDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.BulkUploadProvidersSchedulesProceduresData(request);
                                    bool result = response.BulkUploadProvidersSchedulesProceduresDataResult;
                                }

                                json = null;
                                providersSchedulesProceduresAddList.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                                }
                                catch { }
                            }
                        }
                    }
                    #endregion

                    #region no refresh today's appointment
                    if (processAppointments == true)
                    {
                        errorLineCount = 28;
                        DateTimeOffset today = new DateTimeOffset(DateTime.Today);
                        //for (DateTime processingDate = ProcessDate; processingDate.Date <= DateTime.Today; processingDate = processingDate.AddDays(1))
                        //{
                        string formattedProcessDateToday = FormatExtractionDate(today.DateTime);


                        //get all appointments/procedures for the day
                        appointmentsData = DentalData(
                            "Select a.PatNum as PatNum, a.ProvNum as ProvNum, a.ProvHyg As ProvHyg, a.IsHygiene As IsHygiene, a.AptNum as AptNum, a.AptStatus as AptStatus," +
                           "a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed,DATE(a.AptDateTime) as AptDateTime, a.DateTStamp as DateTStamp " +
                           "From appointment a " +
                           "Where DATE(AptDateTime) = DATE('" + formattedProcessDateToday + "') And ( a.AptStatus <> 5 ) ");

                        procedureLogPaymentData = DentalData("Select AptNum, SUM(ProcFee) AS 'SumProcFee' From procedurelog Where DATE(ProcDate) = DATE('" + formattedProcessDateToday + "') Group By AptNum");

                        errorLineCount = 281;
                        //from the cloud db, get list of appointments we've already gathered
                        List<ProvidersSchedule> providersSchedulesByApptDate = (from ps in existingProvScheduleList
                                                                                where ps.AppointmentDate == today.Date
                                                                                select ps).ToList();

                        //was appointment broken or deleted?
                        foreach (ProvidersSchedule ps in providersSchedulesByApptDate)
                        {
                            errorLineCount = 282;
                            bool apptStillValid = true;

                            if (ps.PMApptID.HasValue == true)
                            {
                                apptStillValid = (from a in appointmentsData.AsEnumerable()
                                                  where a.Field<long>("AptNum") == ps.PMApptID.Value
                                                  select a).Any();
                            }

                            errorLineCount = 283;

                            //false = appt is either broken or deleted
                            if (apptStillValid == false)
                            {
                                //check to see if appt was broken or deleted
                                errorLineCount = 284;

                                DataTable apptData = DentalData("Select a.AptStatus as AptStatus " +
                                   "From appointment a Where AptNum = " + ps.PMApptID.Value.ToString());

                                errorLineCount = 285;

                                long? aptStatus = (long?)(from a in apptData.AsEnumerable()
                                                          select a.Field<byte>("AptStatus")).FirstOrDefault();

                                errorLineCount = 286;
                                //if we have no records then the appointment was deleted; confirm by looking up history?
                                if (aptStatus == null || aptStatus == 6)
                                {
                                    //appointment was deleted
                                    ps.ApptDeleted = true;
                                    providersScheduleUpdateList.Add(ps);
                                }
                                if (aptStatus == 5 || aptStatus == 3)
                                {
                                    //appointment was broken mark appropriately
                                    ps.ApptBroken = true;
                                    providersScheduleUpdateList.Add(ps);
                                }
                            }
                        }

                        errorLineCount = 29;

                        //get unique list of patients seen for the day (patients may have more than 1 appointment)
                        List<long> patientsSeen = (from a in appointmentsData.AsEnumerable()
                                                   select a.Field<long>("PatNum")).Distinct().ToList();


                        foreach (int patient in patientsSeen)
                        {
                            //get all appointments & procedures for each patient
                            List<DataRow> patientAppointments = (from a in appointmentsData.AsEnumerable()
                                                                 where a.Field<long>("PatNum") == patient
                                                                 select a).ToList();


                            allProceduresData.Clear();
                            allProceduresData = null;
                            allProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp,DateTP From procedurelog Where PatNum = " + patient); // + " AND DATE(ProcDate) >= DATE('" + formattedProcessDateToday + "')");

                            completedProceduresData.Clear();
                            completedProceduresData = null;
                            completedProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp From procedurelog Where ProcStatus = 2 And PatNum = " + patient + " AND DATE(ProcDate) = DATE('" + formattedProcessDateToday + "')");

                            plannedTreatmentData.Clear();
                            plannedTreatmentData = null;
                            //plannedTreatmentData = DentalData("Select procid, proccodeid, amt, provid, procdate, createdate From admin.fullproclog Where PatNum = '" + patient + "' And chartstatus = 105");
                            plannedTreatmentData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp,DateTP From procedurelog Where ProcStatus = 1 And PatNum = " + patient);

                            //future appointments
                            patientFutureAppointmentsData.Clear();
                            patientFutureAppointmentsData = null;
                            patientFutureAppointmentsData = DentalData("Select a.AptNum as AptNum, a.PatNum as PatNum, a.ProvNum as ProvNum, a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed, a.DateTStamp as DateTStamp,DATE(a.AptDateTime) as AptDateTime From appointment a " +
                            "Where DATE(AptDateTime) > DATE('" + formattedProcessDateToday + "') And PatNum = " + patient);

                            //create list for patient procedures
                            List<long> allAppointmentsProceduresList = new List<long>();

                            //create list for all completed procedures
                            List<long> allCompletedProceduresList = (from c in completedProceduresData.AsEnumerable()
                                                                     select c.Field<long>("ProcNum")).ToList();

                            List<string> txDiagnosedProvidersNotOnAppointmentList = (from pt in plannedTreatmentData.AsEnumerable()
                                                                                     where pt.Field<DateTime>("ProcDate") == today.Date
                                                                                     select pt.Field<long>("ProvNum").ToString()).Distinct().ToList();

                            errorLineCount = 30;

                            //process each appointment
                            foreach (var appointment in patientAppointments.AsEnumerable())
                            {
                                if (!processedAppointments.Contains(appointment.Field<long>("AptNum")))
                                {
                                    var aptNum = appointment.Field<long>("AptNum");
                                    processedAppointments.Add(appointment.Field<long>("AptNum"));

                                    List<long> appointmentProceduresList = new List<long>();
                                    DateTime aptDateTime = appointment.Field<DateTime>("AptDateTime");
                                    string formattedAptDateTime = FormatExtractionDate(appointment.Field<DateTime>("AptDateTime"));
                                    int patNum = (int)appointment.Field<long>("PatNum");
                                    string appointedProvider = appointment.Field<long>("ProvNum").ToString();

                                    //check if appointment is a hygiene appointment, if so change to ProvHyg
                                    byte isHygieneAppt = appointment.Field<byte>("IsHygiene");
                                    if (isHygieneAppt == 1)
                                    {
                                        appointedProvider = appointment.Field<long>("ProvHyg").ToString();
                                    }

                                    string appointedProcedures = "";
                                    //string addlProvider = appointment.Field<string>("addlprov");

                                    //remove providers from txDiagnosedList
                                    if (txDiagnosedProvidersNotOnAppointmentList.Contains(appointedProvider))
                                    {
                                        txDiagnosedProvidersNotOnAppointmentList.Remove(appointedProvider);
                                    }

                                    //create list for all procedures associated with this aptNum
                                    List<long> completedProceduresForThisAptNumList = (from c in completedProceduresData.AsEnumerable()
                                                                                       where c.Field<long>("AptNum") == aptNum
                                                                                       select c.Field<long>("ProcNum")).ToList();

                                    //create list for all procedures associated with this aptNum
                                    List<long> allProceduresForThisAptNumList = (from c in allProceduresData.AsEnumerable()
                                                                                 where c.Field<long>("AptNum") == aptNum
                                                                                 select c.Field<long>("ProcNum")).ToList();

                                    errorLineCount = 31;

                                    //if completed procedure count is 0, look for specific procid's
                                    if (completedProceduresData.Rows.Count == 0)
                                    {
                                        string procNums = "";

                                        foreach (var procNum in allProceduresForThisAptNumList.AsEnumerable())
                                        {
                                            if (procNum != 0)
                                            {
                                                procNums += procNum + ",";
                                            }
                                        }

                                        if (procNums != "" && procNums.Length > 1)
                                        {
                                            procNums = procNums.Remove(procNums.Length - 1, 1);

                                            completedProceduresData.Clear();
                                            completedProceduresData = null;
                                            //completedProceduresData = DentalData("Select procid, proccodeid, amt, provid, procdate, createdate From admin.fullproclog Where PatNum = '" + patient + "' And chartstatus = 102 And procid In (" + procCodeIds + ")");
                                            completedProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp From procedurelog Where ProcStatus = 2 And PatNum = " + patient + " AND CodeNum In (" + procNums + ")");

                                            allCompletedProceduresList.Clear();
                                            allCompletedProceduresList = (from c in completedProceduresData.AsEnumerable()
                                                                          select c.Field<long>("ProcNum")).ToList();
                                        }
                                    }

                                    //get appointed procedures

                                    errorLineCount = 32;

                                    int i = 1;
                                    foreach (var procNum in allProceduresForThisAptNumList.AsEnumerable())
                                    {
                                        //int procCode = appointment.Field<long>("codeid" + i);

                                        if (procNum != 0)
                                        {
                                            int proc = (int)(from p in completedProceduresData.AsEnumerable()
                                                             where p.Field<long>("ProcNum") == procNum
                                                             select p.Field<long>("ProcNum")).FirstOrDefault();

                                            //see if procedure record exists for the appointment scheduled procedures
                                            if (proc != 0)
                                            {
                                                appointmentProceduresList.Add(proc);

                                                //is procedure still appointed to original provider?
                                                bool originalProvider = (from p in completedProceduresData.AsEnumerable()
                                                                         where p.Field<long>("ProcNum") == proc && p.Field<long>("ProvNum").ToString() == appointedProvider
                                                                         select p).Any();

                                                if (originalProvider == true)
                                                {
                                                    allAppointmentsProceduresList.Add(proc);
                                                }

                                                int procCodeID = (int)(from p in completedProceduresData.AsEnumerable()
                                                                       where p.Field<long>("ProcNum") == procNum
                                                                       select p.Field<long>("CodeNum")).FirstOrDefault();

                                                string pc = procCodeID.ToString();

                                                if (aDACodes.ContainsKey(pc))
                                                {
                                                    if (i > 1)
                                                    {
                                                        appointedProcedures += ",";
                                                    }
                                                    appointedProcedures += aDACodes[pc];
                                                }
                                                else
                                                {
                                                    //we don't have the adacode??
                                                    if (i > 1)
                                                    {
                                                        appointedProcedures += ",";
                                                    }
                                                    appointedProcedures += "unavailable";
                                                }
                                            }
                                            else
                                            {
                                                errorLineCount = 33;

                                                //check treatment planned procedures
                                                int txProc = (int)(from p in plannedTreatmentData.AsEnumerable()
                                                                   where p.Field<long>("ProcNum") == procNum
                                                                   select p.Field<long>("ProcNum")).FirstOrDefault();

                                                //see if procedure record exists for the appointment scheduled procedures
                                                if (txProc != 0)
                                                {
                                                    allAppointmentsProceduresList.Add(txProc);
                                                    appointmentProceduresList.Add(txProc);

                                                    int procCodeID = (int)(from p in plannedTreatmentData.AsEnumerable()
                                                                           where p.Field<long>("ProcNum") == procNum
                                                                           select p.Field<long>("CodeNum")).FirstOrDefault();

                                                    string txpc = procCodeID.ToString();

                                                    if (aDACodes.ContainsKey(txpc))
                                                    {
                                                        if (i > 1)
                                                        {
                                                            appointedProcedures += ",";
                                                        }
                                                        appointedProcedures += aDACodes[txpc];
                                                    }
                                                    else
                                                    {
                                                        //we don't have the adacode??
                                                        if (i > 1)
                                                        {
                                                            appointedProcedures += ",";
                                                        }
                                                        appointedProcedures += "unavailable";
                                                    }
                                                }
                                                else
                                                {
                                                    errorLineCount = 34;
                                                    string pc = procNum.ToString();

                                                    if (aDACodes.ContainsKey(pc))
                                                    {
                                                        if (i > 1)
                                                        {
                                                            appointedProcedures += ",";
                                                        }
                                                        appointedProcedures += aDACodes[pc];
                                                    }
                                                }
                                            }
                                        }
                                        i++;
                                    }


                                    errorLineCount = 35;
                                    //create record for each scheduled appointment
                                    ProvidersSchedule newProviderSchedule = new ProvidersSchedule();
                                    newProviderSchedule.CreatedDate = DateTime.Now;
                                    newProviderSchedule.PMApptID = (int)appointment.Field<long>("AptNum");

                                    //see if existing record exists (from cloud)
                                    ProvidersSchedule existingProviderSchedule = (from ps in providersSchedulesByApptDate
                                                                                  where ps.PMApptID == newProviderSchedule.PMApptID
                                                                                  select ps).FirstOrDefault();
                                    //if existing record exists, set new record to existing record
                                    if (existingProviderSchedule != null)
                                    {
                                        newProviderSchedule = commonData.DeepCopy(existingProviderSchedule);
                                    }

                                    newProviderSchedule.ApptBroken = false;
                                    newProviderSchedule.ApptDeleted = false;
                                    //newProviderSchedule.AppointedTime = (int)appointment.Field<long>("apptlen");


                                    newProviderSchedule.ProviderID = (from p in Providers
                                                                      where p.PMID == appointedProvider
                                                                      select p.ID).FirstOrDefault();

                                    newProviderSchedule.PMPatientID = patNum;
                                    newProviderSchedule.AppointmentDate = aptDateTime;

                                    if (newProviderSchedule.PMCreatedDate == null)
                                    {
                                        newProviderSchedule.PMCreatedDate = appointment.Field<DateTime?>("DateTStamp");
                                    }
                                    newProviderSchedule.PMModifiedDate = appointment.Field<DateTime?>("DateTStamp");

                                    if (appointment.Field<byte?>("AptStatus") != null)
                                    {
                                        if (appointment.Field<byte>("AptStatus") == 2)
                                        {
                                            newProviderSchedule.AppointmentStatus = 1;
                                        }
                                        else
                                        {
                                            newProviderSchedule.AppointmentStatus = 0;
                                        }
                                        // newProviderSchedule.AppointmentStatus = commonData.AppointmentStatus(appointment.Field<byte>("AptStatus"));

                                    }
                                    else
                                    {
                                        newProviderSchedule.AppointmentStatus = -2;
                                    }

                                    decimal provProcAmt = 0;
                                    string provCompletedProcedures = "";
                                    int completeCount = 1;

                                    errorLineCount = 36;

                                    foreach (int proc in appointmentProceduresList)
                                    {
                                        //is procedure still appointed to original provider?
                                        bool originalProvider = (from p in completedProceduresData.AsEnumerable()
                                                                 where p.Field<long>("ProcNum") == proc && p.Field<long>("ProvNum").ToString() == appointedProvider
                                                                 select p).Any();

                                        if (originalProvider == true)
                                        {
                                            provProcAmt += (from c in completedProceduresData.AsEnumerable()
                                                            where c.Field<long>("ProcNum") == proc
                                                            select (decimal)c.Field<double>("ProcFee")).DefaultIfEmpty(0).FirstOrDefault();

                                            int procCodeID = (int)(from p in completedProceduresData.AsEnumerable()
                                                                   where p.Field<long>("ProcNum") == proc
                                                                   select p.Field<long>("CodeNum")).FirstOrDefault();

                                            errorLineCount = 37;

                                            string pc = procCodeID.ToString();

                                            if (aDACodes.ContainsKey(pc))
                                            {
                                                if (completeCount > 1)
                                                {
                                                    provCompletedProcedures += ",";
                                                }
                                                provCompletedProcedures += aDACodes[pc];

                                                completeCount++;
                                            }
                                            else
                                            {
                                                //we don't have the adacode??
                                                if (completeCount > 1)
                                                {
                                                    provCompletedProcedures += ",";
                                                }
                                                provCompletedProcedures += "unavailable";

                                                completeCount++;
                                            }
                                        }
                                    }

                                    newProviderSchedule.CompletedAmount = provProcAmt;
                                    newProviderSchedule.CompletedProcedures = provCompletedProcedures;

                                    //add procedures to ProvidersSchedulesProcedures objects
                                    //completed type = 2
                                    List<string> completedProceduresList = provCompletedProcedures.Split(',').ToList();
                                    foreach (string proc in completedProceduresList)
                                    {
                                        if (proc != null && proc != "")
                                        {
                                            ProvidersSchedulesProcedure psp = new ProvidersSchedulesProcedure();
                                            psp.ProcedureTypeID = 2;
                                            psp.ProviderID = newProviderSchedule.ProviderID;
                                            psp.PMPatientID = newProviderSchedule.PMPatientID.Value;

                                            if (newProviderSchedule.PMApptID.HasValue)
                                            {
                                                psp.PMApptID = newProviderSchedule.PMApptID.Value;
                                            }

                                            psp.AppointmentDate = newProviderSchedule.AppointmentDate.Value;
                                            psp.ProcedureCode = proc;
                                            psp.InitialSnapshot = false;

                                            providersSchedulesProceduresAddList.Add(psp);
                                        }
                                    }

                                    errorLineCount = 38;

                                    //accepted images/xrays taken

                                    newProviderSchedule.AcceptedPicturesTaken = 0;
                                    newProviderSchedule.AcceptedXRaysTaken = 0;

                                    //hidden images/xrays taken

                                    newProviderSchedule.HiddenPicturesTaken = 0;
                                    newProviderSchedule.HiddenXRaysTaken = 0;

                                    //deleted images/xrays taken

                                    newProviderSchedule.DeletedPicturesTaken = 0;
                                    newProviderSchedule.DeletedXRaysTaken = 0;

                                    //treatment planner/case acceptance
                                    decimal presented = 0;
                                    decimal accepted = 0;
                                    string txAccepted = "";
                                    int txCount = 1;
                                    bool examPerformed = false;

                                    //get list of completed adacodes
                                    foreach (DataRow cp in completedProceduresData.Rows)
                                    {
                                        int procCodeID = (int)cp.Field<long>("CodeNum");
                                        string pc = procCodeID.ToString();

                                        if (aDACodes.ContainsKey(pc))
                                        {
                                            if (commonData.ExamCodes.Contains(aDACodes[pc]))
                                            {
                                                examPerformed = true;
                                                newProviderSchedule.ExamPerformed = true;
                                                break;
                                            }
                                        }
                                    }


                                    errorLineCount = 39;
                                    //if (examPerformed == true)
                                    //{
                                    presented = (from p in plannedTreatmentData.AsEnumerable()
                                                 select (decimal)p.Field<double>("ProcFee")).DefaultIfEmpty(0).Sum();

                                    Dictionary<long, decimal> plannedTreatmentsDict = new Dictionary<long, decimal>();

                                    foreach (DataRow pt in plannedTreatmentData.AsEnumerable())
                                    {
                                        if (!plannedTreatmentsDict.ContainsKey(pt.Field<long>("ProcNum")))
                                        {
                                            plannedTreatmentsDict.Add(pt.Field<long>("ProcNum"), (decimal)pt.Field<double>("ProcFee"));
                                        }
                                    }

                                    //look at future appointments for accepted planned treatment
                                    foreach (DataRow pt in patientFutureAppointmentsData.AsEnumerable())
                                    {
                                        var aptNum4 = pt.Field<long>("AptNum");
                                        List<long> allProceduresForThisAptNumList4 = (from c in plannedTreatmentData.AsEnumerable() //allProceduresData
                                                                                      where c.Field<long>("AptNum") == aptNum4
                                                                                      select c.Field<long>("ProcNum")).ToList();
                                        foreach (var procNum in allProceduresForThisAptNumList4.AsEnumerable())
                                        {
                                            if (procNum != 0)
                                            {
                                                if (plannedTreatmentsDict.ContainsKey(procNum))
                                                {
                                                    //treament was accepted!
                                                    accepted += plannedTreatmentsDict[procNum];

                                                    int procCodeID = (int)(from p in plannedTreatmentData.AsEnumerable()
                                                                           where p.Field<long>("ProcNum") == procNum
                                                                           select p.Field<long>("CodeNum")).FirstOrDefault();

                                                    string pc = procCodeID.ToString();

                                                    if (aDACodes.ContainsKey(pc))
                                                    {
                                                        if (txCount > 1)
                                                        {
                                                            txAccepted += ",";
                                                        }
                                                        txAccepted += aDACodes[pc];

                                                        txCount++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //}

                                    errorLineCount = 40;

                                    newProviderSchedule.PresentedAmount = presented;
                                    newProviderSchedule.AcceptedAmount = accepted;
                                    newProviderSchedule.AcceptedProcedures = txAccepted;

                                    //errorLineCount = 41;

                                    // Diagnosed treatment - for appointed provider
                                    //List < DataRow > txDiagnosedProvidersNotOnAppointmentDataRowList = (from pt in plannedTreatmentData.AsEnumerable()
                                    //                                                                    select pt).ToList();

                                    // if (txDiagnosedProvidersNotOnAppointmentDataRowList.Count > 0)
                                    // {
                                    //     DataTable txDiagnosedProvidersNotOnAppointmentData = txDiagnosedProvidersNotOnAppointmentDataRowList.CopyToDataTable();

                                    //     newProviderSchedule.DiagnosedTxAmount = (decimal)GetPatientDiagnosedTreatmentAmount(txDiagnosedProvidersNotOnAppointmentData);
                                    //     newProviderSchedule.DiagnosedTxScheduled = (decimal)GetPatientDiagnosedTreatmentScheduled(txDiagnosedProvidersNotOnAppointmentData);
                                    // }

                                    // Pending treatment - agnostic of who original provider was
                                    //List<DataRow> txPendingProvidersNotOnAppointmentDataRowList = (from pt in plannedTreatmentData.AsEnumerable()
                                    //                                                               where pt.Field<DateTime>("ProcDate") < today.Date
                                    //                                                               select pt).ToList();

                                    //if (txPendingProvidersNotOnAppointmentDataRowList.Count > 0)
                                    //{
                                    //    DataTable txPendingProvidersNotOnAppointmentData = txPendingProvidersNotOnAppointmentDataRowList.CopyToDataTable();

                                    //    GetPatientPendingTreatmentScheduled(newProviderSchedule, allProceduresData);
                                    //}

                                    errorLineCount = 42;

                                    //Snapshot check - Today's appointment
                                    if (newProviderSchedule.ProviderID != 0)
                                    {
                                        if (existingProviderSchedule != null)
                                        {
                                            bool skipCompare = false;

                                            //if initial snapshot date is null then get the snapshot
                                            if (existingProviderSchedule.InitialSnapshotDate == null)
                                            {
                                                InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, appointment, appointedProcedures, providersSchedulesProceduresAddList);
                                                skipCompare = true;
                                            }

                                            //closing snapshot
                                            byte closeSnapTime = 17;
                                            if (_appData.OfficeData.CloseSnapshotTime.HasValue == true)
                                            {
                                                closeSnapTime = _appData.OfficeData.CloseSnapshotTime.Value;
                                            }

                                            if (DateTimeOffset.Now.TimeOfDay.Hours >= closeSnapTime)
                                            {
                                                ClosingScheduleSnapshot(newProviderSchedule, appointedProvider, allProceduresData, plannedTreatmentData);
                                                skipCompare = true;
                                            }

                                            if (skipCompare == false)
                                            {
                                                if (commonData.CompareProvidersSchedules(newProviderSchedule, existingProviderSchedule) == false)
                                                {
                                                    providersScheduleUpdateList.Add(newProviderSchedule);
                                                }
                                            }
                                            else
                                            {
                                                providersScheduleUpdateList.Add(newProviderSchedule);
                                            }
                                        }
                                        else
                                        {

                                            errorLineCount = 43;
                                            InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, appointment, appointedProcedures, providersSchedulesProceduresAddList);

                                            byte closeSnapTime = 17;
                                            if (_appData.OfficeData.CloseSnapshotTime.HasValue == true)
                                            {
                                                closeSnapTime = _appData.OfficeData.CloseSnapshotTime.Value;
                                            }

                                            if (DateTimeOffset.Now.TimeOfDay.Hours >= closeSnapTime)
                                            {
                                                ClosingScheduleSnapshot(newProviderSchedule, appointedProvider, allProceduresData, plannedTreatmentData);
                                            }

                                            providersScheduleAddList.Add(newProviderSchedule);
                                        }
                                    }
                                }

                                errorLineCount = 44;

                                //check for any unappointed procedures and process accordingly
                                List<long> unAppointedProcedures = allCompletedProceduresList.Except(allAppointmentsProceduresList).ToList();

                                if (unAppointedProcedures.Count > 0)
                                {
                                    List<DataRow> unAppointedProceduresList = (from c in completedProceduresData.AsEnumerable()
                                                                               where unAppointedProcedures.Contains(c.Field<long>("ProcNum"))
                                                                               select c).ToList();

                                    List<string> unAppointedProviders = (from u in unAppointedProceduresList
                                                                         select u.Field<long>("ProvNum").ToString()).Distinct().ToList();

                                    foreach (string prov in unAppointedProviders)
                                    {
                                        //create record for each provider that completed procedures
                                        ProvidersSchedule newProviderSchedule = new ProvidersSchedule();
                                        newProviderSchedule.CreatedDate = DateTime.Now;

                                        newProviderSchedule.AppointmentStatus = 0;

                                        List<DataRow> providerUnappointedProcedures = (from u in unAppointedProceduresList
                                                                                       where u.Field<long>("ProvNum").ToString() == prov
                                                                                       select u).ToList();

                                        DateTime procDate = providerUnappointedProcedures[0].Field<DateTime>("ProcDate");
                                        DateTime createDate = providerUnappointedProcedures[0].Field<DateTime>("DateTStamp");

                                        newProviderSchedule.ProviderID = (from p in Providers
                                                                          where p.PMID == prov
                                                                          select p.ID).FirstOrDefault();

                                        newProviderSchedule.PMPatientID = patient;
                                        newProviderSchedule.AppointmentDate = procDate;

                                        //see if existing record exists (from cloud)
                                        ProvidersSchedule existingProviderSchedule = (from ps in providersSchedulesByApptDate
                                                                                      where ps.ProviderID == newProviderSchedule.ProviderID && ps.PMPatientID == newProviderSchedule.PMPatientID && ps.AppointmentDate == newProviderSchedule.AppointmentDate && ps.PMApptID == null
                                                                                      select ps).FirstOrDefault();

                                        //if existing record exists, set new record to existing record
                                        if (existingProviderSchedule != null)
                                        {
                                            newProviderSchedule = commonData.DeepCopy(existingProviderSchedule);
                                        }

                                        newProviderSchedule.ApptBroken = false;
                                        newProviderSchedule.ApptDeleted = false;

                                        newProviderSchedule.AppointmentStatus = 0;

                                        decimal provProcAmt = 0;
                                        string provCompletedProcedures = "";
                                        int completeCount = 1;

                                        foreach (DataRow p in providerUnappointedProcedures)
                                        {
                                            provProcAmt += (decimal)p.Field<double>("ProcFee");

                                            int procCodeID = (int)p.Field<long>("CodeNum");

                                            string pc = procCodeID.ToString();

                                            if (aDACodes.ContainsKey(pc))
                                            {
                                                if (completeCount > 1)
                                                {
                                                    provCompletedProcedures += ",";
                                                }
                                                provCompletedProcedures += aDACodes[pc];

                                                completeCount++;
                                            }
                                            else
                                            {
                                                //we don't have the adacode??
                                                if (completeCount > 1)
                                                {
                                                    provCompletedProcedures += ",";
                                                }
                                                provCompletedProcedures += "unavailable";

                                                completeCount++;
                                            }

                                        }

                                        errorLineCount = 45;

                                        newProviderSchedule.PMCreatedDate = createDate;

                                        newProviderSchedule.CompletedAmount = provProcAmt;
                                        newProviderSchedule.CompletedProcedures = provCompletedProcedures;


                                        //add procedures to ProvidersSchedulesProcedures objects
                                        //completed type = 2
                                        List<string> completedProceduresList = provCompletedProcedures.Split(',').ToList();
                                        foreach (string proc in completedProceduresList)
                                        {
                                            if (proc != null && proc != "")
                                            {
                                                ProvidersSchedulesProcedure psp = new ProvidersSchedulesProcedure();
                                                psp.ProcedureTypeID = 2;
                                                psp.ProviderID = newProviderSchedule.ProviderID;
                                                psp.PMPatientID = newProviderSchedule.PMPatientID.Value;

                                                if (newProviderSchedule.PMApptID.HasValue)
                                                {
                                                    psp.PMApptID = newProviderSchedule.PMApptID.Value;
                                                }

                                                psp.AppointmentDate = newProviderSchedule.AppointmentDate.Value;
                                                psp.ProcedureCode = proc;
                                                psp.InitialSnapshot = false;

                                                providersSchedulesProceduresAddList.Add(psp);
                                            }
                                        }


                                        errorLineCount = 46;

                                        //accepted images/xrays taken
                                        newProviderSchedule.AcceptedPicturesTaken = 0;
                                        newProviderSchedule.AcceptedXRaysTaken = 0;

                                        //hidden images/xrays taken
                                        newProviderSchedule.HiddenPicturesTaken = 0;
                                        newProviderSchedule.HiddenXRaysTaken = 0;


                                        newProviderSchedule.DeletedPicturesTaken = 0;
                                        newProviderSchedule.DeletedXRaysTaken = 0;

                                        //treatment planner/case acceptance
                                        newProviderSchedule.PresentedAmount = 0;
                                        newProviderSchedule.AcceptedAmount = 0;
                                        newProviderSchedule.AcceptedProcedures = "";

                                        newProviderSchedule.CreatedDate = DateTime.Now;

                                        //List<DataRow> txDiagnosedProvidersNotOnAppointmentDataRowList = (from pt in plannedTreatmentData.AsEnumerable()
                                        //                                                                 where pt.Field<DateTime>("ProcDate") == today.Date && pt.Field<long>("ProvNum").ToString() == prov
                                        //                                                                 select pt).ToList();

                                        //if (txDiagnosedProvidersNotOnAppointmentDataRowList.Count > 0)
                                        //{
                                        //    DataTable txDiagnosedProvidersNotOnAppointmentData = txDiagnosedProvidersNotOnAppointmentDataRowList.CopyToDataTable();

                                        //    newProviderSchedule.DiagnosedTxAmount = (decimal)GetPatientDiagnosedTreatmentAmount(txDiagnosedProvidersNotOnAppointmentData);
                                        //    newProviderSchedule.DiagnosedTxScheduled = (decimal)GetPatientDiagnosedTreatmentScheduled(txDiagnosedProvidersNotOnAppointmentData);
                                        //}
                                        //List<DataRow> txPendingProvidersNotOnAppointmentDataRowList = (from pt in plannedTreatmentData.AsEnumerable()
                                        //                                                               where pt.Field<DateTime>("ProcDate") < today.Date
                                        //                                                               select pt).ToList();

                                        //errorLineCount = 47;

                                        //if (txPendingProvidersNotOnAppointmentDataRowList.Count > 0)
                                        //{
                                        //    DataTable txPendingProvidersNotOnAppointmentData = txPendingProvidersNotOnAppointmentDataRowList.CopyToDataTable();

                                        //    GetPatientPendingTreatmentScheduled(newProviderSchedule, allProceduresData);
                                        //}


                                        if (newProviderSchedule.ProviderID != 0)
                                        {
                                            if (existingProviderSchedule != null)
                                            {
                                                bool skipCompare = false;

                                                //if initial snapshot date is null then get the snapshot
                                                if (existingProviderSchedule.InitialSnapshotDate == null)
                                                {
                                                    InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, null, null, providersSchedulesProceduresAddList);
                                                    skipCompare = true;
                                                }

                                                //closing snapshot
                                                byte closeSnapTime = 17;
                                                if (_appData.OfficeData.CloseSnapshotTime.HasValue == true)
                                                {
                                                    closeSnapTime = _appData.OfficeData.CloseSnapshotTime.Value;
                                                }

                                                if (DateTimeOffset.Now.TimeOfDay.Hours >= closeSnapTime)
                                                {
                                                    ClosingScheduleSnapshot(newProviderSchedule, prov, allProceduresData, plannedTreatmentData);
                                                    skipCompare = true;
                                                }

                                                if (skipCompare == false)
                                                {
                                                    if (commonData.CompareProvidersSchedules(newProviderSchedule, existingProviderSchedule) == false)
                                                    {
                                                        providersScheduleUpdateList.Add(newProviderSchedule);
                                                    }
                                                }
                                                else
                                                {
                                                    providersScheduleUpdateList.Add(newProviderSchedule);
                                                }
                                            }
                                            else
                                            {

                                                errorLineCount = 48;
                                                InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, null, null, providersSchedulesProceduresAddList);

                                                byte closeSnapTime = 17;
                                                if (_appData.OfficeData.CloseSnapshotTime.HasValue == true)
                                                {
                                                    closeSnapTime = _appData.OfficeData.CloseSnapshotTime.Value;
                                                }

                                                if (DateTimeOffset.Now.TimeOfDay.Hours >= closeSnapTime)
                                                {
                                                    ClosingScheduleSnapshot(newProviderSchedule, prov, allProceduresData, plannedTreatmentData);
                                                }

                                                providersScheduleAddList.Add(newProviderSchedule);
                                            }
                                        }
                                    }
                                }


                                errorLineCount = 49;
                                //check for any diagnosed treatment not on appointment and process accordingly
                                if (txDiagnosedProvidersNotOnAppointmentList.Count > 0)
                                {
                                    foreach (string prov in txDiagnosedProvidersNotOnAppointmentList)
                                    {

                                        errorLineCount = 50;
                                        //create record for each provider that completed procedures
                                        ProvidersSchedule newProviderSchedule = new ProvidersSchedule();
                                        newProviderSchedule.CreatedDate = DateTime.Now;

                                        DateTime procDate = today.Date;
                                        DateTime createDate = today.Date;

                                        newProviderSchedule.ProviderID = (from p in Providers
                                                                          where p.PMID == prov
                                                                          select p.ID).FirstOrDefault();

                                        newProviderSchedule.PMPatientID = patient;
                                        newProviderSchedule.AppointmentDate = today.Date;

                                        //see if existing record exists (from cloud)
                                        ProvidersSchedule existingProviderSchedule = (from ps in providersSchedulesByApptDate
                                                                                      where ps.ProviderID == newProviderSchedule.ProviderID && ps.PMPatientID == newProviderSchedule.PMPatientID && ps.AppointmentDate == newProviderSchedule.AppointmentDate && ps.PMApptID == null
                                                                                      select ps).FirstOrDefault();

                                        //if existing record exists, set new record to existing record
                                        if (existingProviderSchedule != null)
                                        {
                                            newProviderSchedule = commonData.DeepCopy(existingProviderSchedule);
                                        }

                                        newProviderSchedule.ApptBroken = false;
                                        newProviderSchedule.ApptDeleted = false;

                                        newProviderSchedule.AppointmentStatus = 0;

                                        decimal provProcAmt = 0;
                                        string provCompletedProcedures = "";

                                        newProviderSchedule.PMCreatedDate = createDate;

                                        newProviderSchedule.CompletedAmount = provProcAmt;
                                        newProviderSchedule.CompletedProcedures = provCompletedProcedures;
                                        //add procedures to ProvidersSchedulesProcedures objects
                                        //completed type = 2
                                        List<string> completedProceduresList = provCompletedProcedures.Split(',').ToList();
                                        foreach (string proc in completedProceduresList)
                                        {
                                            if (proc != null && proc != "")
                                            {
                                                errorLineCount = 51;
                                                ProvidersSchedulesProcedure psp = new ProvidersSchedulesProcedure();
                                                psp.ProcedureTypeID = 2;
                                                psp.ProviderID = newProviderSchedule.ProviderID;
                                                psp.PMPatientID = newProviderSchedule.PMPatientID.Value;

                                                if (newProviderSchedule.PMApptID.HasValue)
                                                {
                                                    psp.PMApptID = newProviderSchedule.PMApptID.Value;
                                                }

                                                psp.AppointmentDate = newProviderSchedule.AppointmentDate.Value;
                                                psp.ProcedureCode = proc;
                                                psp.InitialSnapshot = false;

                                                providersSchedulesProceduresAddList.Add(psp);
                                            }
                                        }

                                        //accepted images/xrays taken
                                        newProviderSchedule.AcceptedPicturesTaken = 0;
                                        newProviderSchedule.AcceptedXRaysTaken = 0;

                                        //hidden images/xrays taken
                                        newProviderSchedule.HiddenPicturesTaken = 0;
                                        newProviderSchedule.HiddenXRaysTaken = 0;

                                        //deleted images/xrays taken
                                        newProviderSchedule.DeletedPicturesTaken = 0;
                                        newProviderSchedule.DeletedXRaysTaken = 0;

                                        //treatment planner/case acceptance
                                        newProviderSchedule.PresentedAmount = 0;
                                        newProviderSchedule.AcceptedAmount = 0;
                                        newProviderSchedule.AcceptedProcedures = "";

                                        //List<DataRow> txDiagnosedProvidersNotOnAppointmentDataRowList = (from pt in plannedTreatmentData.AsEnumerable()
                                        //                                                                 where pt.Field<DateTime>("ProcDate") == today.Date && pt.Field<long>("ProvNum").ToString() == prov
                                        //                                                                 select pt).ToList();

                                        //if (txDiagnosedProvidersNotOnAppointmentDataRowList.Count > 0)
                                        //{
                                        //    DataTable txDiagnosedProvidersNotOnAppointmentData = txDiagnosedProvidersNotOnAppointmentDataRowList.CopyToDataTable();

                                        //    newProviderSchedule.DiagnosedTxAmount = (decimal)GetPatientDiagnosedTreatmentAmount(txDiagnosedProvidersNotOnAppointmentData);
                                        //    newProviderSchedule.DiagnosedTxScheduled = (decimal)GetPatientDiagnosedTreatmentScheduled(txDiagnosedProvidersNotOnAppointmentData);
                                        //}
                                        //List<DataRow> txPendingProvidersNotOnAppointmentDataRowList = (from pt in plannedTreatmentData.AsEnumerable()
                                        //                                                               where pt.Field<DateTime>("ProcDate") < today.Date
                                        //                                                               select pt).ToList();

                                        //if (txPendingProvidersNotOnAppointmentDataRowList.Count > 0)
                                        //{
                                        //    DataTable txPendingProvidersNotOnAppointmentData = txPendingProvidersNotOnAppointmentDataRowList.CopyToDataTable();

                                        //    GetPatientPendingTreatmentScheduled(newProviderSchedule, allProceduresData);
                                        //}

                                        errorLineCount = 52;

                                        if (newProviderSchedule.ProviderID != 0)
                                        {
                                            if (existingProviderSchedule != null)
                                            {
                                                bool skipCompare = false;

                                                //if initial snapshot date is null then get the snapshot
                                                if (existingProviderSchedule.InitialSnapshotDate == null)
                                                {
                                                    InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, null, null, providersSchedulesProceduresAddList);
                                                    skipCompare = true;
                                                }

                                                //closing snapshot
                                                byte closeSnapTime = 17;
                                                if (_appData.OfficeData.CloseSnapshotTime.HasValue == true)
                                                {
                                                    closeSnapTime = _appData.OfficeData.CloseSnapshotTime.Value;
                                                }

                                                if (DateTimeOffset.Now.TimeOfDay.Hours >= closeSnapTime)
                                                {
                                                    ClosingScheduleSnapshot(newProviderSchedule, prov, allProceduresData, plannedTreatmentData);
                                                    skipCompare = true;
                                                }

                                                if (skipCompare == false)
                                                {
                                                    if (commonData.CompareProvidersSchedules(newProviderSchedule, existingProviderSchedule) == false)
                                                    {
                                                        providersScheduleUpdateList.Add(newProviderSchedule);
                                                    }
                                                }
                                                else
                                                {
                                                    providersScheduleUpdateList.Add(newProviderSchedule);
                                                }
                                            }
                                            else
                                            {

                                                errorLineCount = 53;
                                                InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, null, null, providersSchedulesProceduresAddList);

                                                byte closeSnapTime = 17;
                                                if (_appData.OfficeData.CloseSnapshotTime.HasValue == true)
                                                {
                                                    closeSnapTime = _appData.OfficeData.CloseSnapshotTime.Value;
                                                }

                                                if (DateTimeOffset.Now.TimeOfDay.Hours >= closeSnapTime)
                                                {
                                                    ClosingScheduleSnapshot(newProviderSchedule, prov, allProceduresData, plannedTreatmentData);
                                                }

                                                providersScheduleAddList.Add(newProviderSchedule);
                                            }
                                        }
                                    }
                                }
                            }


                            if (providersScheduleAddList.Count >= 100)
                            {
                                var json = JsonConvert.SerializeObject(providersScheduleAddList, Newtonsoft.Json.Formatting.Indented);

                                try
                                {
                                    UpdateProvidersSchedulesDataRequest request = new UpdateProvidersSchedulesDataRequest(_appData.OfficeKey, json);
                                    UpdateProvidersSchedulesDataResponse response = new UpdateProvidersSchedulesDataResponse();

                                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                    {
                                        response = sc.UpdateProvidersSchedulesData(request);
                                        bool result = response.UpdateProvidersSchedulesDataResult;
                                    }

                                    json = null;
                                    providersScheduleAddList.Clear();

                                }
                                catch (Exception ex)
                                {
                                    try
                                    {
                                        errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                                    }
                                    catch { }
                                }
                            }

                            if (providersScheduleUpdateList.Count >= 100)
                            {
                                var json = JsonConvert.SerializeObject(providersScheduleUpdateList, Newtonsoft.Json.Formatting.Indented);

                                try
                                {
                                    UpdateModifiedProvidersSchedulesRequest request = new UpdateModifiedProvidersSchedulesRequest(_appData.OfficeKey, json);
                                    UpdateModifiedProvidersSchedulesResponse response = new UpdateModifiedProvidersSchedulesResponse();

                                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                    {
                                        response = sc.UpdateModifiedProvidersSchedules(request);
                                        bool result = response.UpdateModifiedProvidersSchedulesResult;
                                    }

                                    json = null;
                                    providersScheduleUpdateList.Clear();

                                }
                                catch (Exception ex)
                                {
                                    try
                                    {
                                        errorLog.LogError(_appData.OfficeKeyGuid, ex, -2);
                                    }
                                    catch { }
                                }
                            }

                            if (providersSchedulesProceduresAddList.Count >= 100)
                            {
                                var json = JsonConvert.SerializeObject(providersSchedulesProceduresAddList, Newtonsoft.Json.Formatting.Indented);

                                try
                                {
                                    BulkUploadProvidersSchedulesProceduresDataRequest request = new BulkUploadProvidersSchedulesProceduresDataRequest(_appData.OfficeKey, json);
                                    BulkUploadProvidersSchedulesProceduresDataResponse response = new BulkUploadProvidersSchedulesProceduresDataResponse();

                                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                    {
                                        response = sc.BulkUploadProvidersSchedulesProceduresData(request);
                                        bool result = response.BulkUploadProvidersSchedulesProceduresDataResult;
                                    }

                                    json = null;
                                    providersSchedulesProceduresAddList.Clear();

                                }
                                catch (Exception ex)
                                {
                                    try
                                    {
                                        errorLog.LogError(_appData.OfficeKeyGuid, ex, -3);
                                    }
                                    catch { }
                                }
                            }
                        }


                        if (providersScheduleAddList.Count > 0)
                        {
                            var json = JsonConvert.SerializeObject(providersScheduleAddList, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                UpdateProvidersSchedulesDataRequest request = new UpdateProvidersSchedulesDataRequest(_appData.OfficeKey, json);
                                UpdateProvidersSchedulesDataResponse response = new UpdateProvidersSchedulesDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.UpdateProvidersSchedulesData(request);
                                    bool result = response.UpdateProvidersSchedulesDataResult;
                                }

                                json = null;
                                providersScheduleAddList.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -4);
                                }
                                catch { }
                            }
                        }
                        if (providersScheduleUpdateList.Count > 0)
                        {
                            var json = JsonConvert.SerializeObject(providersScheduleUpdateList, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                UpdateModifiedProvidersSchedulesRequest request = new UpdateModifiedProvidersSchedulesRequest(_appData.OfficeKey, json);
                                UpdateModifiedProvidersSchedulesResponse response = new UpdateModifiedProvidersSchedulesResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.UpdateModifiedProvidersSchedules(request);
                                    bool result = response.UpdateModifiedProvidersSchedulesResult;
                                }

                                json = null;
                                providersScheduleUpdateList.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -5);
                                }
                                catch { }
                            }
                        }

                        if (providersSchedulesProceduresAddList.Count > 0)
                        {
                            var json = JsonConvert.SerializeObject(providersSchedulesProceduresAddList, Newtonsoft.Json.Formatting.Indented);

                            try
                            {
                                BulkUploadProvidersSchedulesProceduresDataRequest request = new BulkUploadProvidersSchedulesProceduresDataRequest(_appData.OfficeKey, json);
                                BulkUploadProvidersSchedulesProceduresDataResponse response = new BulkUploadProvidersSchedulesProceduresDataResponse();

                                using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                {
                                    response = sc.BulkUploadProvidersSchedulesProceduresData(request);
                                    bool result = response.BulkUploadProvidersSchedulesProceduresDataResult;
                                }

                                json = null;
                                providersSchedulesProceduresAddList.Clear();

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -6);
                                }
                                catch { }
                            }
                        }
                    }
                    #endregion

                    #region no refresh future appointments with detail

                    DateTime futureApptDetailStartDate = DateTime.Today.AddDays(1);
                    DateTime futureApptDetailEndDate = DateTime.Today.AddDays(180);

                    //scheduled/future appointments
                    for (DateTime processingDate = futureApptDetailStartDate; processingDate.Date <= futureApptDetailEndDate; processingDate = processingDate.AddDays(1))
                    {
                        errorLineCount = 54;
                        string formattedProcessDate = FormatExtractionDate(processingDate);

                        appointmentsData = DentalData("Select a.PatNum as PatNum, a.ProvNum as ProvNum, a.ProvHyg As ProvHyg, a.IsHygiene As IsHygiene, a.AptNum as AptNum, a.AptStatus as AptStatus," +
                        "a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed,DATE(a.AptDateTime) as AptDateTime, a.DateTStamp as DateTStamp " +
                        "From appointment a " +
                        "Where DATE(AptDateTime) = DATE('" + formattedProcessDate + "') And a.AptStatus = 1");

                        //get unique list of patients seen for the day (patients may have more than 1 appointment)
                        List<long> patientsSeen1 = (from a in appointmentsData.AsEnumerable()
                                                    select a.Field<long>("PatNum")).Distinct().ToList();

                        foreach (int patient in patientsSeen1)
                        {
                            errorLineCount = 55;
                            //get all appointments & procedures for each patient
                            List<DataRow> patientAppointments = (from a in appointmentsData.AsEnumerable()
                                                                 where a.Field<long>("PatNum") == patient
                                                                 select a).ToList();

                            allProceduresData.Clear();
                            allProceduresData = null;
                            allProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp,DateTP From procedurelog Where PatNum = " + patient);

                            plannedTreatmentData.Clear();
                            plannedTreatmentData = null;
                            plannedTreatmentData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp,DateTP From procedurelog Where ProcStatus = 1 And PatNum = " + patient);

                            //create list for patient procedures
                            List<long> allAppointmentsProceduresList = new List<long>();

                            //process each appointment
                            foreach (var appointment in patientAppointments.AsEnumerable())
                            {
                                if (!processedAppointments.Contains(appointment.Field<long>("AptNum")))
                                {

                                    errorLineCount = 56;
                                    var aptNum = appointment.Field<long>("AptNum");
                                    processedAppointments.Add(appointment.Field<long>("AptNum"));

                                    List<long> appointmentProceduresList = new List<long>();
                                    DateTime aptDateTime = appointment.Field<DateTime>("AptDateTime");
                                    string formattedAptDateTime = FormatExtractionDate(appointment.Field<DateTime>("AptDateTime"));
                                    int PatNum = (int)appointment.Field<long>("PatNum");
                                    string appointedProvider = appointment.Field<long>("ProvNum").ToString();

                                    //check if appointment is a hygiene appointment, if so change to ProvHyg
                                    byte isHygieneAppt = appointment.Field<byte>("IsHygiene");
                                    if (isHygieneAppt == 1)
                                    {
                                        appointedProvider = appointment.Field<long>("ProvHyg").ToString();
                                    }

                                    string appointedProcedures = "";
                                    string addlProvider = "";

                                    //scheduled procedures
                                    List<long> apptScheduledProcedures = (from c in allProceduresData.AsEnumerable()
                                                                          where c.Field<long>("AptNum") == aptNum && c.Field<long>("PatNum") == PatNum
                                                                          select c.Field<long>("ProcNum")).ToList();

                                    foreach (long proc in apptScheduledProcedures)
                                    {
                                        string pc = proc.ToString();

                                        if (aDACodes.ContainsKey(pc))
                                        {
                                            appointedProcedures += aDACodes[pc] += ",";
                                        }
                                    }

                                    if (appointedProcedures.EndsWith(","))
                                    {
                                        appointedProcedures = appointedProcedures.TrimEnd(',');
                                    }

                                    //create record for each scheduled appointment
                                    ProvidersSchedule newProviderSchedule = new ProvidersSchedule();

                                    newProviderSchedule.ApptBroken = false;
                                    newProviderSchedule.ApptDeleted = false;
                                    //newProviderSchedule.AppointedTime = appointment.Field<Int16>("apptlen");

                                    newProviderSchedule.ProviderID = (from p in Providers
                                                                      where p.PMID == appointedProvider
                                                                      select p.ID).FirstOrDefault();

                                    newProviderSchedule.PMPatientID = PatNum;
                                    newProviderSchedule.AppointmentDate = aptDateTime;
                                    newProviderSchedule.PMCreatedDate = appointment.Field<DateTime?>("DateTStamp");
                                    newProviderSchedule.PMModifiedDate = appointment.Field<DateTime?>("DateTStamp");
                                    newProviderSchedule.PMApptID = (int)appointment.Field<long>("AptNum");

                                    errorLineCount = 60;
                                    if (appointment.Field<byte?>("AptStatus") != null)
                                    {
                                        if (appointment.Field<byte>("AptStatus") == 2)
                                        {
                                            newProviderSchedule.AppointmentStatus = 1;
                                        }
                                        else
                                        {
                                            newProviderSchedule.AppointmentStatus = 0;
                                        }
                                    }
                                    else
                                    {
                                        newProviderSchedule.AppointmentStatus = -2;
                                    }

                                    errorLineCount = 61;
                                    newProviderSchedule.CreatedDate = DateTime.Now;

                                    InitialScheduleSnapshot(newProviderSchedule, allProceduresData, plannedTreatmentData, appointment, appointedProcedures, providersSchedulesProceduresAddList);

                                    if (newProviderSchedule.ProviderID != 0)
                                    {
                                        providersScheduleAddList.Add(newProviderSchedule);
                                    }
                                }
                            }
                            if (providersScheduleAddList.Count >= 100)
                            {
                                var json = JsonConvert.SerializeObject(providersScheduleAddList, Newtonsoft.Json.Formatting.Indented);

                                try
                                {
                                    BulkUploadProvidersSchedulesDataRequest request = new BulkUploadProvidersSchedulesDataRequest(_appData.OfficeKey, json);
                                    BulkUploadProvidersSchedulesDataResponse response = new BulkUploadProvidersSchedulesDataResponse();

                                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                    {
                                        response = sc.BulkUploadProvidersSchedulesData(request);
                                        bool result = response.BulkUploadProvidersSchedulesDataResult;
                                    }

                                    json = null;
                                    providersScheduleAddList.Clear();

                                }
                                catch (Exception ex)
                                {
                                    try
                                    {
                                        errorLog.LogError(_appData.OfficeKeyGuid, ex, -7);
                                    }
                                    catch { }
                                }
                            }
                            if (providersSchedulesProceduresAddList.Count >= 100)
                            {
                                var json = JsonConvert.SerializeObject(providersSchedulesProceduresAddList, Newtonsoft.Json.Formatting.Indented);

                                try
                                {
                                    BulkUploadProvidersSchedulesProceduresDataRequest request = new BulkUploadProvidersSchedulesProceduresDataRequest(_appData.OfficeKey, json);
                                    BulkUploadProvidersSchedulesProceduresDataResponse response = new BulkUploadProvidersSchedulesProceduresDataResponse();

                                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                    {
                                        response = sc.BulkUploadProvidersSchedulesProceduresData(request);
                                        bool result = response.BulkUploadProvidersSchedulesProceduresDataResult;
                                    }

                                    json = null;
                                    providersSchedulesProceduresAddList.Clear();

                                }
                                catch (Exception ex)
                                {
                                    try
                                    {
                                        errorLog.LogError(_appData.OfficeKeyGuid, ex, -8);
                                    }
                                    catch { }
                                }
                            }
                        }
                    }
                    if (providersScheduleAddList.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersScheduleAddList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersSchedulesDataRequest request = new BulkUploadProvidersSchedulesDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersSchedulesDataResponse response = new BulkUploadProvidersSchedulesDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersSchedulesData(request);
                                bool result = response.BulkUploadProvidersSchedulesDataResult;
                            }

                            json = null;
                            providersScheduleAddList.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -9);
                            }
                            catch { }
                        }
                    }
                    if (providersSchedulesProceduresAddList.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersSchedulesProceduresAddList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersSchedulesProceduresDataRequest request = new BulkUploadProvidersSchedulesProceduresDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersSchedulesProceduresDataResponse response = new BulkUploadProvidersSchedulesProceduresDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersSchedulesProceduresData(request);
                                bool result = response.BulkUploadProvidersSchedulesProceduresDataResult;
                            }

                            json = null;
                            providersSchedulesProceduresAddList.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -10);
                            }
                            catch { }
                        }
                    }
                    #endregion

                    #region no refresh future appointments without details
                    DateTime futureApptNoDetailStartDate = DateTime.Today.AddDays(181);
                    DateTime futureApptNoDetailEndDate = DateTime.Today.AddMonths(24);

                    errorLineCount = 62;

                    //scheduled/future appointments
                    for (DateTime processingDate = futureApptNoDetailStartDate; processingDate.Date <= futureApptNoDetailEndDate; processingDate = processingDate.AddDays(1))
                    {
                        string formattedProcessDate = FormatExtractionDate(processingDate);

                        appointmentsData = DentalData("Select a.PatNum as PatNum, a.ProvNum as ProvNum, a.ProvHyg As ProvHyg, a.IsHygiene As IsHygiene, a.AptNum as AptNum, a.AptStatus as AptStatus," +
                        "a.DateTimeArrived as DateTimeArrived, a.DateTimeDismissed as DateTimeDismissed,DATE(a.AptDateTime) as AptDateTime, a.DateTStamp as DateTStamp " +
                        "From appointment a " +
                        "Where DATE(AptDateTime) = DATE('" + formattedProcessDate + "') And a.AptStatus = 1");

                        procedureLogPaymentData = DentalData("Select AptNum, ProcFee, CodeNum, PatNum, ProcNum From procedurelog Where DATE(ProcDate) = DATE('" + formattedProcessDate + "')");

                        //get unique list of patients seen for the day (patients may have more than 1 appointment)
                        List<long> patientsSeen2 = (from a in appointmentsData.AsEnumerable()
                                                    select a.Field<long>("PatNum")).Distinct().ToList();

                        foreach (int patient in patientsSeen2)
                        {

                            errorLineCount = 63;
                            //get all appointments & procedures for each patient
                            List<DataRow> patientAppointments = (from a in appointmentsData.AsEnumerable()
                                                                 where a.Field<long>("PatNum") == patient
                                                                 select a).ToList();

                            //create list for patient procedures
                            List<long> allAppointmentsProceduresList = new List<long>();

                            //process each appointment
                            foreach (var appointment in patientAppointments.AsEnumerable())
                            {

                                errorLineCount = 64;
                                if (!processedAppointments.Contains(appointment.Field<long>("AptNum")))
                                {
                                    var aptNum = appointment.Field<long>("AptNum");
                                    processedAppointments.Add(appointment.Field<long>("AptNum"));

                                    List<long> appointmentProceduresList = new List<long>();
                                    DateTime aptDateTime = appointment.Field<DateTime>("AptDateTime");
                                    string formattedAptDateTime = FormatExtractionDate(appointment.Field<DateTime>("AptDateTime"));
                                    int PatNum = (int)appointment.Field<long>("PatNum");
                                    string appointedProvider = appointment.Field<long>("ProvNum").ToString();

                                    //check if appointment is a hygiene appointment, if so change to ProvHyg
                                    byte isHygieneAppt = appointment.Field<byte>("IsHygiene");
                                    if (isHygieneAppt == 1)
                                    {
                                        appointedProvider = appointment.Field<long>("ProvHyg").ToString();
                                    }

                                    string appointedProcedures = "";
                                    string addlProvider = ""; // appointment.Field<string>("addlprov");

                                    //scheduled procedures
                                    List<long> apptScheduledProcedures = (from c in procedureLogPaymentData.AsEnumerable()
                                                                          where c.Field<long>("AptNum") == aptNum && c.Field<long>("PatNum") == PatNum
                                                                          select c.Field<long>("ProcNum")).ToList();

                                    foreach (long proc in apptScheduledProcedures)
                                    {
                                        string pc = proc.ToString();

                                        if (aDACodes.ContainsKey(pc))
                                        {
                                            appointedProcedures += aDACodes[pc] += ",";
                                        }
                                    }

                                    if (appointedProcedures.EndsWith(","))
                                    {
                                        appointedProcedures = appointedProcedures.TrimEnd(',');
                                    }

                                    errorLineCount = 65;
                                    //create record for each scheduled appointment
                                    ProvidersSchedule newProviderSchedule = new ProvidersSchedule();

                                    newProviderSchedule.ApptBroken = false;
                                    newProviderSchedule.ApptDeleted = false;
                                    //newProviderSchedule.AppointedTime = appointment.Field<Int16>("apptlen");


                                    newProviderSchedule.ProviderID = (from p in Providers
                                                                      where p.PMID == appointedProvider
                                                                      select p.ID).FirstOrDefault();

                                    newProviderSchedule.PMPatientID = PatNum;
                                    newProviderSchedule.AppointmentDate = aptDateTime;
                                    newProviderSchedule.PMCreatedDate = appointment.Field<DateTime?>("DateTStamp");
                                    newProviderSchedule.PMModifiedDate = appointment.Field<DateTime?>("DateTStamp");
                                    newProviderSchedule.PMApptID = (int)appointment.Field<long>("AptNum");
                                    if (appointment.Field<byte?>("AptStatus") != null)
                                    {
                                        if (appointment.Field<byte>("AptStatus") == 2)
                                        {
                                            newProviderSchedule.AppointmentStatus = 1;
                                        }
                                        else
                                        {
                                            newProviderSchedule.AppointmentStatus = 0;
                                        }
                                    }
                                    else
                                    {
                                        newProviderSchedule.AppointmentStatus = -2;
                                    }

                                    errorLineCount = 66;

                                    //scheduled procedures
                                    double? sumProcFee = (from p in procedureLogPaymentData.AsEnumerable()
                                                          where p.Field<long>("AptNum") == aptNum && p.Field<long>("PatNum") == PatNum
                                                          select p.Field<double?>("ProcFee")).DefaultIfEmpty(0).Sum();

                                    newProviderSchedule.ScheduledAmount = (decimal?)sumProcFee;
                                    newProviderSchedule.ScheduledProcedures = appointedProcedures;

                                    //next hygiene appointment
                                    GetPatientNextHygieneAppointment(newProviderSchedule, allProceduresData);

                                    //next doctor appointment
                                    GetPatientNextDoctorAppointment(newProviderSchedule, allProceduresData);

                                    newProviderSchedule.CreatedDate = DateTime.Now;

                                    if (newProviderSchedule.ProviderID != 0)
                                    {
                                        providersScheduleAddList.Add(newProviderSchedule);
                                    }
                                }
                            }

                            errorLineCount = 67;
                            if (providersScheduleAddList.Count >= 100)
                            {
                                var json = JsonConvert.SerializeObject(providersScheduleAddList, Newtonsoft.Json.Formatting.Indented);

                                try
                                {
                                    BulkUploadProvidersSchedulesDataRequest request = new BulkUploadProvidersSchedulesDataRequest(_appData.OfficeKey, json);
                                    BulkUploadProvidersSchedulesDataResponse response = new BulkUploadProvidersSchedulesDataResponse();

                                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                    {
                                        response = sc.BulkUploadProvidersSchedulesData(request);
                                        bool result = response.BulkUploadProvidersSchedulesDataResult;
                                    }

                                    json = null;
                                    providersScheduleAddList.Clear();

                                }
                                catch (Exception ex)
                                {
                                    try
                                    {
                                        errorLog.LogError(_appData.OfficeKeyGuid, ex, -11);
                                    }
                                    catch { }
                                }
                            }

                            if (providersSchedulesProceduresAddList.Count >= 100)
                            {
                                var json = JsonConvert.SerializeObject(providersSchedulesProceduresAddList, Newtonsoft.Json.Formatting.Indented);

                                try
                                {
                                    BulkUploadProvidersSchedulesProceduresDataRequest request = new BulkUploadProvidersSchedulesProceduresDataRequest(_appData.OfficeKey, json);
                                    BulkUploadProvidersSchedulesProceduresDataResponse response = new BulkUploadProvidersSchedulesProceduresDataResponse();

                                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                    {
                                        response = sc.BulkUploadProvidersSchedulesProceduresData(request);
                                        bool result = response.BulkUploadProvidersSchedulesProceduresDataResult;
                                    }

                                    json = null;
                                    providersSchedulesProceduresAddList.Clear();

                                }
                                catch (Exception ex)
                                {
                                    try
                                    {
                                        errorLog.LogError(_appData.OfficeKeyGuid, ex, -12);
                                    }
                                    catch { }
                                }
                            }
                        }
                    }
                    if (providersScheduleAddList.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersScheduleAddList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersSchedulesDataRequest request = new BulkUploadProvidersSchedulesDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersSchedulesDataResponse response = new BulkUploadProvidersSchedulesDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersSchedulesData(request);
                                bool result = response.BulkUploadProvidersSchedulesDataResult;
                            }

                            json = null;
                            providersScheduleAddList.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -13);
                            }
                            catch { }
                        }
                    }
                    if (providersSchedulesProceduresAddList.Count > 0)
                    {
                        var json = JsonConvert.SerializeObject(providersSchedulesProceduresAddList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadProvidersSchedulesProceduresDataRequest request = new BulkUploadProvidersSchedulesProceduresDataRequest(_appData.OfficeKey, json);
                            BulkUploadProvidersSchedulesProceduresDataResponse response = new BulkUploadProvidersSchedulesProceduresDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadProvidersSchedulesProceduresData(request);
                                bool result = response.BulkUploadProvidersSchedulesProceduresDataResult;
                            }

                            json = null;
                            providersSchedulesProceduresAddList.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -14);
                            }
                            catch { }
                        }
                    }
                    #endregion

                }
            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                }
                catch { }
            }

            if (_appData.OfficeData.ScheduleProcessRefresh == true)
            {
                _appData.OfficeData.ScheduleProcessRefresh = false;
            }
            _appData.OfficeData.ScheduleProcessDate = DateTime.Today;
            _appData.SetOfficeData();

        }

        // Straine tools functions
        internal void InitialScheduleSnapshot(ProvidersSchedule ps, DataTable allProceduresData, DataTable pendingTreatmentData, DataRow scheduledAppointment, string scheduledProcedures, List<ProvidersSchedulesProcedure> providersSchedulesProceduresAddList)
        {
            {
                int errorLineCount = 0;

                if (ps.PMPatientID.Value == 0)
                {
                    return;
                }

                try
                {
                    errorLineCount = 1;

                    //set scheduled amount and procedures
                    if (scheduledAppointment != null)
                    {
                        var aptNum = scheduledAppointment.Field<long>("AptNum");
                        double? sumProcFee = (from p in allProceduresData.AsEnumerable()
                                              where p.Field<long>("AptNum") == aptNum
                                              select p.Field<double?>("ProcFee")).DefaultIfEmpty(0).Sum();

                        ps.ScheduledAmount = (decimal?)sumProcFee;
                        ps.ScheduledProcedures = scheduledProcedures;

                        //add procedures to ProvidersSchedulesProcedures objects
                        //scheduled type = 1
                        List<string> appointedProceduresList = scheduledProcedures.Split(',').ToList();
                        foreach (string proc in appointedProceduresList)
                        {
                            if (proc != null && proc != "")
                            {
                                ProvidersSchedulesProcedure psp = new ProvidersSchedulesProcedure();
                                psp.ProcedureTypeID = 1;
                                psp.ProviderID = ps.ProviderID;
                                psp.PMPatientID = ps.PMPatientID.Value;

                                if (ps.PMApptID.HasValue)
                                {
                                    psp.PMApptID = ps.PMApptID.Value;
                                }

                                psp.AppointmentDate = ps.AppointmentDate.Value;

                                psp.ProcedureCode = proc;

                                if (ps.AppointmentDate <= DateTime.Today)
                                {
                                    psp.InitialSnapshot = true;
                                }
                                else
                                {
                                    psp.InitialSnapshot = false;
                                }

                                providersSchedulesProceduresAddList.Add(psp);
                            }
                        }
                    }

                    //setup some initial values
                    string formattedAppointmentDate = FormatExtractionDate(ps.AppointmentDate.Value);

                    //If data is needed for more than one method, gather here and pass into the method
                    //DataTable patientCompletedProceduresData = DentalData("Select fpl.provid, fpl.procdate, pc.adacode From admin.fullproclog As fpl Inner Join admin.proccode As pc On pc.proccodeid = fpl.proccodeid Where chartstatus = 102 And patid = " + ps.PMPatientID.Value.ToString() + " And procdate < {d'" + formattedAppointmentDate + "'} And pc.adacode Is Not Null Order By fpl.procdate DESC");
                    DataTable patientCompletedProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp From procedurelog " +
                        "Where ProcStatus = 2 And PatNum = " + ps.PMPatientID.Value.ToString() + " AND DATE(ProcDate) < DATE('" + formattedAppointmentDate + "') Order By ProcDate DESC");

                    //DataTable futurePatientsData = DentalData("Select apptdate, createdate, patid, provid, codeid1, codeid2, codeid3, codeid4, codeid5, codeid6, codeid7, codeid8, codeid9, codeid10, codeid11, codeid12, codeid13, codeid14, codeid15, 
                    //codeid16, codeid17, codeid18, codeid19, codeid20 From admin.appt Where patid = " + ps.PMPatientID.Value.ToString() + " And apptdate > {d'" + formattedAppointmentDate + "'}");
                    DataTable futurePatientsData = DentalData("SELECT AptNum,PatNum,AptStatus,ProvNum,AptDateTime,DateTStamp From appointment a " +
                                      " Where PatNum = " + ps.PMPatientID.Value.ToString() + " AND DATE(AptDateTime) > DATE('" + formattedAppointmentDate + "')");

                    DataTable pendingTxAppointmentsData = DentalData("SELECT AptNum,PatNum,AptStatus,ProvNum,AptDateTime,DateTStamp From appointment a " +
                                      " Where PatNum = " + ps.PMPatientID.Value.ToString() + " AND DATE(AptDateTime) >= DATE('" + formattedAppointmentDate + "')");

                    // pendingTreatmentData is function input
                    //DataTable pendingTreatmentData = DentalData("Select procid, proccodeid, amt, provid, procdate, createdate From admin.fullproclog Where patid = '" + ps.PMPatientID.Value.ToString() + "' And chartstatus = 105 And procdate < {d'" + formattedAppointmentDate + "'}");

                    //DataTable patientInfoData = DentalData("Select famid, guarid, priminsuredid, primbenefits From admin.patient Where patid = " + ps.PMPatientID.Value.ToString());
                    // famid doesn't exist in OD - familes grouped by Guarantor
                    DataTable patientInfoData = DentalData("Select Guarantor From patient Where PatNum = " + ps.PMPatientID.Value.ToString());

                    string benRemaining = "select sum(annualmax.annualmax) as annualmax,sum(used.amtused) as amountused,(case when isnull(sum(used.amtused)) then (sum(annualmax.annualmax)) else (sum(annualmax.annualmax)-sum(used.amtused)) end) as amtremaining " +
                    " from patient p " +
                    " inner join patplan on p.patnum = patplan.patnum" +
                    " inner join inssub on inssub.inssubnum = patplan.inssubnum" +
                    " inner join(" +
                        " select benefit.plannum, max(benefit.monetaryamt) as annualmax" +
                        " from benefit" +
                        " left join covcat on covcat.covcatnum = benefit.covcatnum" +
                        " where benefit.benefittype = 5 /* limitation " +
                        " and benefit.timeperiod = 2 /* calendar year */" +
                        " and(covcat.ebenefitcat = 1 or isnull(covcat.ebenefitcat))" +
                        " and benefit.monetaryamt > 0 /* (-1) indicates empty */" +
                        " group by benefit.plannum" +
                    " ) annualmax on annualmax.plannum = inssub.plannum" +
                    " left join(" +
                        " select patplan.patplannum," +
                        " sum(ifnull(claimproc.inspayamt, 0)) as amtused" +
                        " from claimproc" +
                        " inner join inssub on claimproc.inssubnum = inssub.inssubnum" +
                        " inner join patplan on inssub.inssubnum = patplan.inssubnum" +
                        "     and patplan.patnum = claimproc.patnum" +
                        " where" +
                        " year(claimproc.procdate) = year(curdate()) /*current calendar year*/" +
                        " group by patplan.patplannum" +
                    " ) used on used.patplannum = patplan.patplannum" +
                    " where" +
                    " (case when isnull(used.amtused) then(annualmax.annualmax) else(annualmax.annualmax - used.amtused) end) > .01" +
                    " and p.patstatus = 0" +
                    " and p.patnum = " + ps.PMPatientID;

                    DataTable patientBenefitRemainingData = DentalData(benRemaining);

                    errorLineCount = 2;

                    if (patientInfoData.Rows.Count > 0)
                    {
                        //int familyID = patientInfoData.Rows[0].Field<int>("famid");
                        long guarID = (int)patientInfoData.Rows[0].Field<long>("Guarantor");

                        // prim benefits is how many benefits a patient has used...end game is to find out benefits available to patient
                        //int primInsID = patientInfoData.Rows[0].Field<int>("priminsuredid");
                        //decimal primBenefitsUsed = patientInfoData.Rows[0].Field<decimal>("primbenefits");

                        errorLineCount = 3;

                        //trim adacodes
                        //foreach (DataRow procRow in patientCompletedProceduresData.AsEnumerable())
                        //{
                        //    if (procRow.Field<string>("adacode") != null)
                        //    {
                        //        procRow["adacode"] = procRow.Field<string>("adacode").Trim();
                        //    }
                        //}

                        errorLineCount = 4;

                        //Outstanding balance?
                        ps.OustandingBalance = (decimal)GetPatientBalance(guarID);

                        errorLineCount = 5;

                        //unappointed family members
                        ps.UnappointedFamilyMembers = GetPatientUnAppointedFamilyMembers(guarID, ps.PMPatientID.Value, ps.AppointmentDate.Value);  //was familyID

                        errorLineCount = 6;

                        //last BWX
                        ps.LastBWX = GetPatientLastBWX(patientCompletedProceduresData);

                        errorLineCount = 7;

                        //last FMX
                        ps.LastFMX = GetPatientLastFMX(patientCompletedProceduresData);

                        errorLineCount = 8;

                        //last Pano
                        ps.LastPano = GetPatientLastPano(patientCompletedProceduresData);

                        errorLineCount = 9;

                        //last hygiene appt
                        GetPatientLastHygieneAppointment(ps, patientCompletedProceduresData);

                        errorLineCount = 10;

                        //next hygiene appt
                        GetPatientNextHygieneAppointment(ps, allProceduresData);

                        errorLineCount = 11;

                        //last doctor appt
                        ps.LastDoctorAppt = GetPatientLastDoctorAppointment(patientCompletedProceduresData);

                        errorLineCount = 12;

                        //next doctor appt
                        GetPatientNextDoctorAppointment(ps, allProceduresData);

                        errorLineCount = 13;

                        //pending treatment
                        GetPatientPendingTreatment(ps, pendingTreatmentData);

                        errorLineCount = 14;

                        //insurance funds available
                        ps.InsuranceFundsAvailableAmount = (decimal?)GetPatientRemainingInsuranceFundsAvailable(patientBenefitRemainingData);

                        errorLineCount = 15;

                        if (ps.AppointmentDate <= DateTime.Today)
                        {
                            ps.InitialSnapshotDate = DateTimeOffset.Now.DateTime;
                        }
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                    }
                    catch { }
                }
            }
        }

        internal void ClosingScheduleSnapshot(ProvidersSchedule ps, string pmProvID, DataTable allProceduresData, DataTable pendingTreatmentData)
        {
            int errorLineCount = 0;

            if (ps.PMPatientID.Value == 0)
            {
                return;
            }

            if (ps.ClosingSnapshotDate != null)
            {
                return;
            }

            try
            {
                errorLineCount = 1;

                //setup some initial values
                string formattedAppointmentDate = FormatExtractionDate(ps.AppointmentDate.Value);

                //If data is needed for more than one method, gather here and pass into the method
                //DataTable futurePatientsData = DentalData("Select apptdate, createdate, patid, provid, codeid1, codeid2, codeid3, codeid4, codeid5, codeid6, codeid7, codeid8, codeid9, codeid10, codeid11, codeid12, codeid13, codeid14, codeid15, codeid16, codeid17, codeid18, codeid19, codeid20 From admin.appt Where patid = " + ps.PMPatientID.Value.ToString() + " And apptdate > {d'" + formattedAppointmentDate + "'}");
                DataTable futurePatientsData = DentalData("SELECT AptNum,PatNum,AptStatus,ProvNum,AptDateTime,DateTStamp From appointment a " +
                                     " Where PatNum = " + ps.PMPatientID.Value.ToString() + " AND DATE(AptDateTime) > DATE('" + formattedAppointmentDate + "')");

                //DataTable patientPaymentsData = DentalData("Select amt From admin.fullproclog Where patid = '" + ps.PMPatientID.Value.ToString() + "' And chartstatus = 90 And proclogclass = 1 And procdate = {d'" + formattedAppointmentDate + "'}");
                DataTable patientPaymentsData = DentalData("SELECT ps.SplitAmt AS 'Amt' FROM payment p INNER JOIN paysplit ps ON ps.PayNum = p.PayNum Where ps.PatNum = " + ps.PMPatientID + " AND p.PayDate = {d'" + formattedAppointmentDate + "'}");

                // Diagnosed Treatment
                // Proc Status = 1
                // Have apt num
                // not part of pending treatment list
                // Proc Date > current date (apt date being processed)
                DataTable diagnosedTreatmentData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee,ProcStatus,AptNum,ProcDate,DateTStamp,DateTP From procedurelog " +
                       " Where PatNum = " + ps.PMPatientID.Value.ToString() +
                       " AND ProcStatus = 1 " +
                       " AND DATE(DateTP) = DATE('" + formattedAppointmentDate + "')");

                errorLineCount = 2;

                ps.UnappointedFamilyMembersScheduledToday = GetPatientUnappointedFamilyMembersAppointed(ps);

                errorLineCount = 3;

                GetPatientPendingTreatmentScheduled(ps, allProceduresData);

                errorLineCount = 4;

                ps.PaymentsReceived = (decimal)GetPatientPastDueBalancesCollected(patientPaymentsData);

                errorLineCount = 5;

                ps.DiagnosedTxAmount = (decimal)GetPatientDiagnosedTreatmentAmount(diagnosedTreatmentData);

                errorLineCount = 6;

                ps.DiagnosedTxScheduled = (decimal)GetPatientDiagnosedTreatmentScheduled(diagnosedTreatmentData);
                errorLineCount = 7;

                if (ps.PMApptID != null && ps.PMApptID != 0)
                {
                    //DataTable appointmentsCreatedDayOfApptData = DentalData("Select apptdate, createdate, patid, provid, codeid1, codeid2, codeid3, codeid4, codeid5, codeid6, codeid7, 
                    //codeid8, codeid9, codeid10 From admin.appt Where patid = " + ps.PMPatientID.Value.ToString() + " And apptid <> " + ps.PMApptID.Value.ToString() + " And createdate = {d'" + formattedAppointmentDate + "'}");
                    DataTable appointmentsCreatedDayOfApptData = DentalData("SELECT AptNum,PatNum,AptStatus,ProvNum,AptDateTime,DateTStamp From appointment a " +
                                      " Where PatNum = " + ps.PMPatientID.Value.ToString() + " AND AptNum <> " + ps.PMApptID.Value.ToString() + " AND DATE(DateTStamp) = DATE('" + formattedAppointmentDate + "')");

                    //hygiene appt created on appointment date
                    GetPatientHygieneApptCreatedOnDate(ps, appointmentsCreatedDayOfApptData, diagnosedTreatmentData, allProceduresData);

                    //doctor appt created on appointment date
                    GetPatientDoctorApptCreatedOnDate(ps, appointmentsCreatedDayOfApptData, diagnosedTreatmentData, allProceduresData);
                }

                errorLineCount = 8;

                ps.PresentedAmount = (ps.PendingTxAmount + ps.DiagnosedTxAmount);
                ps.AcceptedAmount = (ps.PendingTxScheduledAmount + ps.DiagnosedTxScheduled);

                ps.ClosingSnapshotDate = DateTimeOffset.Now.DateTime;
            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                }
                catch { }
            }
        }

        internal double GetPatientBalance(long guarID)
        {
            double patientBalance = 0;

            List<DataRow> patientARList = (from g in _patientARDataTable.AsEnumerable()
                                           where g.Field<long>("Guarantor") == guarID
                                           select g).ToList();

            foreach (DataRow agingRow in patientARList)
            {
                patientBalance = (agingRow.Field<double>("Bal_0_30") + agingRow.Field<double>("Bal_31_60") + agingRow.Field<double>("Bal_61_90") + agingRow.Field<double>("BalOver90"));
            }

            return patientBalance;
        }

        internal string GetPatientUnAppointedFamilyMembers(long familyID, long patID, DateTimeOffset appointmentDate)
        {
            List<string> unAppointedFamilyList = new List<string>();
            string unAppointedFamilyMembers = null;
            string formattedProcessDate = FormatExtractionDate(appointmentDate.DateTime);

            //get all family member id's, except current patient
            //DataTable familyPatients = DentalData("Select patid From admin.patient Where famid = " + familyID.ToString() + " And patid <> " + patID.ToString());
            DataTable familyPatients = DentalData("Select PatNum From patient Where Guarantor = " + familyID.ToString() + " And PatStatus = 0 And PatNum <> " + patID.ToString());
            List<long> familyPatientsList = (from p in familyPatients.AsEnumerable()
                                             select p.Field<long>("PatNum")).Distinct().ToList();

            if (familyPatientsList.Count > 0)
            {
                string patIds = string.Join<long>(",", familyPatientsList);

                //DataTable familyAppointments = DentalData("Select PatNum From admin.appt Where PatNum In (" + patIds + ") And  apptdate > {d'" + formattedProcessDate + "'}");      
                DataTable familyAppointments = DentalData("Select PatNum From appointment Where PatNum In (" + patIds + ") And DATE(AptDateTime) >= DATE('" + formattedProcessDate + "')");

                foreach (int familyPatient in familyPatientsList)
                {
                    bool hasAppt = (from a in familyAppointments.AsEnumerable()
                                    where a.Field<long>("PatNum") == familyPatient
                                    select a).Any();

                    if (hasAppt == false)
                    {
                        if (!unAppointedFamilyList.Contains(familyPatient.ToString()))
                        {
                            unAppointedFamilyList.Add(familyPatient.ToString());
                        }
                    }
                }

                if (unAppointedFamilyList.Count > 0)
                {
                    unAppointedFamilyMembers = string.Join<string>(",", unAppointedFamilyList);
                }
            }

            return unAppointedFamilyMembers;
        }

        internal DateTime? GetPatientLastBWX(DataTable patientCompletedProcedures)
        {

            List<DataRow> completedProceduresList = (from p in patientCompletedProcedures.AsEnumerable()
                                                     select p).ToList();

            DateTime? lastBWXDate = null;
            foreach (DataRow cpRow in completedProceduresList)
            {

                long procCodeID = cpRow.Field<long>("CodeNum");

                string pc = procCodeID.ToString();

                string adaCode = "";

                if (aDACodes.ContainsKey(pc))
                {
                    adaCode = aDACodes[pc];
                    if (commonData.BWXCodes.Contains(adaCode))
                    {
                        lastBWXDate = cpRow.Field<DateTime>("ProcDate");
                        break;
                    }
                }
            }
            //DateTime? lastBWXDate = (from p in patientCompletedProcedures.AsEnumerable()
            //                     where commonData.BWXCodes.Contains(p.Field<string>("adacode"))
            //                     select p.Field<DateTime>("ProcDate")).Take(1).FirstOrDefault();
            if (lastBWXDate.HasValue)
            {
                if (lastBWXDate.Value.Year < 1900)
                {
                    lastBWXDate = null;
                }
            }

            return lastBWXDate;
        }

        internal DateTime? GetPatientLastFMX(DataTable patientCompletedProcedures)
        {
            List<DataRow> completedProceduresList = (from p in patientCompletedProcedures.AsEnumerable()
                                                     select p).ToList();

            DateTime? lastFMXDate = null;
            foreach (DataRow cpRow in completedProceduresList)
            {

                long procCodeID = cpRow.Field<long>("CodeNum");

                string pc = procCodeID.ToString();

                string adaCode = "";

                adaCode = aDACodes[pc];
                if (commonData.FMXCodes.Contains(adaCode))
                {
                    lastFMXDate = cpRow.Field<DateTime>("ProcDate");
                    break;
                }
            }
            //DateTime? lastFMXDate = (from p in patientCompletedProcedures.AsEnumerable()
            //                        where commonData.FMXCodes.Contains(p.Field<string>("adacode"))
            //                        select p.Field<DateTime>("procdate")).Take(1).FirstOrDefault();

            if (lastFMXDate.HasValue)
            {
                if (lastFMXDate.Value.Year < 1900)
                {
                    lastFMXDate = null;
                }
            }

            return lastFMXDate;
        }

        internal DateTime? GetPatientLastPano(DataTable patientCompletedProcedures)
        {
            int errorLineCount = 81;
            try
            {
                List<DataRow> completedProceduresList = (from p in patientCompletedProcedures.AsEnumerable()
                                                         select p).ToList();

                DateTime? lastPanoDate = null;
                foreach (DataRow cpRow in completedProceduresList)
                {

                    long procCodeID = cpRow.Field<long>("CodeNum");

                    string pc = procCodeID.ToString();

                    string adaCode = "";

                    errorLineCount = 82;

                    if (aDACodes.TryGetValue(pc, out adaCode))
                    {
                        errorLineCount = 83;
                        adaCode = aDACodes[pc];
                        errorLineCount = 84;
                    }

                    errorLineCount = 85;
                    if (commonData.PanoCodes.Contains(adaCode))
                    {
                        lastPanoDate = cpRow.Field<DateTime>("ProcDate");
                        break;
                    }
                }
                //DateTime? lastFMXDate = (from p in patientCompletedProcedures.AsEnumerable()
                //                         where commonData.PanoCodes.Contains(p.Field<string>("adacode"))
                //                         select p.Field<DateTime>("procdate")).Take(1).FirstOrDefault();

                if (lastPanoDate.HasValue)
                {
                    if (lastPanoDate.Value.Year < 1900)
                    {
                        lastPanoDate = null;
                    }
                }

                return lastPanoDate;
            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                    return null;
                }
                catch
                {
                    return null;
                }

            }
        }

        internal void GetPatientLastHygieneAppointment(ProvidersSchedule ps, DataTable patientCompletedProcedures)
        {
            List<DataRow> completedProceduresList = (from p in patientCompletedProcedures.AsEnumerable()
                                                     select p).ToList();

            foreach (DataRow cpRow in completedProceduresList)
            {
                long procCodeID = cpRow.Field<long>("CodeNum");

                string pc = procCodeID.ToString();

                string adaCode = "";

                if (aDACodes.ContainsKey(pc))
                {
                    adaCode = aDACodes[pc];
                }

                if (_hygieneProviders.Contains(cpRow.Field<long>("ProvNum").ToString()))
                {
                    ps.LastHygieneAppt = cpRow.Field<DateTime>("ProcDate");

                    if (adaCode.Contains("."))
                    {
                        int commaPos = adaCode.IndexOf(".");
                        adaCode = adaCode.Substring(0, commaPos);
                    }

                    adaCode = adaCode.ToUpper();

                    if (adaCode != "")
                    {
                        if (commonData.HygieneCodes.Contains(adaCode))
                        {
                            ps.LastHygieneProcedure = adaCode;
                        }
                    }

                    break;
                }
                else
                {
                    //string adaCode = cpRow.Field<string>("adacode");
                    if (adaCode.Contains("."))
                    {
                        int commaPos = adaCode.IndexOf(".");
                        adaCode = adaCode.Substring(0, commaPos);
                    }

                    adaCode = adaCode.ToUpper();

                    if (adaCode != "")
                    {
                        if (commonData.HygieneCodes.Contains(adaCode))
                        {
                            ps.LastHygieneAppt = cpRow.Field<DateTime>("ProcDate");
                            ps.LastHygieneProcedure = adaCode;

                            break;
                        }
                    }
                }
            }

            if (ps.LastHygieneProcedure == null)
            {
                foreach (DataRow cpRow in completedProceduresList)
                {
                    if (ps.LastHygieneAppt.HasValue == true)
                    {
                        if (cpRow.Field<DateTime>("ProcDate") == ps.LastHygieneAppt.Value)
                        {
                            long procCodeID = cpRow.Field<long>("CodeNum");

                            string pc = procCodeID.ToString();

                            string adaCode = "";

                            if (aDACodes.ContainsKey(pc))
                            {
                                adaCode = aDACodes[pc];
                            }

                            if (adaCode.Contains("."))
                            {
                                int commaPos = adaCode.IndexOf(".");
                                adaCode = adaCode.Substring(0, commaPos);
                            }

                            adaCode = adaCode.ToUpper();

                            if (adaCode != "")
                            {
                                if (commonData.HygieneCodes.Contains(adaCode))
                                {
                                    ps.LastHygieneProcedure = adaCode;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (ps.LastHygieneAppt.HasValue == true && ps.LastHygieneProcedure == null)
            {
                ps.LastHygieneProcedure = "Unknown";
            }
        }


        internal void GetPatientNextHygieneAppointment(ProvidersSchedule ps, DataTable allPatientProcedures)
        {
            int errorLineCount = 0;
            DateTime nextDay = ps.AppointmentDate.Value.AddDays(1);

            try
            {
                //look for future hygiene appointment
                List<DataRow> allPatientProceduresForThisDate = (from a in allPatientProcedures.AsEnumerable()
                                                                 where a.Field<DateTime>("ProcDate") >= nextDay && a.Field<long>("AptNum") > 0
                                                                 orderby a.Field<DateTime>("ProcDate") ascending
                                                                 select a).ToList();

                foreach (DataRow proc in allPatientProceduresForThisDate)
                {
                    //are any appointments for a hygiene provider?
                    string appointedProv = proc.Field<long>("ProvNum").ToString();

                    if (_hygieneProviders.Contains(appointedProv))
                    {
                        ps.NextHygieneAppt = proc.Field<DateTime>("ProcDate");
                        ps.NextHygieneApptCreated = proc.Field<DateTime>("DateTStamp");
                        break;
                    }
                    // otherwise look for proc code (CodeNum) in the recognized list of hygene codes
                    else
                    {
                        string pc = proc.Field<long>("CodeNum").ToString();

                        string adaCode = "";

                        if (aDACodes.ContainsKey(pc))
                        {
                            adaCode = aDACodes[pc];
                        }

                        if (adaCode.Contains("."))
                        {
                            int commaPos = adaCode.IndexOf(".");
                            adaCode = adaCode.Substring(0, commaPos);
                        }

                        adaCode = adaCode.ToUpper();

                        if (adaCode != "")
                        {
                            if (commonData.HygieneCodes.Any(x => x.Contains(adaCode)))
                            {
                                ps.NextHygieneAppt = proc.Field<DateTime>("ProcDate");
                                ps.NextHygieneApptCreated = proc.Field<DateTime>("DateTStamp");
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                }
                catch { }
            }
        }

        internal DateTime? GetPatientLastDoctorAppointment(DataTable patientCompletedProcedures)
        {
            List<DataRow> completedProceduresList = (from p in patientCompletedProcedures.AsEnumerable()
                                                     select p).ToList();

            foreach (DataRow cpRow in completedProceduresList)
            {
                if (_doctorProviders.Contains(cpRow.Field<long>("ProvNum").ToString()) || _otherProviders.Contains(cpRow.Field<long>("ProvNum").ToString()))
                {
                    return cpRow.Field<DateTime>("ProcDate");
                }

                long procCodeID = cpRow.Field<long>("CodeNum");

                string pc = procCodeID.ToString();

                string adaCode = "";

                if (aDACodes.ContainsKey(pc))
                {
                    adaCode = aDACodes[pc];
                }

                if (adaCode.Contains("."))
                {
                    int commaPos = adaCode.IndexOf(".");
                    adaCode = adaCode.Substring(0, commaPos);
                }

                adaCode = adaCode.ToUpper();

                if (adaCode != "")
                {
                    if (!commonData.HygieneCodes.Contains(adaCode))
                    {
                        return cpRow.Field<DateTime>("ProcDate");
                    }
                }
            }

            return null;
        }

        internal void GetPatientNextDoctorAppointment(ProvidersSchedule ps, DataTable allPatientProcedures)
        {
            int errorLineCount = 0;
            DateTime nextDay = ps.AppointmentDate.Value.AddDays(1);

            try
            {
                List<DataRow> allPatientProceduresForThisDate = (from a in allPatientProcedures.AsEnumerable()
                                                                 where a.Field<DateTime>("ProcDate") >= nextDay && a.Field<long>("AptNum") > 0
                                                                 orderby a.Field<DateTime>("ProcDate") ascending
                                                                 select a).ToList();

                foreach (DataRow proc in allPatientProceduresForThisDate)
                {
                    string appointedProv = proc.Field<long>("ProvNum").ToString();

                    if (_doctorProviders.Contains(appointedProv) || _otherProviders.Contains(appointedProv))
                    {
                        ps.NextDoctorAppt = proc.Field<DateTime>("ProcDate");
                        ps.NextDoctorApptCreated = proc.Field<DateTime>("DateTStamp");
                        break;
                    }
                    else
                    {
                        string pc = proc.Field<long>("CodeNum").ToString();

                        string adaCode = "";

                        if (aDACodes.ContainsKey(pc))
                        {
                            adaCode = aDACodes[pc];
                        }

                        if (adaCode.Contains("."))
                        {
                            int commaPos = adaCode.IndexOf(".");
                            adaCode = adaCode.Substring(0, commaPos);
                        }

                        adaCode = adaCode.ToUpper();

                        if (adaCode != "")
                        {
                            if (!commonData.HygieneCodes.Any(x => x.Contains(adaCode)))
                            {
                                ps.NextDoctorAppt = proc.Field<DateTime>("ProcDate");
                                ps.NextDoctorApptCreated = proc.Field<DateTime>("DateTStamp");
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                }
                catch { }
            }
        }

        internal void GetPatientPendingTreatment(ProvidersSchedule ps, DataTable pendingTreatment)
        {
            double pendingTx = 0;
            string pendingTxProcedureIds = "";

            foreach (DataRow pt in pendingTreatment.AsEnumerable())
            {
                if ((pt.Field<long?>("AptNum") == null || pt.Field<long>("AptNum") == 0) && pt.Field<DateTime>("DateTP") < ps.AppointmentDate)
                {
                    pendingTx += pt.Field<double>("ProcFee");
                    pendingTxProcedureIds += pt.Field<long>("ProcNum") + ",";
                }
            }

            if (pendingTxProcedureIds != "" && pendingTxProcedureIds.EndsWith(","))
            {
                pendingTxProcedureIds = pendingTxProcedureIds.TrimEnd(',');
            }

            ps.PendingTxAmount = (decimal)pendingTx;
            ps.PendingTxProcedureIds = pendingTxProcedureIds;

        }

        internal void GetPatientPendingTreatmentScheduled(ProvidersSchedule ps, DataTable allProceduresData)
        {
            int errorLineCount = 0;
            string procCodeString = "";

            try
            {
                double pendingAmtAccepted = 0;
                List<long> procIdsList = new List<long>();

                errorLineCount = 1;

                if (ps.PendingTxProcedureIds == null)
                {
                    ps.PendingTxScheduledAmount = (decimal)pendingAmtAccepted;
                    return;
                }
                else
                {
                    errorLineCount = 2;
                    procIdsList = ps.PendingTxProcedureIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(long.Parse).ToList(); //Split(',')
                }

                errorLineCount = 3;

                foreach (int procId in procIdsList)
                {

                    double procFee = (from c in allProceduresData.AsEnumerable()
                                      where c.Field<long>("ProcNum") == procId && c.Field<long>("AptNum") > 0
                                      select c.Field<double>("ProcFee")).DefaultIfEmpty(0).FirstOrDefault();

                    pendingAmtAccepted += procFee;
                }

                errorLineCount = 6;

                ps.PendingTxScheduledAmount = (decimal)pendingAmtAccepted;

            }
            catch (Exception ex)
            {
                try
                {
                    //gather diagnostic data
                    string diagData = "";
                    try
                    {
                        diagData = procCodeString;

                        if (ps.PMPatientID.HasValue)
                        {
                            diagData = diagData + ":" + ps.PMPatientID.Value.ToString();
                        }
                        if (ps.AppointmentDate.HasValue)
                        {
                            diagData = diagData + ":" + ps.AppointmentDate.Value.ToShortDateString();
                        }
                    }
                    catch { }

                    errorLog.LogErrorWithDiagnostic(_appData.OfficeKeyGuid, ex, errorLineCount, diagData);
                }
                catch { }
            }
        }

        internal double? GetPatientRemainingInsuranceFundsAvailable(DataTable patientBenefitRemainingData)
        {
            double? remainingBalance = 0;

            try
            {

                if (patientBenefitRemainingData.Rows.Count > 0)
                {
                    remainingBalance = (from p in patientBenefitRemainingData.AsEnumerable()
                                        select p.Field<double?>("AmtRemaining")).DefaultIfEmpty(0).FirstOrDefault();
                }

                return remainingBalance;

            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                }
                catch { }
            }

            return remainingBalance;
        }

        internal void GetPatientHygieneApptCreatedOnDate(ProvidersSchedule ps, DataTable apptsCreatedDayOfAppt, DataTable treatmentPlanned, DataTable allProceduresData)
        {
            string pc = "";
            string adaCode = "";

            //look for hygiene appt created on appoitnment date
            List<DataRow> patientApptsCreatedOnDate = (from a in apptsCreatedDayOfAppt.AsEnumerable()
                                                       select a).ToList();

            foreach (DataRow appointment in patientApptsCreatedOnDate)
            {
                //are any appointments for a hygiene provider?
                string appointedProv = appointment.Field<long>("ProvNum").ToString();

                if (_hygieneProviders.Contains(appointedProv))
                {
                    ps.HygieneApptCreatedToday = appointment.Field<DateTime>("AptDateTime");  //DateTStamp?
                }
                else
                {
                    Dictionary<long, decimal> plannedTreatmentsDict = new Dictionary<long, decimal>();

                    foreach (DataRow pt in treatmentPlanned.AsEnumerable())
                    {
                        if (!plannedTreatmentsDict.ContainsKey(pt.Field<long>("ProcNum")))
                        {
                            plannedTreatmentsDict.Add(pt.Field<long>("ProcNum"), (decimal)pt.Field<double>("ProcFee"));
                        }
                    }

                    var aptNum2 = appointment.Field<long>("AptNum");
                    List<long> allProceduresForThisAptNumList = (from c in treatmentPlanned.AsEnumerable()  //allProceduresData
                                                                 where c.Field<long>("AptNum") == aptNum2
                                                                 select c.Field<long>("ProcNum")).ToList();

                    foreach (var procNum in allProceduresForThisAptNumList.AsEnumerable())
                    {
                        if (procNum != 0)
                        {
                            if (plannedTreatmentsDict.ContainsKey(procNum))
                            {

                                int procCodeID = (int)(from p in treatmentPlanned.AsEnumerable()
                                                       where p.Field<long>("ProcNum") == procNum
                                                       select p.Field<long>("CodeNum")).FirstOrDefault();

                                pc = procCodeID.ToString();

                                adaCode = "";

                                if (aDACodes.ContainsKey(pc))
                                {
                                    adaCode = aDACodes[pc];
                                }

                                if (adaCode.Contains("."))
                                {
                                    int commaPos = adaCode.IndexOf(".");
                                    adaCode = adaCode.Substring(0, commaPos);
                                }

                                adaCode = adaCode.ToUpper();
                                if (adaCode != "")
                                {
                                    if (commonData.HygieneCodes.Any(x => x.Contains(adaCode)))
                                    {
                                        ps.HygieneApptCreatedToday = appointment.Field<DateTime>("AptDateTime");
                                    }
                                }
                                else
                                {
                                    //check treatment planned procedures for a match
                                    var proccodeid = (from tp in treatmentPlanned.AsEnumerable()
                                                      where tp.Field<long>("ProcNum") == procNum
                                                      select tp.Field<long>("CodeNum")).FirstOrDefault();

                                    if (proccodeid != 0)
                                    {
                                        pc = proccodeid.ToString();

                                        if (aDACodes.ContainsKey(pc))
                                        {
                                            adaCode = aDACodes[pc];
                                        }

                                        if (adaCode.Contains("."))
                                        {
                                            int commaPos = adaCode.IndexOf(".");
                                            adaCode = adaCode.Substring(0, commaPos);
                                        }

                                        adaCode = adaCode.ToUpper();

                                        if (adaCode != "")
                                        {
                                            if (commonData.HygieneCodes.Any(x => x.Contains(adaCode)))
                                            {
                                                ps.HygieneApptCreatedToday = appointment.Field<DateTime>("AptDateTime");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }

        internal void GetPatientDoctorApptCreatedOnDate(ProvidersSchedule ps, DataTable apptsCreatedDayOfAppt, DataTable treatmentPlanned, DataTable allProceduresData)
        {
            int errorLineCount = 0;

            string pc = "";
            string adaCode = "";

            try
            {
                //look for doctor appt created on appoitnment date
                List<DataRow> patientApptsCreatedOnDate = (from a in apptsCreatedDayOfAppt.AsEnumerable()
                                                           select a).ToList();

                errorLineCount = 1;

                foreach (DataRow appointment in patientApptsCreatedOnDate)
                {
                    //are any appointments for a hygiene provider?
                    string appointedProv = appointment.Field<long>("ProvNum").ToString();

                    errorLineCount = 2;

                    if (_doctorProviders.Contains(appointedProv) || _otherProviders.Contains(appointedProv))
                    {
                        ps.DoctorApptCreatedToday = appointment.Field<DateTime>("AptDateTime");
                    }
                    else
                    {
                        errorLineCount = 3;

                        Dictionary<long, decimal> plannedTreatmentsDict = new Dictionary<long, decimal>();

                        foreach (DataRow pt in treatmentPlanned.AsEnumerable())
                        {
                            if (!plannedTreatmentsDict.ContainsKey(pt.Field<long>("ProcNum")))
                            {
                                plannedTreatmentsDict.Add(pt.Field<long>("ProcNum"), (decimal)pt.Field<double>("ProcFee"));
                            }
                        }

                        var aptNum2 = appointment.Field<long>("AptNum");
                        List<long> allProceduresForThisAptNumList = (from c in treatmentPlanned.AsEnumerable()  //allProceduresData
                                                                     where c.Field<long>("AptNum") == aptNum2
                                                                     select c.Field<long>("ProcNum")).ToList();

                        foreach (var procNum in allProceduresForThisAptNumList.AsEnumerable())
                        {
                            if (procNum != 0)
                            {
                                if (plannedTreatmentsDict.ContainsKey(procNum))
                                {

                                    int procCodeID = (int)(from p in treatmentPlanned.AsEnumerable()
                                                           where p.Field<long>("ProcNum") == procNum
                                                           select p.Field<long>("CodeNum")).FirstOrDefault();

                                    pc = procCodeID.ToString();

                                    adaCode = "";

                                    if (aDACodes.ContainsKey(pc))
                                    {
                                        adaCode = aDACodes[pc];
                                    }

                                    if (adaCode.Contains("."))
                                    {
                                        int commaPos = adaCode.IndexOf(".");
                                        adaCode = adaCode.Substring(0, commaPos);
                                    }

                                    adaCode = adaCode.ToUpper();
                                    errorLineCount = 4;

                                    if (adaCode != "")
                                    {
                                        if (commonData.DoctorCodes.Any(x => x.Contains(adaCode)))
                                        {
                                            ps.DoctorApptCreatedToday = appointment.Field<DateTime>("AptDateTime");
                                        }
                                    }
                                    else
                                    {
                                        errorLineCount = 5;

                                        //check treatment planned procedures for a match
                                        var proccodeid = (from tp in treatmentPlanned.AsEnumerable()
                                                          where tp.Field<long>("ProcNum") == procNum
                                                          select tp.Field<long>("CodeNum")).FirstOrDefault();

                                        if (proccodeid != 0)
                                        {
                                            pc = proccodeid.ToString();

                                            if (aDACodes.ContainsKey(pc))
                                            {
                                                adaCode = aDACodes[pc];
                                            }

                                            if (adaCode.Contains("."))
                                            {
                                                int commaPos = adaCode.IndexOf(".");
                                                adaCode = adaCode.Substring(0, commaPos);
                                            }

                                            adaCode = adaCode.ToUpper();

                                            errorLineCount = 6;

                                            if (adaCode != "")
                                            {
                                                if (commonData.DoctorCodes.Any(x => x.Contains(adaCode)))
                                                {
                                                    ps.DoctorApptCreatedToday = appointment.Field<DateTime>("AptDateTime");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                }
                catch { }
            }
        }

        internal void GetPatientPerioConversionsMadeOnDate()
        {
            return;
        }

        internal string GetPatientUnappointedFamilyMembersAppointed(ProvidersSchedule ps)
        {
            string appointetedFamilyMembers = null;

            if (ps.UnappointedFamilyMembers != null)
            {
                string formattedApptDate = FormatExtractionDate(ps.AppointmentDate.Value);

                //DataTable familyAppointments = DentalData("Select patid From admin.appt Where patid In (" + ps.UnappointedFamilyMembers + ") And apptdate > {d'" + formattedApptDate + "'} And createdate = {d'" + formattedApptDate + "'}");
                DataTable familyAppointments = DentalData("Select PatNum From appointment Where PatNum In (" + ps.UnappointedFamilyMembers + ") And DATE(AptDateTime) > DATE(" + formattedApptDate + ") And DATE(DateTStamp) = DATE(" + formattedApptDate + ")");

                List<long> familyPatientsList = (from p in familyAppointments.AsEnumerable()
                                                 select p.Field<long>("PatNum")).Distinct().ToList();

                if (familyPatientsList.Count > 0)
                {
                    appointetedFamilyMembers = string.Join<long>(",", familyPatientsList);
                }
            }

            return appointetedFamilyMembers;
        }

        internal double GetPatientPastDueBalancesCollected(DataTable patientPayments)
        {
            double payments = (from pt in patientPayments.AsEnumerable()
                               select pt.Field<double>("Amt")).DefaultIfEmpty(0).Sum();

            return payments;
        }

        internal double GetPatientDiagnosedTreatmentAmount(DataTable diagnosedTreatment)
        {
            double plannedTx = (from pt in diagnosedTreatment.AsEnumerable()
                                select pt.Field<double>("ProcFee")).DefaultIfEmpty(0).Sum();

            return plannedTx;
        }

        internal double GetPatientDiagnosedTreatmentScheduled(DataTable diagnosedTreatment)
        {
            double diagnosedAmtAccepted = 0;

            foreach (DataRow pt in diagnosedTreatment.AsEnumerable())
            {
                long? aptNum = pt.Field<long>("AptNum");
                if (aptNum != null && aptNum > 0)
                {
                    diagnosedAmtAccepted += pt.Field<double>("ProcFee");
                }

            }

            return diagnosedAmtAccepted;
        }

        // End Straine functions

        internal void ProcessOfficePatients(List<Provider> Providers)
        {
            int errorLineCount = 0;

            int totalPatientsCount = 0;

            try
            {
                DataTable patientsData = new DataTable();
                DataTable activePatientsData = new DataTable();
                DataTable futurePatientsData = new DataTable();
                //DataTable patientZipCodeData = new DataTable();
                DataTable insuranceCompanies = new DataTable();
                DataTable nextMonthPatientsData = new DataTable();
                DataTable nextMonthPatientsWithCompletedProceduresData = new DataTable();

                //processing lists & models
                List<OfficesPatient> officePatientsUploadList = new List<OfficesPatient>();

                //dates for checking on new patients
                string formattedNextMonthStartDate = FormatExtractionDate(commonData.NextMonthStart);
                string formattedNextMonthEndDate = FormatExtractionDate(commonData.NextMonthEnd);

                //proc codes to ignore for completed procedures
                string procCodesToIgnore = string.Join(",", _procsToIgnoreList);

                errorLineCount = 1;

                patientsData = DentalData("Select FName, LName, PatStatus, PatNum, Gender, Zip From patient");

                errorLineCount = 2;

                errorLineCount = 3;

                insuranceCompanies = DentalData("Select carrierNum, carrierName from carrier");

                totalPatientsCount = patientsData.Rows.Count;

                errorLineCount = 4;

                nextMonthPatientsData = DentalData("Select Distinct(PatNum) From appointment Where (AptDateTime >= DATE('" + formattedNextMonthStartDate + "') And AptDateTime <= DATE('" + formattedNextMonthEndDate + "'))");

                errorLineCount = 401;

                if (_procsToIgnoreList.Count() > 0)
                {
                    nextMonthPatientsWithCompletedProceduresData = DentalData("Select Distinct(pl.PatNum) From procedurelog as pl Inner Join appointment as a On a.PatNum = pl.PatNum Where pl.ProcStatus = 2 And DATE(pl.ProcDate) < DATE('" + formattedNextMonthStartDate + "') And (a.AptDateTime >= DATE('" + formattedNextMonthStartDate + "') And a.AptDateTime <= DATE('" + formattedNextMonthEndDate + "')) And pl.CodeNum Not In (" + procCodesToIgnore + ")");
                }
                else
                {
                    nextMonthPatientsWithCompletedProceduresData = DentalData("Select Distinct(pl.PatNum) From procedurelog as pl Inner Join appointment as a On a.PatNum = pl.PatNum Where pl.ProcStatus = 2 And DATE(pl.ProcDate) < DATE('" + formattedNextMonthStartDate + "') And (a.AptDateTime >= DATE('" + formattedNextMonthStartDate + "') And a.AptDateTime <= DATE('" + formattedNextMonthEndDate + "'))");
                }

                errorLineCount = 402;

                //new patients lists
                List<long> nextMonthPatientsList = (from p in nextMonthPatientsData.AsEnumerable()
                                                    select p.Field<long>("PatNum")).ToList();

                List<long> nextMonthPatientsWithCompletedProceduresList = (from p in nextMonthPatientsWithCompletedProceduresData.AsEnumerable()
                                                                           select p.Field<long>("PatNum")).ToList();

                List<long> newPatientsNextMonthList = nextMonthPatientsList.Except(nextMonthPatientsWithCompletedProceduresList).ToList();

                errorLineCount = 403;

                foreach (DataRow pat in patientsData.Rows)
                {
                    OfficesPatient op = new OfficesPatient();
                    op.OfficeID = _appData.OfficeKeyGuid;
                    op.PMPatientName = pat.Field<string>("FName").Trim() + " " + pat.Field<string>("LName").Trim();
                    op.PMPatientID = (int)pat.Field<long>("PatNum");
                    //op.FamilyID = pat.Field<int?>("famid");
                    //op.PrimInsuredID = pat.Field<int?>("priminsuredid");

                    //check to see if new patient, if they are, their firstvisitdate represents the first day of the month they are new
                    if (newPatientsNextMonthList.Contains(pat.Field<long>("PatNum")))
                    {
                        op.FirstVisitDate = commonData.NextMonthStart;
                    }
                    else
                    {
                        op.FirstVisitDate = null;
                    }

                    var patStatus = pat.Field<byte>("PatStatus");

                    // Map to Dentrix values
                    if (patStatus < 4)
                    {
                        patStatus++;
                    }
                    //op.PMStatus = patStatus.ToString();

                    errorLineCount = 5;

                    int gender = pat.Field<byte>("Gender");


                    string zipCode = pat.Field<string>("Zip");

                    errorLineCount = 6; ;

                    officePatientsUploadList.Add(op);

                    errorLineCount = 7;

                    if (officePatientsUploadList.Count >= 100)
                    {
                        var json = JsonConvert.SerializeObject(officePatientsUploadList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOfficesPatientsDataRequest request = new BulkUploadOfficesPatientsDataRequest(_appData.OfficeKey, json);
                            BulkUploadOfficesPatientsDataResponse response = new BulkUploadOfficesPatientsDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOfficesPatientsData(request);
                                bool result = response.BulkUploadOfficesPatientsDataResult;
                            }

                            json = null;
                            officePatientsUploadList.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -1);
                            }
                            catch { }
                        }
                    }
                }

                //upload any remaining data
                if (officePatientsUploadList.Count > 0)
                {
                    var json = JsonConvert.SerializeObject(officePatientsUploadList, Newtonsoft.Json.Formatting.Indented);

                    try
                    {
                        BulkUploadOfficesPatientsDataRequest request = new BulkUploadOfficesPatientsDataRequest(_appData.OfficeKey, json);
                        BulkUploadOfficesPatientsDataResponse response = new BulkUploadOfficesPatientsDataResponse();

                        using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                        {
                            response = sc.BulkUploadOfficesPatientsData(request);
                            bool result = response.BulkUploadOfficesPatientsDataResult;
                        }

                        json = null;
                        officePatientsUploadList.Clear();

                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -2);
                        }
                        catch { }
                    }
                }

                List<long> activePatientList = new List<long>();
                List<OfficesActivePatient> officeActivePatientsList = new List<OfficesActivePatient>();
                DateTime processEndingDate = commonData.Add1MonthFirstDayOfMonth();
                List<long> hygienePatientsList = new List<long>();

                errorLineCount = 8;

                for (DateTime processingDate = _appData.OfficeData.ActivePatientProcessDate.Value; processingDate.Date < processEndingDate; processingDate = processingDate.AddMonths(1))
                {
                    errorLineCount = 9;

                    OfficesActivePatient officeActivePatients = new OfficesActivePatient();
                    officeActivePatients.ActivePatients = 0;
                    officeActivePatients.HygienePatients = 0;
                    officeActivePatients.PctInHyg = 0;
                    officeActivePatients.Variance = 0;

                    int activePatientMonthSetting = (int)(_appData.OfficeData.ActivePatientMonths * -1);
                    string activePatientStartDate = FormatExtractionDate(processingDate.AddMonths(activePatientMonthSetting));
                    string activePatientEndDate = FormatExtractionDate(processingDate);
                    DateTime cutoffDate = new DateTime(processingDate.Year, processingDate.Month, DateTime.DaysInMonth(processingDate.Year, processingDate.Month));
                    string activePatientCuttoffDate = FormatExtractionDate(cutoffDate.AddMonths(8));

                    errorLineCount = 10;

                    if (activePatientsData != null)
                    {
                        activePatientsData.Clear();
                    }
                    activePatientsData = null;
                    //activePatientsData = DentalData("Select Distinct(patid) 
                    //From admin.appt 
                    //Where  And (apptdate >= {d'" + activePatientStartDate + "'} 
                    //And apptdate <= {d'" + activePatientEndDate + "'}) And (" + _completedApptStatus + ")", true);
                    activePatientsData = DentalData("Select DISTINCT(a.PatNum) as PatNum From appointment a Where (DATE(AptDateTime) >= DATE('" + activePatientStartDate + "') AND DATE(AptDateTime) <= DATE('" + activePatientEndDate + "')) And (" + _completedApptStatus + ")", false);

                    //get active patients
                    hygienePatientsList.Clear();

                    activePatientList.Clear();
                    activePatientList = (from p in activePatientsData.AsEnumerable()
                                         select p.Field<long>("PatNum")).Distinct().ToList();

                    errorLineCount = 11;

                    if (futurePatientsData != null)
                    {
                        futurePatientsData.Clear();
                    }
                    futurePatientsData = null;

                    if (activePatientList.Count > 0)
                    {
                        if (processingDate.Year == DateTime.Today.Year && processingDate.Month == DateTime.Today.Month)
                        {
                            futurePatientsData = DentalData("Select a.IsHygiene, a.ProvHyg, a.AptNum as AptNum, a.ProvNum as ProvNum, a.PatNum as PatNum, a.AptStatus as AptStatus From appointment a Where DATE(AptDateTime) >= DATE('" + activePatientEndDate + "')", false);
                        }
                        else
                        {
                            futurePatientsData = DentalData("Select a.IsHygiene, a.ProvHyg, a.AptNum as AptNum,a.ProvNum as ProvNum, a.PatNum as PatNum, a.AptStatus as AptStatus From appointment a Where (DATE(AptDateTime) > DATE('" + activePatientEndDate + "') AND DATE(AptDateTime) <= DATE('" + activePatientCuttoffDate + "'))", false);
                        }
                    }

                    errorLineCount = 12;

                    officeActivePatients.ActivePatients = activePatientList.Count();

                    //active patient variables
                    //int hygienePatients = 0;

                    errorLineCount = 13;

                    foreach (var pat in activePatientList)
                    {
                        errorLineCount = 14;

                        //look for future hygiene appointment
                        List<DataRow> patientFutureAppointments = (from a in futurePatientsData.AsEnumerable()
                                                                   where a.Field<long>("PatNum") == pat
                                                                   select a).ToList();

                        errorLineCount = 15;

                        foreach (DataRow appt in patientFutureAppointments)
                        {
                            errorLineCount = 16;

                            if (!hygienePatientsList.Contains(pat))
                            {
                                string appointedProv = "";
                                if (appt.Field<byte>("IsHygiene") == 1)
                                {
                                    appointedProv = appt.Field<long>("ProvHyg").ToString();

                                    //hygienePatients++;
                                    hygienePatientsList.Add(pat);
                                    break;
                                }
                                else
                                {
                                    appointedProv = appt.Field<long>("ProvNum").ToString();
                                }

                                long aptNum = appt.Field<long>("AptNum");

                                DataTable futureProceduresData = DentalData("Select ProcNum, ProvNum, CodeNum, PatNum, ProcFee, ProcStatus, AptNum, ProcDate, DateTStamp From procedurelog Where AptNum = " + aptNum);

                                List<long> futureProceduresForThisAptNumList = (from c in futureProceduresData.AsEnumerable()
                                                                                select c.Field<long>("CodeNum")).ToList();

                                Provider prov = (from p in Providers
                                                 where p.PMID == appointedProv
                                                 select p).FirstOrDefault();

                                errorLineCount = 17;

                                if (prov != null)
                                {
                                    if (prov.ProvType == 1)
                                    {
                                        //hygienePatients++;
                                        hygienePatientsList.Add(pat);
                                        break;
                                    }
                                    else
                                    {
                                        foreach (var procCode in futureProceduresForThisAptNumList)
                                        {
                                            if (procCode != 0)
                                            {
                                                string pc = procCode.ToString();
                                                string adaCode = "";

                                                if (aDACodes.ContainsKey(pc))
                                                {
                                                    adaCode = aDACodes[pc];
                                                }

                                                if (adaCode.Contains("."))
                                                {
                                                    int commaPos = adaCode.IndexOf(".");
                                                    adaCode = adaCode.Substring(0, commaPos);
                                                }

                                                adaCode = adaCode.ToUpper();

                                                if (adaCode != "")
                                                {
                                                    if (commonData.HygieneCodes.Any(x => x.Contains(adaCode)))
                                                    {
                                                        //hygienePatients++;
                                                        hygienePatientsList.Add(pat);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var procCode in futureProceduresForThisAptNumList)
                                    {
                                        if (procCode != 0)
                                        {
                                            string pc = procCode.ToString();
                                            string adaCode = "";

                                            if (aDACodes.ContainsKey(pc))
                                            {
                                                adaCode = aDACodes[pc];
                                            }

                                            if (adaCode.Contains("."))
                                            {
                                                int commaPos = adaCode.IndexOf(".");
                                                adaCode = adaCode.Substring(0, commaPos);
                                            }

                                            adaCode = adaCode.ToUpper();

                                            if (adaCode != "")
                                            {
                                                if (commonData.HygieneCodes.Any(x => x.Contains(adaCode)))
                                                {
                                                    //hygienePatients++;
                                                    hygienePatientsList.Add(pat);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //diagnostic data
                    if (_appData.OfficeKey.ToUpper() == "1DA0C6B9-1F5E-4DC3-9E13-7ED1B3300741")
                    {
                        try
                        {
                            try
                            {
                                errorLog.LogDiagnostic(_appData.OfficeKeyGuid, string.Join(",", activePatientList), "Active Patients");
                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -21);
                                }
                                catch { }
                            }
                            try
                            {
                                List<long> futurePatientList = (from p in futurePatientsData.AsEnumerable()
                                                                select p.Field<long>("PatNum")).Distinct().ToList();

                                errorLog.LogDiagnostic(_appData.OfficeKeyGuid, string.Join(",", futurePatientList), "Future Patients");
                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, -22);
                                }
                                catch { }
                            }

                        }
                        catch { }
                    }
                    //diagnostic data

                    errorLineCount = 18;

                    //finish active patients processing
                    officeActivePatients.OfficeID = _appData.OfficeKeyGuid;

                    if (_appData.OfficeData.ActivePatientProcessRefresh == true)
                    {
                        officeActivePatients.ActiveDate = processingDate;
                    }
                    else
                    {
                        officeActivePatients.ActiveDate = DateTime.Today;
                    }

                    officeActivePatients.HygienePatients = hygienePatientsList.Count();

                    if (officeActivePatients.HygienePatients.HasValue)
                    {
                        officeActivePatients.Variance = (officeActivePatients.ActivePatients - officeActivePatients.HygienePatients);
                    }
                    else
                    {
                        officeActivePatients.Variance = 0;
                    }

                    errorLineCount = 19;

                    decimal pctHyg = 0;
                    if (officeActivePatients.ActivePatients.Value > 0)
                    {
                        pctHyg = Math.Round(((decimal)officeActivePatients.HygienePatients.Value / (decimal)officeActivePatients.ActivePatients.Value), 2);
                    }

                    officeActivePatients.PctInHyg = pctHyg;
                    officeActivePatientsList.Add(officeActivePatients);

                    errorLineCount = 20;

                    if (officeActivePatientsList.Count >= 100)
                    {
                        var json = JsonConvert.SerializeObject(officeActivePatientsList, Newtonsoft.Json.Formatting.Indented);

                        try
                        {
                            BulkUploadOfficesActivePatientsDataRequest request = new BulkUploadOfficesActivePatientsDataRequest(_appData.OfficeKey, json);
                            BulkUploadOfficesActivePatientsDataResponse response = new BulkUploadOfficesActivePatientsDataResponse();

                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                            {
                                response = sc.BulkUploadOfficesActivePatientsData(request);
                                bool result = response.BulkUploadOfficesActivePatientsDataResult;
                            }

                            json = null;
                            officeActivePatientsList.Clear();

                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -3);
                            }
                            catch { }
                        }
                    }
                }

                //upload any remaining data
                if (officeActivePatientsList.Count > 0)
                {
                    var json = JsonConvert.SerializeObject(officeActivePatientsList, Newtonsoft.Json.Formatting.Indented);

                    try
                    {
                        BulkUploadOfficesActivePatientsDataRequest request = new BulkUploadOfficesActivePatientsDataRequest(_appData.OfficeKey, json);
                        BulkUploadOfficesActivePatientsDataResponse response = new BulkUploadOfficesActivePatientsDataResponse();

                        using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                        {
                            response = sc.BulkUploadOfficesActivePatientsData(request);
                            bool result = response.BulkUploadOfficesActivePatientsDataResult;
                        }

                        json = null;
                        officeActivePatientsList.Clear();

                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, -4);
                        }
                        catch { }
                    }
                }

            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                }
                catch { }
            }

            if (_appData.OfficeData.ActivePatientProcessRefresh == true)
            {
                _appData.OfficeData.ActivePatientProcessRefresh = false;
            }
            _appData.OfficeData.ActivePatientProcessDate = DateTime.Now;
            _appData.OfficeData.TotalPatientsCount = totalPatientsCount;

            _appData.SetOfficeData();

        }

        internal void UpdateBrokenAppointments(DateTime UnscheduledProcessDate, List<Provider> Providers)
        {

            int errorLineCount = 0;

            try
            {

                if (_processBrokenAppointments == false)
                {
                    return;
                }

                if (_appData.OfficeData.UnscheduledProcessRefresh == true)
                {
                    return;
                }

                List<ProvidersBrokenAppointment> existingBrokenAppointments = new List<ProvidersBrokenAppointment>();
                DateTimeOffset ProcessDate = new DateTime(DateTime.Today.AddYears(-2).Year, 1, 1);
                DateTimeOffset endDate = new DateTime(DateTime.Today.Year, 12, 31);

                errorLineCount = 1;
                for (DateTimeOffset processingDate = ProcessDate; processingDate.Date < endDate; processingDate = processingDate.AddMonths(6))
                {
                    DateTimeOffset procEndDate = processingDate.AddMonths(6);

                    if (existingBrokenAppointments != null)
                    {
                        existingBrokenAppointments.Clear();
                    }

                    //get existing broken appointments
                    try
                    {
                        GetProvidersBrokenAppointmentsByDateRequest pbaRequest = new GetProvidersBrokenAppointmentsByDateRequest(_appData.OfficeKey, processingDate.Date, procEndDate.Date);
                        GetProvidersBrokenAppointmentsByDateResponse pbaResponse = new GetProvidersBrokenAppointmentsByDateResponse();

                        errorLineCount = 2;
                        using (ThriveDataServiceClient thriveService = new ThriveDataServiceClient())
                        {
                            pbaResponse = thriveService.GetProvidersBrokenAppointmentsByDate(pbaRequest);
                            errorLineCount = 200;
                            string result = pbaResponse.GetProvidersBrokenAppointmentsByDateResult;
                            errorLineCount = 201;
                            existingBrokenAppointments = JsonConvert.DeserializeObject<List<ProvidersBrokenAppointment>>(result);
                            errorLineCount = 202;
                        }

                        foreach (ProvidersBrokenAppointment pba in existingBrokenAppointments)
                        {
                            try
                            {
                                errorLineCount = 203;

                                #region existingBrokenAppts
                                if (pba.Walkout == false)
                                {
                                    errorLineCount = 3;

                                    bool updateRecord = false;
                                    string patGuid = "";

                                    string brokenProvider = (from p in Providers
                                                             where p.ID == pba.ProviderID
                                                             select p.PMID).FirstOrDefault();

                                    //current appointment
                                    //DataTable currentAppointment = DentalData("Select apptdate, brokendate, provid, amt, patid, apptid, pinboard, status, patname From admin.appt Where apptid = " + pba.PMApptID);

                                    DataTable currentAppointment = DentalData("Select " +
                                      "a.DateTStamp as DateTStamp, a.ProvNum as ProvNum, a.PatNum as PatNum, a.AptNum as AptNum, a.AptStatus as AptStatus, a.AptDateTime as AptDateTime, " +
                                      "CONCAT(p.FName, ', ', p.LName)  AS 'PatName' " +
                                      "From appointment a " +
                                      "INNER JOIN patient p ON p.PatNum = a.PatNum " +
                                      "Where a.AptNum = " + pba.PMApptID);

                                    DataTable currentProcedure = DentalData("Select SUM(ProcFee) AS 'SumProcFee' From procedurelog Where AptNum = " + pba.PMApptID + " Group By AptNum");
                                    double? sumProcFee = (from p in currentProcedure.AsEnumerable()
                                                          select p.Field<double?>("SumProcFee")).DefaultIfEmpty(0).FirstOrDefault();

                                    errorLineCount = 4;

                                    if (currentAppointment.Rows.Count == 1)
                                    {
                                        DataRow currentApptRow = currentAppointment.Rows[0];
                                        DateTime? apptDate = currentApptRow.Field<DateTime?>("AptDateTime");
                                        //DateTime? brokenDate = pba.BrokenDate.Value;

                                        int apptStatus = -2;
                                        if (currentApptRow.Field<byte?>("AptStatus") != null)
                                        {
                                            apptStatus = (int)currentApptRow.Field<byte>("AptStatus");
                                        }

                                        int patID = (int)currentApptRow.Field<long>("PatNum");
                                        string patName = currentApptRow.Field<string>("PatName").Trim();

                                        if (patID != 0)
                                        {
                                            //DataTable patient = DentalData("Select firstname, lastname, status, patguid From admin.patient Where patid = " + patID);
                                            DataTable patient = DentalData("Select FName, LName, PatStatus, PatNum From patient Where PatNum = " + patID);

                                            errorLineCount = 5;

                                            if (patient.Rows.Count >= 1)
                                            {
                                                patGuid = patient.Rows[0].Field<long>("PatNum").ToString();

                                                //check to see if patient has changed
                                                if (patID != pba.PMPatientID)
                                                {
                                                    updateRecord = true;

                                                    //patient has changed
                                                    pba.PatientName = patName;
                                                    pba.PMPatientID = patID;
                                                }

                                                //check if patient status has changed
                                                if (patient.Rows[0].Field<byte>("PatStatus") != pba.PatientStatus)
                                                {
                                                    updateRecord = true;
                                                    var patStatus = patient.Rows[0].Field<byte>("PatStatus");

                                                    // Map to Dentrix values
                                                    if (patStatus < 4)
                                                    {
                                                        patStatus++;
                                                    }
                                                    pba.PatientStatus = patStatus;  //patient.Rows[0].Field<byte>("PatStatus");

                                                }
                                            }
                                        }

                                        errorLineCount = 6;

                                        //check to see if appointment has been rescheduled (scheduled status is 1)
                                        if (apptStatus == 1 || apptStatus == 2)
                                        //if (apptDate != null)
                                        {
                                            if (apptDate != pba.RescheduledDate)
                                            {
                                                updateRecord = true;

                                                pba.RescheduledDate = apptDate;
                                                pba.WonDate = DateTime.Now;
                                            }


                                            if ((decimal?)sumProcFee != pba.RescheduledAmount) //currentApptRow.Field<double?>("SumProcFee")
                                            {
                                                updateRecord = true;

                                                pba.RescheduledAmount = (decimal?)sumProcFee; // currentApptRow.Field<double?>("SumProcFee");
                                            }
                                            if (currentApptRow.Field<long>("ProvNum").ToString() != pba.RescheduledProvider)
                                            {
                                                updateRecord = true;

                                                pba.RescheduledProvider = currentApptRow.Field<long>("ProvNum").ToString();
                                            }
                                        }

                                        errorLineCount = 7;

                                        //check to see if appointment is still broken
                                        if (apptStatus == 5)
                                        //if (brokenDate != null)
                                        {
                                            //check to see if appointment had been rescheuled in the past, if so null out the reschedule (only if broken date is after the reschedule date)
                                            if (pba.RescheduledDate != null) // && brokenDate > pba.RescheduledDate
                                            {
                                                updateRecord = true;

                                                pba.RescheduledDate = null;
                                                pba.RescheduledAmount = null;
                                                pba.RescheduledProvider = null;
                                                pba.WonDate = null;
                                            }

                                            errorLineCount = 701;
                                            if ((decimal?)sumProcFee != pba.BrokenAmount) //currentApptRow.Field<double?>("SumProcFee")
                                            {
                                                updateRecord = true;

                                                pba.BrokenAmount = (decimal?)sumProcFee; // currentApptRow.Field<double?>("SumProcFee");
                                            }

                                            errorLineCount = 8;

                                            //check to see if provider has changed
                                            if (currentApptRow.Field<long>("ProvNum").ToString() != brokenProvider)
                                            {
                                                updateRecord = true;

                                                int tempProviderID = pba.ProviderID;

                                                pba.ProviderID = (from p in Providers
                                                                  where p.PMID == currentApptRow.Field<long>("ProvNum").ToString()
                                                                  select p.ID).FirstOrDefault();

                                                if (pba.ProviderID == 0)
                                                {
                                                    pba.ProviderID = tempProviderID;
                                                }
                                            }
                                        }

                                        //check to see if appointment is only pinboarded - removed

                                        errorLineCount = 9;

                                        //check to see if pinboard status has changed
                                        bool pb = false; // ConvertPinboard(currentApptRow.Field<Int16>("pinboard"));

                                        if (pb != pba.PinBoarded)
                                        {
                                            updateRecord = true;

                                            pba.PinBoarded = pb;
                                        }

                                        errorLineCount = 10;

                                        //check to see if appointment status has changed
                                        if (currentApptRow.Field<byte?>("AptStatus") != null)
                                        {
                                            //int apptStatus = (int)currentApptRow.Field<byte>("AptStatus");

                                            if (apptStatus != pba.AppointmentStatus)
                                            {
                                                updateRecord = true;

                                                pba.AppointmentStatus = apptStatus;

                                                // AptStatus = 2
                                                if (apptStatus == 2) //-106 || apptStatus == 150)
                                                {
                                                    pba.Completed = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            pba.AppointmentStatus = -2;
                                        }

                                        errorLineCount = 11;

                                        //future appointment
                                        string futureApptDate = "";

                                        futureApptDate = FormatExtractionDate(pba.BrokenDate.Value);

                                        if (pba.RescheduledDate.HasValue == true)
                                        {
                                            futureApptDate = FormatExtractionDate(pba.RescheduledDate.Value);
                                        }

                                        if (futureApptDate == "")
                                        {
                                            errorLineCount = 1101;

                                            futureApptDate = FormatExtractionDate(UnscheduledProcessDate);
                                        }

                                        //DataTable futureAppointment = DentalData("Select Top 1 apptdate, amt, provid From admin.appt Where patid = '" + pba.PMPatientID + "' And apptdate > {d'" + futureApptDate + "'} And apptid <> " + pba.PMApptID + " Order By apptdate");

                                        DataTable futureAppointment = DentalData("Select " +
                                             "a.AptDateTime, a.ProvNum, a.AptNum " +
                                             "From appointment a " +
                                             "Where a.PatNum = " + patID + " And DATE(a.AptDateTime) > DATE('" + futureApptDate + "') And a.AptNum <> " + pba.PMApptID + " ORDER BY AptDateTime DESC LIMIT 1");

                                        errorLineCount = 12;

                                        if (futureAppointment.Rows.Count == 1)
                                        {
                                            long? provNum = futureAppointment.Rows[0].Field<long?>("ProvNum");
                                            long aptNum = futureAppointment.Rows[0].Field<long>("AptNum");

                                            DataTable futureProcedure = DentalData("Select SUM(ProcFee) AS 'SumProcFee' From procedurelog Where AptNum = " + aptNum + " Group By AptNum");
                                            double? sumProcFee2 = (from p in futureProcedure.AsEnumerable()
                                                                   select p.Field<double?>("SumProcFee")).DefaultIfEmpty(0).FirstOrDefault();

                                            if (provNum != null)
                                            {
                                                if (patID != 0)
                                                {
                                                    //they have a future appointment
                                                    if (pba.OtherAppointmentAmount != (decimal?)sumProcFee2) // futureAppointment.Rows[0].Field<double?>("SumProcFee"))
                                                    {
                                                        updateRecord = true;
                                                        pba.OtherAppointmentAmount = (decimal?)sumProcFee2; // futureAppointment.Rows[0].Field<double?>("SumProcFee");
                                                    }
                                                    if (pba.OtherAppointmentDate != futureAppointment.Rows[0].Field<DateTime?>("AptDateTime"))
                                                    {
                                                        updateRecord = true;
                                                        pba.OtherAppointmentDate = futureAppointment.Rows[0].Field<DateTime?>("AptDateTime");
                                                    }
                                                    if (pba.OtherAppointmentProvider != futureAppointment.Rows[0].Field<long>("ProvNum").ToString())
                                                    {
                                                        updateRecord = true;
                                                        pba.OtherAppointmentProvider = futureAppointment.Rows[0].Field<long>("ProvNum").ToString();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (pba.OtherAppointmentDate != null)
                                                {
                                                    updateRecord = true;

                                                    pba.OtherAppointmentDate = null;
                                                    pba.OtherAppointmentAmount = null;
                                                    pba.OtherAppointmentProvider = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (pba.OtherAppointmentDate != null)
                                            {
                                                updateRecord = true;

                                                pba.OtherAppointmentDate = null;
                                                pba.OtherAppointmentAmount = null;
                                                pba.OtherAppointmentProvider = null;
                                            }
                                        }

                                        errorLineCount = 13;

                                        //check for contact info
                                        bool upRec = PatientLastContactUpdate(patID, pba);

                                        if (upRec == true)
                                        {
                                            updateRecord = true;
                                        }

                                    }
                                    else //appointment was deleted
                                    {
                                        if (pba.DeletedAppointment == false)
                                        {
                                            updateRecord = true;

                                            pba.DeletedAppointment = true;
                                        }

                                        if (pba.PMPatientID != 0)
                                        {
                                            patGuid = "";

                                            //DataTable patient = DentalData("Select firstname, lastname, status, patguid From admin.patient Where patid = " + pba.PMPatientID);
                                            DataTable patient = DentalData("Select FName, LName, PatStatus, PatNum From patient Where PatNum = " + pba.PMPatientID);

                                            if (patient.Rows.Count >= 1)
                                            {
                                                patGuid = patient.Rows[0].Field<long>("PatNum").ToString();
                                                errorLineCount = 1301;

                                                //check if patient status has changed
                                                if (patient.Rows[0].Field<byte>("PatStatus") != pba.PatientStatus)
                                                {
                                                    errorLineCount = 1302;
                                                    updateRecord = true;
                                                    var patStatus = patient.Rows[0].Field<byte>("PatStatus");

                                                    // Map to Dentrix values
                                                    if (patStatus < 4)
                                                    {
                                                        patStatus++;
                                                    }
                                                    pba.PatientStatus = patStatus;  //patient.Rows[0].Field<byte>("PatStatus");
                                                }

                                                //check for contact info
                                                if (pba.PMPatientID.HasValue == true)
                                                {
                                                    bool upRec = PatientLastContactUpdate(pba.PMPatientID.Value, pba);

                                                    if (upRec == true)
                                                    {
                                                        updateRecord = true;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    errorLineCount = 14;

                                    if (updateRecord == true)
                                    {
                                        pba.ModifiedDate = DateTime.Now;

                                        if (!updatedBrokenAppointments.Any(x => x.ID == pba.ID))
                                        {
                                            updatedBrokenAppointments.Add(pba);
                                        }
                                    }

                                    errorLineCount = 15;

                                    //providers broken appointments
                                    if (updatedBrokenAppointments.Count >= 100)
                                    {
                                        var json = JsonConvert.SerializeObject(updatedBrokenAppointments, Newtonsoft.Json.Formatting.Indented);

                                        try
                                        {
                                            UpdateProvidersBrokenAppointmentsDataRequest request = new UpdateProvidersBrokenAppointmentsDataRequest(_appData.OfficeKey, json);
                                            UpdateProvidersBrokenAppointmentsDataResponse response = new UpdateProvidersBrokenAppointmentsDataResponse();

                                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                            {
                                                response = sc.UpdateProvidersBrokenAppointmentsData(request);
                                                bool result = response.UpdateProvidersBrokenAppointmentsDataResult;
                                            }

                                            json = null;
                                            updatedBrokenAppointments.Clear();

                                        }
                                        catch (Exception ex)
                                        {
                                            try
                                            {
                                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -2);
                                            }
                                            catch { }
                                        }
                                    }
                                }
                                // This is for walkouts
                                else
                                {

                                    errorLineCount = 16;
                                    if (pba.PMPatientID != 0)
                                    {
                                        string futureApptDate = FormatExtractionDate(pba.BrokenDate.Value);
                                        bool updateRecord = false;
                                        string patGuid = "";

                                        //DataTable futureAppointment = DentalData("Select Top 1 apptid, apptdate, amt, provid From admin.appt Where patid = '" + pba.PMPatientID + "' And apptdate > {d'" + futureApptDate + "'} Order By apptdate");

                                        //And a.AptNum <> " + pba.PMApptID + " 

                                        string query = "Select " +
                                             "a.AptNum, a.AptDateTime, a.ProvNum, a.AptStatus " +
                                             "From appointment a " +
                                             "Where a.PatNum = " + pba.PMPatientID + " And DATE(a.AptDateTime) > DATE('" + futureApptDate + "') ORDER BY AptDateTime DESC LIMIT 1";

                                        DataTable futureAppointment = DentalData(query);

                                        if (futureAppointment.Rows.Count == 1)
                                        {
                                            long? provNum = futureAppointment.Rows[0].Field<long?>("ProvNum");
                                            long aptNum = futureAppointment.Rows[0].Field<long>("AptNum");
                                            int apptStatus = -2;
                                            if (futureAppointment.Rows[0].Field<byte?>("AptStatus") != null)
                                            {
                                                apptStatus = (int)futureAppointment.Rows[0].Field<byte>("AptStatus");
                                            }
                                            DataTable futureProcedure = DentalData("Select SUM(ProcFee) AS 'SumProcFee' From procedurelog Where AptNum = " + aptNum + " Group By AptNum");
                                            double? sumProcFee = (from p in futureProcedure.AsEnumerable()
                                                                  select p.Field<double?>("SumProcFee")).DefaultIfEmpty(0).FirstOrDefault();

                                            errorLineCount = 17;

                                            if (provNum != null && apptStatus != 5)
                                            {
                                                //they have a future appointment
                                                updateRecord = true;
                                                pba.RescheduledAmount = (decimal?)sumProcFee; // futureAppointment.Rows[0].Field<double?>("SumProcFee");
                                                pba.RescheduledDate = futureAppointment.Rows[0].Field<DateTime?>("AptDateTime");
                                                pba.RescheduledProvider = futureAppointment.Rows[0].Field<long>("ProvNum").ToString();
                                                pba.PMApptID = (int)futureAppointment.Rows[0].Field<long>("AptNum");
                                                pba.WonDate = DateTime.Now;

                                                //check for future other appointment
                                                string otherFutureApptDate = FormatExtractionDate(pba.RescheduledDate.Value);
                                                //DataTable otherFutureAppointment = DentalData("Select Top 1 apptdate, amt, provid From admin.appt Where patid = '" + pba.PMPatientID + "' And apptdate > {d'" + otherFutureApptDate + "'} Order By apptdate");
                                                DataTable otherFutureAppointment = DentalData("Select " +
                                                   "a.AptDateTime, a.ProvNum " +
                                                   "From appointment a " +
                                                   "Where a.PatNum = " + pba.PMPatientID + " And DATE(a.AptDateTime) > DATE('" + otherFutureApptDate + "') ORDER BY AptDateTime DESC LIMIT 1");
                                                if (otherFutureAppointment.Rows.Count == 1)
                                                {
                                                    long? provNum2 = otherFutureAppointment.Rows[0].Field<long?>("ProvNum");
                                                    long aptNum2 = otherFutureAppointment.Rows[0].Field<long>("AptNum");

                                                    DataTable futureProcedure2 = DentalData("Select SUM(ProcFee) AS 'SumProcFee' From procedurelog Where AptNum = " + aptNum2 + " Group By AptNum");
                                                    double? sumProcFee2 = (from p in futureProcedure2.AsEnumerable()
                                                                           select p.Field<double?>("SumProcFee")).DefaultIfEmpty(0).FirstOrDefault();
                                                    if (provNum2 != null)
                                                    {
                                                        //they have a future other appointment
                                                        updateRecord = true;
                                                        pba.OtherAppointmentAmount = (decimal?)sumProcFee2; // otherFutureAppointment.Rows[0].Field<double?>("SumProcFee");
                                                        pba.OtherAppointmentDate = otherFutureAppointment.Rows[0].Field<DateTime?>("AptDateTime");
                                                        pba.OtherAppointmentProvider = otherFutureAppointment.Rows[0].Field<long>("ProvNum").ToString();
                                                    }
                                                }
                                            }
                                        }

                                        errorLineCount = 18;

                                        //DataTable patient = DentalData("Select firstname, lastname, status, patguid From admin.patient Where patid = " + pba.PMPatientID);
                                        DataTable patient = DentalData("Select FName, LName, PatStatus, PatNum From patient Where PatNum = " + pba.PMPatientID);

                                        if (patient.Rows.Count >= 1)
                                        {
                                            patGuid = patient.Rows[0].Field<long>("PatNum").ToString();

                                            //check if patient status has changed
                                            if (patient.Rows[0].Field<byte>("PatStatus") != pba.PatientStatus)
                                            {
                                                updateRecord = true;
                                                var patStatus = patient.Rows[0].Field<byte>("PatStatus");

                                                // Map to Dentrix values
                                                if (patStatus < 4)
                                                {
                                                    patStatus++;
                                                }
                                                pba.PatientStatus = patStatus;  //patient.Rows[0].Field<byte>("PatStatus");
                                            }

                                            if (pba.PMPatientID.HasValue == true)
                                            {
                                                bool upRec = PatientLastContactUpdate(pba.PMPatientID.Value, pba);

                                                if (upRec == true)
                                                {
                                                    updateRecord = true;
                                                }
                                            }
                                        }

                                        errorLineCount = 19;

                                        if (updateRecord == true)
                                        {
                                            pba.ModifiedDate = DateTime.Now;

                                            if (!updatedBrokenAppointments.Any(x => x.ID == pba.ID))
                                            {
                                                updatedBrokenAppointments.Add(pba);
                                            }
                                        }
                                    }

                                    //providers broken appointments
                                    if (updatedBrokenAppointments.Count >= 100)
                                    {
                                        var json = JsonConvert.SerializeObject(updatedBrokenAppointments, Newtonsoft.Json.Formatting.Indented);

                                        try
                                        {
                                            UpdateProvidersBrokenAppointmentsDataRequest request = new UpdateProvidersBrokenAppointmentsDataRequest(_appData.OfficeKey, json);
                                            UpdateProvidersBrokenAppointmentsDataResponse response = new UpdateProvidersBrokenAppointmentsDataResponse();

                                            using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                                            {
                                                response = sc.UpdateProvidersBrokenAppointmentsData(request);
                                                bool result = response.UpdateProvidersBrokenAppointmentsDataResult;
                                            }

                                            json = null;
                                            updatedBrokenAppointments.Clear();

                                        }
                                        catch (Exception ex)
                                        {
                                            try
                                            {
                                                errorLog.LogError(_appData.OfficeKeyGuid, ex, -3);
                                            }
                                            catch { }
                                        }
                                    }

                                }
                                #endregion

                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                                }
                                catch { }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                        }
                        catch { }
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex, errorLineCount);
                }
                catch { }
            }

            //providers broken appointments
            if (updatedBrokenAppointments.Count > 0)
            {
                var json = JsonConvert.SerializeObject(updatedBrokenAppointments, Newtonsoft.Json.Formatting.Indented);

                try
                {
                    UpdateProvidersBrokenAppointmentsDataRequest request = new UpdateProvidersBrokenAppointmentsDataRequest(_appData.OfficeKey, json);
                    UpdateProvidersBrokenAppointmentsDataResponse response = new UpdateProvidersBrokenAppointmentsDataResponse();

                    using (ThriveDataServiceClient sc = new ThriveDataServiceClient())
                    {
                        response = sc.UpdateProvidersBrokenAppointmentsData(request);
                        bool result = response.UpdateProvidersBrokenAppointmentsDataResult;
                    }

                    json = null;
                    updatedBrokenAppointments.Clear();

                }
                catch (Exception ex)
                {
                    try
                    {
                        errorLog.LogError(_appData.OfficeKeyGuid, ex, -4);
                    }
                    catch { }
                }
            }
        }

        private bool ConvertPinboard(int pb)
        {
            bool pbVal = false;

            if (pb == 1)
            {
                pbVal = true;
            }

            return pbVal;
        }

        private bool IsNewPatient(int patid, string processDate)
        {
            string procCodesToIgnore = string.Join(",", _procsToIgnoreList);

            DataTable patientData = new DataTable();
            //patientData = DentalData("Select DateFirstVisit From patient Where DateFirstVisit = {d'" + processDate + "'} And PatNum = " + patid);
            if (_procsToIgnoreList.Count() > 0)
            {
                patientData = DentalData("Select PatNum From procedurelog Where ProcStatus = 2 And DATE(ProcDate) < DATE('" + processDate + "') And PatNum = " + patid + " And CodeNum Not In (" + procCodesToIgnore + ") LIMIT 1");
            }
            else
            {
                patientData = DentalData("Select PatNum From procedurelog Where ProcStatus = 2 And DATE(ProcDate) < DATE('" + processDate + "') And PatNum = " + patid + " LIMIT 1");
            }

            if (patientData.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private string GetReferralType(int patid)
        {
            string referral = "";

            DataTable refActsData = new DataTable();
            //refActsData = DentalData("Select ReferralNum From referral Where PatNum = " + patid);
            refActsData = DentalData("Select ReferralNum From refattach Where PatNum = " + patid);

            var refid = (from r in refActsData.AsEnumerable()
                         select r.Field<long>("ReferralNum")).FirstOrDefault();

            if (refid != 0)
            {
                DataTable referralData = new DataTable();
                referralData = DentalData("Select LName, FName From referral Where ReferralNum = " + refid);

                var referrer = (from r in referralData.AsEnumerable()
                                select r).FirstOrDefault();

                if (referrer != null)
                {
                    referral = referrer.Field<string>("FName").Trim() + " " + referrer.Field<string>("LName").Trim();
                    if (referral.StartsWith(" "))
                    {
                        referral = referral.Trim();
                    }
                }
            }

            return referral;
        }
        private bool IsReferralExternal(int? patid)
        {
            bool externalRef = false;

            DataTable refActsData = new DataTable();
            //refActsData = DentalData("Select refid From admin.refacts Where patid = " + patid);
            refActsData = DentalData("Select ReferralNum From refattach Where PatNum = " + patid);

            var refid = (from r in refActsData.AsEnumerable()
                         select r.Field<long>("ReferralNum")).FirstOrDefault();

            if (refid != 0)
            {
                DataTable referralData = new DataTable();
                //referralData = DentalData("Select reftype From admin.referral Where refid = " + refid);
                referralData = DentalData("Select PatNum From referral Where ReferralNum = " + refid);

                var patNum2 = (from r in referralData.AsEnumerable()
                               select r.Field<long?>("PatNum")).FirstOrDefault();


                if (patNum2 != null && patNum2 > 0)
                {
                    externalRef = false;
                }
                else
                {
                    externalRef = true;
                }
            }

            return externalRef;
        }

        private string GetPatientPaymentType(long patid, DataTable inscompanies)
        {
            //Select carrierNum, carrierName from carrier
            try
            {
                DataTable insCarrierData = new DataTable();
                insCarrierData = DentalData(
    "SELECT carrier.CarrierName as CarrierName, patient.PatNum as PatNum " +
    " FROM carrier, insplan, claim, claimproc, patient  " +
    " WHERE insplan.PlanNum = claim.PlanNum AND patient.PatNum = " + patid +
    " AND insplan.CarrierNum = carrier.CarrierNum " +
    " AND claimproc.ClaimNum = claim.ClaimNum " +
    " AND claimproc.PatNum = patient.PatNum " +
    " GROUP BY patient.PatNum, CarrierName " +
    " ORDER BY CarrierName"
    );

                var carrierList = (from i in insCarrierData.AsEnumerable()
                                   where i.Field<long>("PatNum") == patid
                                   select i.Field<string>("CarrierName"));

                //if (insuredList.ContainsKey((int)patid))
                if (carrierList.Count() > 0)
                {
                    var insCo = carrierList.FirstOrDefault();
                    //var insCo = (from i in inscompanies.AsEnumerable()
                    //         where i.Field<long>("carrierNum") == (long)insuredList[(int)patid]
                    //         select i.Field<string>("carrierName")).FirstOrDefault();

                    if (insCo != null)
                    {
                        string insName = insCo.Trim();

                        if (insName == "" || insName == ".")
                        {
                            insName = "Fee for service";
                        }

                        return insName;
                    }
                    else
                    {
                        return "Unknown";
                    }
                }
                else
                {
                    return "Fee for service";
                }
            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                }
                catch { }
            }

            return "Unknown";
        }

        private string GetInsurancePaymentCarrier(int patid, DataTable inscompanies)
        {
            try
            {

                DataTable insCarrierData = new DataTable();
                insCarrierData = DentalData(
    "SELECT carrier.CarrierName as CarrierName, patient.PatNum as PatNum " +
    " FROM carrier, insplan, claim, claimproc, patient  " +
    " WHERE insplan.PlanNum = claim.PlanNum AND patient.PatNum = " + patid +
    " AND insplan.CarrierNum = carrier.CarrierNum " +
    " AND claimproc.ClaimNum = claim.ClaimNum " +
    " AND claimproc.PatNum = patient.PatNum " +
    " GROUP BY patient.PatNum, CarrierName " +
    " ORDER BY CarrierName"
    );

                var carrierList = (from i in insCarrierData.AsEnumerable()
                                   where i.Field<long>("PatNum") == patid
                                   select i.Field<string>("CarrierName"));

                if (carrierList.Count() > 0)
                {
                    var insCo = carrierList.FirstOrDefault();

                    if (insCo != null)
                    {
                        return insCo.Trim();
                    }
                    else
                    {
                        return "Unknown";
                    }
                }
                else
                {
                    return "Unknown";
                }
            }
            catch (Exception ex)
            {
                try
                {
                    errorLog.LogError(_appData.OfficeKeyGuid, ex);
                }
                catch { }
            }

            return "Unknown";
        }

        private Dictionary<string, string> FillTypeData(Dictionary<string, string> TypeData, string SqlStatement, string KeyColumn, string DataColumn)
        {
            DataTable dentalData = new DataTable();

            using (MySqlConnection dbConnection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand dbCommand = new MySqlCommand(SqlStatement, dbConnection))
                {
                    using (MySqlDataAdapter dbAdapter = new MySqlDataAdapter(dbCommand))
                    {
                        dbConnection.Open();
                        dbAdapter.Fill(dentalData);
                        dbConnection.Close();
                    }
                }
            }

            if (dentalData.Rows.Count > 0)
            {
                foreach (DataRow oRow in dentalData.Rows)
                {
                    if (!TypeData.ContainsKey(oRow[KeyColumn].ToString()))
                    {
                        TypeData.Add(oRow[KeyColumn].ToString(), oRow[DataColumn].ToString().TrimEnd());
                    }
                }
            }

            return TypeData;
        }

        private Dictionary<int, int> FillTypeDataInt(Dictionary<int, int> TypeData, string SqlStatement, string KeyColumn, string DataColumn)
        {
            DataTable dentalData = new DataTable();

            using (MySqlConnection dbConnection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand dbCommand = new MySqlCommand(SqlStatement, dbConnection))
                {
                    using (MySqlDataAdapter dbAdapter = new MySqlDataAdapter(dbCommand))
                    {
                        dbConnection.Open();
                        dbAdapter.Fill(dentalData);
                        dbConnection.Close();
                    }
                }
            }

            if (dentalData.Rows.Count > 0)
            {
                foreach (DataRow oRow in dentalData.Rows)
                {
                    if (!TypeData.ContainsKey(convertData.ConvertToInt(oRow[KeyColumn].ToString())))
                    {
                        TypeData.Add(convertData.ConvertToInt(oRow[KeyColumn].ToString()), convertData.ConvertToInt(oRow[DataColumn].ToString()));
                    }
                }
            }

            return TypeData;
        }

        private Dictionary<int, string> FillReferrersData()
        {
            DataTable dentalData = new DataTable();
            Dictionary<int, string> TypeData = new Dictionary<int, string>();
            string sQLStatement = "Select referralnum, lname, fname, mname From referral";

            using (MySqlConnection dbConnection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand dbCommand = new MySqlCommand(sQLStatement, dbConnection))
                {
                    using (MySqlDataAdapter dbAdapter = new MySqlDataAdapter(dbCommand))
                    {
                        dbConnection.Open();
                        dbAdapter.Fill(dentalData);
                        dbConnection.Close();
                    }
                }
            }

            if (dentalData.Rows.Count > 0)
            {
                foreach (DataRow oRow in dentalData.Rows)
                {
                    if (!TypeData.ContainsKey(convertData.ConvertToInt(oRow["referralnum"].ToString())))
                    {
                        string source = "";
                        if (oRow["fname"] != null)
                        {
                            source += oRow["fname"].ToString();
                            source = source.TrimStart();
                            source = source.TrimEnd();
                        }
                        if (oRow["mname"] != null)
                        {
                            source += " " + oRow["mname"].ToString();
                            source = source.TrimStart();
                            source = source.TrimEnd();
                        }
                        if (oRow["lname"] != null)
                        {
                            source += " " + oRow["lname"].ToString();
                            source = source.TrimStart();
                            source = source.TrimEnd();
                        }

                        TypeData.Add(convertData.ConvertToInt(oRow["referralnum"].ToString()), source);
                    }
                }
            }

            return TypeData;
        }

        private string FormatExtractionDate(DateTime ExtractionDate)
        {
            StringBuilder odbcDateString = new StringBuilder();

            odbcDateString.Append(ExtractionDate.Year);

            if (ExtractionDate.Month < 10)
            {
                odbcDateString.Append("-0" + ExtractionDate.Month);
            }
            else
            {
                odbcDateString.Append("-" + ExtractionDate.Month);
            }
            if (ExtractionDate.Day < 10)
            {
                odbcDateString.Append("-0" + ExtractionDate.Day);
            }
            else
            {
                odbcDateString.Append("-" + ExtractionDate.Day);
            }

            return odbcDateString.ToString();

        }

        private string GetReferralSource(int RefID)
        {
            if (referrerTypes.ContainsKey(RefID))
            {
                return referrerTypes[RefID];
            }
            else
            {
                return "Unknown";
            }
        }

        private string GetPaymentType(string defPayID)
        {
            if (paymentTypes.ContainsKey(defPayID))
            {
                return paymentTypes[defPayID];
            }
            else
            {
                return "Unknown";
            }
        }

        private string GetAdjustmentType(string defid)
        {
            if (adjustmentTypes.ContainsKey(defid))
            {
                string adjType = adjustmentTypes[defid];
                if (!Char.IsLetter(adjType.FirstOrDefault()))
                {
                    adjType = adjType.Remove(0, 1);
                }

                return adjType;
            }
            else
            {
                return "Unknown";
            }
        }

        private int GetInsuredID(int defid)
        {
            if (insuredList.ContainsKey(defid))
            {
                return insuredList[defid];
            }
            else
            {
                return -1;
            }
        }

        private string GetPatientProviders(string Prov1, string Prov2)
        {
            if (Prov2 != null && Prov2 != "")
            {
                return Prov1 + "," + Prov2;
            }

            return Prov1;
        }

        private int DaysBetweenMonths(DateTime Date1, DateTime Date2)
        {
            return (Date1 - Date2).Days;
        }
    }
}
