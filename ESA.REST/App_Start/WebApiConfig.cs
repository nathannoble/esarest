﻿
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

using System.Web.Http.OData.Builder;
using ESA.REST.Models;


namespace ESA.REST
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Step 1
            // http://www.asp.net/web-api/overview/web-api-routing-and-actions/attribute-routing-in-web-api-2
            // Step 2
            // http://stackoverflow.com/questions/19575600/why-do-i-get-an-invalidoperationexception-when-i-try-to-use-attribute-routing-wi

            // Default  to JSON Reponse
            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

            // Default webapi routes
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
           
            // Use Attribute Routes
            config.MapHttpAttributeRoutes();

            // Map odata endpoints                                                                                                                                                                                      
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<tblProgressEmployee>("ProgressEmployee");
            builder.EntitySet<vPortalEmployee>("PortalEmployee");
            builder.EntitySet<tblProgressPlan>("ProgressPlan");
            builder.EntitySet<tblProgressCompany>("ProgressCompany");
            builder.EntitySet<vProgressClient>("ProgressClient");
            builder.EntitySet<vProgressClientOrig>("ProgressClientOrig");
            //builder.EntitySet<vProgressClientOrig_2>("ProgressClientOrig_2");
            builder.EntitySet<tblProgressClient>("ProgressClientTable");
            builder.EntitySet<tblProgressData>("ProgressData");
            builder.EntitySet<vProgressStatData>("ProgressStatData");
            builder.EntitySet<vProgressClientData_2>("ProgressClientData");
            builder.EntitySet<vProgressWorkingDay>("ProgressWorkingDay");

            builder.EntitySet<vPortalHoliday>("PortalHoliday");
            builder.EntitySet<tblPortalScoreBoard>("PortalScoreBoard");
            builder.EntitySet<tblPortalScoreBoardStat>("PortalScoreBoardStat");
            builder.EntitySet<vPortalTimeCard>("PortalTimeCard");
            builder.EntitySet<tblPortalTip>("PortalTip");
            builder.EntitySet<tblPortalValue>("PortalValue");
            builder.EntitySet<tblPortalUser>("PortalUserTable");
            builder.EntitySet<vPortalUser>("PortalUser");
            builder.EntitySet<tblPortalVendor>("PortalVendor");
            builder.EntitySet<tblPortalMessage>("PortalMessage");
            builder.EntitySet<vPortalComment>("PortalComment");
            builder.EntitySet<tblPortalMessagesComment>("PortalMessagesComment");
            builder.EntitySet<vPortalClientsNote>("PortalClientsNote");
            builder.EntitySet<tblProgressClientsNote>("ProgressClientsNote");
            builder.EntitySet<vPortalCompaniesNote>("PortalCompaniesNote");
            builder.EntitySet<tblProgressCompaniesNote>("ProgressCompaniesNote");
            builder.EntitySet<vPortalEmployeesNote>("PortalEmployeesNote");
            builder.EntitySet<tblProgressEmployeesNote>("ProgressEmployeesNote");
            builder.EntitySet<vPortalTimerTaskBreakdown>("PortalTimerTaskBreakdown");
            builder.EntitySet<vSelectScheduler>("SelectScheduler");

            builder.EntitySet<tblPortalTimer>("PortalTimer");
            //builder.EntitySet<tblPortalTimer>("PortalTimerIntTimerID");

            builder.EntitySet<tblPortalServiceItem>("PortalServiceItem");
            builder.EntitySet<vPortalTimerTotalTime>("PortalTimerTotalTime");
            builder.EntitySet<vPortalTimerTotalClientTime>("PortalTimerTotalClientTime");
            builder.EntitySet<tblClientPlan>("ClientPlan");
            builder.EntitySet<tblPortalAdmin>("PortalAdmin");
            builder.EntitySet<tblPortalTimerLog>("PortalTimerLog");

            config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());

            // Enable CORs
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
             
        }
    }
}
