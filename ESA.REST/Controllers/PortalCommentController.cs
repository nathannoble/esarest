﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalComment>("PortalComment");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalCommentController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalComment
        [Queryable]
        public IQueryable<vPortalComment> GetPortalComment()
        {
            try
            {
                return db.vPortalComments;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalComment
        [Queryable]
        public SingleResult<vPortalComment> GetPortalComment([FromODataUri] int key)
        {
            return SingleResult.Create(db.vPortalComments.Where(PortalComment => PortalComment.commentID == key));
        }

        // PUT odata/PortalComment(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, vPortalComment rec)
        //public IHttpActionResult Put([FromODataUri] string key,  vPortalComment rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, vPortalComment rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.commentID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!PortalCommentExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<vPortalComment[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalComment
        //public IHttpActionResult Post(PortalComment PortalComment)
        public HttpResponseMessage Post(vPortalComment rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }
           
            db.vPortalComments.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalComment);
            return Request.CreateResponse <vPortalComment[]>(HttpStatusCode.Created, new[] { rec }); //vPortalComment
        }

        // PATCH odata/vPortalComment
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vPortalComment> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vPortalComment rec = db.vPortalComments.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalCommentExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalComment
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vPortalComment PortalComment = db.vPortalComments.Find(key);
            if (PortalComment == null)
            {
                return NotFound();
            }

            foreach (vPortalComment rec in db.vPortalComments.Where(e => e.commentID == key))
            {
                db.vPortalComments.Remove(rec);
            }

            db.vPortalComments.Remove(PortalComment);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalComment PortalComment = db.PortalComments.Find(key);
        //    if (PortalComment == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalComments.Remove(PortalComment);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalCommentExists(int key)
        {
            return db.vPortalComments.Count(e => e.commentID == key) > 0;
        }
    }
}
