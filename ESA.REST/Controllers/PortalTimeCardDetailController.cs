﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalTimeCard>("PortalTimeCard");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalTimeCardController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalTimeCard
        [Queryable]
        public IQueryable<vPortalTimeCard> GetPortalTimeCard()
        {
            try
            {
                return db.vPortalTimeCards;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalTimeCard
        [Queryable]
        public SingleResult<vPortalTimeCard> GetPortalTimeCard([FromODataUri] int key)
        {
            return SingleResult.Create(db.vPortalTimeCards.Where(PortalTimeCard => PortalTimeCard.intTimerID == key));
        }

        // PUT odata/PortalTimeCard(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, vPortalTimeCard rec)
        //public IHttpActionResult Put([FromODataUri] string key,  vPortalTimeCard rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, vPortalTimeCard rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.intTimerID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!PortalTimeCardExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<vPortalTimeCard[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalTimeCard
        //public IHttpActionResult Post(PortalTimeCard PortalTimeCard)
        public HttpResponseMessage Post(vPortalTimeCard rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vPortalTimeCards.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalTimeCard);
            return Request.CreateResponse <vPortalTimeCard[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/vPortalTimeCard
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vPortalTimeCard> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vPortalTimeCard rec = db.vPortalTimeCards.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalTimeCardExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalTimeCard
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vPortalTimeCard PortalTimeCard = db.vPortalTimeCards.Find(key);
            if (PortalTimeCard == null)
            {
                return NotFound();
            }

            foreach (vPortalTimeCard rec in db.vPortalTimeCards.Where(e => e.intTimerID == key))
            {
                db.vPortalTimeCards.Remove(rec);
            }

            db.vPortalTimeCards.Remove(PortalTimeCard);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalTimeCard PortalTimeCard = db.PortalTimeCards.Find(key);
        //    if (PortalTimeCard == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalTimeCards.Remove(PortalTimeCard);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalTimeCardExists(int key)
        {
            return db.vPortalTimeCards.Count(e => e.intTimerID == key) > 0;
        }
    }
}
