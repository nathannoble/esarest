﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalValue>("PortalValue");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalValueController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalValue
        [Queryable]
        public IQueryable<tblPortalValue> GetPortalValue()
        {
            try
            {
                return db.tblPortalValues;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalValue
        [Queryable]
        public SingleResult<tblPortalValue> GetPortalValue([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblPortalValues.Where(PortalValue => PortalValue.valueID == key));
        }

        // PUT odata/PortalValue(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblPortalValue rec)
        //public IHttpActionResult Put([FromODataUri] string key,  PortalValue rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, PortalValue rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.valueID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalValueExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<PortalValue[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalValue
        //public IHttpActionResult Post(PortalValue PortalValue)
        public HttpResponseMessage Post(tblPortalValue rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalValues.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalValue);
            return Request.CreateResponse <tblPortalValue[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/PortalValue
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblPortalValue> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblPortalValue rec = db.tblPortalValues.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalValueExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalValue
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblPortalValue rec2 = db.tblPortalValues.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblPortalValue rec in db.tblPortalValues.Where(e => e.valueID == key))
            {
                db.tblPortalValues.Remove(rec);
            }

            db.tblPortalValues.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalValue PortalValue = db.PortalValues.Find(key);
        //    if (PortalValue == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalValues.Remove(PortalValue);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalValueExists(int key)
        {
            return db.tblPortalValues.Count(e => e.valueID == key) > 0;
        }
    }
}
