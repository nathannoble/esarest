﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Diagnostics;
using Newtonsoft.Json;

using System.Data;
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;

using System.Linq;
using System.Web.Http;
using System.Web.Security;
using System.Web.Http.Cors;

using ESA.REST.Utilities;
using ESA.REST.Models;
using System.Net;
using System.Web;

namespace ESA.REST.Models
{
    public partial class timerThisClient
    {
        public long clientID { get; set; }
        public DateTime workDate { get; set; }
        public double schedulerTime { get; set; }
        public int? schedulerCalls { get; set; }
        public int? schedulerMeetings { get; set; }
        public double support1Time { get; set; }
        public int? support1Calls { get; set; }
        public int? support1Meetings { get; set; }
        public double compTime { get; set; }
        public int? compCalls { get; set; }
        public int? compMeetings { get; set; }
        public double adminTime { get; set; }
    }
}
namespace ESA.REST.Controllers
{
    //[EnableCors(origins: "http://myclient.azurewebsites.net", headers: "*", methods: "*")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ESARestController : ApiController
    {
        private Entities db = new Entities();

        private List<tblPortalServiceItem> serviceRecords = new List<tblPortalServiceItem>();

        [Route("rest/getVersion")]
        public int GetVersion()
        {
            return 2074;
        }

        [Route("rest/GetCalcClientStats/{clientID}/{startDate}/{endDate}")]
        public bool GetCalcClientStats(string clientID, string startDate, string endDate)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            string logFile = "ESA_Admin\\RestGetCalcClientStatsLog_" + DateTime.Now.ToLocalTime().ToShortDateString() + "_" + DateTime.Now.ToLocalTime().ToShortTimeString() + ".txt";
            logFile = logFile.Replace("/", "-");
            logFile = logFile.Replace(" ", "_");
            logFile = logFile.Replace(":", "_");
            logFile = "C:\\" + logFile;
            try
            {

                using (StreamWriter outfile = new StreamWriter(logFile, true))
                {
                    outfile.WriteLine("------------------------------------------------");
                    outfile.WriteLine("------------------------------------------------");
                    outfile.WriteLine("Date:      " + DateTime.Now.ToLocalTime().ToLongTimeString());
                    outfile.WriteLine("Message:   Running GetCalcClientsStats for client:startDate:endDate:" + clientID + ":" + startDate + ":" + endDate);
                    outfile.WriteLine("Status:    Starting run...");

                    Debug.WriteLine("------------------------------------------------");
                    Debug.WriteLine("------------------------------------------------");
                    Debug.WriteLine("Date:      " + DateTime.Now.ToLocalTime().ToLongTimeString());
                    Debug.WriteLine("Message:   Running GetCalcClientsStats for client:startDate:endDate:" + clientID + ":" + startDate + ":" + endDate);
                    Debug.WriteLine("Status:    Starting run...");

                    // Loops through clients (if 'all' passed)
                    // loops through dates from startDate to endDate and create list of dates
                    var workDateValue = DateTime.Parse(startDate);
                    List<DateTime> dates = new List<DateTime>();
                    var i = 1;

                    while (workDateValue <= DateTime.Parse(endDate))
                    {
                        dates.Add(workDateValue);
                        workDateValue = workDateValue.AddDays(1);
                        i++;
                        if (i > 100)  //only calc 100 days max for now
                        {
                            break;
                        }
                    }

                    // All active client records
                    List<vProgressClientOrig> clientAllNameRecords = db.vProgressClientOrigs.Where(x => x.status == "4 - Active").ToList();

                    if (clientID != "all")
                    {
                        var clientIDNum = int.Parse(clientID);
                        clientAllNameRecords = db.vProgressClientOrigs.Where(x => x.clientID == clientIDNum).ToList();
                    }


                    //                using (SqlConnection connection = new SqlConnection(connectionString))
                    //                {
                    //                    connection.Open();

                    //                    string sql = String.Format(
                    //"SELECT   * " +
                    //"FROM         dbo.vPortalTimerTotalClientTime " +
                    //"WHERE workDate BETWEEN '{0}' AND '{1}' " +
                    ////AND companyID = {2} " +
                    //"ORDER BY clientID, serviceID, workDate", startDate, endDate);

                    //                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    //                    SqlDataAdapter adapter = new SqlDataAdapter();
                    //                    adapter.SelectCommand = selectCommand;
                    //                    adapter.Fill(totalTime);

                    //List<vPortalTimerTotalClientTime> totalTime = db.vPortalTimerTotalClientTimes.ToList().Where(x => (DateTime.ParseExact(x.workDate, "yyyy-MM-dd", null) >= DateTime.ParseExact(startDate, "yyyy-MM-dd", null) && DateTime.ParseExact(x.workDate, "yyyy-MM-dd", null) <= DateTime.ParseExact(endDate, "yyyy-MM-dd", null))).ToList();


                    foreach (vProgressClientOrig clientData in clientAllNameRecords)
                    {
                        List<timerThisClient> timerThisClientRecords = new List<timerThisClient>();

                        foreach (DateTime date in dates)
                        {
                            var day = date.ToString("yyyy-MM-dd");
                            DataTable totalTime = new DataTable();

                            using (SqlConnection connection = new SqlConnection(connectionString))
                            {
                                connection.Open();

                                string sql = String.Format(
            "SELECT   * " +
            " FROM         dbo.vPortalTimerTotalClientTime " +
            " WHERE workDate = '{0}' AND TotalTimeWorked IS NOT NULL" +
            " AND clientID = {1}" +
            " ORDER BY clientID, serviceID, workDate", day, clientData.clientID);

                                SqlCommand selectCommand = new SqlCommand(sql, connection);

                                SqlDataAdapter adapter = new SqlDataAdapter();
                                adapter.SelectCommand = selectCommand;
                                adapter.Fill(totalTime);

                                var timerClientDateRecords = totalTime.AsEnumerable().Select(r => new vPortalTimerTotalClientTime()
                                {
                                    rowID = (long)r["rowID"],
                                    clientID = (Nullable<int>)r["clientID"],
                                    serviceID = (Nullable<int>)r["serviceID"],
                                    workDate = (string)r["workDate"],
                                    totalTimeWorked = (Nullable<decimal>)r["totalTimeWorked"],
                                    totalTasks = r.Field<int?>("totalTasks"),
                                    totalMeetings = r.Field<int?>("totalMeetings"),
                                    //totalTasks = (Nullable<int>)r["totalTasks"],
                                    //totalMeetings = (Nullable<int>)r["totalMeetings"],
                                    workYear = (Nullable<int>)r["workYear"],
                                    workMonth = (Nullable<int>)r["workMonth"],
                                    workDay = (Nullable<int>)r["workDay"],
                                }).ToList();
                                //var timerClientDateRecords2 = from vPortalTimerTotalClientTime row in db.vPortalTimerTotalClientTimes //DataRow row in totalTime
                                //                             where ((row.clientID == clientData.clientID && row.clientID != null) && row.workDate == day)
                                //                             select row;

                                //var timerClientDateRecords = db.vPortalTimerTotalClientTimes.Where(row =>(row.workDate == day && row.clientID.ToString() == clientData.clientID.ToString()));

                                outfile.WriteLine("Timer records for Client ID: " + clientData.clientID + " for date " + date.ToShortDateString() + ":" + JsonConvert.SerializeObject(timerClientDateRecords));
                                Debug.WriteLine("Timer records for Client ID: " + clientData.clientID + " for date " + date.ToShortDateString() + ":" + JsonConvert.SerializeObject(timerClientDateRecords));

                                double sumSchedulerTime = 0;
                                int? sumSchedulerCalls = 0;
                                int? sumSchedulerMeetings = 0;

                                double sumSupport1Time = 0;
                                int? sumSupport1Calls = 0;
                                int? sumSupport1Meetings = 0;

                                double sumCompTime = 0;
                                int? sumCompCalls = 0;
                                int? sumCompMeetings = 0;

                                double sumAdminTime = 0;

                                if (timerClientDateRecords.Count() > 0)
                                {
                                    //foreach (DataRow row in totalTime)
                                    foreach (vPortalTimerTotalClientTime record in timerClientDateRecords)
                                    {
                                        //vPortalTimerTotalClientTime record = new vPortalTimerTotalClientTime();
                                        //record.Load(row.ItemArray);
                                        double totalTimeWorked = 0;
                                        if (record.totalTimeWorked != null)
                                        {
                                            totalTimeWorked = (double)record.totalTimeWorked;
                                        }

                                        var serviceRecord = db.tblPortalServiceItems.Where(x => x.serviceID == record.serviceID).SingleOrDefault();

                                        if (serviceRecord.recordTasksMtgs == false && record.serviceID != 1013 && record.serviceID != 1001)
                                        {
                                            sumAdminTime += totalTimeWorked;
                                        }
                                        else
                                        {

                                            if (record.serviceID == 1011)
                                            {
                                                //Support Scheduling
                                                sumSupport1Time += totalTimeWorked;
                                                if (record.totalTasks != null)
                                                    sumSupport1Calls += record.totalTasks;
                                                if (record.totalMeetings != null)
                                                    sumSupport1Meetings += record.totalMeetings;
                                            }
                                            else if (record.serviceID == 1008)
                                            {
                                                // Comp Time
                                                sumCompTime += totalTimeWorked;
                                                if (record.totalTasks != null)
                                                    sumCompCalls += record.totalTasks;
                                                if (record.totalMeetings != null)
                                                    sumCompMeetings += record.totalMeetings;
                                            }
                                            else
                                            {
                                                // Scheduling (1001) and other
                                                sumSchedulerTime += totalTimeWorked;
                                                if (record.totalTasks != null)
                                                    sumSchedulerCalls += record.totalTasks;
                                                if (record.totalMeetings != null)
                                                    sumSchedulerMeetings += record.totalMeetings;
                                            }
                                        }
                                    }
                                }

                                timerThisClient newRecord = new timerThisClient();
                                newRecord.clientID = clientData.clientID;
                                newRecord.workDate = date;
                                newRecord.schedulerTime = sumSchedulerTime;
                                newRecord.schedulerCalls = sumSchedulerCalls;
                                newRecord.schedulerMeetings = sumSchedulerMeetings;
                                newRecord.support1Time = sumSupport1Time;
                                newRecord.support1Calls = sumSupport1Calls;
                                newRecord.support1Meetings = sumSupport1Meetings;
                                newRecord.compTime = sumCompTime;
                                newRecord.compCalls = sumCompCalls;
                                newRecord.compMeetings = sumCompMeetings;
                                newRecord.adminTime = sumAdminTime;

                                outfile.WriteLine("Total timerThisClient record for Client ID: " + clientData.clientID + " for date " + date.ToShortDateString() + ":" + JsonConvert.SerializeObject(newRecord));
                                Debug.WriteLine("Total timerThisClient record for Client ID: " + clientData.clientID + " for date " + date.ToShortDateString() + ":" + JsonConvert.SerializeObject(newRecord));

                                timerThisClientRecords.Add(newRecord);
                            }
                        }

                        // now populate ProgressData
                        DateTime now = DateTime.Now;

                        foreach (DateTime date2 in dates)
                        {

                            timerThisClient timerRecord = null;
                            List<timerThisClient> timerRecords = (from timerThisClient row in timerThisClientRecords
                                                                  where row.workDate.Date.ToString("yyyy-MM-dd") == date2.Date.ToString("yyyy-MM-dd") //&& (int)row["clientID"] == clientData.clientID
                                                                  select row).ToList();

                            var startDate2 = date2;
                            var endDate2 = date2.AddDays(1);
                            var progressRecord = (from tblProgressData x in db.tblProgressDatas
                                                  where (DateTime)x.workDate >= startDate2 && (DateTime)x.workDate < endDate2 && x.clientID == clientData.clientID
                                                  select x).ToList();
                            //List<tblProgressData> progressRecord2 = db.tblProgressDatas.Where(x => ((DateTime)x.workDate).Date.ToString("yyyy-MM-dd") == date2.Date.ToString("yyyy-MM-dd") && x.clientID == clientData.clientID).ToList();


                            if (timerRecords.Count > 0)
                            {
                                timerRecord = timerRecords.First();
                            }
                            else
                            {
                                outfile.WriteLine("No timer record for Client ID: " + clientData.clientID + " for date " + date2.ToShortDateString());
                                Debug.WriteLine("No timer record for Client ID: " + clientData.clientID + " for date " + date2.ToShortDateString());

                                timerRecord = null;
                                continue;
                            }

                            if (progressRecord.Count > 0)
                            {
                                foreach (tblProgressData progressDataRecord in progressRecord.Reverse<tblProgressData>())
                                {
                                    //var progressDataRecord = progressRecord[0];

                                    //progressDataRecord.schedulerTime = timerRecord.schedulerTime;
                                    //progressDataRecord.schedulerCalls = timerRecord.schedulerCalls;
                                    //progressDataRecord.schedulerMeetings = timerRecord.schedulerMeetings;

                                    //progressDataRecord.support1Time = timerRecord.support1Time;
                                    //progressDataRecord.support1Calls = timerRecord.support1Calls;
                                    //progressDataRecord.support1Meetings = timerRecord.support1Meetings;

                                    //progressDataRecord.compTime = timerRecord.compTime;
                                    //progressDataRecord.compCalls = timerRecord.compCalls;
                                    //progressDataRecord.compMeetings = timerRecord.compMeetings;

                                    //progressDataRecord.adminTime = timerRecord.adminTime;

                                    // Remove existing record
                                    outfile.WriteLine("Old ProgressData record for Client ID: " + clientData.clientID + " for date " + date2.ToShortDateString() + " :cid:date:adminTime:compTime:scheduleTime:" + progressDataRecord.clientID + ", " + progressDataRecord.workDate + ", " + progressDataRecord.adminTime + ", " + progressDataRecord.compTime + ", " + progressDataRecord.schedulerTime);
                                    outfile.WriteLine("Deleted old ProgressData record for Client ID: " + clientData.clientID + " for date " + date2.ToShortDateString() + " of this many ProgressData records: " + progressRecord.Count);
                                    Debug.WriteLine("Old ProgressData record for Client ID: " + clientData.clientID + " for date " + date2.ToShortDateString() + " :cid:date:adminTime:compTime:scheduleTime:" + progressDataRecord.clientID + ", " + progressDataRecord.workDate + ", " + progressDataRecord.adminTime + ", " + progressDataRecord.compTime + ", " + progressDataRecord.schedulerTime);
                                    Debug.WriteLine("Deleted old ProgressData record for Client ID: " + clientData.clientID + " for date " + date2.ToShortDateString() + " of this many ProgressData records: " + progressRecord.Count);

                                    db.tblProgressDatas.Remove(progressDataRecord);
                                }
                            }
                            //else
                            //{


                            if (timerRecord.schedulerTime > 0 || timerRecord.schedulerCalls > 0 || timerRecord.schedulerMeetings > 0 ||
                            timerRecord.support1Time > 0 || timerRecord.support1Calls > 0 || timerRecord.support1Meetings > 0 ||
                            timerRecord.compTime > 0 || timerRecord.compCalls > 0 || timerRecord.compMeetings > 0 || timerRecord.adminTime > 0)
                            {
                                // Create new progressData record
                                var progressDataRecord2 = new tblProgressData();

                                progressDataRecord2.clientID = clientData.clientID;
                                progressDataRecord2.timeStamp = now;
                                progressDataRecord2.workDate = date2;
                                progressDataRecord2.schedulerID = 0;
                                //progressDataRecord2.schedulerTime = 0;
                                //progressDataRecord2.schedulerCalls = 0;
                                //progressDataRecord2.schedulerMeetings = 0;
                                progressDataRecord2.support1ID = 0;
                                //progressDataRecord2.support1Time = 0;
                                //progressDataRecord2.support1Calls = 0;
                                //progressDataRecord2.support1Meetings = 0;
                                progressDataRecord2.support2ID = 0;
                                progressDataRecord2.support2Time = 0;
                                progressDataRecord2.support2Calls = 0;
                                progressDataRecord2.support2Meetings = 0;
                                progressDataRecord2.compID = 0;
                                //progressDataRecord2.compTime = 0;
                                //progressDataRecord2.compCalls = 0;
                                //progressDataRecord2.compMeetings = 0;
                                //progressDataRecord2.adminTime = 0;
                                progressDataRecord2.schedulerTime = timerRecord.schedulerTime;
                                progressDataRecord2.schedulerCalls = timerRecord.schedulerCalls;
                                progressDataRecord2.schedulerMeetings = timerRecord.schedulerMeetings;

                                progressDataRecord2.support1Time = timerRecord.support1Time;
                                progressDataRecord2.support1Calls = timerRecord.support1Calls;
                                progressDataRecord2.support1Meetings = timerRecord.support1Meetings;

                                progressDataRecord2.compTime = timerRecord.compTime;
                                progressDataRecord2.compCalls = timerRecord.compCalls;
                                progressDataRecord2.compMeetings = timerRecord.compMeetings;

                                progressDataRecord2.adminTime = timerRecord.adminTime;

                                db.tblProgressDatas.Add(progressDataRecord2);

                                outfile.WriteLine("New ProgressData record for Client ID: " + clientData.clientID + " for date " + date2.ToShortDateString() + " :cid:date:adminTime:compTime:scheduleTime:" + progressDataRecord2.clientID + ", " + progressDataRecord2.workDate + ", " + progressDataRecord2.adminTime + ", " + progressDataRecord2.compTime + ", " + progressDataRecord2.schedulerTime);
                                outfile.WriteLine("Added new ProgressData record for Client ID: " + clientData.clientID + " for date " + date2.ToShortDateString());
                                Debug.WriteLine("New ProgressData record for Client ID: " + clientData.clientID + " for date " + date2.ToShortDateString() + " :cid:date:adminTime:compTime:scheduleTime:" + progressDataRecord2.clientID + ", " + progressDataRecord2.workDate + ", " + progressDataRecord2.adminTime + ", " + progressDataRecord2.compTime + ", " + progressDataRecord2.schedulerTime);
                                Debug.WriteLine("Added new ProgressData record for Client ID: " + clientData.clientID + " for date " + date2.ToShortDateString());

                            }
                            else
                            {
                                //console.log('SubHeaderView:calculateProgressData:no time for this date:clientID:workDateValue:' + clientID + ":" + date2);
                            }

                        }

                        outfile.WriteLine("Processed Client ID: " + clientData.clientID);
                        Debug.WriteLine("Processed Client ID: " + clientData.clientID);
                    }

                    var emptyTimerRecords = (from tblPortalTimer x in db.tblPortalTimers
                                             where x.timerAction != "none" && x.intTimerID == null
                                             select x).ToList();

                    outfile.WriteLine("Finished processing all clients.");
                    Debug.WriteLine("Finished processing all clients.");

                    if (emptyTimerRecords.Count > 0)
                    {
                        foreach (tblPortalTimer emptyTimerRecord in emptyTimerRecords.Reverse<tblPortalTimer>())
                        {


                            // Remove existing record
                            outfile.WriteLine("Deleted empty timer record: " + emptyTimerRecord.timerID + " for date " + emptyTimerRecord.startTime.ToString() + " of this many PortalTimer records: " + emptyTimerRecords.Count);
                            Debug.WriteLine("Deleted empty timer record: " + emptyTimerRecord.timerID + " for date " + emptyTimerRecord.startTime.ToString() + " of this many PortalTimer records: " + emptyTimerRecords.Count);

                            db.tblPortalTimers.Remove(emptyTimerRecord);
                        }
                    }

                    db.SaveChanges();

                }
            }
            catch (Exception e)
            {
                throw;

            }

            return true; // new SerializableDataTable(totalTime);
        }

        [Route("rest/GetResetPassword/{userKey}/{email}")]
        public string GetResetPassword(int userKey, string email)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            string newPassword = (Membership.GeneratePassword(8,1));
            string sql = "";
            try
            {
                email = HttpUtility.UrlDecode(email);

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    sql = String.Format(
" update tblPortalUsers " +
" SET prevPasswords = " +
" CASE " +
"   WHEN prevPasswords IS NULL THEN userPassword " +
"   ELSE " +
" CASE WHEN prevPasswords LIKE '%' + userPassword + '%'" +
" THEN prevPasswords " +
" ELSE prevPasswords + ',' + userPassword END" +
"   END " +
" , passwordCode = '{0}' " +
" where userKey = {1};", newPassword, userKey);

                     SqlCommand selectCommand = new SqlCommand(sql, connection);
                     selectCommand.ExecuteNonQuery();
                }


            }
            catch (Exception ex)
            {
                string m = "GetResetPassword Exception: " + ex.ToString();
                Console.WriteLine(m);
                return m;
            }

            try
            {
                string result = sendEmail(email, newPassword,sql);
                return result;
            }
            catch (Exception ex)
            {
                string m = "sendEmail Exception:1: " + ex.ToString();
                Console.WriteLine(m);
                return m;
            }
        }

        private string sendEmail(string email, string password, string sql)
        {

            string targetPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string url = "https://secure.esasolutions.net/portal/";
            if (targetPath.Contains("test"))
            {
                url = "https://secure.esasolutions.net/esaPortalTest/release/";
                //url = "http://localhost/esaPortal/public/";
            }
            var fromAddress = new MailAddress("no-reply@esasolutions.com");
            var fromPassword = "Esaportal1";
            var toAddress = new MailAddress(email);
            string body = "Click the following link to update your password and log in to your ESA Portal account:\n\n" + url + "#setPassword?resetId=" + WebUtility.UrlEncode(password); // + "\n\n\n" +targetPath;
            string subject = "ESA Portal Password Reset";

            var client = new SmtpClient("smtp.office365.com", 587)
            {
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                EnableSsl = true
            };

            try
            {
                client.Send(fromAddress.Address, email, subject,body);
                return "Password changed successfully"; // + ", " + sql ;
            }
            catch (Exception ex)
            {
                string m = "sendEmail Exception:2: " + ex.ToString();
                Console.WriteLine(m);
                return m;
            }
        }

        [Route("rest/GetTop5Day/{year}/{month}/{day}")]
        public SerializableDataTable GetTop5Day(int year, int month, int day)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = String.Format(
" select top (5) employeeName, cast(avg(mtgsPerHour) as decimal(10,2)) AS mtgsPerHour " +
" from vPortalTop5_Day_2 " +
" where workYear = {0} and workMonth = {1} and workDay = {2} and status LIKE '1%' " +
" group by employeeName " +
" order by mtgsPerHour DESC", year, month, day);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetTop5Month/{year}/{month}")]
        public SerializableDataTable GetTop5Month(int year, int month)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = String.Format(
" select top (20) employeeID, employeeName, cast(avg(mtgsPerHour) as decimal(10,2)) AS mtgsPerHour " +
" from vPortalTop5_Month_2 " +
" where workYear = {0} " +
" and workMonth = {1} " +
" and status LIKE '1%' " +
" group by employeeID, employeeName " +
" order by mtgsPerHour DESC", year, month);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetMyOpenTimers/{employeeID}")]
        public SerializableDataTable GetMyOpenTimers(int employeeID)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = String.Format(
                    "SELECT workDate, min(startTime)AS startTime, max(stopTime) AS stopTime, sum(timeWorked) AS timeWorked," +
                    " clientID, clientName, customerName, companyID, clientCompany, employeeID, employeeName," +
                    " serviceID, serviceItem, entryType, intTimerID, count(intTimerID) AS idCount," +
                    " max(tasks) AS tasks, max(meetings) AS meetings, max(recordType) AS recordType, max(timerAction) AS timerAction," +
                    " max(timerNote) AS timerNote, max(mgrNote) AS mgrNote, editedByName" +
                    " FROM [dbo].[vPortalTimeCard]" +

                    " WHERE       intTimerID IN(" +

                    " SELECT      intTimerID" +
                    " FROM [dbo].[vPortalTimeCard]" +
                    " WHERE(employeeID = {0} AND recordType LIKE '%NORM%'  AND recordType NOT LIKE '%STOP%')" +
                    " OR (employeeID = {0} AND timerAction NOT LIKE 'none')" +
                    " GROUP BY    intTimerID)" +

                    " GROUP BY    workDate, companyID, clientCompany, clientID, clientName, customerName, employeeID, employeeName, " +
                                  "serviceID, serviceItem, entryType, intTimerID, editedByName" +
                    " ORDER BY    workDate", employeeID);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetOpenTimers/{date}")]
        public SerializableDataTable GetOpenTimers(string date)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = String.Format(
                    "SELECT workDate, min(startTime)AS startTime, max(stopTime) AS stopTime, sum(timeWorked) AS timeWorked," +
                    " clientID, clientName, customerName, companyID, clientCompany, employeeID, employeeName," +
                    " serviceID, serviceItem, entryType, " +
                    " intTimerID, " +
                    //" CASE WHEN intTimerID IS NULL THEN timerID ELSE intTimerID END AS intTimerID, " +
                    " count(intTimerID) AS idCount," +
                    " max(tasks) AS tasks, max(meetings) AS meetings, max(recordType) AS recordType, max(timerAction) AS timerAction," +
                    " max(timerNote) AS timerNote, max(mgrNote) AS mgrNote, editedByName" +
                    " FROM [dbo].[vPortalTimeCard]" +

                    " WHERE       intTimerID IN(" +

                    " SELECT      " +
                    "intTimerID" +
                    //" CASE WHEN intTimerID IS NULL THEN timerID ELSE intTimerID END AS intTimerID " +
                    " FROM [dbo].[vPortalTimeCard]" +
                    " WHERE(workDate < '{0}' AND recordType LIKE '%NORM%'  AND recordType NOT LIKE '%STOP%')" +
                    " OR(workDate < '{0}' AND timerAction NOT LIKE 'none')" +
                    " GROUP BY    " +
                    "intTimerID) " +
                    //" CASE WHEN intTimerID IS NULL THEN timerID ELSE intTimerID END AS intTimerID) " +
                    " OR intTimerID IS NULL " +

                    " GROUP BY    workDate, companyID, clientCompany, clientID, clientName, customerName, employeeID, employeeName, " +
                                  "serviceID, serviceItem, entryType, " +
                                  "intTimerID, " +
                                  //" CASE WHEN intTimerID IS NULL THEN timerID ELSE intTimerID END AS intTimerID, " +
                                  "editedByName" +
                    " ORDER BY    workDate", date);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetTimeCardDetailForOneIntTimerID/{intTimerID}")]
        public SerializableDataTable GetTimeCardDetailForOneIntTimerID(int intTimerID)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string sql = String.Format(
"SELECT     TOP (10000) workDate, MIN(startTime) AS startTime, " +
"MAX(stopTime) AS stopTime, SUM(timeWorked) AS timeWorked, clientID, " +
"clientName, customerName, companyID, clientCompany, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID, " +
"COUNT(intTimerID) AS idCount, SUM(tasks) AS tasks, SUM(meetings) AS meetings, " +
"SUM(tasksCalls) AS tasksCalls, SUM(tasksEmails) AS tasksEmails, SUM(tasksVoiceMails) AS tasksVoiceMails, " +
"MAX(recordType) AS recordType, MAX(timerAction) AS timerAction, MAX(timerNote) AS timerNote, " +
"MAX(mgrNote) AS mgrNote " +
"FROM         dbo.vPortalTimeCard " +
" WHERE intTimerID = {0}" +
" GROUP BY workDate, companyID, clientCompany, clientID, clientName, customerName, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID " +
"ORDER BY workDate", intTimerID);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetTimeCardDetailByIntTimerID/{intTimerID}")]
        public SerializableDataTable GetTimeCardDetailByIntTimerID(int intTimerID)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string sql = String.Format(
"SELECT     TOP (100) workDate,startTime,stopTime, timeWorked, clientID, " +
"clientName, customerName, companyID, clientCompany, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID,timerID, " +
"tasks, meetings, tasksCalls, tasksEmails, tasksVoiceMails, recordType, timerAction,timerNote, " +
"mgrNote, editedByName " +
"FROM         dbo.vPortalTimeCard " +
" WHERE intTimerID = {0}", intTimerID);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }


        [Route("rest/GetTimeCardDetailByIntTimerID/{startTime}/{stopTime}/{sortField}")]
        public SerializableDataTable GetTimeCardDetailByIntTimerID(string startTime, string stopTime, string sortField)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string sql = String.Format(
"SELECT     TOP (50000) workDate, MIN(startTime) AS startTime, " +
"MAX(stopTime) AS stopTime, SUM(timeWorked) AS timeWorked, clientID, " +
"clientName, customerName, companyID, clientCompany, employeeID, " +
"employeeName, serviceID, serviceItem, " +
//"entryType, " + 
"intTimerID, " +
//"COUNT(intTimerID) AS idCount, " + 
"SUM(tasks) AS tasks, SUM(meetings) AS meetings, " +
//"SUM(tasksCalls) AS tasksCalls, SUM(tasksEmails) AS tasksEmails, SUM(tasksVoiceMails) AS tasksVoiceMails, " +
"MAX(recordType) AS recordType, MAX(timerAction) AS timerAction, MAX(timerNote) AS timerNote, " +
"MAX(mgrNote) AS mgrNote, MAX(editedByName) AS editedByName " +
"FROM         dbo.vPortalTimeCard " +
 "WHERE workDate BETWEEN '{0}' AND '{1}' " +
"GROUP BY workDate, companyID, clientCompany, clientID, clientName, customerName, employeeID, " +
"employeeName, serviceID, serviceItem,intTimerID " + // entryType, 
"ORDER BY workDate,'{2}'",

                    startTime, stopTime, sortField);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetTimeCardDetail/{startTime}/{stopTime}/{sortField}")]
        public SerializableDataTable GetTimeCardDetail(string startTime, string stopTime, string sortField)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string sql = String.Format(
"SELECT     TOP (10000) workDate, MIN(startTime) AS startTime, " +
"MAX(stopTime) AS stopTime, SUM(timeWorked) AS timeWorked, clientID, " +
"clientName, customerName, companyID, clientCompany, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID, timerID, " +
"COUNT(intTimerID) AS idCount, MAX(tasks) AS tasks, MAX(meetings) AS meetings, " +
"MAX(recordType) AS recordType, MAX(timerAction) AS timerAction, MAX(timerNote) AS timerNote, " +
"MAX(mgrNote) AS mgrNote, editedByName " +
"FROM         dbo.vPortalTimeCard " +
 "WHERE workDate BETWEEN '{0}' AND '{1}' " +
"GROUP BY workDate, companyID, clientCompany, clientID, clientName, customerName, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID, timerID, editedByName " +
"ORDER BY workDate,'{2}',clientName, serviceItem",

                    startTime, stopTime, sortField);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetTimeCardDetail_HHMM/{startTime}/{stopTime}/{sortField}")]
        public SerializableDataTable GetTimeCardDetail_HHMM(string startTime, string stopTime, string sortField)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string sql = String.Format(
"SELECT     TOP (10000) workDate, MIN(startTime) AS startTime, " +
"MAX(stopTime) AS stopTime, " +
"SUM(" +
" CASE EntryType" +
" WHEN 'A'" +
" THEN" +
" (CASE WHEN timeWorkedText NOT LIKE '%:%' THEN 0.0 " +
" ELSE(LEFT(timeWorkedText, CHARINDEX(':', timeWorkedText) - 1)) + '.' " +
" + CAST(REPLACE(timeWorkedText, LEFT(timeWorkedText, CHARINDEX(':', timeWorkedText)), '') as float) / 60.0 " +
" END)" +
" WHEN 'M'" +
" THEN timeWorked" +
" ELSE" +
" 0.0" +
" END)" +
" as timeWorked, " +
"SUM(timeWorked) AS timeWorkedOrig, " +
"clientID, " +
"clientName, customerName, companyID, clientCompany, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID, timerID, " +
"COUNT(intTimerID) AS idCount, MAX(tasks) AS tasks, MAX(meetings) AS meetings, " +
"MAX(recordType) AS recordType, MAX(timerAction) AS timerAction, MAX(timerNote) AS timerNote, " +
"MAX(mgrNote) AS mgrNote, editedByName " +
"FROM         dbo.vPortalTimeCard_2 " +
 "WHERE workDate BETWEEN '{0}' AND '{1}' " +
"GROUP BY workDate, companyID, clientCompany, clientID, clientName, customerName, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID, timerID, editedByName " +
"ORDER BY workDate,'{2}',clientName, serviceItem",

                    startTime, stopTime, sortField);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetMyTimeCardDetail/{startTime}/{stopTime}/{employeeID}")]
        public SerializableDataTable GetMyTimeCardDetail(string startTime, string stopTime, int employeeID)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string sql = String.Format(
"SELECT     TOP (10000) workDate, MIN(startTime) AS startTime, " +
"MAX(stopTime) AS stopTime, SUM(timeWorked) AS timeWorked, clientID, " +
"clientName, customerName, companyID, clientCompany, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID, " +
"COUNT(intTimerID) AS idCount, MAX(tasks) AS tasks, MAX(meetings) AS meetings, " +
"MAX(recordType) AS recordType, MAX(timerAction) AS timerAction, MAX(timerNote) AS timerNote, " +
"MAX(mgrNote) AS mgrNote, editedByName " +
"FROM         dbo.vPortalTimeCard " +
 //"WHERE startTime >= '{0}' AND stopTime <= '{1}' " +
 "WHERE workDate BETWEEN '{0}' AND '{1}' AND employeeID = {2} " +
"GROUP BY workDate, companyID, clientCompany, clientID, clientName, customerName, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID, editedByName " +
"ORDER BY workDate DESC",

                    startTime, stopTime, employeeID);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetTeamTimeCardDetail/{startTime}/{stopTime}/{employeeID}")]
        public SerializableDataTable GetTeamTimeCardDetail(string startTime, string stopTime, int employeeID)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string sql = String.Format(
"SELECT     TOP (10000) workDate, MIN(startTime) AS startTime, " +
"MAX(stopTime) AS stopTime, SUM(timeWorked) AS timeWorked, clientID, " +
"clientName, customerName, companyID, clientCompany, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID, " +
"COUNT(intTimerID) AS idCount, MAX(tasks) AS tasks, MAX(meetings) AS meetings, " +
"MAX(recordType) AS recordType, MAX(timerAction) AS timerAction, MAX(timerNote) AS timerNote, " +
"MAX(mgrNote) AS mgrNote, editedByName, MAX(clientTeamLeaderID) as clientTeamLeaderID " +
"FROM         dbo.vPortalTimeCard_2 " +
 //"WHERE startTime >= '{0}' AND stopTime <= '{1}' " +
 "WHERE workDate BETWEEN '{0}' AND '{1}' AND clientTeamLeaderID = {2} " +
"GROUP BY workDate, companyID, clientCompany, clientID, clientName, customerName, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID, editedByName " +
"ORDER BY workDate DESC",

                    startTime, stopTime, employeeID);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetTeamTimeCardDetail/{startTime}/{stopTime}")]
        public SerializableDataTable GetTeamTimeCardDetail(string startTime, string stopTime)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string sql = String.Format(
"SELECT     TOP (10000) workDate, MIN(startTime) AS startTime, " +
"MAX(stopTime) AS stopTime, SUM(timeWorked) AS timeWorked, clientID, " +
"clientName, customerName, companyID, clientCompany, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID, " +
"COUNT(intTimerID) AS idCount, MAX(tasks) AS tasks, MAX(meetings) AS meetings, " +
"MAX(recordType) AS recordType, MAX(timerAction) AS timerAction, MAX(timerNote) AS timerNote, " +
"MAX(mgrNote) AS mgrNote, editedByName, teamLeaderID " +
"FROM         dbo.vPortalTimeCard " +
 //"WHERE startTime >= '{0}' AND stopTime <= '{1}' " +
 "WHERE workDate BETWEEN '{0}' AND '{1}' " +
"GROUP BY workDate, companyID, clientCompany, clientID, clientName, customerName, employeeID, " +
"employeeName, serviceID, serviceItem, entryType, intTimerID, editedByName, teamLeaderID " +
"ORDER BY workDate DESC",

                    startTime, stopTime);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetTeamTimeCardMeetingsByTeamLeader/{startTime}/{stopTime}")]
        public SerializableDataTable GetTeamTimeCardMeetingsByTeamLeader(string startTime, string stopTime)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string sql = String.Format(
//"SELECT     TOP (100) SUM(meetings) AS meetings,teamLeaderID " +
//"FROM         dbo.vPortalTimeCard " +
// "WHERE workDate BETWEEN '{0}' AND '{1}' AND serviceID = 1001" +
//"GROUP BY teamLeaderID ",

"SELECT     TOP (100000) SUM(meetings) AS meetings,SUM(timeWorked) as timeWorked, clientID, clientTeamLeaderID, serviceID " +
"FROM         dbo.vPortalTimeCard_2 " +
 "WHERE workDate >= '{0}' AND workDate <= '{1}'" +  //OR serviceID = 1011
"GROUP BY clientID, clientTeamLeaderID,serviceID ",

                    startTime, stopTime);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }

        [Route("rest/GetPortalTimerTaskBreakdown/{startTime}/{stopTime}/{companyID}")]
        public SerializableDataTable GetPortalTimerTaskBreakdown(string startTime, string stopTime, int companyID)
        {
            string entityConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["entities"].ConnectionString;
            string connectionString = new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;

            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string sql = String.Format(
"SELECT   * " +
"FROM         dbo.vPortalTimerTaskBreakdown " +
"WHERE workDate BETWEEN '{0}' AND '{1}' AND companyID = {2} " +
"ORDER BY lastName, firstName, serviceItem",

                    startTime, stopTime, companyID);

                    SqlCommand selectCommand = new SqlCommand(sql, connection);

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return new SerializableDataTable(dataTable);
        }
    }
}
