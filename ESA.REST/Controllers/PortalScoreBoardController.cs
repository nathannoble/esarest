﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalScoreBoard>("PortalScoreBoard");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalScoreBoardController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalScoreBoard
        [Queryable]
        public IQueryable<tblPortalScoreBoard> GetPortalScoreBoard()
        {
            try
            {
                return db.tblPortalScoreBoards;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalScoreBoard
        [Queryable]
        //public SingleResult<tblPortalScoreBoard> GetPortalScoreBoard([FromODataUri] int key)
        public SingleResult<tblPortalScoreBoard> GetPortalScoreBoard([FromODataUri] int key)
        {

            string keyString = key.ToString();
        
            int year = Convert.ToInt32(keyString.Substring(0, 4));
            int month = Convert.ToInt32(keyString.Substring(4, keyString.Length -4));

            return SingleResult.Create(db.tblPortalScoreBoards.Where(PortalScoreBoard => PortalScoreBoard.year == year && PortalScoreBoard.month == month));
        }

        // PUT odata/PortalScoreBoard(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, tblPortalScoreBoard rec)
        //public IHttpActionResult Put([FromODataUri] string key,  tblPortalScoreBoard rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key,  tblPortalScoreBoard rec)
        //[FromODataUri] int key2,
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.employeeID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!PortalScoreBoardExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<tblPortalScoreBoard[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalScoreBoard
        //public IHttpActionResult Post(PortalScoreBoard PortalScoreBoard)
        public HttpResponseMessage Post(tblPortalScoreBoard rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalScoreBoards.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalScoreBoard);
            return Request.CreateResponse <tblPortalScoreBoard[]>(HttpStatusCode.Created, new[] { rec });
        }


        // DELETE odata/PortalScoreBoard
        //public async Task<IHttpActionResult> Delete([FromODataUri] int key, [FromODataUri] int key2)
        //{
        //    tblPortalScoreBoard PortalScoreBoard = db.tblPortalScoreBoards.Find(key);
        //    if (PortalScoreBoard == null)
        //    {
        //        return NotFound();
        //    }

        //    foreach (tblPortalScoreBoard rec in db.tblPortalScoreBoards.Where(e => e.year == key && e.month == key2))
        //    {
        //        db.tblPortalScoreBoards.Remove(rec);
        //    }

        //    db.tblPortalScoreBoards.Remove(PortalScoreBoard);

        //    // TODO: Execute this in background and keep the UI live
        //    //db.SaveChanges();
        //    await db.SaveChangesAsync();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalScoreBoardExists(int key)
        {
            string keyString = key.ToString();

            int year = Convert.ToInt32(keyString.Substring(0, 4));
            int month = Convert.ToInt32(keyString.Substring(4, keyString.Length - 4));

            return db.tblPortalScoreBoards.Count(e => e.year == year && e.month == month) > 0;
        }
    }
}
