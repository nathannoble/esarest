﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalUserTable>("PortalUserTable");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalUserTableController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalUserTable
        [Queryable]
        public IQueryable<tblPortalUser> GetPortalUserTable()
        {
            try
            {
                return db.tblPortalUsers;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalUserTable
        [Queryable]
        public SingleResult<tblPortalUser> GetPortalUserTable([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblPortalUsers.Where(PortalUserTable => PortalUserTable.userKey == key));
        }

        // PUT odata/PortalUserTable(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblPortalUser rec)
        //public IHttpActionResult Put([FromODataUri] string key,  PortalUserTable rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, PortalUserTable rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.userKey)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalUserTableExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<PortalUserTable[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalUserTable
        //public IHttpActionResult Post(PortalUserTable PortalUserTable)
        public HttpResponseMessage Post(tblPortalUser rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalUsers.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalUserTable);
            return Request.CreateResponse <tblPortalUser[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/PortalUserTable
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblPortalUser> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblPortalUser rec = db.tblPortalUsers.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalUserTableExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalUserTable
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblPortalUser rec2 = db.tblPortalUsers.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblPortalUser rec in db.tblPortalUsers.Where(e => e.userKey == key))
            {
                db.tblPortalUsers.Remove(rec);
            }

            db.tblPortalUsers.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalUserTable PortalUserTable = db.PortalUserTables.Find(key);
        //    if (PortalUserTable == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalUserTables.Remove(PortalUserTable);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalUserTableExists(int key)
        {
            return db.tblPortalUsers.Count(e => e.userKey == key) > 0;
        }
    }
}
