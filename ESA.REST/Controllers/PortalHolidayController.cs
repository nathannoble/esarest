﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalHoliday>("PortalHoliday");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalHolidayController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalHoliday
        [Queryable]
        public IQueryable<vPortalHoliday> GetPortalHoliday()
        {
            try
            {
                return db.vPortalHolidays;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalHoliday
        [Queryable]
        public SingleResult<vPortalHoliday> GetPortalHoliday([FromODataUri] DateTime key)
        {
            return SingleResult.Create(db.vPortalHolidays.Where(PortalHoliday => PortalHoliday.caldate == key));
        }

        // PUT odata/PortalHoliday(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, vPortalHoliday rec)
        //public IHttpActionResult Put([FromODataUri] string key,  vPortalHoliday rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, vPortalHoliday rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.caldate)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!PortalHolidayExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<vPortalHoliday[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalHoliday
        //public IHttpActionResult Post(PortalHoliday PortalHoliday)
        public HttpResponseMessage Post(vPortalHoliday rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vPortalHolidays.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalHoliday);
            return Request.CreateResponse <vPortalHoliday[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/vPortalHoliday
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] DateTime key, Delta<vPortalHoliday> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vPortalHoliday rec = db.vPortalHolidays.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalHolidayExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalHoliday
        //public async Task<IHttpActionResult> Delete([FromODataUri] DateTime key)
        //{
        //    vPortalHoliday PortalHoliday = db.vPortalHolidays.Find(key);
        //    if (PortalHoliday == null)
        //    {
        //        return NotFound();
        //    }

        //    foreach (vPortalHoliday rec in db.vPortalHolidays.Where(e => e.caldate == key))
        //    {
        //        db.vPortalHolidays.Remove(rec);
        //    }

        //    db.vPortalHolidays.Remove(PortalHoliday);

        //    // TODO: Execute this in background and keep the UI live
        //    //db.SaveChanges();
        //    await db.SaveChangesAsync();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalHolidayExists(DateTime key)
        {
            return db.vPortalHolidays.Count(e => e.caldate == key) > 0;
        }
    }
}
