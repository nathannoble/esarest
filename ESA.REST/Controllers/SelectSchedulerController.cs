﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SelectScheduler>("SelectScheduler");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SelectSchedulerController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/SelectScheduler
        [Queryable]
        public IQueryable<vSelectScheduler> GetSelectScheduler()
        {
            try
            {
                return db.vSelectSchedulers;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/SelectScheduler
        [Queryable]
        public SingleResult<vSelectScheduler> GetSelectScheduler([FromODataUri] int key)
        {
            return SingleResult.Create(db.vSelectSchedulers.Where(SelectScheduler => SelectScheduler.schedulerID == key));
        }

        // PUT odata/SelectScheduler(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, vSelectScheduler rec)
        //public IHttpActionResult Put([FromODataUri] string key,  vSelectScheduler rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, vSelectScheduler rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.schedulerID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!SelectSchedulerExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<vSelectScheduler[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/SelectScheduler
        //public IHttpActionResult Post(SelectScheduler SelectScheduler)
        public HttpResponseMessage Post(vSelectScheduler rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vSelectSchedulers.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(SelectScheduler);
            return Request.CreateResponse <vSelectScheduler[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/vSelectScheduler
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vSelectScheduler> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vSelectScheduler rec = db.vSelectSchedulers.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SelectSchedulerExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/SelectScheduler
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vSelectScheduler SelectScheduler = db.vSelectSchedulers.Find(key);
            if (SelectScheduler == null)
            {
                return NotFound();
            }

            foreach (vSelectScheduler rec in db.vSelectSchedulers.Where(e => e.schedulerID == key))
            {
                db.vSelectSchedulers.Remove(rec);
            }

            db.vSelectSchedulers.Remove(SelectScheduler);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    SelectScheduler SelectScheduler = db.SelectSchedulers.Find(key);
        //    if (SelectScheduler == null)
        //    {
        //        return NotFound();
        //    }

        //    db.SelectSchedulers.Remove(SelectScheduler);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SelectSchedulerExists(int key)
        {
            return db.vSelectSchedulers.Count(e => e.schedulerID == key) > 0;
        }
    }
}
