﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressCompaniesNote>("ProgressCompaniesNote");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressCompaniesNoteController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressCompaniesNote
        [Queryable]
        public IQueryable<tblProgressCompaniesNote> GetProgressCompaniesNote()
        {
            try
            {
                return db.tblProgressCompaniesNotes;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressCompaniesNote
        [Queryable]
        public SingleResult<tblProgressCompaniesNote> GetProgressCompaniesNote([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblProgressCompaniesNotes.Where(ProgressCompaniesNote => ProgressCompaniesNote.companyNoteID == key));
        }

        // PUT odata/ProgressCompaniesNote(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblProgressCompaniesNote rec)
        //public IHttpActionResult Put([FromODataUri] string key,  ProgressCompaniesNote rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, ProgressCompaniesNote rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.companyNoteID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressCompaniesNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<ProgressCompaniesNote[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressCompaniesNote
        //public IHttpActionResult Post(ProgressCompaniesNote ProgressCompaniesNote)
        public HttpResponseMessage Post(tblProgressCompaniesNote rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblProgressCompaniesNotes.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressCompaniesNote);
            return Request.CreateResponse <tblProgressCompaniesNote[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/ProgressCompaniesNote
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblProgressCompaniesNote> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblProgressCompaniesNote rec = db.tblProgressCompaniesNotes.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressCompaniesNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressCompaniesNote
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblProgressCompaniesNote rec2 = db.tblProgressCompaniesNotes.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblProgressCompaniesNote rec in db.tblProgressCompaniesNotes.Where(e => e.companyNoteID == key))
            {
                db.tblProgressCompaniesNotes.Remove(rec);
            }

            db.tblProgressCompaniesNotes.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressCompaniesNote ProgressCompaniesNote = db.ProgressCompaniesNotes.Find(key);
        //    if (ProgressCompaniesNote == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressCompaniesNotes.Remove(ProgressCompaniesNote);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressCompaniesNoteExists(int key)
        {
            return db.tblProgressCompaniesNotes.Count(e => e.companyNoteID == key) > 0;
        }
    }
}
