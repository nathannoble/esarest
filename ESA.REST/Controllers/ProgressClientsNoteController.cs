﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressClientsNote>("ProgressClientsNote");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressClientsNoteController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressClientsNote
        [Queryable]
        public IQueryable<tblProgressClientsNote> GetProgressClientsNote()
        {
            try
            {
                return db.tblProgressClientsNotes;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressClientsNote
        [Queryable]
        public SingleResult<tblProgressClientsNote> GetProgressClientsNote([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblProgressClientsNotes.Where(ProgressClientsNote => ProgressClientsNote.clientNoteID == key));
        }

        // PUT odata/ProgressClientsNote(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblProgressClientsNote rec)
        //public IHttpActionResult Put([FromODataUri] string key,  ProgressClientsNote rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, ProgressClientsNote rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.clientNoteID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressClientsNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<ProgressClientsNote[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressClientsNote
        //public IHttpActionResult Post(ProgressClientsNote ProgressClientsNote)
        public HttpResponseMessage Post(tblProgressClientsNote rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblProgressClientsNotes.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressClientsNote);
            return Request.CreateResponse <tblProgressClientsNote[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/ProgressClientsNote
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblProgressClientsNote> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblProgressClientsNote rec = db.tblProgressClientsNotes.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressClientsNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressClientsNote
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblProgressClientsNote rec2 = db.tblProgressClientsNotes.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblProgressClientsNote rec in db.tblProgressClientsNotes.Where(e => e.clientNoteID == key))
            {
                db.tblProgressClientsNotes.Remove(rec);
            }

            db.tblProgressClientsNotes.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressClientsNote ProgressClientsNote = db.ProgressClientsNotes.Find(key);
        //    if (ProgressClientsNote == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressClientsNotes.Remove(ProgressClientsNote);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressClientsNoteExists(int key)
        {
            return db.tblProgressClientsNotes.Count(e => e.clientNoteID == key) > 0;
        }
    }
}
