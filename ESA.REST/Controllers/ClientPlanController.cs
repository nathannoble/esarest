﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ClientPlan>("ClientPlan");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ClientPlanController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ClientPlan
        [Queryable]
        public IQueryable<tblClientPlan> GetClientPlan()
        {
            try
            {
                return db.tblClientPlans;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ClientPlan
        [Queryable]
        public SingleResult<tblClientPlan> GetClientPlan([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblClientPlans.Where(ClientPlan => ClientPlan.clientPlanID == key));
        }

        // PUT odata/ClientPlan(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblClientPlan rec)
        //public IHttpActionResult Put([FromODataUri] string key,  ClientPlan rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, ClientPlan rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.clientPlanID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientPlanExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<ClientPlan[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ClientPlan
        //public IHttpActionResult Post(ClientPlan ClientPlan)
        public HttpResponseMessage Post(tblClientPlan rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblClientPlans.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ClientPlan);
            return Request.CreateResponse <tblClientPlan[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/ClientPlan
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblClientPlan> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblClientPlan rec = db.tblClientPlans.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientPlanExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ClientPlan
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblClientPlan rec2 = db.tblClientPlans.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblClientPlan rec in db.tblClientPlans.Where(e => e.clientPlanID == key))
            {
                db.tblClientPlans.Remove(rec);
            }

            db.tblClientPlans.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ClientPlan ClientPlan = db.ClientPlans.Find(key);
        //    if (ClientPlan == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ClientPlans.Remove(ClientPlan);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClientPlanExists(int key)
        {
            return db.tblClientPlans.Count(e => e.clientPlanID == key) > 0;
        }
    }
}
