﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalTimer>("PortalTimer");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalTimerController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalTimer
        [Queryable]
        public IQueryable<tblPortalTimer> GetPortalTimer()
        {
            try
            {
                return db.tblPortalTimers;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalTimer
        [Queryable]
        public SingleResult<tblPortalTimer> GetPortalTimer([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblPortalTimers.Where(PortalTimer => PortalTimer.timerID == key));
        }

        // PUT odata/PortalTimer(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, tblPortalTimer rec)
        //public IHttpActionResult Put([FromODataUri] string key,  tblPortalTimer rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, tblPortalTimer rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }


            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException e)
            {
                string m = e.Message;
                throw;
            }
            catch (Exception e)
            {
                string m = e.Message;
                throw;
            }
           
            //return Updated(rec);
            return Request.CreateResponse<tblPortalTimer[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalTimer
        //public IHttpActionResult Post(PortalTimer PortalTimer)
        public HttpResponseMessage Post(tblPortalTimer rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalTimers.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalTimer);
            return Request.CreateResponse <tblPortalTimer[]>(HttpStatusCode.Created, new[] { rec });
        }

     

        // DELETE odata/PortalTimer
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblPortalTimer PortalTimer = db.tblPortalTimers.Find(key);
            if (PortalTimer == null)
            {
                return NotFound();
            }

            foreach (tblPortalTimer rec in db.tblPortalTimers.Where(e => e.intTimerID == key))
            {
                db.tblPortalTimers.Remove(rec);
            }

            db.tblPortalTimers.Remove(PortalTimer);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalTimer PortalTimer = db.PortalTimers.Find(key);
        //    if (PortalTimer == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalTimers.Remove(PortalTimer);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalTimerExists(int key)
        {
            return db.tblPortalTimers.Count(e => e.timerID == key) > 0;
        }
    }
}
