﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalTip>("PortalTip");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalTipController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalTip
        [Queryable]
        public IQueryable<tblPortalTip> GetPortalTip()
        {
            try
            {
                return db.tblPortalTips;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalTip
        [Queryable]
        public SingleResult<tblPortalTip> GetPortalTip([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblPortalTips.Where(PortalTip => PortalTip.tipID == key));
        }

        // PUT odata/PortalTip(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblPortalTip rec)
        //public IHttpActionResult Put([FromODataUri] string key,  PortalTip rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, PortalTip rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.tipID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalTipExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<PortalTip[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalTip
        //public IHttpActionResult Post(PortalTip PortalTip)
        public HttpResponseMessage Post(tblPortalTip rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalTips.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalTip);
            return Request.CreateResponse <tblPortalTip[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/PortalTip
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblPortalTip> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblPortalTip rec = db.tblPortalTips.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalTipExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalTip
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblPortalTip rec2 = db.tblPortalTips.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblPortalTip rec in db.tblPortalTips.Where(e => e.tipID == key))
            {
                db.tblPortalTips.Remove(rec);
            }

            db.tblPortalTips.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalTip PortalTip = db.PortalTips.Find(key);
        //    if (PortalTip == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalTips.Remove(PortalTip);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalTipExists(int key)
        {
            return db.tblPortalTips.Count(e => e.tipID == key) > 0;
        }
    }
}
