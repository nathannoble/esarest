﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalClientsNote>("PortalClientsNote");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalClientsNoteController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalClientsNote
        [Queryable]
        public IQueryable<vPortalClientsNote> GetPortalClientsNote()
        {
            try
            {
                return db.vPortalClientsNotes;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalClientsNote
        [Queryable]
        public SingleResult<vPortalClientsNote> GetPortalClientsNote([FromODataUri] int key)
        {
            return SingleResult.Create(db.vPortalClientsNotes.Where(PortalClientsNote => PortalClientsNote.clientNoteID == key));
        }

        // PUT odata/PortalClientsNote(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, vPortalClientsNote rec)
        //public IHttpActionResult Put([FromODataUri] string key,  PortalClientsNote rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, PortalClientsNote rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.clientNoteID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalClientsNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<PortalClientsNote[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalClientsNote
        //public IHttpActionResult Post(PortalClientsNote PortalClientsNote)
        public HttpResponseMessage Post(vPortalClientsNote rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vPortalClientsNotes.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalClientsNote);
            return Request.CreateResponse <vPortalClientsNote[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/PortalClientsNote
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vPortalClientsNote> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vPortalClientsNote rec = db.vPortalClientsNotes.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalClientsNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalClientsNote
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vPortalClientsNote rec2 = db.vPortalClientsNotes.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (vPortalClientsNote rec in db.vPortalClientsNotes.Where(e => e.clientNoteID == key))
            {
                db.vPortalClientsNotes.Remove(rec);
            }

            db.vPortalClientsNotes.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalClientsNote PortalClientsNote = db.PortalClientsNotes.Find(key);
        //    if (PortalClientsNote == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalClientsNotes.Remove(PortalClientsNote);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalClientsNoteExists(int key)
        {
            return db.vPortalClientsNotes.Count(e => e.clientNoteID == key) > 0;
        }
    }
}
