﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalTimerLog>("PortalTimerLog");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalTimerLogController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalTimerLog
        [Queryable]
        public IQueryable<tblPortalTimerLog> GetPortalTimerLog()
        {
            try
            {
                return db.tblPortalTimerLogs;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalTimerLog
        [Queryable]
        public SingleResult<tblPortalTimerLog> GetPortalTimerLog([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblPortalTimerLogs.Where(PortalTimerLog => PortalTimerLog.logID == key));
        }

        // PUT odata/PortalTimerLog(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblPortalTimerLog rec)
        //public IHttpActionResult Put([FromODataUri] string key,  PortalTimerLog rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, PortalTimerLog rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.logID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalTimerLogExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<PortalTimerLog[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalTimerLog
        //public IHttpActionResult Post(PortalTimerLog PortalTimerLog)
        public HttpResponseMessage Post(tblPortalTimerLog rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalTimerLogs.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalTimerLog);
            return Request.CreateResponse <tblPortalTimerLog[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/PortalTimerLog
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblPortalTimerLog> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblPortalTimerLog rec = db.tblPortalTimerLogs.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalTimerLogExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalTimerLog
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblPortalTimerLog rec2 = db.tblPortalTimerLogs.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblPortalTimerLog rec in db.tblPortalTimerLogs.Where(e => e.logID == key))
            {
                db.tblPortalTimerLogs.Remove(rec);
            }

            db.tblPortalTimerLogs.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalTimerLog PortalTimerLog = db.PortalTimerLogs.Find(key);
        //    if (PortalTimerLog == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalTimerLogs.Remove(PortalTimerLog);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalTimerLogExists(int key)
        {
            return db.tblPortalTimerLogs.Count(e => e.logID == key) > 0;
        }
    }
}
