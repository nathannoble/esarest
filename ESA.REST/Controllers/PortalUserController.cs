﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalUser>("PortalUser");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalUserController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalUser
        [Queryable]
        public IQueryable<vPortalUser> GetPortalUser()
        {
            try
            {
                return db.vPortalUsers;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalUser
        [Queryable]
        public SingleResult<vPortalUser> GetPortalUser([FromODataUri] int key)
        {
            return SingleResult.Create(db.vPortalUsers.Where(PortalUser => PortalUser.userKey == key));
        }

        // PUT odata/PortalUser(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, vPortalUser rec)
        //public IHttpActionResult Put([FromODataUri] string key,  PortalUser rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, PortalUser rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.userKey)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalUserExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<PortalUser[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalUser
        //public IHttpActionResult Post(PortalUser PortalUser)
        public HttpResponseMessage Post(vPortalUser rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vPortalUsers.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalUser);
            return Request.CreateResponse <vPortalUser[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/PortalUser
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vPortalUser> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vPortalUser rec = db.vPortalUsers.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalUserExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalUser
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vPortalUser rec2 = db.vPortalUsers.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (vPortalUser rec in db.vPortalUsers.Where(e => e.userKey == key))
            {
                db.vPortalUsers.Remove(rec);
            }

            db.vPortalUsers.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalUser PortalUser = db.PortalUsers.Find(key);
        //    if (PortalUser == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalUsers.Remove(PortalUser);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalUserExists(int key)
        {
            return db.vPortalUsers.Count(e => e.userKey == key) > 0;
        }
    }
}
