﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalCompaniesNote>("PortalCompaniesNote");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalCompaniesNoteController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalCompaniesNote
        [Queryable]
        public IQueryable<vPortalCompaniesNote> GetPortalCompaniesNote()
        {
            try
            {
                return db.vPortalCompaniesNotes;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalCompaniesNote
        [Queryable]
        public SingleResult<vPortalCompaniesNote> GetPortalCompaniesNote([FromODataUri] int key)
        {
            return SingleResult.Create(db.vPortalCompaniesNotes.Where(PortalCompaniesNote => PortalCompaniesNote.companyNoteID == key));
        }

        // PUT odata/PortalCompaniesNote(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, vPortalCompaniesNote rec)
        //public IHttpActionResult Put([FromODataUri] string key,  PortalCompaniesNote rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, PortalCompaniesNote rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.companyNoteID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalCompaniesNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<PortalCompaniesNote[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalCompaniesNote
        //public IHttpActionResult Post(PortalCompaniesNote PortalCompaniesNote)
        public HttpResponseMessage Post(vPortalCompaniesNote rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vPortalCompaniesNotes.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalCompaniesNote);
            return Request.CreateResponse <vPortalCompaniesNote[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/PortalCompaniesNote
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vPortalCompaniesNote> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vPortalCompaniesNote rec = db.vPortalCompaniesNotes.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalCompaniesNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalCompaniesNote
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vPortalCompaniesNote rec2 = db.vPortalCompaniesNotes.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (vPortalCompaniesNote rec in db.vPortalCompaniesNotes.Where(e => e.companyNoteID == key))
            {
                db.vPortalCompaniesNotes.Remove(rec);
            }

            db.vPortalCompaniesNotes.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalCompaniesNote PortalCompaniesNote = db.PortalCompaniesNotes.Find(key);
        //    if (PortalCompaniesNote == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalCompaniesNotes.Remove(PortalCompaniesNote);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalCompaniesNoteExists(int key)
        {
            return db.vPortalCompaniesNotes.Count(e => e.companyNoteID == key) > 0;
        }
    }
}
