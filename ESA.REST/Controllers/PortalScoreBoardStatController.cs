﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalScoreBoardStat>("PortalScoreBoardStat");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalScoreBoardStatController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalScoreBoardStat
        [Queryable]
        public IQueryable<tblPortalScoreBoardStat> GetPortalScoreBoardStat()
        {
            try
            {
                return db.tblPortalScoreBoardStats;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalScoreBoardStat
        [Queryable]
        public SingleResult<tblPortalScoreBoardStat> GetPortalScoreBoardStat([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblPortalScoreBoardStats.Where(PortalScoreBoardStat => PortalScoreBoardStat.year == key));
        }

        // PUT odata/PortalScoreBoardStat(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, tblPortalScoreBoardStat rec)
        //public IHttpActionResult Put([FromODataUri] string key,  tblPortalScoreBoardStat rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, tblPortalScoreBoardStat rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.employeeID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!PortalScoreBoardStatExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<tblPortalScoreBoardStat[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalScoreBoardStat
        //public IHttpActionResult Post(PortalScoreBoardStat PortalScoreBoardStat)
        public HttpResponseMessage Post(tblPortalScoreBoardStat rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalScoreBoardStats.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalScoreBoardStat);
            return Request.CreateResponse <tblPortalScoreBoardStat[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/tblPortalScoreBoardStat
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblPortalScoreBoardStat> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblPortalScoreBoardStat rec = db.tblPortalScoreBoardStats.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalScoreBoardStatExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalScoreBoardStat
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblPortalScoreBoardStat PortalScoreBoardStat = db.tblPortalScoreBoardStats.Find(key);
            if (PortalScoreBoardStat == null)
            {
                return NotFound();
            }

            foreach (tblPortalScoreBoardStat rec in db.tblPortalScoreBoardStats.Where(e => e.year == key))
            {
                db.tblPortalScoreBoardStats.Remove(rec);
            }

            db.tblPortalScoreBoardStats.Remove(PortalScoreBoardStat);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalScoreBoardStat PortalScoreBoardStat = db.PortalScoreBoardStats.Find(key);
        //    if (PortalScoreBoardStat == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalScoreBoardStats.Remove(PortalScoreBoardStat);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalScoreBoardStatExists(int key)
        {
            return db.tblPortalScoreBoardStats.Count(e => e.year == key) > 0;
        }
    }
}
