﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

using System.Data.Entity.Core.EntityClient;
using System.Web.Http.Cors;
using ESA.REST.Utilities;
using System.Data.SqlClient;
using System.Web;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace ESA.REST.Controllers
{
    public class FileController : ApiController
    {

        /// <summary>
        /// Post files to be uploaded to the server; IE9 uses iFrame, all others use FormData
        /// </summary>
        /// <returns></returns>
        [Route("rest/files/Upload")]
        public HttpResponseMessage Post()
        {
            HttpResponseMessage result = null;
            bool ie9 = false;
            //var httpRequest = HttpContext.Current.Request;

            HttpContext postedContext = HttpContext.Current;

            try
            {
                // Check if files are available
                if (postedContext.Request.Files.Count > 0)
                {
                    var files = new List<string>();

                    // Interate the files and save on the server
                    for (int i = 0; i < postedContext.Request.Files.Count; i++)
                    {
                        HttpPostedFile file = postedContext.Request.Files[i];
                        string type =postedContext.Request.Form["type"];
                        var id = Convert.ToInt32(postedContext.Request.Form["id"]);

                        // Parse out the file and IE9Flag
                        //var postedFile = httpRequest.Files[file];
                        var ie9Flag = Convert.ToInt32(postedContext.Request.Form["IE9Flag"]);

                        if (ie9Flag == 0)
                        {
                            // IE9 case
                            ie9 = true;
                        }

                        // Limit the types of files that can be uploaded
                        //string fileType = Path.GetExtension(file.FileName);
                        string fileName = file.FileName;
                        fileName = Regex.Replace(fileName, "[^a-zA-Z0-9_.]+", "_", RegexOptions.Compiled);
                        //fileName = fileName.Replace(".", "_");
                        // This is the name of the file that the app is expecting
                        //string targetFile = System.Configuration.ConfigurationManager.AppSettings["FileName"];
                        string targetPath = System.Configuration.ConfigurationManager.AppSettings["ESAFilesFolder"];
                        string folder = "";

                        if (type == "Client")
                        {
                            folder = "filesClient\\";
                        }
                        if (type == "Company")
                        {
                            folder = "filesCompany\\";
                        }
                        if (type == "Employee")
                        {
                            folder = "filesEmployee\\";
                        }
                        if (type == "Payroll")
                        {
                            folder = "filesPayroll\\";
                        }
                        if (type == "Reports")
                        {
                            folder = "filesReports\\";
                        }
                        if (type == "Vendor")
                        {
                            folder = "filesVendor\\";
                        }

                        if (targetPath.Substring(targetPath.Length - 2) != "\\") targetPath += "\\";

                        // NOTE: Not supported in the cloud as there could be multiple instances...
                        //string saveFileAs = HttpContext.Current.Server.MapPath("~//uploadcontent//" + targetFile);
                        string saveFileAs = targetPath + folder + id + "_" + fileName;
                        renameFileIfExists(saveFileAs);

                        Debug.WriteLine("Uploading");
                        byte[] binaryWriteArray = new
                        byte[file.InputStream.Length];
                        file.InputStream.Read(binaryWriteArray, 0, (int)file.InputStream.Length);

                        // Create the filestream that we will be writing
                        FileStream objfilestream = new FileStream(saveFileAs, FileMode.Create, FileAccess.ReadWrite);
                        objfilestream.Write(binaryWriteArray, 0,
                        binaryWriteArray.Length);
                        objfilestream.Close();

                        //add the name of the file (without path) to the list of files that were added
                        files.Add(Path.GetFileName(file.FileName));
                    }

                    // Return result
                    if (ie9)
                    {
                        result = Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        result = Request.CreateResponse(HttpStatusCode.OK, files);
                    }
                }
                else
                {
                    // return BadRequest (no file(s) available)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return result;
        }

        /// <summary>
        /// Rename an existing file (if it exists) with the current timestamp
        /// </summary>
        /// <param name="FileNameWithPath">Filename with full path (e.g., C:\temp\myfile.txt)</param>
        private void renameFileIfExists(string FileNameWithPath)
        {
            Debug.WriteLine("checking for: " + FileNameWithPath);
            try
            {
                if (System.IO.File.Exists(FileNameWithPath))
                {
                    // Rename the exist file using the current datetime, e.g., FileName_20150717_103428_bak.csv
                    DateTime thedate = DateTime.Now;
                    System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.InvariantCulture;

                    string dateString = DateTime.Now.ToString("yyyyMMdd_HHmmss", ci);
                    string path = Path.GetDirectoryName(FileNameWithPath) + @"\";
                    string filenameNoExt = Path.GetFileNameWithoutExtension(FileNameWithPath);
                    string ext = Path.GetExtension(FileNameWithPath);

                    string newFileNameWithPath = String.Format("{0}{1}_{2}_bak{3}", path, filenameNoExt, dateString, ext);
                    Debug.WriteLine("renaming existing file as: " + newFileNameWithPath);
                    System.IO.File.Move(FileNameWithPath, newFileNameWithPath);
                }

            }
            catch (Exception ex)
            {
                string t = ex.Message;
                Debug.WriteLine(t);
            }
        }


        [Route("rest/files/GetFileInfo/{type}/{id}")]
        public List<ESAFileInfo> GetFileInfo(string type, string id)
        {
            Debug.WriteLine(String.Format("{0}:  REST  GetFileInfo - started", DateTime.Now.ToLongTimeString()));

            List<ESAFileInfo> fileList = new List<ESAFileInfo>();

            try
            {
                string targetPath = System.Configuration.ConfigurationManager.AppSettings["ESAFilesFolder"];
                if (targetPath.Substring(targetPath.Length - 2) != "\\") targetPath += "\\";

                string path = targetPath + "files" + type;

                string[] files = Directory.GetFiles(path, id +"_*");
                Console.WriteLine("The number of files starting with" + id + "_" + " is {0}.", files.Length);
                
                foreach (string file in files)
                {
                    Console.WriteLine(file);
                    ESAFileInfo fileInfo = new ESAFileInfo();
                    System.IO.FileInfo fi = new System.IO.FileInfo(file);
                    fileInfo.FileName = fi.Name;
                    fileInfo.FilePath = fi.DirectoryName;
                    fileInfo.FileNameWithPath = fi.FullName;
                    fileInfo.FileDate = fi.CreationTime.ToShortDateString();
                    fileInfo.FileSize = fi.Length;
                    fileList.Add(fileInfo);
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("{0}:  REST  !!GeFileInfo - finished with errors!! - {1}", DateTime.Now.ToLongTimeString(), ex.Message));
                //fileInfo.errorMessage = ex.Message;
                return fileList;
            }

            Debug.WriteLine(String.Format("{0}:  REST  GetFileInfo - finished", DateTime.Now.ToLongTimeString()));
            return fileList;
        }


        [Route("rest/files/GetDeleteFile/{type}/{fileName}")]
        public bool GetDeleteFile(string type, string fileName)
        {
            Debug.WriteLine(String.Format("{0}:  REST DeleteFile - started", DateTime.Now.ToLongTimeString()));

            try
            {
                string targetPath = System.Configuration.ConfigurationManager.AppSettings["ESAFilesFolder"];
                if (targetPath.Substring(targetPath.Length - 2) != "\\") targetPath += "\\";

                string fileNameWithPath = targetPath + "files" + type + "\\" + fileName;
                if (System.IO.File.Exists(fileNameWithPath))
                {
                    File.Delete(fileNameWithPath);
                }
                else
                {
                    Debug.WriteLine(String.Format("{0}:  REST  !!DeleteFiles -file not found!! - {1}", DateTime.Now.ToLongTimeString(),fileNameWithPath + " not found."));
                    //fileInfo.errorMessage = ex.Message;
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("{0}:  REST  !!DeleteFiles - finished with errors!! - {1}", DateTime.Now.ToLongTimeString(), ex.Message));
                //fileInfo.errorMessage = ex.Message;
                return false;
            }

            Debug.WriteLine(String.Format("{0}:  REST  DeleteFiles - finished", DateTime.Now.ToLongTimeString()));
            return true;
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
