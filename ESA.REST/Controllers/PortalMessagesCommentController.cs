﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalMessagesComment>("PortalMessagesComment");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalMessagesCommentController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalMessagesComment
        [Queryable]
        public IQueryable<tblPortalMessagesComment> GetPortalMessagesComment()
        {
            try
            {
                return db.tblPortalMessagesComments;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalMessagesComment
        [Queryable]
        public SingleResult<tblPortalMessagesComment> GetPortalMessagesComment([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblPortalMessagesComments.Where(PortalMessagesComment => PortalMessagesComment.commentID == key));
        }

        // PUT odata/PortalMessagesComment(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, tblPortalMessagesComment rec)
        //public IHttpActionResult Put([FromODataUri] string key,  tblPortalMessagesComment rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, tblPortalMessagesComment rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.commentID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!PortalMessagesCommentExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<tblPortalMessagesComment[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalMessagesComment
        //public IHttpActionResult Post(PortalMessagesComment PortalMessagesComment)
        public HttpResponseMessage Post(tblPortalMessagesComment rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalMessagesComments.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalMessagesComment);
            return Request.CreateResponse <tblPortalMessagesComment[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/tblPortalMessagesComment
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblPortalMessagesComment> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblPortalMessagesComment rec = db.tblPortalMessagesComments.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalMessagesCommentExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalMessagesComment
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblPortalMessagesComment PortalMessagesComment = db.tblPortalMessagesComments.Find(key);
            if (PortalMessagesComment == null)
            {
                return NotFound();
            }

            foreach (tblPortalMessagesComment rec in db.tblPortalMessagesComments.Where(e => e.commentID == key))
            {
                db.tblPortalMessagesComments.Remove(rec);
            }

            db.tblPortalMessagesComments.Remove(PortalMessagesComment);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalMessagesComment PortalMessagesComment = db.PortalMessagesComments.Find(key);
        //    if (PortalMessagesComment == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalMessagesComments.Remove(PortalMessagesComment);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalMessagesCommentExists(int key)
        {
            return db.tblPortalMessagesComments.Count(e => e.commentID == key) > 0;
        }
    }
}
