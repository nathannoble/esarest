﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressEmployee>("ProgressEmployee");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressEmployeeController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressEmployee
        [Queryable]
        public IQueryable<tblProgressEmployee> GetProgressEmployee()
        {
            try
            {
                return db.tblProgressEmployees;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressEmployee
        [Queryable]
        public SingleResult<tblProgressEmployee> GetProgressEmployee([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblProgressEmployees.Where(ProgressEmployee => ProgressEmployee.employeeID == key));
        }

        // PUT odata/ProgressEmployee(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblProgressEmployee rec)
        //public IHttpActionResult Put([FromODataUri] string key,  ProgressEmployee rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, ProgressEmployee rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.employeeID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressEmployeeExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<ProgressEmployee[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressEmployee
        //public IHttpActionResult Post(ProgressEmployee ProgressEmployee)
        public HttpResponseMessage Post(tblProgressEmployee rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblProgressEmployees.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressEmployee);
            return Request.CreateResponse <tblProgressEmployee[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/ProgressEmployee
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblProgressEmployee> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblProgressEmployee rec = db.tblProgressEmployees.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressEmployeeExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressEmployee
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblProgressEmployee rec2 = db.tblProgressEmployees.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblProgressEmployee rec in db.tblProgressEmployees.Where(e => e.employeeID == key))
            {
                db.tblProgressEmployees.Remove(rec);
            }

            db.tblProgressEmployees.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressEmployee ProgressEmployee = db.ProgressEmployees.Find(key);
        //    if (ProgressEmployee == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressEmployees.Remove(ProgressEmployee);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressEmployeeExists(int key)
        {
            return db.tblProgressEmployees.Count(e => e.employeeID == key) > 0;
        }
    }
}
