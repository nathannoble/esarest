﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressEmployeesNote>("ProgressEmployeesNote");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressEmployeesNoteController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressEmployeesNote
        [Queryable]
        public IQueryable<tblProgressEmployeesNote> GetProgressEmployeesNote()
        {
            try
            {
                return db.tblProgressEmployeesNotes;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressEmployeesNote
        [Queryable]
        public SingleResult<tblProgressEmployeesNote> GetProgressEmployeesNote([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblProgressEmployeesNotes.Where(ProgressEmployeesNote => ProgressEmployeesNote.employeeNoteID == key));
        }

        // PUT odata/ProgressEmployeesNote(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblProgressEmployeesNote rec)
        //public IHttpActionResult Put([FromODataUri] string key,  ProgressEmployeesNote rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, ProgressEmployeesNote rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.employeeNoteID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressEmployeesNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<ProgressEmployeesNote[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressEmployeesNote
        //public IHttpActionResult Post(ProgressEmployeesNote ProgressEmployeesNote)
        public HttpResponseMessage Post(tblProgressEmployeesNote rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblProgressEmployeesNotes.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressEmployeesNote);
            return Request.CreateResponse <tblProgressEmployeesNote[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/ProgressEmployeesNote
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblProgressEmployeesNote> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblProgressEmployeesNote rec = db.tblProgressEmployeesNotes.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressEmployeesNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressEmployeesNote
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblProgressEmployeesNote rec2 = db.tblProgressEmployeesNotes.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblProgressEmployeesNote rec in db.tblProgressEmployeesNotes.Where(e => e.employeeNoteID == key))
            {
                db.tblProgressEmployeesNotes.Remove(rec);
            }

            db.tblProgressEmployeesNotes.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressEmployeesNote ProgressEmployeesNote = db.ProgressEmployeesNotes.Find(key);
        //    if (ProgressEmployeesNote == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressEmployeesNotes.Remove(ProgressEmployeesNote);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressEmployeesNoteExists(int key)
        {
            return db.tblProgressEmployeesNotes.Count(e => e.employeeNoteID == key) > 0;
        }
    }
}
