﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalTimerTotalTime>("PortalTimerTotalTime");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalTimerTotalTimeController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalTimerTotalTime
        [Queryable]
        public IQueryable<vPortalTimerTotalTime> GetPortalTimerTotalTime()
        {
            try
            {
                return db.vPortalTimerTotalTimes;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalTimerTotalTime
        [Queryable]
        public SingleResult<vPortalTimerTotalTime> GetPortalTimerTotalTime([FromODataUri] int key)
        {
            return SingleResult.Create(db.vPortalTimerTotalTimes.Where(PortalTimerTotalTime => PortalTimerTotalTime.rowID == key));
        }

        // PUT odata/PortalTimerTotalTime(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, vPortalTimerTotalTime rec)
        //public IHttpActionResult Put([FromODataUri] string key,  vPortalTimerTotalTime rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, vPortalTimerTotalTime rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.rowID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!PortalTimerTotalTimeExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<vPortalTimerTotalTime[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalTimerTotalTime
        //public IHttpActionResult Post(PortalTimerTotalTime PortalTimerTotalTime)
        public HttpResponseMessage Post(vPortalTimerTotalTime rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vPortalTimerTotalTimes.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalTimerTotalTime);
            return Request.CreateResponse <vPortalTimerTotalTime[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/vPortalTimerTotalTime
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vPortalTimerTotalTime> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vPortalTimerTotalTime rec = db.vPortalTimerTotalTimes.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalTimerTotalTimeExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalTimerTotalTime
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vPortalTimerTotalTime PortalTimerTotalTime = db.vPortalTimerTotalTimes.Find(key);
            if (PortalTimerTotalTime == null)
            {
                return NotFound();
            }

            foreach (vPortalTimerTotalTime rec in db.vPortalTimerTotalTimes.Where(e => e.rowID == key))
            {
                db.vPortalTimerTotalTimes.Remove(rec);
            }

            db.vPortalTimerTotalTimes.Remove(PortalTimerTotalTime);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalTimerTotalTime PortalTimerTotalTime = db.PortalTimerTotalTimes.Find(key);
        //    if (PortalTimerTotalTime == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalTimerTotalTimes.Remove(PortalTimerTotalTime);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalTimerTotalTimeExists(int key)
        {
            return db.vPortalTimerTotalTimes.Count(e => e.rowID == key) > 0;
        }
    }
}
