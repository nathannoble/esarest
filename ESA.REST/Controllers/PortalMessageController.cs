﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalMessage>("PortalMessage");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalMessageController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalMessage
        [Queryable]
        public IQueryable<tblPortalMessage> GetPortalMessage()
        {
            try
            {
                return db.tblPortalMessages;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalMessage
        [Queryable]
        public SingleResult<tblPortalMessage> GetPortalMessage([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblPortalMessages.Where(PortalMessage => PortalMessage.messageID == key));
        }

        // PUT odata/PortalMessage(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblPortalMessage rec)
        //public IHttpActionResult Put([FromODataUri] string key,  PortalMessage rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, PortalMessage rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.messageID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalMessageExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<PortalMessage[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalMessage
        //public IHttpActionResult Post(PortalMessage PortalMessage)
        public HttpResponseMessage Post(tblPortalMessage rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalMessages.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalMessage);
            return Request.CreateResponse <tblPortalMessage[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/PortalMessage
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblPortalMessage> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblPortalMessage rec = db.tblPortalMessages.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalMessageExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalMessage
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblPortalMessage rec2 = db.tblPortalMessages.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblPortalMessage rec in db.tblPortalMessages.Where(e => e.messageID == key))
            {
                db.tblPortalMessages.Remove(rec);
            }

            db.tblPortalMessages.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalMessage PortalMessage = db.PortalMessages.Find(key);
        //    if (PortalMessage == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalMessages.Remove(PortalMessage);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalMessageExists(int key)
        {
            return db.tblPortalMessages.Count(e => e.messageID == key) > 0;
        }
    }
}
