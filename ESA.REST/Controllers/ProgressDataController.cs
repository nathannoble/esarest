﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressData>("ProgressData");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressDataController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressData
        [Queryable]
        public IQueryable<tblProgressData> GetProgressData()
        {
            try
            {
                return db.tblProgressDatas;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressData
        [Queryable]
        public SingleResult<tblProgressData> GetProgressData([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblProgressDatas.Where(ProgressData => ProgressData.entryID == key));
        }

        // PUT odata/ProgressData(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblProgressData rec)
        //public IHttpActionResult Put([FromODataUri] string key,  ProgressData rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, ProgressData rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.entryID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressDataExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<ProgressData[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressData
        //public IHttpActionResult Post(ProgressData ProgressData)
        public HttpResponseMessage Post(tblProgressData rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblProgressDatas.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressData);
            return Request.CreateResponse <tblProgressData[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/ProgressData
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblProgressData> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblProgressData rec = db.tblProgressDatas.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressDataExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressData
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblProgressData rec2 = db.tblProgressDatas.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblProgressData rec in db.tblProgressDatas.Where(e => e.entryID == key))
            {
                db.tblProgressDatas.Remove(rec);
            }

            db.tblProgressDatas.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressData ProgressData = db.ProgressDatas.Find(key);
        //    if (ProgressData == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressDatas.Remove(ProgressData);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressDataExists(int key)
        {
            return db.tblProgressDatas.Count(e => e.entryID == key) > 0;
        }
    }
}
