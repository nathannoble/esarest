﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressClientTable>("ProgressClientTable");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressClientTableController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressClientTable
        [Queryable]
        public IQueryable<tblProgressClient> GetProgressClientTable()
        {
            try
            {
                return db.tblProgressClients;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressClientTable
        [Queryable]
        public SingleResult<tblProgressClient> GetProgressClientTable([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblProgressClients.Where(ProgressClientTable => ProgressClientTable.clientID == key));
        }

        // PUT odata/ProgressClientTable(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, tblProgressClient rec)
        //public IHttpActionResult Put([FromODataUri] string key,  tblProgressClient rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, tblProgressClient rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.employeeID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!ProgressClientTableExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<tblProgressClient[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressClientTable
        //public IHttpActionResult Post(ProgressClientTable ProgressClientTable)
        public HttpResponseMessage Post(tblProgressClient rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblProgressClients.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressClientTable);
            return Request.CreateResponse <tblProgressClient[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/tblProgressClient
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblProgressClient> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblProgressClient rec = db.tblProgressClients.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressClientTableExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressClientTable
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblProgressClient ProgressClientTable = db.tblProgressClients.Find(key);
            if (ProgressClientTable == null)
            {
                return NotFound();
            }

            foreach (tblProgressClient rec in db.tblProgressClients.Where(e => e.clientID == key))
            {
                db.tblProgressClients.Remove(rec);
            }

            db.tblProgressClients.Remove(ProgressClientTable);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressClientTable ProgressClientTable = db.ProgressClientTables.Find(key);
        //    if (ProgressClientTable == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressClientTables.Remove(ProgressClientTable);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressClientTableExists(int key)
        {
            return db.tblProgressClients.Count(e => e.clientID == key) > 0;
        }
    }
}
