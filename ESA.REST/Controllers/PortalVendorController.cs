﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalVendor>("PortalVendor");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalVendorController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalVendor
        [Queryable]
        public IQueryable<tblPortalVendor> GetPortalVendor()
        {
            try
            {
                return db.tblPortalVendors;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalVendor
        [Queryable]
        public SingleResult<tblPortalVendor> GetPortalVendor([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblPortalVendors.Where(PortalVendor => PortalVendor.vendorID == key));
        }

        // PUT odata/PortalVendor(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblPortalVendor rec)
        //public IHttpActionResult Put([FromODataUri] string key,  PortalVendor rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, PortalVendor rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.vendorID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalVendorExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<PortalVendor[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalVendor
        //public IHttpActionResult Post(PortalVendor PortalVendor)
        public HttpResponseMessage Post(tblPortalVendor rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalVendors.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalVendor);
            return Request.CreateResponse <tblPortalVendor[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/PortalVendor
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblPortalVendor> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblPortalVendor rec = db.tblPortalVendors.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalVendorExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalVendor
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblPortalVendor rec2 = db.tblPortalVendors.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblPortalVendor rec in db.tblPortalVendors.Where(e => e.vendorID == key))
            {
                db.tblPortalVendors.Remove(rec);
            }

            db.tblPortalVendors.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalVendor PortalVendor = db.PortalVendors.Find(key);
        //    if (PortalVendor == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalVendors.Remove(PortalVendor);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalVendorExists(int key)
        {
            return db.tblPortalVendors.Count(e => e.vendorID == key) > 0;
        }
    }
}
