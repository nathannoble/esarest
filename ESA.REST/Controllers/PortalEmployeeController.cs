﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalEmployee>("PortalEmployee");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalEmployeeController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalEmployee
        [Queryable]
        public IQueryable<vPortalEmployee> GetPortalEmployee()
        {
            try
            {
                return db.vPortalEmployees;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalEmployee
        [Queryable]
        public SingleResult<vPortalEmployee> GetPortalEmployee([FromODataUri] int key)
        {
            return SingleResult.Create(db.vPortalEmployees.Where(PortalEmployee => PortalEmployee.employeeID == key));
        }

        // PUT odata/PortalEmployee(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, vPortalEmployee rec)
        //public IHttpActionResult Put([FromODataUri] string key,  vPortalEmployee rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, vPortalEmployee rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.employeeID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!PortalEmployeeExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<vPortalEmployee[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalEmployee
        //public IHttpActionResult Post(PortalEmployee PortalEmployee)
        public HttpResponseMessage Post(vPortalEmployee rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vPortalEmployees.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalEmployee);
            return Request.CreateResponse <vPortalEmployee[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/vPortalEmployee
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vPortalEmployee> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vPortalEmployee rec = db.vPortalEmployees.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalEmployeeExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalEmployee
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vPortalEmployee PortalEmployee = db.vPortalEmployees.Find(key);
            if (PortalEmployee == null)
            {
                return NotFound();
            }

            foreach (vPortalEmployee rec in db.vPortalEmployees.Where(e => e.employeeID == key))
            {
                db.vPortalEmployees.Remove(rec);
            }

            db.vPortalEmployees.Remove(PortalEmployee);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalEmployee PortalEmployee = db.PortalEmployees.Find(key);
        //    if (PortalEmployee == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalEmployees.Remove(PortalEmployee);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalEmployeeExists(int key)
        {
            return db.vPortalEmployees.Count(e => e.employeeID == key) > 0;
        }
    }
}
