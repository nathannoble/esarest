﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressCompany>("ProgressCompany");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressCompanyController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressCompany
        [Queryable]
        public IQueryable<tblProgressCompany> GetProgressCompany()
        {
            try
            {
                return db.tblProgressCompanies;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressCompany
        [Queryable]
        public SingleResult<tblProgressCompany> GetProgressCompany([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblProgressCompanies.Where(ProgressCompany => ProgressCompany.companyID == key));
        }

        // PUT odata/ProgressCompany(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblProgressCompany rec)
        //public IHttpActionResult Put([FromODataUri] string key,  ProgressCompany rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, ProgressCompany rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.companyID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressCompanyExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<ProgressCompany[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressCompany
        //public IHttpActionResult Post(ProgressCompany ProgressCompany)
        public HttpResponseMessage Post(tblProgressCompany rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblProgressCompanies.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressCompany);
            return Request.CreateResponse <tblProgressCompany[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/ProgressCompany
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblProgressCompany> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblProgressCompany rec = db.tblProgressCompanies.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressCompanyExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressCompany
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblProgressCompany rec2 = db.tblProgressCompanies.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblProgressCompany rec in db.tblProgressCompanies.Where(e => e.companyID == key))
            {
                db.tblProgressCompanies.Remove(rec);
            }

            db.tblProgressCompanies.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressCompany ProgressCompany = db.ProgressCompanies.Find(key);
        //    if (ProgressCompany == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressCompanies.Remove(ProgressCompany);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressCompanyExists(int key)
        {
            return db.tblProgressCompanies.Count(e => e.companyID == key) > 0;
        }
    }
}
