﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressPlan>("ProgressPlan");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressPlanController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressPlan
        [Queryable]
        public IQueryable<tblProgressPlan> GetProgressPlan()
        {
            try
            {
                return db.tblProgressPlans;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressPlan
        [Queryable]
        public SingleResult<tblProgressPlan> GetProgressPlan([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblProgressPlans.Where(ProgressPlan => ProgressPlan.planID == key));
        }

        // PUT odata/ProgressPlan(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblProgressPlan rec)
        //public IHttpActionResult Put([FromODataUri] string key,  ProgressPlan rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, ProgressPlan rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.planID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressPlanExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<ProgressPlan[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressPlan
        //public IHttpActionResult Post(ProgressPlan ProgressPlan)
        public HttpResponseMessage Post(tblProgressPlan rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblProgressPlans.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressPlan);
            return Request.CreateResponse <tblProgressPlan[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/ProgressPlan
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblProgressPlan> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblProgressPlan rec = db.tblProgressPlans.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressPlanExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressPlan
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblProgressPlan rec2 = db.tblProgressPlans.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblProgressPlan rec in db.tblProgressPlans.Where(e => e.planID == key))
            {
                db.tblProgressPlans.Remove(rec);
            }

            db.tblProgressPlans.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressPlan ProgressPlan = db.ProgressPlans.Find(key);
        //    if (ProgressPlan == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressPlans.Remove(ProgressPlan);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressPlanExists(int key)
        {
            return db.tblProgressPlans.Count(e => e.planID == key) > 0;
        }
    }
}
