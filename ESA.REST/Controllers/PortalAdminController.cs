﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalAdmin>("PortalAdmin");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalAdminController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalAdmin
        [Queryable]
        public IQueryable<tblPortalAdmin> GetPortalAdmin()
        {
            try
            {
                return db.tblPortalAdmins;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalAdmin
        [Queryable]
        public SingleResult<tblPortalAdmin> GetPortalAdmin([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblPortalAdmins.Where(PortalAdmin => PortalAdmin.ID == key));
        }

        // PUT odata/PortalAdmin(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, tblPortalAdmin rec)
        //public IHttpActionResult Put([FromODataUri] string key,  PortalAdmin rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, PortalAdmin rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.ID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalAdminExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<PortalAdmin[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalAdmin
        //public IHttpActionResult Post(PortalAdmin PortalAdmin)
        public HttpResponseMessage Post(tblPortalAdmin rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalAdmins.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalAdmin);
            return Request.CreateResponse <tblPortalAdmin[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/PortalAdmin
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblPortalAdmin> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblPortalAdmin rec = db.tblPortalAdmins.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalAdminExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalAdmin
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblPortalAdmin rec2 = db.tblPortalAdmins.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (tblPortalAdmin rec in db.tblPortalAdmins.Where(e => e.ID == key))
            {
                db.tblPortalAdmins.Remove(rec);
            }

            db.tblPortalAdmins.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalAdmin PortalAdmin = db.PortalAdmins.Find(key);
        //    if (PortalAdmin == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalAdmins.Remove(PortalAdmin);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalAdminExists(int key)
        {
            return db.tblPortalAdmins.Count(e => e.ID == key) > 0;
        }
    }
}
