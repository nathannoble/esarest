﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressWorkingDay>("ProgressWorkingDay");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressWorkingDayController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressWorkingDay
        [Queryable]
        public IQueryable<vProgressWorkingDay> GetProgressWorkingDay()
        {
            try
            {
                return db.vProgressWorkingDays;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressWorkingDay
        [Queryable]
        public SingleResult<vProgressWorkingDay> GetProgressWorkingDay([FromODataUri] int key)
        {
            return SingleResult.Create(db.vProgressWorkingDays.Where(ProgressWorkingDay => ProgressWorkingDay.workDayCount == key));
        }

        // PUT odata/ProgressWorkingDay(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, vProgressWorkingDay rec)
        //public IHttpActionResult Put([FromODataUri] string key,  vProgressWorkingDay rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, vProgressWorkingDay rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.workDayCount)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!ProgressWorkingDayExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<vProgressWorkingDay[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressWorkingDay
        //public IHttpActionResult Post(ProgressWorkingDay ProgressWorkingDay)
        public HttpResponseMessage Post(vProgressWorkingDay rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vProgressWorkingDays.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressWorkingDay);
            return Request.CreateResponse <vProgressWorkingDay[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/vProgressWorkingDay
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vProgressWorkingDay> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vProgressWorkingDay rec = db.vProgressWorkingDays.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressWorkingDayExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        //// DELETE odata/ProgressWorkingDay
        //public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        //{
        //    vProgressWorkingDay ProgressWorkingDay = db.vProgressWorkingDays.Find(key);
        //    if (ProgressWorkingDay == null)
        //    {
        //        return NotFound();
        //    }

        //    foreach (vProgressWorkingDay rec in db.vProgressWorkingDays.Where(e => e.workDayCount == key))
        //    {
        //        db.vProgressWorkingDays.Remove(rec);
        //    }

        //    db.vProgressWorkingDays.Remove(ProgressWorkingDay);

        //    // TODO: Execute this in background and keep the UI live
        //    //db.SaveChanges();
        //    await db.SaveChangesAsync();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressWorkingDayExists(int key)
        {
            return db.vProgressWorkingDays.Count(e => e.workDayCount == key) > 0;
        }
    }
}
