﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressClientOrig>("ProgressClientOrig");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressClientOrigController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressClientOrig
        [Queryable]
        public IQueryable<vProgressClientOrig> GetProgressClientOrig()
        {
            try
            {
                return db.vProgressClientOrigs;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressClientOrig
        [Queryable]
        public SingleResult<vProgressClientOrig> GetProgressClientOrig([FromODataUri] int key)
        {
            return SingleResult.Create(db.vProgressClientOrigs.Where(ProgressClientOrig => ProgressClientOrig.clientID == key));
        }

        // PUT odata/ProgressClientOrig(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, vProgressClientOrig rec)
        //public IHttpActionResult Put([FromODataUri] string key,  vProgressClientOrig rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, vProgressClientOrig rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.employeeID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!ProgressClientOrigExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<vProgressClientOrig[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressClientOrig
        //public IHttpActionResult Post(ProgressClientOrig ProgressClientOrig)
        public HttpResponseMessage Post(vProgressClientOrig rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vProgressClientOrigs.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressClientOrig);
            return Request.CreateResponse <vProgressClientOrig[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/vProgressClientOrig
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vProgressClientOrig> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vProgressClientOrig rec = db.vProgressClientOrigs.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressClientOrigExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressClientOrig
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vProgressClientOrig ProgressClientOrig = db.vProgressClientOrigs.Find(key);
            if (ProgressClientOrig == null)
            {
                return NotFound();
            }

            foreach (vProgressClientOrig rec in db.vProgressClientOrigs.Where(e => e.clientID == key))
            {
                db.vProgressClientOrigs.Remove(rec);
            }

            db.vProgressClientOrigs.Remove(ProgressClientOrig);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressClientOrig ProgressClientOrig = db.ProgressClientOrigs.Find(key);
        //    if (ProgressClientOrig == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressClientOrigs.Remove(ProgressClientOrig);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressClientOrigExists(int key)
        {
            return db.vProgressClientOrigs.Count(e => e.clientID == key) > 0;
        }
    }
}
