﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressClientData>("ProgressClientData");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressClientDataController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressClientData
        [Queryable]
        public IQueryable<vProgressClientData_2> GetProgressClientData()
        {
            try
            {
                return db.vProgressClientData_2;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressClientData
        [Queryable]
        public SingleResult<vProgressClientData_2> GetProgressClientData([FromODataUri] int key)
        {
            return SingleResult.Create(db.vProgressClientData_2.Where(ProgressClientData => ProgressClientData.entryID == key));
        }

        // PUT odata/ProgressClientData(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, vProgressClientData_2 rec)
        //public IHttpActionResult Put([FromODataUri] string key,  ProgressClientData rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, ProgressClientData rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.entryID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressClientDataExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<ProgressClientData[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressClientData
        //public IHttpActionResult Post(ProgressClientData ProgressClientData)
        public HttpResponseMessage Post(vProgressClientData_2 rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vProgressClientData_2.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressClientData);
            return Request.CreateResponse <vProgressClientData_2[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/ProgressClientData
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vProgressClientData_2> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vProgressClientData_2 rec = db.vProgressClientData_2.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressClientDataExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressClientData
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vProgressClientData_2 rec2 = db.vProgressClientData_2.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (vProgressClientData_2 rec in db.vProgressClientData_2.Where(e => e.entryID == key))
            {
                db.vProgressClientData_2.Remove(rec);
            }

            db.vProgressClientData_2.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressClientData ProgressClientData = db.ProgressClientDatas.Find(key);
        //    if (ProgressClientData == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressClientDatas.Remove(ProgressClientData);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressClientDataExists(int key)
        {
            return db.vProgressClientData_2.Count(e => e.entryID == key) > 0;
        }
    }
}
