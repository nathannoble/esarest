﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalTimerTotalClientTime>("PortalTimerTotalClientTime");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalTimerTotalClientTimeController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalTimerTotalClientTime
        [Queryable]
        public IQueryable<vPortalTimerTotalClientTime> GetPortalTimerTotalClientTime()
        {
            try
            {
                return db.vPortalTimerTotalClientTimes;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalTimerTotalClientTime
        [Queryable]
        public SingleResult<vPortalTimerTotalClientTime> GetPortalTimerTotalClientTime([FromODataUri] int key)
        {
            return SingleResult.Create(db.vPortalTimerTotalClientTimes.Where(PortalTimerTotalClientTime => PortalTimerTotalClientTime.rowID == key));
        }

        // PUT odata/PortalTimerTotalClientTime(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, vPortalTimerTotalClientTime rec)
        //public IHttpActionResult Put([FromODataUri] string key,  vPortalTimerTotalClientTime rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, vPortalTimerTotalClientTime rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.rowID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!PortalTimerTotalClientTimeExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<vPortalTimerTotalClientTime[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalTimerTotalClientTime
        //public IHttpActionResult Post(PortalTimerTotalClientTime PortalTimerTotalClientTime)
        public HttpResponseMessage Post(vPortalTimerTotalClientTime rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vPortalTimerTotalClientTimes.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalTimerTotalClientTime);
            return Request.CreateResponse <vPortalTimerTotalClientTime[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/vPortalTimerTotalClientTime
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vPortalTimerTotalClientTime> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vPortalTimerTotalClientTime rec = db.vPortalTimerTotalClientTimes.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalTimerTotalClientTimeExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalTimerTotalClientTime
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vPortalTimerTotalClientTime PortalTimerTotalClientTime = db.vPortalTimerTotalClientTimes.Find(key);
            if (PortalTimerTotalClientTime == null)
            {
                return NotFound();
            }

            foreach (vPortalTimerTotalClientTime rec in db.vPortalTimerTotalClientTimes.Where(e => e.rowID == key))
            {
                db.vPortalTimerTotalClientTimes.Remove(rec);
            }

            db.vPortalTimerTotalClientTimes.Remove(PortalTimerTotalClientTime);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalTimerTotalClientTime PortalTimerTotalClientTime = db.PortalTimerTotalClientTimes.Find(key);
        //    if (PortalTimerTotalClientTime == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalTimerTotalClientTimes.Remove(PortalTimerTotalClientTime);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalTimerTotalClientTimeExists(int key)
        {
            return db.vPortalTimerTotalClientTimes.Count(e => e.rowID == key) > 0;
        }
    }
}
