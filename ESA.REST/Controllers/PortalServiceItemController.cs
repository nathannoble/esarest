﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalServiceItem>("PortalServiceItem");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalServiceItemController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalServiceItem
        [Queryable]
        public IQueryable<tblPortalServiceItem> GetPortalServiceItem()
        {
            try
            {
                return db.tblPortalServiceItems;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalServiceItem
        [Queryable]
        public SingleResult<tblPortalServiceItem> GetPortalServiceItem([FromODataUri] int key)
        {
            return SingleResult.Create(db.tblPortalServiceItems.Where(PortalServiceItem => PortalServiceItem.serviceID == key));
        }

        // PUT odata/PortalServiceItem(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, tblPortalServiceItem rec)
        //public IHttpActionResult Put([FromODataUri] string key,  tblPortalServiceItem rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, tblPortalServiceItem rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.employeeID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!PortalServiceItemExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<tblPortalServiceItem[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalServiceItem
        //public IHttpActionResult Post(PortalServiceItem PortalServiceItem)
        public HttpResponseMessage Post(tblPortalServiceItem rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.tblPortalServiceItems.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalServiceItem);
            return Request.CreateResponse <tblPortalServiceItem[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/tblPortalServiceItem
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<tblPortalServiceItem> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tblPortalServiceItem rec = db.tblPortalServiceItems.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalServiceItemExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalServiceItem
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            tblPortalServiceItem PortalServiceItem = db.tblPortalServiceItems.Find(key);
            if (PortalServiceItem == null)
            {
                return NotFound();
            }

            foreach (tblPortalServiceItem rec in db.tblPortalServiceItems.Where(e => e.serviceID == key))
            {
                db.tblPortalServiceItems.Remove(rec);
            }

            db.tblPortalServiceItems.Remove(PortalServiceItem);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalServiceItem PortalServiceItem = db.PortalServiceItems.Find(key);
        //    if (PortalServiceItem == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalServiceItems.Remove(PortalServiceItem);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalServiceItemExists(int key)
        {
            return db.tblPortalServiceItems.Count(e => e.serviceID == key) > 0;
        }
    }
}
