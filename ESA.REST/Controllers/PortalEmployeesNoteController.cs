﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalEmployeesNote>("PortalEmployeesNote");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalEmployeesNoteController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalEmployeesNote
        [Queryable]
        public IQueryable<vPortalEmployeesNote> GetPortalEmployeesNote()
        {
            try
            {
                return db.vPortalEmployeesNotes;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalEmployeesNote
        [Queryable]
        public SingleResult<vPortalEmployeesNote> GetPortalEmployeesNote([FromODataUri] int key)
        {
            return SingleResult.Create(db.vPortalEmployeesNotes.Where(PortalEmployeesNote => PortalEmployeesNote.employeeNoteID == key));
        }

        // PUT odata/PortalEmployeesNote(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, vPortalEmployeesNote rec)
        //public IHttpActionResult Put([FromODataUri] string key,  PortalEmployeesNote rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, PortalEmployeesNote rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.employeeNoteID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalEmployeesNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<PortalEmployeesNote[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalEmployeesNote
        //public IHttpActionResult Post(PortalEmployeesNote PortalEmployeesNote)
        public HttpResponseMessage Post(vPortalEmployeesNote rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vPortalEmployeesNotes.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalEmployeesNote);
            return Request.CreateResponse <vPortalEmployeesNote[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/PortalEmployeesNote
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vPortalEmployeesNote> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vPortalEmployeesNote rec = db.vPortalEmployeesNotes.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalEmployeesNoteExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalEmployeesNote
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vPortalEmployeesNote rec2 = db.vPortalEmployeesNotes.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (vPortalEmployeesNote rec in db.vPortalEmployeesNotes.Where(e => e.employeeNoteID == key))
            {
                db.vPortalEmployeesNotes.Remove(rec);
            }

            db.vPortalEmployeesNotes.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalEmployeesNote PortalEmployeesNote = db.PortalEmployeesNotes.Find(key);
        //    if (PortalEmployeesNote == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalEmployeesNotes.Remove(PortalEmployeesNote);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalEmployeesNoteExists(int key)
        {
            return db.vPortalEmployeesNotes.Count(e => e.employeeNoteID == key) > 0;
        }
    }
}
