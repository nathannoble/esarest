﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<PortalTimerTaskBreakdown>("PortalTimerTaskBreakdown");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PortalTimerTaskBreakdownController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/PortalTimerTaskBreakdown
        [Queryable]
        public IQueryable<vPortalTimerTaskBreakdown> GetPortalTimerTaskBreakdown()
        {
            try
            {
                return db.vPortalTimerTaskBreakdowns;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/PortalTimerTaskBreakdown
        [Queryable]
        public SingleResult<vPortalTimerTaskBreakdown> GetPortalTimerTaskBreakdown([FromODataUri] int key)
        {
            return SingleResult.Create(db.vPortalTimerTaskBreakdowns.Where(PortalTimerTaskBreakdown => PortalTimerTaskBreakdown.timerID == key));
        }

        // PUT odata/PortalTimerTaskBreakdown(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, vPortalTimerTaskBreakdown rec)
        //public IHttpActionResult Put([FromODataUri] string key,  vPortalTimerTaskBreakdown rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, vPortalTimerTaskBreakdown rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.timerID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!PortalTimerTaskBreakdownExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<vPortalTimerTaskBreakdown[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/PortalTimerTaskBreakdown
        //public IHttpActionResult Post(PortalTimerTaskBreakdown PortalTimerTaskBreakdown)
        public HttpResponseMessage Post(vPortalTimerTaskBreakdown rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vPortalTimerTaskBreakdowns.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(PortalTimerTaskBreakdown);
            return Request.CreateResponse <vPortalTimerTaskBreakdown[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/vPortalTimerTaskBreakdown
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vPortalTimerTaskBreakdown> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vPortalTimerTaskBreakdown rec = db.vPortalTimerTaskBreakdowns.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortalTimerTaskBreakdownExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/PortalTimerTaskBreakdown
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vPortalTimerTaskBreakdown PortalTimerTaskBreakdown = db.vPortalTimerTaskBreakdowns.Find(key);
            if (PortalTimerTaskBreakdown == null)
            {
                return NotFound();
            }

            foreach (vPortalTimerTaskBreakdown rec in db.vPortalTimerTaskBreakdowns.Where(e => e.timerID == key))
            {
                db.vPortalTimerTaskBreakdowns.Remove(rec);
            }

            db.vPortalTimerTaskBreakdowns.Remove(PortalTimerTaskBreakdown);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    PortalTimerTaskBreakdown PortalTimerTaskBreakdown = db.PortalTimerTaskBreakdowns.Find(key);
        //    if (PortalTimerTaskBreakdown == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PortalTimerTaskBreakdowns.Remove(PortalTimerTaskBreakdown);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PortalTimerTaskBreakdownExists(int key)
        {
            return db.vPortalTimerTaskBreakdowns.Count(e => e.timerID == key) > 0;
        }
    }
}
