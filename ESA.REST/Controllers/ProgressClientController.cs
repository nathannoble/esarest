﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressClient>("ProgressClient");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressClientController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressClient
        [Queryable]
        public IQueryable<vProgressClient> GetProgressClient()
        {
            try
            {
                return db.vProgressClients;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressClient
        [Queryable]
        public SingleResult<vProgressClient> GetProgressClient([FromODataUri] int key)
        {
            return SingleResult.Create(db.vProgressClients.Where(ProgressClient => ProgressClient.clientPlanID == key));
        }

        // PUT odata/ProgressClient(5)
        // View edited (like casscading risk view)
        //public IHttpActionResult Put([FromODataUri] int key, vProgressClient rec)
        //public IHttpActionResult Put([FromODataUri] string key,  vProgressClient rec)
        
        // Table edited (like wit) using kendo ui grid
        public HttpResponseMessage Put([FromODataUri] int key, vProgressClient rec)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            //if (key != rec.employeeID)
            //{
            //    return BadRequest();
            //}

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                //if (!ProgressClientExists(key))
                //{
                //    return NotFound();
                //}
                //else
                //{
                    throw;
                //}
            }

            //return Updated(rec);
            return Request.CreateResponse<vProgressClient[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressClient
        //public IHttpActionResult Post(ProgressClient ProgressClient)
        public HttpResponseMessage Post(vProgressClient rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vProgressClients.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressClient);
            return Request.CreateResponse <vProgressClient[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/vProgressClient
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vProgressClient> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vProgressClient rec = db.vProgressClients.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressClientExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressClient
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vProgressClient ProgressClient = db.vProgressClients.Find(key);
            if (ProgressClient == null)
            {
                return NotFound();
            }

            foreach (vProgressClient rec in db.vProgressClients.Where(e => e.clientPlanID == key))
            {
                db.vProgressClients.Remove(rec);
            }

            db.vProgressClients.Remove(ProgressClient);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressClient ProgressClient = db.ProgressClients.Find(key);
        //    if (ProgressClient == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressClients.Remove(ProgressClient);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressClientExists(int key)
        {
            return db.vProgressClients.Count(e => e.clientPlanID == key) > 0;
        }
    }
}
