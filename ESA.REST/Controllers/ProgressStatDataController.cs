﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ESA.REST.Models;
using System.Threading.Tasks;

namespace ESA.REST.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using <Namespace>.REST.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ProgressStatData>("ProgressStatData");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProgressStatDataController : ODataController
    {
        private Entities db = new Entities();

        // GET odata/ProgressStatData
        [Queryable]
        public IQueryable<vProgressStatData> GetProgressStatData()
        {
            try
            {
                return db.vProgressStatDatas;
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }
        }

        // GET odata/ProgressStatData
        [Queryable]
        public SingleResult<vProgressStatData> GetProgressStatData([FromODataUri] int key)
        {
            return SingleResult.Create(db.vProgressStatDatas.Where(ProgressStatData => ProgressStatData.entryID == key));
        }

        // PUT odata/ProgressStatData(5)
        // View edited (like casscading risk view)
        public IHttpActionResult Put([FromODataUri] int key, vProgressStatData rec)
        //public IHttpActionResult Put([FromODataUri] string key,  ProgressStatData rec)
        
        // Table edited (like wit) using kendo ui grid
        //public HttpResponseMessage Put([FromODataUri] int key, ProgressStatData rec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
                //return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            if (key != rec.entryID)
            {
                return BadRequest();
            }

            db.Entry(rec).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressStatDataExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
            //return Request.CreateResponse<ProgressStatData[]>(HttpStatusCode.OK, new[] { rec });
        }

        // POST odata/ProgressStatData
        //public IHttpActionResult Post(ProgressStatData ProgressStatData)
        public HttpResponseMessage Post(vProgressStatData rec)    
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
                return Request.CreateResponse<ModelStateDictionary>(HttpStatusCode.BadRequest, ModelState);
            }

            db.vProgressStatDatas.Add(rec);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                string m = e.Message;
            }

            //return Created(ProgressStatData);
            return Request.CreateResponse <vProgressStatData[]>(HttpStatusCode.Created, new[] { rec });
        }

        // PATCH odata/ProgressStatData
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<vProgressStatData> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            vProgressStatData rec = db.vProgressStatDatas.Find(key);
            if (rec == null)
            {
                return NotFound();
            }

            patch.Patch(rec);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressStatDataExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rec);
        }


        // DELETE odata/ProgressStatData
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            vProgressStatData rec2 = db.vProgressStatDatas.Find(key);
            if (rec2 == null)
            {
                return NotFound();
            }

            foreach (vProgressStatData rec in db.vProgressStatDatas.Where(e => e.entryID == key))
            {
                db.vProgressStatDatas.Remove(rec);
            }

            db.vProgressStatDatas.Remove(rec2);

            // TODO: Execute this in background and keep the UI live
            //db.SaveChanges();
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    ProgressStatData ProgressStatData = db.ProgressStatDatas.Find(key);
        //    if (ProgressStatData == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ProgressStatDatas.Remove(ProgressStatData);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressStatDataExists(int key)
        {
            return db.vProgressStatDatas.Count(e => e.entryID == key) > 0;
        }
    }
}
